@extends('errors::itserror')

@section('title', __('Too Many Requests'))
@section('code', '429')
@section('image', url('assets/img/error-429.svg'))
@section('message', __($exception->getMessage() ?: 'Anda terlalu banyak mengirim request, santai saja'))
