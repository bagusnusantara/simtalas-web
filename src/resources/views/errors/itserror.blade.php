<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Other meta tags -->
        <meta name="description" content="Template yang digunakan untuk Sistem Informasi di ITS">
        <meta name="author" content="DPTSI ITS">

        <title>@yield('title') &bullet; myITS Template</title>

        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/img/favicon.png') }}">

        <!-- DashForge CSS -->
        <link rel="stylesheet" href="{{ asset('assets/css/dashforge.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/dashforge.customs.css') }}">

        @if (session('skin') == 'dark')
            <link rel="stylesheet" href="{{ asset('assets/css/skin.dark.css') }}">
        @else
            <link rel="stylesheet" href="{{ asset('assets/css/skin.light.css') }}">
        @endif
    </head>

    <body>
        <header class="navbar navbar-header navbar-header-fixed">
            <a href="{{ url('') }}">
                @if (session('skin') == 'dark')
                    <a class="navbar-brand" href="{{ url('') }}">
                        <img src="{{ asset('assets/img/myits-template-white.png') }}" height="30" alt="">
                    </a>
                @else
                    <a class="navbar-brand" href="{{ url('') }}">
                        <img src="{{ asset('assets/img/myits-template-blue.png') }}" height="30" alt="">
                    </a>
                @endif
            </a>
        </header><!-- navbar -->

        <div class="content content-fixed content-auth-alt">
            <div class="container ht-100p tx-center">
                <div class="row row-xs">

                    <div class="col-sm-12 col-lg-12 mg-b-10 mg-t-20">
                        <div class="ht-500 d-flex flex-column align-items-center justify-content-center">
                            <img src="@yield('image')" class="wd-90p wd-md-40p wd-lg-50p" alt="Error image">
                            <h1 class="tx-color-01 mg-b-0 mg-t-20 mg-b-20 tx-montserrat tx-bold tx-lg-54 tx-md-48">@yield('code')</h1>
                            <h5 class="tx-normal mg-b-20">@yield('message')</h5>
                            <p class="tx-color-03 mg-b-30">@yield('description')</p>
                        </div>
                    </div><!-- col -->

                </div><!-- row -->
            </div>
        </div>


        <script src="{{ asset('lib/jquery/jquery.min.js') }}"></script>
        <script src="{{ asset('lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

        <script src="{{ asset('assets/js/dashforge.js') }}"></script>
        <script src="{{ asset('assets/js/dashforge.aside.js') }}"></script>

    </body>

</html>
