@extends('errors::itserror')

@section('title', __('Unauthorized'))
@section('code', '401')
@section('image', url('assets/img/error-401.svg'))
@section('message', __($exception->getMessage() ?: 'Kredensial yang digunakan tidak bisa mengakses halaman ini.'))
