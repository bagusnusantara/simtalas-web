@extends('errors::itserror')

@section('title', 'Maintenance')
@section('code', '503')
@section('image', url('assets/img/error-503.svg'))
@section('message', 'Sistem sedang dalam perawatan, harap bersabar.')
