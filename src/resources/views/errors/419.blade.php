@extends('errors::itserror')

@section('title', __('Page Expired'))
@section('code', '419')
@section('image', url('assets/img/error-419.svg'))
@section('message', __($exception->getMessage() ?: 'Sesi yang anda gunakan telah berakhir, refresh halaman.'))
