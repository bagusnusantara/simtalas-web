<?php

namespace App\Modules\Setting\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    protected string $module_name = 'Setting';
    protected string $view_path = '../resources/views';

    public function boot()
    {
        View::addNamespace($this->module_name, __DIR__ . '/' . $this->view_path);
    }
}
