<?php

namespace App\Modules\Setting\Providers;

use App\Modules\Setting\Middleware\Language;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\ServiceProvider;

class LangServiceProvider extends ServiceProvider
{
    protected string $module_name = 'Setting';
    protected string $lang_path = '../resources/lang';

    public function boot()
    {
        $router = $this->app->make(Router::class);
        $router->pushMiddlewareToGroup('web', Language::class);
        Lang::addNamespace($this->module_name, __DIR__ . '/' . $this->lang_path);
    }
}
