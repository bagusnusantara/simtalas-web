<?php

namespace App\Modules\Setting;

use Dptsi\Modular\Base\BaseModule;

class Module extends BaseModule
{
    public function getProviders(): array
    {
        return [
            \App\Modules\Setting\Providers\RouteServiceProvider::class,
            \App\Modules\Setting\Providers\DatabaseServiceProvider::class,
            \App\Modules\Setting\Providers\ViewServiceProvider::class,
            \App\Modules\Setting\Providers\LangServiceProvider::class,
            \App\Modules\Setting\Providers\BladeComponentServiceProvider::class,
        ];
    }
}
