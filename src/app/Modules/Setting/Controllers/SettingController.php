<?php

namespace App\Modules\Setting\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Session;

class SettingController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function changeLocale(Request $request)
    {
        if (in_array($request->locale, ['id', 'en'])) {
            Session::put('locale', $request->locale);
        }

        return redirect()->back();
    }

    public function changeSkin(Request $request)
    {
        if (in_array($request->skin, ['light', 'dark'])) {
            Session::put('skin', $request->skin);
        }

        return redirect()->back();
    }
}
