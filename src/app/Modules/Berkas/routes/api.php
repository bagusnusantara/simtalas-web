<?php

use Illuminate\Support\Facades\Route;
use App\Modules\Berkas\Controllers\BerkasController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/jenis-dokumen', [BerkasController::class, 'jenisDokumen'] );
Route::get('/', [BerkasController::class, 'berkas'] );
Route::get('/stream/{aksi}/{id_dokumen}/{nama_file}', [BerkasController::class, 'stream']);
