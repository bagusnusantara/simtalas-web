<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJenisDokumenTable extends Migration
{
    protected $connection = 'berkas';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ref.jenis_dokumen', function (Blueprint $table) {
            $table->id('id_jenis_dok');
            $table->string('nama', 80);
            $table->string('nama_en', 80)->nullable();
            $table->string('tipe_file', 200);
            $table->integer('ukuran_file_min')->nullable();
            $table->integer('ukuran_file_maks');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ref.jenis_dok');
    }
}
