<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDokumenTable extends Migration
{
    protected $connection = 'berkas';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dokumen', function (Blueprint $table) {
            $table->uuid('id_dokumen')->primary();
            $table->foreignId('id_jenis_dok')->references('id_jenis_dok')->on('ref.jenis_dokumen');
            $table->foreignUuid('id_user')->references('id_user')->on('user');
            $table->string('nama_file');
            $table->string('mime', 127);
            $table->string('ekstensi', 10);
            $table->string('keterangan', 100)->nullable();
            $table->bigInteger('ukuran');
            $table->string('file_id')->nullable();
            $table->string('public_link')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->uuid('updater');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dokumen');
    }
}
