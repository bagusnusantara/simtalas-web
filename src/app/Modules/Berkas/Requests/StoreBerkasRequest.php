<?php

namespace App\Modules\Berkas\Requests;

use Illuminate\Validation\Rule;

class StoreBerkasRequest extends JsonResponseRequest
{
    public function authorize()
    {
        return sso()->check();
    }

    public function attributes()
    {
        return [
            'id_jenis_dok' => 'jenis dokumen',
        ];
    }

    public function messages()
    {
        return [
            'nama_file.regex' => 'Isian nama berkas hanya boleh berisi huruf, angka, spasi dan strip.',
        ];
    }

    public function rules()
    {
        $rules = [
            'id_jenis_dok'  => [
                'required',
                Rule::exists('berkas.ref.jenis_dokumen', 'id_jenis_dok'),
            ],
            'nama_file'     => 'required|string|regex:/^[\w\-\s]+$/|max:255',
            'keterangan'    => 'nullable|string|max:100',
            'berkas'        => 'required|mimes:'.request()->tipe_file.'|min:'.request()->ukuran_min.'|max:'.request()->ukuran_maks,
        ];

        return $rules;
    }
}