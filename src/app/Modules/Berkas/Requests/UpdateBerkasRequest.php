<?php

namespace App\Modules\Berkas\Requests;

use Illuminate\Validation\Rule;

class UpdateBerkasRequest extends JsonResponseRequest
{
    public function authorize()
    {
        return sso()->check();
    }

    public function messages()
    {
        return [
            'nama_file.regex' => 'Isian nama berkas hanya boleh berisi huruf, angka, spasi dan strip.',
        ];
    }

    public function rules()
    {
        $rules = [
            'nama_file'     => 'required|string|regex:/^[\w\-\s]+$/|max:255',
            'keterangan'    => 'nullable|string|max:100',
        ];

        return $rules;
    }
}