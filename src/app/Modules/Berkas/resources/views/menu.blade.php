<li class="nav-label mg-t-15">Berkas</li>
<li class="nav-item {{ (Request::is('berkas')) ? 'active' : '' }}">
    <a href="{{ url('berkas') }}" class="nav-link">
        <i data-feather="folder"></i>
        <span>Berkas</span>
    </a>
</li>
