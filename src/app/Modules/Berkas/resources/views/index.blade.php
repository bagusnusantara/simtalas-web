@extends('Ui::base')

@section('title')
    Berkas
@endsection

@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/css/dashforge.filemgr.css') }}">
    <style>
        .modal-spinner {
            position: fixed;
            z-index: 100;
            width: 100%;
            height: 100%;
            background: rgba(0,0,0,0.6);
            border-radius: 0.5rem !important;
        }

        .spinner-alignment {
            height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
        }
        .page-spinner {
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            position: fixed;
            display: block;
            background: rgba(0,0,0,0.6);
            z-index: 9999;
        }

        .page-spinner-alignment {
            height: 100%;
            display: flex;
            justify-content: center;
            align-items: center;
            text-align: center;
        }
    </style>
@endsection

@section('header_title')
    Berkas
@endsection

@section('header_right')
    <a class="btn btn-white tx-montserrat tx-semibold mg-r-10" data-toggle="collapse" href="#aturberkas" role="button" aria-expanded="false" aria-controls="aturberkas"><i data-feather="sliders" class="wd-10 mg-r-5"></i>Atur</a>
    <a href="#" onclick="unggahBerkas()" data-toggle="modal" data-animation="effect-scale" class="btn btn-its tx-montserrat tx-semibold float-right"><i data-feather="upload" class="wd-10 mg-r-5 tx-color-its2"></i> Unggah Berkas</a>
@endsection

@section('content')
    <div class="page-spinner">
        <div class="page-spinner-alignment">
            <span class="spinner-border tx-white"></span>
        </div>
    </div>

    <div class="col-sm-12 col-lg-12 berkas-messages">
        @include('Ui::partials.messages')
        <div class="alert alert-success alert-dismissible fade show" role="alert" id="alert-success" style="display: none">
        </div>
        <div class="alert alert-danger alert-dismissible fade show" role="alert" id="alert-danger" style="display: none">
        </div>
    </div>

    <div class="col-sm-12 col-lg-12">
        <div class="collapse mg-t-5" id="aturberkas">
            <div class="form-row">
                <div class="form-group col-md-3">
                    <select class="custom-select" id="id_jenis_dok_filter" name="id_jenis_dok">
                        <option value="" selected>Semua Jenis Berkas</option>
                        @forelse ($jenisDokumen as $row)
                            <option value="{{$row->id_jenis_dok}}">{{$row->nama}}</option>
                        @empty
                            <option value="">Tidak ada data</option>
                        @endforelse
                    </select>
                </div>
                <div class="form-group col-md-8">
                    <input type="text" class="form-control" id="nama_file_filter" name="nama_file" placeholder="Cari berkas...">
                </div>
                <div class="form-group col-md-1">
                    <button type="button" onclick="generateBerkasIndex()" class="btn btn-white btn-block">
                        <i data-feather="search"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12 col-lg-12" id="berkas-baru">

    </div>

    {{-- Begin modal unggah berkas --}}
        <div class="modal fade effect-scale" id="unggah-berkas" tabindex="-1" role="dialog" aria-labelledby="unggah-berkas" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content bg-white">
                    <div class="modal-spinner">
                        <div class="spinner-alignment">
                            <span class="spinner-border tx-white"></span>
                        </div>
                    </div>
                    <div class="modal-body">
                        <a href="" role="button" class="close pos-absolute t-15 r-15" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </a>
                        <div>
                            <form id="form-unggah-berkas" enctype="multipart/form-data" method="POST" action="{{ route('Berkas::store') }}">
                                @csrf
                                <h5 class="tx-montserrat tx-medium">Unggah Berkas</h5>
                                <div class="form-group">
                                    <label>Jenis Berkas</label>
                                    <select class="form-control" id="id_jenis_dok" name="id_jenis_dok" required>
                                        <option value="" selected>Pilih salah satu</option>
                                        @forelse ($jenisDokumen as $row)
                                            <option value="{{$row->id_jenis_dok}}">{{$row->nama}}</option>
                                        @empty
                                            <option value="">Tidak ada data</option>
                                        @endforelse
                                    </select>
                                    <small class="text-danger id_jenis_dok-error"></small>
                                </div>
                                <div class="form-group">
                                    <label>Nama Berkas</label>
                                    <input type="text" id="nama_file" name="nama_file" class="form-control" placeholder="" required>
                                    <small class="text-danger nama_file-error"></small>
                                </div>
                                <div class="form-group">
                                    <label>Keterangan</label>
                                    <textarea name="keterangan" id="keterangan" rows="2" class="form-control" placeholder=""></textarea>
                                    <small class="text-danger keterangan-error"></small>
                                </div>
                                <div class="form-group">
                                    <label for="berkas" class="d-block">Unggah Berkas</label>
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="berkas" name="berkas" accept="" required>
                                        <label class="custom-file-label" for="berkas">Pilih Berkas</label>
                                    </div>
                                    <div class="progress mg-t-10">
                                    <div class="progress-bar bg-its progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style=""></div>
                                    </div>
                                    <small class="form-text text-danger berkas-error"></small>
                                    <small class="form-text text-muted" id="format"></small>
                                    <small class="form-text text-muted" id="ukuran"></small>
                                </div>
                                <input type="hidden" name="tipe_file" id="tipe_file">
                                <input type="hidden" name="ukuran_maks" id="ukuran_maks">
                                <input type="hidden" name="ukuran_min" id="ukuran_min">
                                <button class="btn btn-its btn-block tx-montserrat tx-semibold form-submit-button" type="submit">Submit</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    {{-- End modal unggah berkas --}}

    {{-- Begin modal sunting berkas --}}
        <div class="modal fade effect-scale" id="sunting-berkas" tabindex="-1" role="dialog" aria-labelledby="sunting-berkas" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <form id="form-sunting-berkas" method="POST" action="{{ route('Berkas::update') }}">
                    @csrf
                    <div class="modal-content bg-white">
                        <div class="modal-spinner">
                            <div class="spinner-alignment">
                                <span class="spinner-border tx-white"></span>
                            </div>
                        </div>
                        <div class="modal-body">
                            <button type="button" class="close pos-absolute t-15 r-20" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                            <input type="hidden" name="id_dokumen" id="id_dokumen_sunting">
                            <h5 class="tx-montserrat tx-medium">Edit Berkas</h5>
                            <div class="form-group">
                                <label>Nama Berkas</label>
                                <input type="text" id="nama_file_sunting" name="nama_file" class="form-control" placeholder="" required>
                                <small class="text-danger nama_file-error"></small>
                            </div>
                            <div class="form-group">
                                <label>Keterangan</label>
                                <textarea name="keterangan" id="keterangan_sunting" rows="2" class="form-control" placeholder=""></textarea>
                                <small class="text-danger keterangan-error"></small>
                            </div>
                        </div>
                        <div class="modal-footer bd-t-0">
                            <a href="#" data-toggle="modal" data-animation="effect-scale" class="btn btn-white tx-montserrat tx-semibold" data-dismiss="modal">Batal</a>
                            <button type="submit" class="btn btn-its tx-montserrat tx-semibold form-submit-button">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    {{-- End modal sunting berkas --}}

    {{-- Begin modal detail berkas --}}
        <div class="modal fade effect-scale" id="detail-berkas" tabindex="-1" role="dialog" aria-labelledby="detail-berkas" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content bg-white">
                    <div class="modal-spinner">
                        <div class="spinner-alignment">
                            <span class="spinner-border tx-white"></span>
                        </div>
                    </div>
                    <div class="modal-body">
                        <button type="button" class="close pos-absolute t-15 r-20" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>

                        <h5 class="tx-montserrat tx-medium">Detail Berkas</h5>
                        <div class="row mg-b-10">
                            <div class="col-4">Jenis:</div>
                            <div class="col-8" id="jenis_dokumen"></div>
                        </div>
                        <div class="row mg-b-10">
                            <div class="col-4">Nama:</div>
                            <div class="col-8" id="nama_berkas"></div>
                        </div>
                        <div class="row mg-b-10">
                            <div class="col-4">Ekstensi:</div>
                            <div class="col-8" id="ekstensi_berkas"></div>
                        </div>
                        <div class="row mg-b-10">
                            <div class="col-4">Ukuran:</div>
                            <div class="col-8" id="ukuran_berkas"></div>
                        </div>
                        <div class="row mg-b-10">
                            <div class="col-4">Diunggah pada:</div>
                            <div class="col-8" id="created_at_berkas"></div>
                        </div>
                        <div class="row mg-b-10">
                            <div class="col-4">Terakhir diubah:</div>
                            <div class="col-8" id="updated_at_berkas"></div>
                        </div>
                        <div class="row mg-b-10">
                            <div class="col-4">Keterangan:</div>
                            <div class="col-8" id="keterangan_berkas"></div>
                        </div>
                    </div>
                    <div class="modal-footer bd-t-0">
                        <button type="button" class="btn btn-white tx-montserrat tx-semibold float-right" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
    {{-- End modal detail berkas --}}

    {{-- Begin modal hapus berkas --}}
        <div class="modal fade effect-scale" id="hapus-berkas" tabindex="-1" role="dialog" aria-labelledby="hapus-berkas" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content tx-14 bg-white">
                    <div class="modal-spinner">
                        <div class="spinner-alignment">
                            <span class="spinner-border tx-white"></span>
                        </div>
                    </div>
                    <div class="modal-body">
                        <h5 class="tx-montserrat tx-medium">Apakah anda yakin ingin menghapus berkas ini?</h5>
                        <span>Tindakan ini tidak bisa dibatalkan</span>
                    </div>
                    <div class="modal-footer bd-t-0">
                        <a href="#" data-toggle="modal" data-animation="effect-scale" class="btn btn-white tx-montserrat tx-semibold" data-dismiss="modal">Batal</a>
                        <form id="form-hapus-berkas" method="POST" action="{{ route('Berkas::delete') }}">
                            @csrf
                            <input type="hidden" name="id_dokumen" id="id_dokumen_hapus">
                            <button type="submit" class="btn btn-its tx-montserrat tx-semibold form-submit-button">Hapus</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    {{-- End modal sunting berkas --}}
@endsection

@section('scripts')
    <script src="{{ asset('lib/jquery/jquery.form.min.js') }}"></script>
    <script src="{{ asset('assets/js/dashforge.filemgr.js') }}"></script>
    <script>
        function enableLoader() {
            $(".modal-spinner").fadeIn();
            let loader = `<div class="spinner-border spinner-border-sm tx-white" role="status"><span class="sr-only">Loading...</span></div>`;
            $('.form-submit-button').attr('disabled', true);
            $('.form-submit-button').empty().append(loader);
        }

        function disableLoader() {
            $(".modal-spinner").fadeOut();
            $('.progress-bar').css('width', '0%');
            $('.form-submit-button').attr('disabled', false);
            $('.form-submit-button').empty().append("Submit");
        }

        var attribute = [
            'nama_file',
            'id_jenis_dok',
            'keterangan',
            'berkas',
        ];

        function clearError(attribute) {
            $('.' + attribute + '-error').html('');
        }

        function showError(attribute, errors) {
            $('.' + attribute + '-error').html( errors[attribute][0] );
        }

        function displayAjaxMessages(data) {
            if(data.success) {
                $('#alert-success').show();
                $('#alert-success').text(data.success);
                let html = '';
                html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
                html += '<span aria-hidden="true">×</span>';
                html += '</button>';
                $('#alert-success').append(html);
            }
            if(data.error) {
                $('#alert-danger').show();
                $('#alert-danger').text(data.error);
                let html = '';
                html += '<button type="button" class="close" data-dismiss="alert" aria-label="Close">';
                html += '<span aria-hidden="true">×</span>';
                html += '</button>';
                $('#alert-danger').append(html);
            }
            generateBerkasIndex();
            setTimeout(function(){
                $('#alert-success').hide();
            }, 5000 );
            setTimeout(function(){
                $('#alert-danger').hide();
            }, 5000 );
        }
    </script>
    <script>
        function generateBerkasIndex() {
            let id_jenis_dok = $('#id_jenis_dok_filter').val();
            let nama_file = $('#nama_file_filter').val();
            let url = `{{ url('api/berkas') }}`;
            $('#berkas-baru').empty();
            $(".page-spinner").fadeIn();
            $.ajax({
                url: url,
                type: 'GET',
                data: {
                    'id_jenis_dok': id_jenis_dok,
                    'nama_file': nama_file,
                },
                success: function (data) {
                    $('#berkas-baru').empty();
                    let jenis_dokumen = new Array();
                    let html = '';
                    let url_stream = `{{ url('api/berkas/stream') }}`;
                    html += `<div class="row row-xs mg-b-10">`;
                    $.each(data, function (index) {
                        if(typeof jenis_dokumen[data[index]['id_jenis_dok']] === 'undefined') {
                            jenis_dokumen[data[index]['id_jenis_dok']] = data[index]['jenis_dokumen'];
                            html += `<div class="col-12 mg-t-10">`;
                            html += `<label class="d-block tx-semibold tx-12 tx-uppercase tx-sans tx-spacing-1 tx-color-03 mg-b-10">${data[index]['jenis_dokumen']}</label>`;
                            html += `</div>`;
                        }
                        let url_unduh = `${url_stream}/unduh/${data[index]['id_dokumen']}/${data[index]['nama_file']}.${data[index]['ekstensi']}`;
                        let url_detail = `${url_stream}/detail/${data[index]['id_dokumen']}/${data[index]['nama_file']}.${data[index]['ekstensi']}`;

                        html += `<div class="col-6 col-sm-6 col-md-4 col-lg-3 mg-b-10">`;
                        html += `<div class="card card-file bg-white">`;
                        html += `<div class="dropdown-file">`;
                        html += `<a href="" class="dropdown-link btn btn-icon btn-its-icon" data-toggle="dropdown"><i data-feather="more-vertical"></i></a>`;
                        html += `<div class="dropdown-menu dropdown-menu-right">`;
                        html += `<a href="${url_unduh}" class="dropdown-item download"><i data-feather="download"></i>Unduh</a>`;
                        html += `<a href="#" onclick="detailBerkas('${data[index]['id_dokumen']}')" data-toggle="modal" class="dropdown-item details"><i data-feather="info"></i>Detail</a>`;
                        html += `<a href="#" onclick="suntingBerkas('${data[index]['id_dokumen']}')" data-toggle="modal" class="dropdown-item details"><i data-feather="edit"></i>Edit</a>`;
                        html += `<a href="#" onclick="hapusBerkas('${data[index]['id_dokumen']}')" class="dropdown-item delete"><i data-feather="trash"></i>Hapus</a>`;
                        html += `</div>`;
                        html += `</div>`;
                        html += `<a href="${url_detail}" target="_blank">`;
                        if(data[index]['ekstensi'].trim() == 'pdf') {
                            html += `<div class="card-file-thumb tx-danger bg-white">`;
                            html += `<i class="fas fa-file-pdf"></i>`;
                            html += `</div>`;
                        } else {
                            html += `<div class="card-file-thumb tx-teal bg-white">`;
                            html += `<i class="fas fa-file-image"></i>`;
                            html += `</div>`;
                        }
                        html += `</a>`;
                        html += `<div class="card-body d-flex align-items-center">`;
                        html += `<div class="text-truncate">`;
                        html += `<p class="mg-b-0 tx-medium wd-100p text-truncate">`;
                        html += `<a href="${url_detail}" target="_blank" class="link-02">${data[index]['nama_file']}.${data[index]['ekstensi']}</a>`;
                        html += `</p>`;
                        html += `<span class="mg-b-0 tx-color-03 tx-13 wd-100p text-truncate">${(data[index]['keterangan'] != null ? data[index]['keterangan'] : '-')}</span>`;
                        html += `</div>`;
                        html += `</div>`;
                        html += `</div>`;
                        html += `</div>`;
                    });
                    html += `</div>`;
                    $('#berkas-baru').append(html);
                    $(".page-spinner").fadeOut();
                    feather.replace();
                },
            });
        }

        generateBerkasIndex();
    </script>
    <script>
        $('#id_jenis_dok').on('change', function(){
            enableLoader();
            let id_jenis_dok = $('#id_jenis_dok').val();
            let url = `{{ url('api/berkas/jenis-dokumen') }}`;
            $.ajax({
                url: url,
                type: 'GET',
                data: {
                    'id_jenis_dok': id_jenis_dok
                },
                success: function (data) {
                    $('#format').html('Hanya dapat mengunggah berkas dengan format ' + data.tipe_file);
                    $('#ukuran').html('Ukuran maksimum berkas ' + data.ukuran_file_maks + ' kb');
                    let acc = data.tipe_file.split(',');
                    acc = $.map(acc, function(tipe){
                        return '.' + tipe;
                    })
                    acc = acc.join(',');
                    $('#berkas').attr('accept', acc);
                    $('#tipe_file').val(data.tipe_file);
                    $('#ukuran_maks').val(data.ukuran_file_maks);
                    $('#ukuran_min').val(data.ukuran_file_min);
                    disableLoader();
                },
                error: function(data){
                    $('#format').html('');
                    $('#ukuran').html('');
                    $('#berkas').attr('accept', '');
                    $('#tipe_file').val('');
                    $('#ukuran_maks').val('');
                    $('#ukuran_min').val('');
                    disableLoader();
                }
            });
        });
    </script>
    <script>
        function unggahBerkas() {
            $.each(attribute, function(index, value){
                clearError(value);
            });
            disableLoader();
            $('#format').html('');
            $('#ukuran').html('');
            $('#berkas').attr('accept', '');
            $('#tipe_file').val('');
            $('#ukuran_maks').val('');
            $('#ukuran_min').val('');
            $('#unggah-berkas').modal('show');
            $('#unggah-berkas form')[0].reset();
        }

        $('#form-unggah-berkas').ajaxForm({
            beforeSend: function(){
                enableLoader();
            },
            uploadProgress: function(event, position, total, percentComplete)
            {
                $('.progress-bar').css('width', percentComplete + '%');
            },
            success: function(data)
            {
                disableLoader();
                $('#unggah-berkas').modal('hide');
                displayAjaxMessages(data);
            },
            error: function(data){
                disableLoader();
                let errors = $.parseJSON(data.responseText).errors;
                $.each(attribute, function(index, value) {
                    if (errors[value]) {
                        showError(value, errors);
                    }
                });
            }
        });
    </script>
    <script>
        function suntingBerkas(id_dokumen) {
            $.each(attribute, function(index, value){
                clearError(value);
            });
            disableLoader();
            let url = `{{ url('api/berkas') }}`;
            $('#id_dokumen_sunting').val(id_dokumen);
            $('#nama_file_sunting').val('');
            $('#keterangan_sunting').val('');
            enableLoader();
            $.ajax({
                url: url,
                type: 'GET',
                data: {
                    'id_dokumen': id_dokumen
                },
                success: function (data) {
                    $('#nama_file_sunting').val(data.nama_file);
                    $('#keterangan_sunting').val(data.keterangan);
                    disableLoader();
                },
            });
            $('#sunting-berkas').modal('show');
        }

        $('#form-sunting-berkas').ajaxForm({
            beforeSend: function(){
                enableLoader();
            },
            success: function(data)
            {
                disableLoader();
                $('#sunting-berkas').modal('hide');
                displayAjaxMessages(data);
            },
            error: function(data){
                disableLoader();
                let errors = $.parseJSON(data.responseText).errors;
                console.log(errors);
                $.each(attribute, function(index, value) {
                    if (errors[value]) {
                        console.log('error occur');
                        showError(value, errors);
                    }
                });
            }
        });
    </script>
    <script>
        function detailBerkas(id_dokumen) {
            disableLoader();
            let url = `{{ url('api/berkas') }}`;
            $('#jenis_dokumen').html('');
            $('#nama_berkas').html('');
            $('#ekstensi_berkas').html('');
            $('#ukuran_berkas').html('');
            $('#created_at_berkas').html('');
            $('#updated_at_berkas').html('');
            $('#keterangan_berkas').html('');
            enableLoader();
            $.ajax({
                url: url,
                type: 'GET',
                data: {
                    'id_dokumen': id_dokumen
                },
                success: function (data) {
                    $('#jenis_dokumen').html(data.jenis_dokumen);
                    $('#nama_berkas').html(data.nama_file + '.' + data.ekstensi);
                    $('#ekstensi_berkas').html(data.ekstensi);
                    $('#ukuran_berkas').html(data.ukuran + ' KB');
                    $('#created_at_berkas').html(data.created_at);
                    $('#updated_at_berkas').html(data.updated_at);
                    $('#keterangan_berkas').html(data.keterangan);
                    disableLoader();
                },
            });
            $('#detail-berkas').modal('show');
        }
    </script>
    <script>
        function hapusBerkas(id_dokumen) {
            $('#id_dokumen_hapus').val(id_dokumen);
            disableLoader();
            $('#hapus-berkas').modal('show');
            $('#hapus-berkas form')[0].reset();
        }

        $('#form-hapus-berkas').ajaxForm({
            beforeSend: function(){
                enableLoader();
            },
            success: function(data)
            {
                disableLoader();
                $('#hapus-berkas').modal('hide');
                displayAjaxMessages(data);
            },
            error: function(data){
                disableLoader();
            }
        });
    </script>
@endsection
