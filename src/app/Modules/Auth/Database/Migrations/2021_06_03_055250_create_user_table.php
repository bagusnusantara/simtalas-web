<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTable extends Migration
{
    protected $connection = 'auth';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->uuid('id_user')->primary();

            $table->string('name', 200)->nullable();
            $table->string('username')->nullable();
            $table->string('gender', 10)->nullable();
            $table->dateTime('birthdate')->nullable();

            $table->string('email')->nullable();
            $table->tinyInteger('email_verified')->default(0);

            $table->string('alternate_email')->nullable();
            $table->tinyInteger('alternate_email_verified')->default(0);

            $table->string('phone', 18)->nullable();
            $table->tinyInteger('phone_verified')->default(0);

            $table->string('zoneinfo', 50)->nullable();
            $table->string('locale', 10)->nullable();
            $table->string('picture')->nullable();

            $table->timestamps();
            $table->uuid('updater');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
