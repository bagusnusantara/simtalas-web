<?php

return [
    'selamat_datang' => 'Welcome to Auth module',
    'pesan' => 'Message from IndexController',
    'belajar' => 'read the documentation and understand the flow',
    'sukses_ubah_hak_akses' => 'Role changed successfully.',
    'hak_akses_tidak_aktif' => 'Role is no longer active.',
];
