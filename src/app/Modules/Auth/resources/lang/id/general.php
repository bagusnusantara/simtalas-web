<?php

return [
    'selamat_datang' => 'Selamat datang di modul Auth',
    'pesan' => 'Pesan dari IndexController',
    'belajar' => 'baca dokumentasi dan pahami alurnya',
    'sukses_ubah_hak_akses' => 'Hak akses berhasil diubah.',
    'hak_akses_tidak_aktif' => 'Hak akses sudah tidak aktif.',
];
