<?php

namespace App\Modules\Auth;

use Dptsi\Modular\Base\BaseModule;

class Module extends BaseModule
{
    public function getProviders(): array
    {
        return [
            \App\Modules\Auth\Providers\RouteServiceProvider::class,
            \App\Modules\Auth\Providers\DatabaseServiceProvider::class,
            \App\Modules\Auth\Providers\LangServiceProvider::class,
        ];
    }
}
