<?php

namespace App\Modules\Pegawai\Controllers;

use App\Models\Pegawai;
use App\Models\Pengajuan;
use App\Modules\Pegawai\Repositories\JenisPengajuanRepository;
use App\Modules\Pegawai\Repositories\PegawaiRepository;
use App\Modules\Pegawai\Repositories\UnitRepository;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class PegawaiController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        $unitRepository = new UnitRepository();
        $jenisPengajuanRepository = new JenisPengajuanRepository();
        $data['unit'] = $unitRepository->getAll();
        $data['jenis_pengajuan'] = $jenisPengajuanRepository->getAll();

        $page['menu'] = "pegawai";
        $page['submenu'] = "daftar pegawai";
        $page['halaman'] = $page['submenu'];
        return view('Pegawai::pegawai.index', compact('page', 'data'));
    }
    public function show($id)
    {
        $pegawaiRepository = new PegawaiRepository();
        $data = $pegawaiRepository->getById($id);
        $page['menu'] = "pegawai";
        $page['submenu'] = "daftar pegawai";
        $page['halaman'] = $page['submenu'];

        return view('Pegawai::pegawai.show', compact('page', 'data'));
    }
    public function server_side(Request $request)
    {
        $pegawaiRepository = new PegawaiRepository();
        $data = $pegawaiRepository->getArrayWhere($request, $server_side = true);
        return $data;
    }

    public function nonaktifkan(Pegawai $pegawai, Request $request)
    {
        try {
            $data = $request->validate([
                'alasan_nonaktif' => 'string|required',
                'tgl_nonaktif' => 'required|date',
            ]);

            $pegawai->update($data);
            $pegawai->updater = sso()->user()->getId();
            $pegawai->save();

            $pengajuan = Pengajuan::where('id_pengajuan',$pegawai->id_pengajuan)->update([
                'alasan_nonaktif' => $data['alasan_nonaktif'],
                'tgl_nonaktif' => $data['tgl_nonaktif'],
            ]);

            return redirect()->back()->with('success', "Pegawai telah dinonaktfikan.");
        } catch (\Exception $e) {
            return redirect()->back()->with('failed', "Penonaktifan pegawai gagal.");
        }
    }
}
