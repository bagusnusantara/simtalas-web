<?php

namespace App\Modules\Pegawai\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller;
use App\Modules\Pegawai\Repositories\NomorSuratRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NomorSuratController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        if (sso()->user()->getActiveRole()->getName() != 'Super Administrator') {
            abort(403);
        } else {
            $page['menu'] = "nomor surat";
            $page['submenu'] = "daftar nomor surat";
            $page['halaman'] = $page['submenu'];
            return view('Pegawai::nomor-surat.index', compact('page'));
        }
    }
    public function server_side()
    {
        $nomorSuratRepository = new NomorSuratRepository();
        $data = $nomorSuratRepository->getDatatable();
        return $data;
    }
    public function create()
    {
        if (sso()->user()->getActiveRole()->getName() != 'Super Administrator') {
            abort(403);
        } else {
            $page['menu'] = "nomor surat";
            $page['submenu'] = "daftar nomor surat";
            $page['halaman'] = $page['submenu'];
            return view('Pegawai::nomor-surat.create', compact('page'));
        }
    }

    public function store(Request $request)
    {
        $nomor_surat = explode(",", $request->nomor_surat);
        DB::beginTransaction();

        try {
            foreach ($nomor_surat as $item) {
                DB::table('nomor_surat')->insert([
                    'nomor_surat' => $item,
                    'is_used' => 0,
                    'created_at' => date('Y-m-d'),
                    'updated_at' => date('Y-m-d')
                ]);
            }
            DB::commit();
            return redirect('pegawai/nomor-surat')->with('success', 'Nomor surat berhasil ditambahkan');
        } catch (\Throwable $e) {
            DB::rollback();
            throw $e;
            return redirect('pegawai/nomor-surat')->with('danger', 'Nomor surat gagal ditambahkan');
        }
    }

}
