<?php

namespace App\Modules\Pegawai\Controllers;

use App\Models\BebanKerjaDosen;
use App\Models\PengajuanDosen;
use App\Modules\Pegawai\Requests\TambahBebanKerjaPengajuanDosenRequest;
use App\Modules\Pegawai\Requests\UbahBebanKerjaPengajuanDosenRequest;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;

class BebanKerjaDosenController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  PengajuanDosen  $pengajuan_dosen
     * @param  \App\Modules\Pegawai\Requests\TambahTugasPengajuanDosenRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PengajuanDosen $pengajuan_dosen, TambahBebanKerjaPengajuanDosenRequest $request)
    {
        if(sso()->user()->getActiveRole()->getName() != 'Administrator' && sso()->user()->getActiveRole()->getName() != 'Super Administrator') {
            abort(403);
        }
        
        if ($pengajuan_dosen->is_approved0 != NULL && $pengajuan_dosen->is_approved0 != '2' && $pengajuan_dosen->is_approved1 != '2') {
            abort(403);
        }
        $data = $request->validated();
        try {
            $beban_kerja_dosen = new BebanKerjaDosen();
            $beban_kerja_dosen->fill($data);
            $beban_kerja_dosen->pengajuan_dosen()->associate($pengajuan_dosen);
            $beban_kerja_dosen->save();
            return redirect()->back()->with('success', "Beban kerja berhasil disimpan");
        } catch (\Exception $e) {
            return redirect()->back()->with('failed', "Beban kerja gagal disimpan");
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PengajuanDosen  $pengajuan_dosen
     * @param  \App\Modules\Pegawai\Requests\UbahBebanKerjaPengajuanDosenRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function update(PengajuanDosen $pengajuan_dosen, UbahBebanKerjaPengajuanDosenRequest $request)
    {
        if(sso()->user()->getActiveRole()->getName() != 'Administrator' && sso()->user()->getActiveRole()->getName() != 'Super Administrator') {
            abort(403);
        }
        
        if ($pengajuan_dosen->is_approved0 != NULL && $pengajuan_dosen->is_approved0 != '2' && $pengajuan_dosen->is_approved1 != '2') {
            abort(403);
        }
        $data = $request->validated();
        $beban_kerja_dosen = BebanKerjaDosen::find($data['id']);
        
        if($beban_kerja_dosen->id_pengajuan_dosen !== $pengajuan_dosen->id_pengajuan_dosen) {
            abort(404);
        }
        
        try {
            $beban_kerja_dosen->update($data);
            // dd($beban_kerja_dosen);
            return redirect()->back()->with('success', "Beban kerja berhasil diubah");
        } catch (\Exception $e) {
            return redirect()->back()->with('failed', "Beban kerja gagal diubah");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  PengajuanDosen $pengajuan_dosen
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(PengajuanDosen $pengajuan_dosen, Request $request)
    {
        if(sso()->user()->getActiveRole()->getName() != 'Administrator' && sso()->user()->getActiveRole()->getName() != 'Super Administrator') {
            abort(403);
        }
        
        if ($pengajuan_dosen->is_approved0 != NULL && $pengajuan_dosen->is_approved0 != '2' && $pengajuan_dosen->is_approved1 != '2') {
            abort(403);
        }
        $data = $request->validate(['id' => 'required|exists:beban_kerja_dosen,id_beban_kerja_dosen']);
        $beban_kerja_dosen = BebanKerjaDosen::find($data['id']);
        if($beban_kerja_dosen->id_pengajuan_dosen !== $pengajuan_dosen->id_pengajuan_dosen) {
            abort(404);
        }

        try {
            $beban_kerja_dosen->delete();
            return redirect()->back()->with('success', "Beban kerja berhasil dihapus");
        } catch (\Exception $e) {
            return redirect()->back()->with('failed', "Beban kerja gagal dihapus");
        }
    }
}
