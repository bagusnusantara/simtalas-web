<?php

namespace App\Modules\Pegawai\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller;

class BaseController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        $message = __('Pegawai::general.belajar');
        return view('Pegawai::index', compact('message'));
    }
    public function create()
    {
        return "ok";
    }
}