<?php

namespace App\Modules\Pegawai\Controllers;

use App\Models\Pegawai;
use App\Models\PegawaiTanggungan;
use App\Models\Pengajuan;
use App\Models\RincianTugas;
use App\Modules\Pegawai\Repositories\JenisPengajuanRepository;
use App\Modules\Pegawai\Repositories\PegawaiRepository;
use App\Modules\Pegawai\Repositories\PendidikanRepository;
use App\Modules\Pegawai\Repositories\PengajuanRepository;
use App\Modules\Pegawai\Repositories\UnitRepository;
use App\Models\PengajuanDosen;
use DateTime;
use Dptsi\FileStorage\Facade\FileStorage;
use Exception;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use PDF;
use Ramsey\Uuid\Uuid;

class PengajuanController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index()
    {
        $unitRepository = new UnitRepository();
        $jenisPengajuanRepository = new JenisPengajuanRepository();
        $data['unit'] = $unitRepository->getAll();
        $data['jenis_pengajuan'] = $jenisPengajuanRepository->getAll();

        $page['menu'] = "pegawai";
        $page['submenu'] = "pengajuan pegawai";
        $page['halaman'] = "daftar pengajuan THL";
        return view('Pegawai::pengajuan.index', compact('page', 'data'));
    }
    public function server_side(Request $request)
    {
        $pengajuanRepository = new pengajuanRepository();
        $data = $pengajuanRepository->getArrayWhere($request, $server_side = true);

        return $data;
    }
    public function create()
    {
        if (session('is_viewer') == true) {
            abort(403);
        } else {
            $unitRepository = new UnitRepository();
            $jenisPengajuanRepository = new JenisPengajuanRepository();
            $pendidikanRepository = new PendidikanRepository();
            $pegawaiRepository = new PegawaiRepository();

            $data['unit'] = $unitRepository->getAll();

            $data['jenis_pengajuan'] = $jenisPengajuanRepository->getAll();
            $data['pendidikan'] = $pendidikanRepository->getAll();
            $data['pegawai'] = $pegawaiRepository->getAll();
            $data['nik'] = json_encode($data['pegawai']->pluck('nik'));
            $data['pegawai_perpanjangan'] = $pegawaiRepository->getOnlyAvailable();

            $page['menu'] = "pegawai";
            $page['submenu'] = "pengajuan pegawai";
            $page['halaman'] = "tambah";
            return view('Pegawai::pengajuan.create', compact('page', 'data'));
        }
    }

    public function show($id_pengajuan)
    {
        try {
            
            $pengajuanRepository = new PengajuanRepository();
            $data = $pengajuanRepository->getById($id_pengajuan);

            if (sso()->user()->getActiveRole()->getOrgId() != $data->id_unit && sso()->user()->getActiveRole()->getName() != 'Super Administrator' && session('is_viewer') != true) { //akses halaman show yang bukan miliknya. kalau bukan SA atau bukan viewer
                abort(403);
            } else {

                $rincian_tugas =  DB::table('rincian_tugas')->where('id_pengajuan', $id_pengajuan)->get();
                $tanggungan = DB::table('pegawai_tanggungan')->where('id_pegawai', $data->nik)->get();
                $riwayat = DB::table('pengajuan')->where('nik', $data->nik)->get();

                $page['menu'] = "pegawai";
                $page['submenu'] = "pengajuan pegawai";
                $page['halaman'] = "detail";

                return view('Pegawai::pengajuan.show', compact('page', 'data', 'rincian_tugas', 'tanggungan', 'riwayat'));
            }
        } catch (Exception $e) { //akses halaman show yang idnya ngawur
            abort(403);
        }

    }
    public function show_pdf($id_berkas)
    {
        $response = FileStorage::getFileById($id_berkas);
        return view('Pegawai::pengajuan.show_pdf', compact('response'));
    }

    public function store(Request $request)
    {
        $data = $request->input();

        if ($data['id_status_pengajuan'] == '0') {
            $validatedData = $request->validate([
                'nik' => 'required|digits:16',
                'nama' => 'required',
                'jenis_kelamin' => 'required',
                'id_pendidikan' => 'required',
                'tgl_lahir' => 'required',
                'alamat' => 'required',
                'pas_foto' => 'required',
                'ktp' => 'required',
                'ijazah' => 'required'
            ]);
        } else {
            $validatedData = $request->validate([
                'nik' => 'required|digits:16',
            ]);
        }
        // if ($validatedData->fails()) {
        //     return back()->withInput()->withErrors(['nik' => 'NIK harus 16 digit']);
        // }

        $pengajuan = new Pengajuan();
        $pengajuan->id_pengajuan = Uuid::uuid4()->toString();
        $pengajuan->id_status_pengajuan = $data['id_status_pengajuan'];
        $pengajuan->id_jenis_pengajuan = $data['id_jenis_pengajuan'];
        $pengajuan->id_unit = $data['id_unit'];

        // $pengajuan->rincian_tugas = $data['rincian_tugas'];
        $pengajuan->tgl_pengajuan      = $data['tgl_pengajuan'];
        $pengajuan->tgl_mulai_kerja      = $data['tgl_mulai_kerja'];
        $pengajuan->tgl_akhir_kerja      = $data['tgl_akhir_kerja'];
        $pengajuan->honor = $data['honor'];
        $pengajuan->uang_makan = $data['uang_makan'];
        $pengajuan->updater = sso()->user()->getId();

        DB::beginTransaction();
        if ($data['id_status_pengajuan'] == '0') {
            $pengajuan->nik = $data['nik'];
            $pengajuan->save();
            $pegawai = new Pegawai();
            $pegawai->id_pegawai = Uuid::uuid4()->toString();
            $pegawai->id_pengajuan = $pengajuan->id_pengajuan;
            $pegawai->nama = $data['nama'];
            $pegawai->nik = $pengajuan->nik;
            $pegawai->tgl_lahir = $data['tgl_lahir'];
            $pegawai->jenis_kelamin = $data['jenis_kelamin'];
            $pegawai->id_pendidikan = $data['id_pendidikan'];
            $pegawai->alamat = $data['alamat'];
            $pegawai->email = $data['email'];
            $pegawai->hp = $data['hp'];

            $pegawai->id_status_bpjs = $data['id_status_bpjs'];
            $pegawai->nomor_bpjs = $data['nomor_bpjs'];
            //upload berkas
            if ($request->file('pas_foto') != null) {
                $response = FileStorage::upload($request->file('pas_foto'));
                $pegawai->public_link_pas_foto = $response->info->public_link;
                $pegawai->id_pas_foto = $response->info->file_id;
                $pegawai->mime_type_pas_foto = $response->info->file_mimetype;

            }
            if ($request->file('ktp') != null) {
                $response = FileStorage::upload($request->file('ktp'));
                $pegawai->public_link_ktp = $response->info->public_link;
                $pegawai->id_ktp = $response->info->file_id;
                $pegawai->mime_type_ktp = $response->info->file_mimetype;
            }
            if ($request->file('ijazah') != null) {
                $response = FileStorage::upload($request->file('ijazah'));
                $pegawai->public_link_ijazah = $response->info->public_link;
                $pegawai->id_ijazah = $response->info->file_id;
                $pegawai->mime_type_ijazah = $response->info->file_mimetype;
            }

            $pegawai->updater = sso()->user()->getId();
            $pegawai->save();
        } else { //perpanjangan
            // $pegawai = $pegawaiRepository->getById($data['cari_nik']);
            $pegawai = Pegawai::where('id_pegawai', $data['cari_nik'])->first();
            $pegawai->id_pengajuan = $pengajuan->id_pengajuan;
            $pegawai->tgl_nonaktif = null;
            $pegawai->alasan_nonaktif = null;
            $pegawai->save();

            $pengajuan->nik = $pegawai->nik;
            $pengajuan->save();
        }
        if ($data['id_status_pengajuan'] == '0') {
            $count = count($data['nama_anggota']);
            if ($data['nama_anggota'][0] != NULL) {
                for ($i = 0; $i < $count; $i++) {
                    if ($data['nama_anggota'][$i] != NULL) {
                        $tanggungan = new PegawaiTanggungan();
                        $tanggungan->nama = $data['nama_anggota'][$i];
                        $tanggungan->status = $data['status_anggota'][$i];
                        $tanggungan->tgl_lahir = $data['tgl_lahir_anggota'][$i];
                        $tanggungan->id_pegawai = $data['nik'];
                        $tanggungan->save();
                    }
                }
            }
        }
        $count = count($data['deskripsi']);
        if ($data['deskripsi'][0] != NULL) {
            for ($i = 0; $i < $count; $i++) {
                if ($data['deskripsi'][$i] != NULL) {
                    $rincian = new RincianTugas();
                    $rincian->id_pengajuan = $pengajuan->id_pengajuan;
                    $rincian->deskripsi = $data['deskripsi'][$i];
                    $rincian->save();
                }
            }
        }
        DB::commit();

        return redirect('pegawai/pengajuan')->with('success', "Data berhasil disimpan");
    }

    public function edit($id_pengajuan)
    {
        if (session('is_viewer') == true) {
            abort(403);
        } else {
            $pengajuanRepository = new PengajuanRepository();
            $data = $pengajuanRepository->getById($id_pengajuan);
            if ($data->is_approved0 == 2 || $data->is_approved1 == 2) {
                $rincian_tugas =  DB::table('rincian_tugas')->where('id_pengajuan', $id_pengajuan)->get();
                $tanggungan = DB::table('pegawai_tanggungan')->where('id_pegawai', $data->nik)->get();
                $riwayat = DB::table('pengajuan')->where('nik', $data->nik)->get();

                $page['menu'] = "pegawai";
                $page['submenu'] = "pengajuan pegawai";
                $page['halaman'] = "detail";
                return view('Pegawai::pengajuan.edit', compact('page', 'data', 'rincian_tugas', 'tanggungan', 'riwayat'));
            } else {
                abort(403);
            }
        }
    }

    public function approve(Request $request)
    {
        $input = $request->input();
        if (sso()->user()->getActiveRole()->getName() == 'Super Administrator') {
            $int = 1;
        } else if (Session::get('is_validator')) {
            $int = 0;
        }
        if ($input['status'] == 'setuju') {
            $data = [
                'is_approved' . $int => 1,
                'id_approved' . $int => sso()->user()->getId(),
                'name_approved' . $int => sso()->user()->getName(),
                'date_approved' . $int => date('Y-m-d H:i:s'),
            ];
        } else if ($input['status'] == 'perbaikan') {
            $data = [
                'is_approved' . $int  => 2,
                'id_approved' . $int  => sso()->user()->getId(),
                'name_approved' . $int  => sso()->user()->getName(),
                'date_approved' . $int  => date('Y-m-d H:i:s'),
                'alasan' . $int  => $input['alasan'],
            ];
        } else if ($input['status'] == 'batalkan') {
            $data = [
                'is_approved' . $int  => NULL,
                'id_approved' . $int  => NULL,
                'name_approved' . $int  => NULL,
                'date_approved' . $int  =>  NULL,
                'alasan' . $int => NULL,
            ];
        } else if ($input['status'] == 'tidak_setuju') {
            $data = [
                'is_approved' . $int  => 0,
                'id_approved' . $int  => sso()->user()->getId(),
                'name_approved' . $int  => sso()->user()->getName(),
                'date_approved' . $int  =>  date('Y-m-d H:i:s'),
                'alasan' . $int => $input['alasan'],
            ];
        }

        DB::table('pengajuan')->where('id_pengajuan', $input['id_pengajuan'])->update($data);
        return redirect()->back()->with('success', "Status pengajuan berhasil diupdate.");
    }

    public function draft($id_pengajuan)
    {
        try {
            
            $pengajuanRepository = new PengajuanRepository();
            $detail = $pengajuanRepository->getById($id_pengajuan);

             if (sso()->user()->getActiveRole()->getOrgId() != $detail->id_unit && sso()->user()->getActiveRole()->getName() != 'Super Administrator') { //akses draft yang bukan miliknya
                abort(403);
            } else {
                //tanggal pengajuan
                $hariIni = \Carbon\Carbon::createFromDate($detail->tgl_pengajuan)->locale('id');
                $tgl_mulai = \Carbon\Carbon::createFromDate($detail->tgl_mulai_kerja)->locale('id');
                $tgl_akhir = \Carbon\Carbon::createFromDate($detail->tgl_akhir_kerja)->locale('id');

                //hitung usia
                $birthDate = new DateTime($detail->tgl_lahir);
                $today = new DateTime("today");

                $y = $today->diff($birthDate)->y;
                $data = [
                    'no_surat' => $detail->no_surat,
                    'nama' => $detail->nama,
                    'nik' => $detail->nik,
                    'unit' => $detail->unit,
                    'honor' => number_format($detail->honor, 2),
                    'uang_makan' => number_format($detail->uang_makan, 2),
                    'terbilang_honor' => $this->terbilang($detail->honor),
                    'terbilang_uang_makan' => $this->terbilang($detail->uang_makan),
                    'tgl_mulai_kerja_d' => $tgl_mulai->format('j'),
                    'tgl_mulai_kerja_m' => $tgl_mulai->monthName,
                    'tgl_mulai_kerja_y' => $tgl_mulai->format('Y'),

                    'tgl_akhir_kerja_d' => $tgl_akhir->format('j'),
                    'tgl_akhir_kerja_m' => $tgl_akhir->monthName,
                    'tgl_akhir_kerja_y' => $tgl_akhir->format('Y'),


                    'tgl_akhir_kerja' => date('d-m-Y', strtotime($detail->tgl_akhir_kerja)),
                    'alamat' => $detail->alamat,
                    'hari' => $hariIni->dayName,
                    'tanggal' => $hariIni->format('j'),
                    'bulan' => $hariIni->monthName,
                    'tahun' => $hariIni->format('Y'),
                    'tahun_terbilang' => ucwords($this->terbilang($hariIni->format('Y'))),
                    'hari' => $hariIni->dayName,
                    'umur' => $y . " tahun ",
                    'id_jenis_pengajuan' => $detail->id_jenis_pengajuan,
                    'id_unit' => $detail->id_unit
                ];
                $rincian_tugas =  DB::table('rincian_tugas')->where('id_pengajuan', $id_pengajuan)->get();

                $pdf = PDF::loadView('Pegawai::pengajuan.pdf', compact('data', 'rincian_tugas'));

                return $pdf->stream();
            }

        } catch (Exception $e) {
            abort(403);   //akses draft yang idnya ngawur
        }
    }
    public function isi_nomor_surat(Request $request)
    {
        $input = $request->input();
        $data = [
            'no_surat'  => $input['nomor_surat'],
        ];

        // cek availability nomor surat
        $cek = DB::table('nomor_surat')->where('nomor_surat','=',$data['no_surat'])->first();

        if ($cek->is_used == 1) {
            return redirect()->back()->with('failed', "Nomor surat pengajuan telah digunakan. Antum kurang gercep");
        } else {

            DB::table('pengajuan')->where('id_pengajuan', $input['id_pengajuan'])->update($data);
            DB::table('nomor_surat')->where('nomor_surat', $input['nomor_surat'])->update(['is_used' => '1']);
            return redirect()->back()->with('success', "Nomor surat pengajuan berhasil diupdate.");

        }
    }

    //Fungsi Terbilang
    public function penyebut($nilai)
    {
        $nilai = abs($nilai);
        $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";
        if ($nilai < 12) {
            $temp = " " . $huruf[$nilai];
        } else if ($nilai < 20) {
            $temp = $this->penyebut($nilai - 10) . " belas";
        } else if ($nilai < 100) {
            $temp = $this->penyebut($nilai / 10) . " puluh" . $this->penyebut($nilai % 10);
        } else if ($nilai < 200) {
            $temp = " seratus" . $this->penyebut($nilai - 100);
        } else if ($nilai < 1000) {
            $temp = $this->penyebut($nilai / 100) . " ratus" . $this->penyebut($nilai % 100);
        } else if ($nilai < 2000) {
            $temp = " seribu" . $this->penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
            $temp = $this->penyebut($nilai / 1000) . " ribu" . $this->penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
            $temp = $this->penyebut($nilai / 1000000) . " juta" . $this->penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
            $temp = $this->penyebut($nilai / 1000000000) . " milyar" . $this->penyebut(fmod($nilai, 1000000000));
        } else if ($nilai < 1000000000000000) {
            $temp = $this->penyebut($nilai / 1000000000000) . " trilyun" . $this->penyebut(fmod($nilai, 1000000000000));
        }
        return $temp;
    }

    public function terbilang($nilai)
    {
        if ($nilai < 0) {
            $hasil = "minus " . trim($this->penyebut($nilai));
        } else {
            $hasil = trim($this->penyebut($nilai));
        }
        return $hasil;
    }

    public function upload(Request $request)
    {
        $input = $request->input();
        $response = FileStorage::upload($request->file('file'));
        $berkas = $response->info->public_link;

        $data = [
            'berkas'  => $berkas,
        ];

        DB::table('pengajuan')->where('id_pengajuan', $input['id_pengajuan'])->update($data);
        return redirect()->back()->with('success', "Berkas berhasil diunggah.");
    }

    public function hapus_berkas(Request $request)
    {
        $input = $request->input();
        $data = [
            'berkas'  => NULL,
        ];

        DB::table('pengajuan')->where('id_pengajuan', $input['id_pengajuan'])->update($data);
        return redirect()->back()->with('success', "Berkas berhasil dihapus.");
    }

    public function update(Request $request)
    {
        $data = $request->input();
        //update data pengajuan
        Pengajuan::where('id_pengajuan', $data['id_pengajuan'])->update([
            'id_status_pengajuan' => $data['id_status_pengajuan'],
            'id_jenis_pengajuan' => $data['id_jenis_pengajuan'],
            'tgl_pengajuan' =>  Carbon::createFromFormat('d-m-Y', $data['tgl_pengajuan'])->format('Y-m-d'),
            'tgl_mulai_kerja' => Carbon::createFromFormat('d-m-Y', $data['tgl_mulai_kerja'])->format('Y-m-d'),
            'tgl_akhir_kerja' => Carbon::createFromFormat('d-m-Y', $data['tgl_akhir_kerja'])->format('Y-m-d'),
            'honor' => $data['honor'],
            'uang_makan' => $data['uang_makan'],
            'updater' => sso()->user()->getId(),
            'nik' => $data['nik'],
        ]);

        //update data pegawai
        Pegawai::where('nik', $data['nik_lama'])->update([
            'nama' => $data['nama'],
            'nik' => $data['nik'],
            'tgl_lahir' =>  Carbon::createFromFormat('d-m-Y', $data['tgl_lahir'])->format('Y-m-d'),
            'jenis_kelamin' => $data['jenis_kelamin'],
            'id_pendidikan' => $data['id_pendidikan'],
            'alamat' => $data['alamat'],
            'email' => $data['email'],
            'hp' => $data['hp'],
            'id_status_bpjs' => $data['id_status_bpjs'],
            'nomor_bpjs' => $data['nomor_bpjs'],
        ]);

        // //upload berkas
        if ($request->file('pas_foto') != null) {
            $response = FileStorage::upload($request->file('pas_foto'));
            Pegawai::where('nik', $data['nik'])->update([
                'public_link_pas_foto' => $response->info->public_link,
            ]);
        }
        if ($request->file('ktp') != null) {
            $response = FileStorage::upload($request->file('ktp'));
            Pegawai::where('nik', $data['nik'])->update([
                'public_link_ktp' => $response->info->public_link,
            ]);
        }
        if ($request->file('ijazah') != null) {
            $response = FileStorage::upload($request->file('ijazah'));
            Pegawai::where('nik', $data['nik'])->update([
                'public_link_ijazah' => $response->info->public_link,
            ]);
        }

        // $pegawai->updater = sso()->user()->getId();
        // $pegawai->save();
        // if ($data['id_status_pengajuan'] == '0') {
        //     $count = count($data['nama_anggota']);
        //     if ($data['nama_anggota'][0] != NULL) {
        //         for ($i = 0; $i < $count; $i++) {
        //             if ($data['nama_anggota'][$i] != NULL) {
        //                 $tanggungan = new PegawaiTanggungan();
        //                 $tanggungan->nama = $data['nama_anggota'][$i];
        //                 $tanggungan->status = $data['status_anggota'][$i];
        //                 $tanggungan->tgl_lahir = $data['tgl_lahir_anggota'][$i];
        //                 $tanggungan->id_pegawai = $data['nik'];
        //                 $tanggungan->save();
        //             }
        //         }
        //     }
        // }
        // $count = count($data['deskripsi']);
        // if ($data['deskripsi'][0] != NULL) {
        //     for ($i = 0; $i < $count; $i++) {
        //         if ($data['deskripsi'][$i] != NULL) {
        //             $rincian = new RincianTugas();
        //             $rincian->id_pengajuan = $pengajuan->id_pengajuan;
        //             $rincian->deskripsi = $data['deskripsi'][$i];
        //             $rincian->save();
        //         }
        //     }
        // }


        return redirect()->back()->with('success', "Data berhasil disimpan");
    }

    public function tambah_tugas(Request $request)
    {
        $validatedData = $request->validate([
            'id_pengajuan' => 'required',
            'deskripsi' => 'required',
        ]);
        try {
            $data = [
                'id_pengajuan'    => $request->id_pengajuan,
                'deskripsi'  => $request->deskripsi,
                'created_at'    => now(),
                'updated_at'    => now(),
            ];
            DB::table('dbo.rincian_tugas')->insert($data);
            return redirect()->back()->with('success', "Data berhasil disimpan");
        } catch (\Exception $e) {
            return redirect()->back()->with('failed', "Data gagal disimpan");
        }
    }
    public function edit_tugas(Request $request)
    {
        $validatedData = $request->validate([
            'deskripsi' => 'required',
        ]);
        try {
            $data = [
                'deskripsi'  => $request->deskripsi,
                'updated_at'    => now(),
            ];
            DB::table('dbo.rincian_tugas')->where('id', $request->id)->update($data);
            return redirect()->back()->with('success', "Data berhasil disimpan");
        } catch (\Exception $e) {
            return redirect()->back()->with('failed', "Data gagal disimpan");
        }
    }
    public function hapus_tugas(Request $request)
    {
        $validatedData = $request->validate([
            'id' => 'required',
        ]);
        try {
            DB::table('dbo.rincian_tugas')->where('id', $request->id)->delete();
            return redirect()->back()->with('success', "Data berhasil dihapus");
        } catch (\Exception $e) {
            return redirect()->back()->with('failed', "Data gagal dihapus");
        }
    }

    public function tambah_keluarga(Request $request)
    {
        $validatedData = $request->validate([
            'id_pegawai' => 'required',
            'nama' => 'required',
            'tgl_lahir' => 'required',
            'status' => 'required',
        ]);
        try {
            $data = [
                'id_pegawai'    => $request->id_pegawai,
                'nama'  => $request->nama,
                'tgl_lahir'  => $request->tgl_lahir,
                'status'  => $request->status,
                'created_at'    => now(),
                'updated_at'    => now(),
            ];
            DB::table('dbo.pegawai_tanggungan')->insert($data);
            return redirect()->back()->with('success', "Data berhasil disimpan");
        } catch (\Exception $e) {
            return redirect()->back()->with('failed', "Data gagal disimpan");
        }
    }
    public function edit_keluarga(Request $request)
    {
        $validatedData = $request->validate([
            'id' => 'required',
            'nama' => 'required',
            'tgl_lahir' => 'required',
            'status' => 'required',
        ]);
        try {
            $data = [
                'nama'  => $request->nama,
                'tgl_lahir'  => $request->tgl_lahir,
                'status'  => $request->status,
                'updated_at'    => now(),
            ];
            DB::table('dbo.pegawai_tanggungan')->where('id_pegawai_tanggungan', $request->id)->update($data);
            return redirect()->back()->with('success', "Data berhasil disimpan");
        } catch (\Exception $e) {
            return redirect()->back()->with('failed', "Data gagal disimpan");
        }
    }
    public function hapus_keluarga(Request $request)
    {
        $validatedData = $request->validate([
            'id' => 'required',
        ]);
        try {
            DB::table('dbo.pegawai_tanggungan')->where('id_pegawai_tanggungan', $request->id)->delete();
            return redirect()->back()->with('success', "Data berhasil dihapus");
        } catch (\Exception $e) {
            return redirect()->back()->with('failed', "Data gagal dihapus");
        }
    }

    public function unduh_berkas_pendukung($id_pengajuan, $kategori)
    {
        $pengajuanRepository = new PengajuanRepository();
        $pengajuan = $pengajuanRepository->getById($id_pengajuan);

        $url = 'https://storage-api.its.ac.id' . $pengajuan->{$kategori};

        return Redirect::to($url);
    }

    public function preview_berkas_pendukung(string $id_berkas)
    {
        $response = FileStorage::getFileById($id_berkas);
        $file = base64_decode($response->data);
        $file_mimetype = base64_decode($response->info->file_mimetype);

        return response($file)->header('Content-Type', $file_mimetype);
    }

    //dosen template
    public function dosen($id_pengajuan)
    {

        $pengajuan_dosen = PengajuanDosen::where('id_pengajuan_dosen', $id_pengajuan)->first();
        if ($pengajuan_dosen != NULL) {
            $beban_kerja_dosen_mengajar = $pengajuan_dosen->beban_kerja_dosen()->whereIn('id_jenis_beban_kerja_dosen', [1, 2, 3, 4])->get();
            $beban_kerja_dosen_ta = $pengajuan_dosen->beban_kerja_dosen()->whereIn('id_jenis_beban_kerja_dosen', [5, 6, 7, 8])->get();
            $data['dosen'] = $pengajuan_dosen->dosen()->first();
            $data['unit'] = DB::table('unit')->select(['nama', 'parent1'])->where('id_unit', '=', $pengajuan_dosen->id_unit)->first();

            //tanggal pengajuan
            $tgl_mulai = \Carbon\Carbon::createFromDate($pengajuan_dosen->tanggal_mulai_kerja)->locale('id');
            $tgl_akhir = \Carbon\Carbon::createFromDate($pengajuan_dosen->tanggal_akhir_kerja)->locale('id');
            $tgl_pengesahan = \Carbon\Carbon::createFromDate($pengajuan_dosen->tanggal_pengesahan)->locale('id');
            $tgl_lahir = \Carbon\Carbon::createFromDate($data['dosen']->tgl_lahir)->locale('id');
            $data = [
                'no_surat' => $pengajuan_dosen->no_surat,
                'nama' => $data['dosen']->gelar_depan . ' ' . $data['dosen']->nama . ', ' . $data['dosen']->gelar_belakang,
                'nik' => $data['dosen']->nik,
                'departemen' => $data['unit']->nama,
                'fakultas' => $data['unit']->parent1,
                'honor' => number_format(500000, 2),
                'uang_makan' => number_format(400000, 2),
                'terbilang_honor' => $this->terbilang(500000),
                'terbilang_uang_makan' => $this->terbilang(400000),
                'hari_pengesahan' => $tgl_pengesahan->dayName,
                'tgl_pengesahan_d' => $tgl_pengesahan->format('j'),
                'tgl_pengesahan_m' => $tgl_pengesahan->monthName,
                'tgl_pengesahan_y' => ucwords($this->terbilang($tgl_pengesahan->format('Y'))),

                'tgl_lahir' => $tgl_lahir->format('j') . ' ' . $tgl_lahir->monthName . ' ' . $tgl_lahir->format('Y'),

                'tgl_mulai_d' => $tgl_mulai->format('j'),
                'tgl_mulai_m' => $tgl_mulai->monthName,
                'tgl_mulai_y' => $tgl_mulai->format('Y'),

                'tgl_akhir_d' => $tgl_akhir->format('j'),
                'tgl_akhir_m' => $tgl_akhir->monthName,
                'tgl_akhir_y' => $tgl_akhir->format('Y'),

                'jenis_kelamin' => $data['dosen']->jenis_kelamin,
                'pendidikan_terakhir' => strtoupper($data['dosen']->id_pendidikan),
                'kepakaran' => $data['dosen']->kepakaran,
                'alamat' => $data['dosen']->alamat,

            ];
            
            $pdf = PDF::loadView('Pegawai::pengajuan_dosen.pdf', compact('data', 'beban_kerja_dosen_mengajar', 'beban_kerja_dosen_ta'));

            return $pdf->stream();
        } else {
            abort(404);
        }
    }
}
