<?php

namespace App\Modules\Pegawai\Controllers;

use App\Models\BebanKerjaDosen;
use App\Models\Dosen;
use App\Models\MataKuliahBersamaWajib;
use App\Models\PengajuanDosen;
use App\Modules\Pegawai\Repositories\DosenRepository;
use App\Modules\Pegawai\Repositories\JenisBebanKerjaDosenRepository;
use App\Modules\Pegawai\Repositories\JenisPengajuanDosenRepository;
use App\Modules\Pegawai\Repositories\PendidikanRepository;
use App\Modules\Pegawai\Repositories\PengajuanDosenRepository;
use App\Modules\Pegawai\Repositories\UnitRepository;
use App\Modules\Pegawai\Requests\StorePengajuanDosenRequest;
use App\Modules\Pegawai\Requests\UpdatePengajuanDosenRequest;
use Dptsi\FileStorage\Facade\FileStorage;
use Exception;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class PengajuanDosenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (session('is_viewer') == true) {
            abort(403);
        } else {
            $unitRepository = new UnitRepository();

            $data['unit'] = $unitRepository->getAll();
            
            $page['menu'] = "pegawai";
            $page['submenu'] = "pengajuan dosen";
            $page['halaman'] = "daftar pengajuan dosen luar biasa";

            return view('Pegawai::pengajuan_dosen.index', compact('page', 'data'));
        }
    }

    public function server_side(Request $request) {
        $pengajuanDosenRepository = new PengajuanDosenRepository();
        $data = $pengajuanDosenRepository->getArrayWhere($request, $server_side = true);

        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (session('is_viewer') == true) {
            abort(403);
        } else {
            $unitRepository = new UnitRepository();
            $pendidikanRepository = new PendidikanRepository();
            $dosenRepository = new DosenRepository();
            $jenisBebanKerjaRepository = new JenisBebanKerjaDosenRepository();

            $data['unit'] = $unitRepository->getAll();

            $data['jenis_beban_kerja'] = $jenisBebanKerjaRepository->getAll();
            $data['jenis_pengajuan_dosen'] = DB::table('jenis_pengajuan_dosen')->get();
            $data['mata_kuliah_bersama_wajib'] = MataKuliahBersamaWajib::all();
            $data['pendidikan'] = $pendidikanRepository->getForDosenLB();
            $data['dosen'] = $dosenRepository->getAll();
            $data['nik'] = json_encode($data['dosen']->pluck('nik'));
            $data['dosen_perpanjangan'] = $dosenRepository->getOnlyAvailable();

            $page['menu'] = "pegawai";
            $page['submenu'] = "pengajuan dosen";
            $page['halaman'] = "tambah";
            return view('Pegawai::pengajuan_dosen.create', compact('page', 'data'));
        }
    }

    private function isi_berkas_dosen(FormRequest $request, Dosen $dosen) {
        $pas_foto = $request->file('pas_foto');
        if($pas_foto) {
            $response = FileStorage::upload($pas_foto);
            $dosen->id_berkas_pas_foto = $response->info->file_id;
        }

        $ktp = $request->file('ktp');
        if($ktp) {
            $response = FileStorage::upload($ktp);
            $dosen->id_berkas_ktp = $response->info->file_id;
        }

        $riwayat_hidup = $request->file('riwayat_hidup');
        if($riwayat_hidup) {
            $response = FileStorage::upload($riwayat_hidup);
            $dosen->id_berkas_riwayat_hidup = $response->info->file_id;
        }

        $ijazah_s2 = $request->file('ijazah_s2');
        if($ijazah_s2) {
            $response = FileStorage::upload($ijazah_s2);
            $dosen->id_ijazah_s2 = $response->info->file_id;
        }

        $transkrip_s2 = $request->file('transkrip_s2');
        if($transkrip_s2) {
            $response = FileStorage::upload($transkrip_s2);
            $dosen->id_transkrip_s2 = $response->info->file_id;
        }

        $penyetaraan_s2 = $request->file('penyetaraan_s2');
        if($penyetaraan_s2) {
            $response = FileStorage::upload($penyetaraan_s2);
            $dosen->id_berkas_penyetaraan_s2 = $response->info->file_id;
        }

        $ijazah_s3 = $request->file('ijazah_s3');
        if($ijazah_s3) {
            $response = FileStorage::upload($ijazah_s3);
            $dosen->id_ijazah_s3 = $response->info->file_id;
        }

        $transkrip_s3 = $request->file('transkrip_s3');
        if($transkrip_s3) {
            $response = FileStorage::upload($transkrip_s3);
            $dosen->id_transkrip_S3 = $response->info->file_id;
        }

        $penyetaraan_s3 = $request->file('penyetaraan_s3');
        if($penyetaraan_s3) {
            $response = FileStorage::upload($penyetaraan_s3);
            $dosen->id_berkas_penyetaraan_s3 = $response->info->file_id;
        }

        $analisis_bkd = $request->file('analisis_bkd');
        if($analisis_bkd) {
            $response = FileStorage::upload($analisis_bkd);
            $dosen->id_berkas_analisis_bkd = $response->info->file_id;
        }

        $surat_izin_institusi_asal = $request->file('surat_izin_institusi_asal');
        if($surat_izin_institusi_asal) {
            $response = FileStorage::upload($surat_izin_institusi_asal);
            $dosen->id_surat_izin_institusi_asal = $response->info->file_id;
        }

        $ijazah_s1 = $request->file('ijazah_s1');
        if($ijazah_s1) {
            $response = FileStorage::upload($ijazah_s1);
            $dosen->id_ijazah_s1 = $response->info->file_id;
        }

        $transkrip_s1 = $request->file('transkrip_s1');
        if($transkrip_s1) {
            $response = FileStorage::upload($transkrip_s1);
            $dosen->id_transkrip_s1 = $response->info->file_id;
        }

        $penyetaraan_s1 = $request->file('penyetaraan_s1');
        if($penyetaraan_s1) {
            $response = FileStorage::upload($penyetaraan_s1);
            $dosen->id_berkas_penyetaraan_s1 = $response->info->file_id;
        }

        $sehat_rohani = $request->file('sehat_rohani');
        if($sehat_rohani) {
            $response = FileStorage::upload($sehat_rohani);
            $dosen->id_berkas_sehat_rohani = $response->info->file_id;
        }

        $sehat_jasmani = $request->file('sehat_jasmani');
        if($sehat_jasmani) {
            $response = FileStorage::upload($sehat_jasmani);
            $dosen->id_berkas_sehat_jasmani = $response->info->file_id;
        }

        $bebas_narkotika = $request->file('bebas_narkotika');
        if($bebas_narkotika) {
            $response = FileStorage::upload($bebas_narkotika);
            $dosen->id_berkas_bebas_narkotika = $response->info->file_id;
        }

        $pernyataan_dekan = $request->file('pernyataan_dekan');
        if($pernyataan_dekan) {
            $response = FileStorage::upload($pernyataan_dekan);
            $dosen->id_berkas_pernyataan_dekan = $response->info->file_id;
        }

        $minimal_mengajar = $request->file('minimal_mengajar');
        if($minimal_mengajar) {
            $response = FileStorage::upload($minimal_mengajar);
            $dosen->id_berkas_minimal_mengajar = $response->info->file_id;
        }
    }

    private function buat_dosen_baru(StorePengajuanDosenRequest $request)
    {   
        $dosen_attr = $request
                        ->safe()
                        ->only([
                            'gelar_depan', 'gelar_belakang', 'npp',
                            'kepakaran', 'email', 'nomor_hp', 'nama',
                            'nik', 'nidn', 'tgl_lahir', 'jenis_kelamin',
                            'id_pendidikan', 'alamat', 'tempat_lahir',
                        ]);
        $dosen = new Dosen();
        $dosen->fill($dosen_attr);

        $this->isi_berkas_dosen($request, $dosen);

        $dosen->updater = sso()->user()->getId();
        $dosen->save();

        return $dosen;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Modules\Pegawai\Requests\StorePengajuanDosenRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePengajuanDosenRequest $request)
    {
        $data = $request->validated();
        $pengajuan_dosen_attr = $request
                        ->safe()
                        ->only([
                            'id_status_pengajuan_dosen',
                            'id_unit', 'tanggal_mulai_kerja', 'tanggal_akhir_kerja',
                            'tanggal_pengajuan', 'id_jenis_pengajuan_dosen'
                        ]);
        DB::beginTransaction();
        try {
            $pengajuan_dosen = new PengajuanDosen();
            $pengajuan_dosen->fill($pengajuan_dosen_attr);
            $pengajuan_dosen->id_updater = sso()->user()->getId();

            if ($data['id_status_pengajuan_dosen'] === '0') {
                $dosen = $this->buat_dosen_baru($request);
            } else {
                $dosen = Dosen::find($request->input('nik'));
                $npp = $request->input('npp');
                if($npp) {
                    $dosen->npp = $npp;
                    $dosen->save();
                }
            }

            $pengajuan_dosen->dosen()->associate($dosen);
            $pengajuan_dosen->save();
            
            $id_jenis_beban_kerja = $request->input('id_jenis_beban_kerja');
            $uraian = $request->input('uraian');
            $kelas = $request->input('kelas');
            $jumlah = $request->input('jumlah');
            $upah = $request->input('upah');
            $id_mkb_wajib = $request->input('id_mkb_wajib');
            
            $count_beban_kerja = $id_jenis_beban_kerja ? count($id_jenis_beban_kerja) : 0;
            for ($i = 0; $i < $count_beban_kerja; $i++) {
                if(!$id_jenis_beban_kerja[$i]) continue;
                $beban_kerja_dosen = new BebanKerjaDosen();
                $beban_kerja_dosen->fill([
                    'id_jenis_beban_kerja_dosen' => $id_jenis_beban_kerja[$i],
                    'id_mkb_wajib' => $id_mkb_wajib[$i],
                    'deskripsi' => $uraian[$i],
                    'kelas' => $kelas[$i],
                    'jumlah' => $jumlah[$i],
                    'upah' => $upah[$i],
                ]);
                $beban_kerja_dosen->pengajuan_dosen()->associate($pengajuan_dosen);
                $beban_kerja_dosen->save();
            }

            DB::commit();
            return redirect('pegawai/pengajuan-dosen')->with('success', "Data berhasil disimpan");
        } catch(\Exception $e) {
            return redirect('pegawai/pengajuan-dosen')->with('failed', "Data gagal disimpan");
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PengajuanDosen  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PengajuanDosen $pengajuan_dosen)
    {
        if (sso()->user()->getActiveRole()->getOrgId() != $pengajuan_dosen->id_unit && sso()->user()->getActiveRole()->getName() != 'Super Administrator') { //akses halaman show yang bukan miliknya
            abort(403);
        }

        $dosen = $pengajuan_dosen->dosen()->first();

        $data['pengajuan_dosen'] = $pengajuan_dosen;
        $data['pendidikan'] = DB::table('pendidikan')->select(['nama'])->where('id_pendidikan', '=', $dosen->id_pendidikan)->first();
        $data['beban_kerja_dosen'] = $pengajuan_dosen->beban_kerja_dosen()->with('jenis_beban_kerja_dosen')->get();
        $data['jenis_pengajuan_dosen'] = DB::table('jenis_pengajuan_dosen')->get();
        $data['sks_ajar'] = round($pengajuan_dosen->get_sks_pengajaran(), 1);
        $data['dosen'] = $pengajuan_dosen->dosen()->first();
        $data['unit'] = DB::table('unit')->select(['nama'])->where('id_unit', '=', $pengajuan_dosen->id_unit)->first();
        $data['riwayat_pekerjaan'] = DB::table('pengajuan_dosen')
                                        ->select(['no_surat', 'is_approved0', 'is_approved1', 'date_approved0', 'date_approved1', 'pengajuan_dosen.updated_at', 'unit.nama as unit'])
                                        ->where('nik', '=', $dosen->nik)
                                        ->join('unit', 'unit.id_unit', '=', 'pengajuan_dosen.id_unit')
                                        ->get();

        $page['menu'] = "dosen";
        $page['submenu'] = "pengajuan dosen";
        $page['halaman'] = "detail";

        // return response()->json($data);
        return view('Pegawai::pengajuan_dosen.show', compact('page', 'data')); 
    }

    public function berkas(PengajuanDosen $pengajuan_dosen, string $id_berkas)
    {
        if (sso()->user()->getActiveRole()->getOrgId() != $pengajuan_dosen->id_unit && sso()->user()->getActiveRole()->getName() != 'Super Administrator') { //akses halaman show yang bukan miliknya
            abort(403);
        }

        try {
            $response = FileStorage::getFileById($id_berkas);
            if(!property_exists($response, 'data')) {
                abort(404, 'Berkas tidak ditemukan');
            }
            
            $file = base64_decode($response->data);
            $file_mimetype = $response->info->file_mimetype;
            return response($file)->header('Content-Type', $file_mimetype);
        } catch(\Symfony\Component\HttpKernel\Exception\HttpException $e) {
            // Throw hasil abort 404
            throw $e;
        } catch(\Exception $e) {
            // Throw error di file storage
            abort(500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  PengajuanDosen $pengajuan_dosen
     * @return \Illuminate\Http\Response
     */
    public function edit(PengajuanDosen $pengajuan_dosen)
    {
        if (sso()->user()->getActiveRole()->getOrgId() != $pengajuan_dosen->id_unit && sso()->user()->getActiveRole()->getName() != 'Super Administrator') { //akses halaman show yang bukan miliknya
            abort(403);
        }

        if ($pengajuan_dosen->is_approved0 != NULL && $pengajuan_dosen->is_approved0 != '2' && $pengajuan_dosen->is_approved1 != '2') {
            abort(403);
        }

        $pendidikanRepository = new PendidikanRepository();
        $jenisBebanKerjaRepository = new JenisBebanKerjaDosenRepository();

        $data['jenis_pengajuan_dosen'] = DB::table('jenis_pengajuan_dosen')->get();
        $data['pengajuan_dosen'] = $pengajuan_dosen;
        $data['dosen'] = $pengajuan_dosen->dosen()->first();
        $data['nik'] = json_encode($data['dosen']->pluck('nik'));
        $data['mata_kuliah_bersama_wajib'] = MataKuliahBersamaWajib::all();
        $data['sks_ajar'] = round($data['pengajuan_dosen']->get_sks_pengajaran(), 1);
        $data['beban_kerja_dosen'] =  $pengajuan_dosen->beban_kerja_dosen()->get();
        $data['riwayat'] = $pengajuan_dosen->dosen()->first()->pengajuan_dosen()->get();
        $data['unit'] = DB::table('unit')->where('id_unit', '=', $pengajuan_dosen->id_unit)->first();
        $data['pendidikan'] = $pendidikanRepository->getAll();
        $data['jenis_beban_kerja'] = $jenisBebanKerjaRepository->getAll();

        $page['menu'] = "dosen";
        $page['submenu'] = "pengajuan dosen";
        $page['halaman'] = "ubah";
        return view('Pegawai::pengajuan_dosen.edit', compact('page', 'data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PengajuanDosen $pengajuan_dosen
     * @param  UpdatePengajuanDosenRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function update(PengajuanDosen $pengajuan_dosen, UpdatePengajuanDosenRequest $request)
    {
        if (sso()->user()->getActiveRole()->getOrgId() != $pengajuan_dosen->id_unit && sso()->user()->getActiveRole()->getName() != 'Super Administrator') { //akses halaman show yang bukan miliknya
            abort(403);
        }

        if ($pengajuan_dosen->is_approved0 != NULL && $pengajuan_dosen->is_approved0 != '2' && $pengajuan_dosen->is_approved1 != '2') {
            abort(403);
        }
        try {
            DB::beginTransaction();
            // update data pengajuan
            $pengajuan_dosen_attr = $request->safe()->only([
                                        'id_status_pengajuan_dosen', 'id_jenis_pengajuan_dosen',
                                        'tanggal_mulai_kerja', 'tanggal_akhir_kerja', 'tanggal_pengajuan',
                                        'nik'
                                    ]);
            $pengajuan_dosen->update($pengajuan_dosen_attr);

            // update data dosen
            $dosen_attr = $request->safe()->only([
                'gelar_depan', 'gelar_belakang', 'npp',
                'kepakaran', 'email', 'nomor_hp', 'nama',
                'nidn', 'tgl_lahir', 'jenis_kelamin',
                'id_pendidikan', 'alamat', 'tempat_lahir',
                'nik'
            ]);
            $dosen = $pengajuan_dosen->dosen()->first();
            $dosen->update($dosen_attr);

            // upload berkas
            $this->isi_berkas_dosen($request, $dosen);

            $pengajuan_dosen->id_updater = sso()->user()->getId();
            $pengajuan_dosen->save();
            $dosen->updater = sso()->user()->getId();
            $dosen->save();

            DB::commit();
            return redirect()->back()->with('success', "Data berhasil disimpan");
        } catch(Exception $e) {
            throw $e;
            return redirect()->back()->with('failed', "Data gagal disimpan");
        }

    }

    public function approve(PengajuanDosen $pengajuan_dosen, Request $request)
    {
        if (sso()->user()->getActiveRole()->getOrgId() != $pengajuan_dosen->id_unit && sso()->user()->getActiveRole()->getName() != 'Super Administrator') { //akses halaman show yang bukan miliknya
            abort(403);
        }

        $input = $request->input();
        if (sso()->user()->getActiveRole()->getName() == 'Super Administrator') {
            $int = 1;
        } else if (Session::get('is_validator')) {
            $int = 0;
        }

        $approval_status = [
            'setuju' => 1,
            'perbaikan' => 2,
            'tidak_setuju' => 0,
            'batalkan' => NULL,
        ];

        $data = [
            'is_approved' . $int => $approval_status[$input['status']],
            'id_approved' . $int  => ($input['status'] == 'batalkan' ? NULL : sso()->user()->getId()),
            'name_approved' . $int  => ($input['status'] == 'batalkan' ? NULL : sso()->user()->getName()),
            'date_approved' . $int  => ($input['status'] == 'batalkan' ? NULL : date('Y-m-d H:i:s')),
            'alasan' . $int => ($input['status'] == 'batalkan' || $input['status'] == 'setuju' ? $pengajuan_dosen['alasan' . $int] : $input['alasan'])
        ];

        $pengajuan_dosen->update($data);
        return redirect()->back()->with('success', "Status pengajuan berhasil diupdate.");
    }

    // public function draft($id_pengajuan)
    // {
    //     $pengajuanRepository = new PengajuanRepository();
    //     $detail = $pengajuanRepository->getById($id_pengajuan);
    //     //tanggal pengajuan
    //     $hariIni = \Carbon\Carbon::createFromDate($detail->tgl_pengajuan)->locale('id');
    //     $tgl_mulai = \Carbon\Carbon::createFromDate($detail->tgl_mulai_kerja)->locale('id');
    //     $tgl_akhir = \Carbon\Carbon::createFromDate($detail->tgl_akhir_kerja)->locale('id');

    //     //hitung usia
    //     $birthDate = new DateTime($detail->tgl_lahir);
    //     $today = new DateTime("today");

    //     $y = $today->diff($birthDate)->y;
    //     $data = [
    //         'no_surat' => $detail->no_surat,
    //         'nama' => $detail->nama,
    //         'nik' => $detail->nik,
    //         'unit' => $detail->unit,
    //         'honor' => number_format($detail->honor, 2),
    //         'uang_makan' => number_format($detail->uang_makan, 2),
    //         'terbilang_honor' => $this->terbilang($detail->honor),
    //         'terbilang_uang_makan' => $this->terbilang($detail->uang_makan),
    //         'tgl_mulai_kerja_d' => $tgl_mulai->format('j'),
    //         'tgl_mulai_kerja_m' => $tgl_mulai->monthName,
    //         'tgl_mulai_kerja_y' => $tgl_mulai->format('Y'),

    //         'tgl_akhir_kerja_d' => $tgl_akhir->format('j'),
    //         'tgl_akhir_kerja_m' => $tgl_akhir->monthName,
    //         'tgl_akhir_kerja_y' => $tgl_akhir->format('Y'),


    //         'tgl_akhir_kerja' => date('d-m-Y', strtotime($detail->tgl_akhir_kerja)),
    //         'alamat' => $detail->alamat,
    //         'hari' => $hariIni->dayName,
    //         'tanggal' => $hariIni->format('j'),
    //         'bulan' => $hariIni->monthName,
    //         'tahun' => $hariIni->format('Y'),
    //         'tahun_terbilang' => ucwords($this->terbilang($hariIni->format('Y'))),
    //         'hari' => $hariIni->dayName,
    //         'umur' => $y . " tahun ",
    //         'id_jenis_pengajuan' => $detail->id_jenis_pengajuan,
    //     ];
    //     $rincian_tugas =  DB::table('rincian_tugas')->where('id_pengajuan', $id_pengajuan)->get();

    //     $pdf = PDF::loadView('Pegawai::pengajuan.pdf', compact('data', 'rincian_tugas'));

    //     return $pdf->stream();
    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  PengajuanDosen $pengajuan_dosen
     * @return \Illuminate\Http\Response
     */
    public function destroy(PengajuanDosen $pengajuan_dosen)
    {
        $is_unit_sesuai = sso()->user()->getActiveRole()->getOrgId() != $pengajuan_dosen->id_unit;
        $is_super_admin = sso()->user()->getActiveRole()->getName() != 'Super Administrator';
        $is_admin_unit = sso()->user()->getActiveRole()->getName() != 'Administrator';
        $is_admin_unit_yang_sesuai = $is_unit_sesuai && $is_admin_unit;

        if (!$is_admin_unit_yang_sesuai && !$is_super_admin) {
            abort(403);
        }

        try {
            DB::beginTransaction();
            $dosen = Dosen::where('nik', $pengajuan_dosen->nik);
            $pengajuan_dosen->delete();
            $jml_pengajuan_terkait_dosen = $dosen->withCount('pengajuan_dosen')->first()->pengajuan_dosen_count;
            $is_masih_ada_pengajuan_oleh_dosen_ini = $jml_pengajuan_terkait_dosen > 0;
    
            if (!$is_masih_ada_pengajuan_oleh_dosen_ini) {
                $dosen->delete();
            }
            DB::commit();

            return back()->with('success', 'Berhasil menghapus pengajuan dosen');
        } catch(Exception $e) {
            throw $e;
            return back()->with('failed', 'Gagal menghapus pengajuan dosen');
        }
    }

    public function isi_no_surat(PengajuanDosen $pengajuan_dosen, Request $request) {
        if (sso()->user()->getActiveRole()->getOrgId() != $pengajuan_dosen->id_unit && sso()->user()->getActiveRole()->getName() != 'Super Administrator') { //akses halaman show yang bukan miliknya
            abort(403);
        }

        try {
            $data = $request->validate([
                'no_surat' => 'string|required',
                'tanggal_pengesahan' => 'required|date',
            ]);

            $pengajuan_dosen->update($data);

            return redirect()->back()->with('success', "Nomor surat berhasil diisi.");
        } catch (\Exception $e) {
            return redirect()->back()->with('failed', "Nomor surat gagal diisi.");
        }
    }
}
