<?php

namespace App\Modules\Pegawai\Requests;

use App\Models\BebanKerjaDosen;
use Illuminate\Foundation\Http\FormRequest;

class StorePengajuanDosenRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id_status_pengajuan_dosen = $this->input('id_status_pengajuan_dosen');
        $batas_sks = $this->input('batas_sks');

        // Baru
        if ($id_status_pengajuan_dosen === '0') { 
            return [
                // Jenis pengajuan dosen
                'id_status_pengajuan_dosen' => 'required',
                'id_jenis_pengajuan_dosen' => 'string|required',
                'id_unit' => 'required',
                'tanggal_mulai_kerja' => 'required|date',
                'tanggal_akhir_kerja' => 'required|date',
                'tanggal_pengajuan' => 'required|date',
                'total_sks' => 'numeric|max:' . $batas_sks,
                'beban_kerja' => 'numeric|min:1',
                // Data dosen
                'gelar_depan' => 'string|nullable',
                'nama' => 'required|string',
                'gelar_belakang' => 'string|nullable',
                'nik' => 'required|digits:16',
                'nidn' => 'nullable|digits:10',
                'kepakaran' => 'required|string',
                'id_pendidikan' => 'required|string',
                'tempat_lahir' => 'required|string',
                'tgl_lahir' => 'required|date',
                'jenis_kelamin' => 'required|string',
                'alamat' => 'required|string',
                'email' => 'string|required',
                'nomor_hp' => 'numeric|nullable|max:999999999999999',
                // Berkas
                'pas_foto' => 'required|file|mimes:jpg,jpeg,pdf',
                'ktp' => 'required|file|mimes:jpg,jpeg,pdf',
                'riwayat_hidup' => 'file|mimes:jpg,jpeg,pdf',
                'ijazah_s2' => 'required|file|mimes:jpg,jpeg,pdf',
                'transkrip_s2' => 'required|file|mimes:jpg,jpeg,pdf',
                'penyetaraan_s2' => 'file|mimes:jpg,jpeg,pdf',
                'ijazah_s3' => 'file|mimes:jpg,jpeg,pdf',
                'transkrip_s3' => 'file|mimes:jpg,jpeg,pdf',
                'penyetaraan_s3' => 'file|mimes:jpg,jpeg,pdf',
                'analisis_bkd' => 'required|file|mimes:jpg,jpeg,pdf',
                'surat_izin_institusi_asal' => 'required|file|mimes:jpg,jpeg,pdf',
                'ijazah_s1' => 'required|file|mimes:jpg,jpeg,pdf',
                'transkrip_s1' => 'required|file|mimes:jpg,jpeg,pdf',
                'penyetaraan_s1' => 'file|mimes:jpg,jpeg,pdf',
                'sehat_rohani' => 'file|mimes:jpg,jpeg,pdf',
                'sehat_jasmani' => 'file|mimes:jpg,jpeg,pdf',
                'bebas_narkotika' => 'file|mimes:jpg,jpeg,pdf',
                'pernyataan_dekan' => 'file|mimes:jpg,jpeg,pdf',
                'minimal_mengajar' => 'file|mimes:jpg,jpeg,pdf',
            ];
        }

        // Perpanjangan
        return [
            // Jenis pengajuan dosen
            'id_status_pengajuan_dosen' => 'required',
            'id_jenis_pengajuan_dosen' => 'string|required',
            'id_unit' => 'required',
            'tanggal_mulai_kerja' => 'required|date',
            'tanggal_akhir_kerja' => 'required|date',
            'tanggal_pengajuan' => 'required|date',
            'total_sks' => 'numeric|max:' . $batas_sks,
            'beban_kerja' => 'numeric|min:1',
            // Data dosen
            'nik' => 'required|digits:16',
            'npp' => 'min:13|max:16|nullable',
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $id_mkb_wajib = $this->input('id_mkb_wajib');
        $id_jenis_beban_kerja = $this->input('id_jenis_beban_kerja');
        $jumlah = $this->input('jumlah');
        if($this->input('id_jenis_beban_kerja'))
            $count_beban_kerja = count($this->input('id_jenis_beban_kerja'));

        $count_mkb_wajib = $id_mkb_wajib ? count($id_mkb_wajib) : 0;
        $total_sks = 0;
        for ($i = 0; $i < $count_mkb_wajib; $i++) {
            $is_pengajaran = BebanKerjaDosen::is_pengajaran($id_jenis_beban_kerja[$i]);
            if(!$is_pengajaran) continue;

            $total_sks += $jumlah[$i];
        }

        $this->merge([
            'total_sks' => $total_sks,
            'batas_sks' => $this->input('id_jenis_pengajuan_dosen') == 'mkbwajib' ? 10 : 6,
            'beban_kerja' => $count_beban_kerja,
        ]);
    }
}