<?php

namespace App\Modules\Pegawai\Requests;

use App\Models\BebanKerjaDosen;
use Illuminate\Foundation\Http\FormRequest;

class UbahBebanKerjaPengajuanDosenRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return sso()->user()->getActiveRole()->getName() == 'Administrator' || sso()->user()->getActiveRole()->getName() == 'Super Administrator';
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $batas_pengubahan = $this->pengajuan_dosen->is_mengajar_mkb_wajib() ? 10 : 6;
        $batas_pengubahan -= $this->pengajuan_dosen->get_sks_pengajaran();
        $batas_pengubahan += BebanKerjaDosen::find($this->input('id'))->jumlah;

        $is_pengajaran = BebanKerjaDosen::is_pengajaran($this->input('id_jenis_beban_kerja_dosen'));

        return [
            'id' => 'string|required|exists:beban_kerja_dosen,id_beban_kerja_dosen',
            'id_mkb_wajib' => 'string|nullable',
            'id_jenis_beban_kerja_dosen' => 'string|required',
            'deskripsi' => 'string|required',
            'kelas' => 'string|max:50|nullable',
            'jumlah' => $is_pengajaran ? 'numeric|required|max:' . $batas_pengubahan : 'numeric|required',
            'upah' => 'numeric|required',
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        // 
    }
}