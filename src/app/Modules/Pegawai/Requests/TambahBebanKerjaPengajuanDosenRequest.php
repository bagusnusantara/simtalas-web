<?php

namespace App\Modules\Pegawai\Requests;

use App\Models\BebanKerjaDosen;
use Illuminate\Foundation\Http\FormRequest;

class TambahBebanKerjaPengajuanDosenRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $batas_penambahan = $this->pengajuan_dosen->is_mengajar_mkb_wajib() ? 10 : 6;
        $batas_penambahan -= $this->pengajuan_dosen->get_sks_pengajaran();
        $is_pengajaran = BebanKerjaDosen::is_pengajaran($this->input('id_jenis_beban_kerja_dosen'));

        return [
            'id_jenis_beban_kerja_dosen' => 'string|required',
            'id_mkb_wajib' => 'string|nullable',
            'deskripsi' => 'string|required',
            'kelas' => 'string|max:50|nullable',
            'jumlah' => $is_pengajaran ? 'numeric|required|max:' . $batas_penambahan : 'numeric|required',
            'upah' => 'numeric|required',
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        // 
    }
}