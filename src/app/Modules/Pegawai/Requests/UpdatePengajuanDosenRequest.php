<?php

namespace App\Modules\Pegawai\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdatePengajuanDosenRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return sso()->user()->getActiveRole()->getName() == 'Administrator' || sso()->user()->getActiveRole()->getName() == 'Super Administrator';
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // Jenis pengajuan dosen
            'id_status_pengajuan_dosen' => 'required',
            'id_jenis_pengajuan_dosen' => 'string|required',
            'tanggal_mulai_kerja' => 'required|date',
            'tanggal_akhir_kerja' => 'required|date',
            'tanggal_pengajuan' => 'required|date',
            // Data dosen
            'gelar_depan' => 'string|nullable',
            'nama' => 'required|string',
            'gelar_belakang' => 'string|nullable',
            'npp' => 'nullable|digits:16',
            'nidn' => 'nullable|digits:10',
            'kepakaran' => 'required|string',
            'id_pendidikan' => 'required|string',
            'tempat_lahir' => 'required|string',
            'tgl_lahir' => 'required|date',
            'jenis_kelamin' => 'required|string',
            'alamat' => 'required|string',
            'email' => 'string|required',
            'nomor_hp' => 'numeric|nullable|max:999999999999999',
            // Berkas
            'pas_foto' => 'file|mimes:jpg,jpeg,pdf',
            'ktp' => 'file|mimes:jpg,jpeg,pdf',
            'riwayat_hidup' => 'file|mimes:jpg,jpeg,pdf',
            'ijazah_s2' => 'file|mimes:jpg,jpeg,pdf',
            'transkrip_s2' => 'file|mimes:jpg,jpeg,pdf',
            'penyetaraan_s2' => 'file|mimes:jpg,jpeg,pdf',
            'ijazah_s3' => 'file|mimes:jpg,jpeg,pdf',
            'transkrip_s3' => 'file|mimes:jpg,jpeg,pdf',
            'penyetaraan_s3' => 'file|mimes:jpg,jpeg,pdf',
            'analisis_bkd' => 'file|mimes:jpg,jpeg,pdf',
            'surat_izin_institusi_asal' => 'file|mimes:jpg,jpeg,pdf',
            'ijazah_s1' => 'file|mimes:jpg,jpeg,pdf',
            'transkrip_s1' => 'file|mimes:jpg,jpeg,pdf',
            'penyetaraan_s1' => 'file|mimes:jpg,jpeg,pdf',
            'sehat_rohani' => 'file|mimes:jpg,jpeg,pdf',
            'sehat_jasmani' => 'file|mimes:jpg,jpeg,pdf',
            'bebas_narkotika' => 'file|mimes:jpg,jpeg,pdf',
            'pernyataan_dekan' => 'file|mimes:jpg,jpeg,pdf',
            'minimal_mengajar' => 'file|mimes:jpg,jpeg,pdf',
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        // 
    }
}