<?php

namespace App\Modules\Pegawai;

use App\Modules\Ui\Contracts\BladeMenu;
use Dptsi\Modular\Base\BaseModule;

class Module extends BaseModule implements BladeMenu
{
    public function getProviders(): array
    {
        return [
            \App\Modules\Pegawai\Providers\RouteServiceProvider::class,
            \App\Modules\Pegawai\Providers\DatabaseServiceProvider::class,
            \App\Modules\Pegawai\Providers\ViewServiceProvider::class,
            \App\Modules\Pegawai\Providers\LangServiceProvider::class,
            \App\Modules\Pegawai\Providers\BladeComponentServiceProvider::class,
            \App\Modules\Pegawai\Providers\DependencyServiceProvider::class,
            \App\Modules\Pegawai\Providers\EventServiceProvider::class,
            \App\Modules\Pegawai\Providers\MessagingServiceProvider::class,
        ];
    }
    
    public function renderMenu(): string
    {
        if (session('is_viewer') == true) {
            return view("Pegawai::menu-viewer")->render();
        } else {
            return view("Pegawai::menu")->render();
        }
    }
}