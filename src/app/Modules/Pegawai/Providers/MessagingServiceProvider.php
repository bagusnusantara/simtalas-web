<?php

namespace App\Modules\Pegawai\Providers;

use Dptsi\Modular\Facade\Messaging;
use Illuminate\Support\ServiceProvider;

class MessagingServiceProvider extends ServiceProvider
{
    protected string $module_name = 'pegawai';

    public function register()
    {
    }

    public function boot()
    {
        Messaging::setChannel('pegawai');
//        Messaging::listenTo();
    }
}