<?php

namespace App\Modules\Pegawai\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class BladeComponentServiceProvider extends ServiceProvider
{
    protected string $module_name = 'Pegawai';

    public function register()
    {
        Blade::componentNamespace($this->getNamespace(), $this->module_name);
    }

    public function getNamespace(): string
    {
        return "App\\Modules\\Pegawai\\Components";
    }
}