<?php

namespace App\Modules\Pegawai\Interfaces;


interface DosenInterface
{
    public function getAll();
    public function getByNik($nik);
    public function getArrayWhere($arrayWhere);
}
