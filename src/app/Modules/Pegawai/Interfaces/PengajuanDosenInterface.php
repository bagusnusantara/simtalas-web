<?php

namespace App\Modules\Pegawai\Interfaces;


interface PengajuanDosenInterface
{
    public function getAll();
    public function getArrayWhere($arrayWhere);
}
