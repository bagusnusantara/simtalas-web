<?php

namespace App\Modules\Pegawai\Interfaces;


interface PegawaiInterface
{
    public function getAll();
    public function getById($id);
    public function getArrayWhere($arrayWhere);
}
