<?php

namespace App\Modules\Pegawai\Interfaces;


interface PengajuanInterface
{
    public function getAll();
    public function getArrayWhere($arrayWhere);
    // public function getById($id);
    // public function getArrayWhere($arrayWhere);
}
