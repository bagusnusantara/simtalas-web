<?php

use Illuminate\Support\Facades\Route;
use App\Modules\Pegawai\Controllers\BaseController;
use App\Modules\Pegawai\Controllers\BebanKerjaDosenController;
use App\Modules\Pegawai\Controllers\PengajuanController;
use App\Modules\Pegawai\Controllers\PegawaiController;
use App\Modules\Pegawai\Controllers\NomorSuratController;
use App\Modules\Pegawai\Controllers\PengajuanDosenController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PegawaiController::class, 'index'] );
Route::get('/server-side', [PegawaiController::class, 'server_side'] );
Route::get('/show/{id}', [PegawaiController::class, 'show'] );
Route::get('/show_pdf/{id}', [PengajuanController::class, 'show_pdf'] );
Route::put('/nonaktifkan/{pegawai}', [PegawaiController::class, 'nonaktifkan'] )->name('pegawai.menonaktifkan');

Route::prefix('pengajuan')->name('pengajuan.')->group(function () {
    Route::get('/', [PengajuanController::class, 'index'] );
    Route::get('/server-side', [PengajuanController::class, 'server_side'] );
    Route::get('/show/{id_pengajuan}', [PengajuanController::class, 'show'] );
    Route::get('/create', [PengajuanController::class, 'create'] );
    Route::post('/store', [PengajuanController::class, 'store'] );
    Route::post('/update', [PengajuanController::class, 'update'] );
    Route::get('/edit/{id_pengajuan}', [PengajuanController::class, 'edit'] );
    Route::post('/approve', [PengajuanController::class, 'approve'] );
    Route::post('/upload', [PengajuanController::class, 'upload'] );
    Route::post('/hapus-berkas', [PengajuanController::class, 'hapus_berkas'] );

    Route::post('/isi-nomor-surat', [PengajuanController::class, 'isi_nomor_surat'] );
    Route::get('/draft/{id_pengajuan}', [PengajuanController::class, 'draft'] );
    

    //rincian tugas
    Route::post('/tambah-tugas', [PengajuanController::class, 'tambah_tugas'] );
    Route::post('/edit-tugas', [PengajuanController::class, 'edit_tugas'] );
    Route::post('/hapus-tugas', [PengajuanController::class, 'hapus_tugas'] );
    //tanggungan keluarga
    Route::post('/tambah-keluarga', [PengajuanController::class, 'tambah_keluarga'] );
    Route::post('/edit-keluarga', [PengajuanController::class, 'edit_keluarga'] );
    Route::post('/hapus-keluarga', [PengajuanController::class, 'hapus_keluarga'] );
});

Route::prefix('pengajuan-dosen')->group(function () {
    Route::get('server-side', [PengajuanDosenController::class, 'server_side'])->name('pengajuan-dosen.server-side');
    Route::get('{pengajuan_dosen}/berkas/{id_berkas}', [PengajuanDosenController::class, 'berkas'])->name('berkas-pengajuan-dosen');
    Route::post('{pengajuan_dosen}/approve', [PengajuanDosenController::class, 'approve'] )->name('approve-pengajuan-dosen');
    Route::post('{pengajuan_dosen}/beban-kerja', [BebanKerjaDosenController::class, 'store'] )->name('tambah-beban-kerja-pengajuan-dosen');
    Route::delete('{pengajuan_dosen}/beban-kerja', [BebanKerjaDosenController::class, 'destroy'] )->name('hapus-beban-kerja-pengajuan-dosen');
    Route::put('{pengajuan_dosen}/beban-kerja', [BebanKerjaDosenController::class, 'update'] )->name('ubah-beban-kerja-pengajuan-dosen');
    Route::put('{pengajuan_dosen}/isi-nomor-surat', [PengajuanDosenController::class, 'isi_no_surat'] )->name('isi-nomor-surat-pengajuan-dosen');
    Route::get('/draft/{id_pengajuan}', [PengajuanController::class, 'dosen'] );
});
Route::resource('pengajuan-dosen', PengajuanDosenController::class);

Route::get('unduh-berkas-pendukung/{id_pengajuan}/{kategori}', [PengajuanController::class, 'unduh_berkas_pendukung'])->name('unduh-berkas-pendukung');
Route::get('preview-berkas-pendukung/{id_berkas}', [PengajuanController::class, 'preview_berkas_pendukung'])->name('preview-berkas-pendukung');

Route::prefix('nomor-surat')->name('nomor-surat.')->group(function () {
    Route::get('/', [NomorSuratController::class, 'index'] );
    Route::get('/server-side', [NomorSuratController::class, 'server_side'])->name('nomor-surat.server-side');
    Route::get('/create', [NomorSuratController::class, 'create'] );
    Route::post('/store', [NomorSuratController::class, 'store'] );
    Route::put('/update', [NomorSuratController::class, 'update'] );
    Route::delete('/delete', [NomorSuratController::class, 'delete'] );
});