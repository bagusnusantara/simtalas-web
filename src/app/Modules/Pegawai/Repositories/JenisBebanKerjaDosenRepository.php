<?php

namespace App\Modules\Pegawai\Repositories;

use App\Modules\Pegawai\Interfaces\JenisBebanKerjaDosenInterface;
use Illuminate\Support\Facades\DB;

class JenisBebanKerjaDosenRepository implements JenisBebanKerjaDosenInterface
{

    public function getAll()
    {
        $data = DB::table('jenis_beban_kerja_dosen')->get();

        return $data;
    }
}
