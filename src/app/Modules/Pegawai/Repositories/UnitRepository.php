<?php

namespace App\Modules\Pegawai\Repositories;

use Illuminate\Support\Facades\DB;
use App\Modules\Pegawai\Interfaces\UnitInterface;

class UnitRepository implements UnitInterface
{

    public function getAll()
    {
        if (sso()->user()->getActiveRole()->getName() == 'Super Administrator') {
            $data = DB::table('unit')->orderBy('nama')->get();
        }
        else{
            $id_unit = sso()->user()->getActiveRole()->getOrgId();
            $data = DB::table('unit')->orderBy('nama')->where('id_unit', $id_unit)->get();
        }
        return $data;
    }
}
