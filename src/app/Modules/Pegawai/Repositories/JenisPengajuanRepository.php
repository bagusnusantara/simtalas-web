<?php

namespace App\Modules\Pegawai\Repositories;

use Illuminate\Support\Facades\DB;
use App\Modules\Pegawai\Interfaces\JenisPengajuanInterface;

class JenisPengajuanRepository implements JenisPengajuanInterface
{

    public function getAll()
    {
        $data = DB::table('jenis_pengajuan')->get();

        return $data;
    }
}
