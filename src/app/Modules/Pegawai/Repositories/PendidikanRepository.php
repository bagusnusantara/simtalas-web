<?php

namespace App\Modules\Pegawai\Repositories;

use Illuminate\Support\Facades\DB;
use App\Modules\Pegawai\Interfaces\PendidikanInterface;

class PendidikanRepository implements PendidikanInterface
{

    public function getAll()
    {
        $data = DB::table('pendidikan')->get();

        return $data;
    }

    public function getForDosenLB()
    {
    	$data = DB::table('pendidikan')->whereIn('id_pendidikan', ['s2','s3'])->get();

        return $data;
    }
}
