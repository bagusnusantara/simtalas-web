<?php

namespace App\Modules\Pegawai\Repositories;

use App\Modules\Pegawai\Interfaces\PegawaiInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class PegawaiRepository implements PegawaiInterface
{

    public function getAll()
    {
        $data = DB::table('pegawai')->get();

        return $data;
    }

    public function getArrayWhere($request, $server_side = false)
    {
        $where = [];
        // if ($request->id_unit != null) {
        //     $where['pengajuan.id_unit'] = $request->id_unit;
        // }
        // if ($request->id_status != null) {
        //     $where['pengajuan.id_jenis_pengajuan'] = $request->id_status;
        // }

        if ($server_side) {
            if (count($where) > 0) {
                $data = DB::table('pegawai')
                    ->join('pengajuan', 'pengajuan.nik', 'pegawai.nik')
                    ->join('unit', 'pengajuan.id_unit', 'unit.id_unit')
                    ->join('jenis_pengajuan', 'pengajuan.id_jenis_pengajuan', 'pengajuan.id_jenis_pengajuan')
                    ->select('pegawai.nik', 'pengajuan.id_status', 'id_pegawai', 'pegawai.nama', 'jenis_kelamin', 'unit.nama as unit', 'jenis_pengajuan.nama as jenis_pengajuan', 'pegawai.tgl_nonaktif','pengajuan.tgl_akhir_kerja')
                    ->where($where)
                    ->where('pengajuan.is_approved1','1')
                    ->get();
            } else {
                $data = DB::table('pegawai')
                    ->join('pengajuan', 'pengajuan.nik', 'pegawai.nik')
                    ->join('unit', 'pengajuan.id_unit', 'unit.id_unit')
                    ->join('jenis_pengajuan', 'pengajuan.id_jenis_pengajuan', 'jenis_pengajuan.id_jenis_pengajuan')
                    ->where('pengajuan.is_approved1','1')
                    ->select('pegawai.nik', 'id_pegawai', 'pegawai.nama', 'jenis_kelamin', 'unit.nama as unit', 'jenis_pengajuan.nama as jenis_pengajuan', 'pegawai.tgl_nonaktif','pengajuan.tgl_akhir_kerja')->get();
            }

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('detail', function ($data) {
                    $detail = '<a href="/pegawai/show/' . $data->id_pegawai . '" class="btn btn-xs btn-info"><i class="fa fa-list"></i> Lihat Detail</a>';
                    return $detail;
                })
                ->addColumn('status', function ($data) {
                    if (is_null($data->tgl_nonaktif)) {
                        $status = '<span class="small">'.$data->jenis_pengajuan . '</span><br><span class="badge badge-primary">Aktif</span>';
                    } else {
                        $status = '<span class="small">'.$data->jenis_pengajuan . '</span><br><span class="badge badge-warning">Pegawai tidak aktif</span>';
                    }
                    if(Carbon::now() > $data->tgl_akhir_kerja){
                        $status = '<small>' . $data->jenis_pengajuan . '</small><br><span class="badge badge-warning"> Pegawai tidak aktif</span>';
                    }
                    return $status;
                })
                ->rawColumns(['detail', 'status'])
                ->make(true);
        } else {
            $data = DB::table('pegawai')->get();
            return $data;
        }
    }
    public function getById($id)
    {
        $data = DB::table('pegawai')->where('id_pegawai',$id)->first();

        return $data;
    }

    public function getOnlyAvailable() //hanya yang tidak sedang aktif pengajuannya
    {
        $pegawai = DB::table('pegawai')
                ->select('pegawai.id_pegawai', 'pegawai.nik', 'nama')
                ->distinct()
                ->join('pengajuan', 'pengajuan.nik', 'pegawai.nik')
                ->where('pengajuan.tgl_akhir_kerja', '<',  Carbon::now())
                ->orWhereNotNull('pengajuan.tgl_nonaktif')
                ->get();
        
        return $pegawai;
    }
}
