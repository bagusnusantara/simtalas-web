<?php

namespace App\Modules\Pegawai\Repositories;

use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use App\Modules\Pegawai\Interfaces\NomorSuratInterface;

class NomorSuratRepository implements NomorSuratInterface
{

    public function getAll()
    {
        $data = DB::table('nomor_surat')->get();
        return $data;
    }

    public function getDatatable()
    {

        $data = DB::table('nomor_surat')->get();
        return Datatables::of($data)->addIndexColumn()
            ->addColumn('edit', function ($data) {
                $ubah = '<a href="/pegawai/nomor-surat/ubah/' . $data->id . '" class="btn btn-xs btn-warning"><i class="fa fa-edit"></i> Ubah</a>';
                return $ubah;
            })
            ->addColumn('delete', function ($data) {
                $ubah = '<a href="/pegawai/nomor-surat/hapus/' . $data->id . '" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i> Hapus</a>';
                return $ubah;
            })
            ->addColumn('status', function ($data) {
                if ($data->is_used == '0') {
                    return '<span class="badge badge-primary">Belum terpakai</span>';
                } else {
                    return  '<span class="badge badge-danger">Sudah terpakai</span>';
                }
            })
            ->rawColumns(['edit', 'delete', 'status'])
            ->make(true);
    }
}
