<?php

namespace App\Modules\Pegawai\Repositories;

use Illuminate\Support\Facades\DB;
use App\Modules\Pegawai\Interfaces\PengajuanInterface;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Carbon;

class PengajuanRepository implements PengajuanInterface
{

    public function getAll()
    {
        $data = DB::table('unit')->get();

        return $data;
    }
    // public function getArrayWhere($request, $server_side = false)
    // {
    //     $where = [];
    //     if ($request->id_unit != null) {
    //         $where['pengajuan.id_unit'] = $request->id_unit;
    //     }
    //     if ($request->id_status != null) {
    //         if($request->id_status == '0'){

    //             $where['pengajuan.is_approved0'] = NULL;
    //             if(sso()->user()->getActiveRole()->getName() != 'Kepala Kantor'){
    //                 $where['pengajuan.is_approved1'] = NULL;
    //             }

    //         }
    //         else if($request->id_status == '1'){
    //             $where['pengajuan.is_approved0'] = '1';
    //             $where['pengajuan.is_approved1'] = '1';
    //         }
    //         else if($request->id_status == '2'){
    //             $where['pengajuan.is_approved0'] = '2';
    //         }

    //     }

    //     if ($server_side) {
    //         if (count($where) > 0) {
    //             $data = DB::table('pengajuan')
    //                 ->join('pegawai', 'pengajuan.nik', 'pegawai.nik')
    //                 ->join('unit', 'pengajuan.id_unit', 'unit.id_unit')
    //                 ->join('jenis_pengajuan', 'pengajuan.id_jenis_pengajuan', 'jenis_pengajuan.id_jenis_pengajuan')
    //                 ->select('pegawai.nik', 'pengajuan.no_surat', 'pengajuan.is_approved0', 'pengajuan.is_approved1', 'id_pegawai', 'pegawai.nama', 'jenis_kelamin', 'unit.nama as unit', 'jenis_pengajuan.nama as jenis_pengajuan', 'pengajuan.id_pengajuan')
    //                 ->where($where)
    //                 ->get();
    //         } else {
    //             $query = 'SELECT pengajuan.id_pengajuan,pegawai.nik, pengajuan.no_surat,pengajuan.is_approved0,pengajuan.is_approved1, pegawai.nama, pegawai.jenis_kelamin, unit.nama as unit, jenis_pengajuan.nama as jenis_pengajuan
    //             FROM pengajuan
    //             JOIN pegawai ON pegawai.nik=pengajuan.nik
    //             JOIN unit ON pengajuan.id_unit=unit.id_unit
    //             JOIN jenis_pengajuan ON pengajuan.id_jenis_pengajuan=jenis_pengajuan.id_jenis_pengajuan';
    //             $data = DB::select($query);
    //         }

    //         return DataTables::of($data)
    //             ->addIndexColumn()
    //             ->addColumn('aksi', function ($data) {
    //                 $detail = '<a href="/pegawai/pengajuan/show/' . $data->id_pengajuan . '" class="btn btn-xs btn-white tx-montserrat tx-semibold"><i class="fa fa-list"></i> Detail</a>';
    //                 return $detail;
    //             })
    //             ->addColumn('nomor_surat', function ($data) {
    //                 if ($data->no_surat == NULL) {
    //                     return "-";
    //                 }
    //                 return $data->no_surat;
    //             })
    //             ->addColumn('status', function ($data) {
    //                 if ($data->is_approved0 == NULL) {
    //                     $status = '<small>' . $data->jenis_pengajuan . '</small><br><span class="badge badge-secondary"> Menunggu Approval Unit</span>';
    //                 } else if ($data->is_approved1 == NULL && $data->is_approved0 != NULL) {
    //                     $status = '<small>' . $data->jenis_pengajuan . '</small><br><span class="badge badge-primary"> Menunggu Approval Pimpinan SDMO</span>';
    //                 } else {
    //                     $status = '<small>' . $data->jenis_pengajuan . '</small><br><span class="badge badge-success"> Disetujui</span>';
    //                 }
    //                 return $status;
    //             })
    //             ->rawColumns(['aksi', 'status', 'draft'])
    //             ->make(true);
    //     } else {
    //         $data = DB::table('pegawai')->get();
    //         return $data;
    //     }
    // }

    public function getArrayWhere($request, $server_side = false)
    {
        $where = [];
        $whereUnit = []; //filter unit
        $whereKU = []; //kondisi untuk kepala unit 
        $whereDSDMO = []; // kondisi untuk admin DSDMO
        $id_unit = sso()->user()->getActiveRole()->getOrgId();
        if (sso()->user()->getActiveRole()->getName() != 'Super Administrator' && session('is_viewer') != true) {
            $where['pengajuan.id_unit'] = $id_unit;
        }
        if ($request->id_unit != null) {
            $where['pengajuan.id_unit'] = $request->id_unit;
        }

        // if ($request->id_status != null) {
        //     if ($request->id_status == '0') {

        //         $whereKU['pengajuan.is_approved0'] = NULL;
        //         if (sso()->user()->getActiveRole()->getName() != 'Kepala Kantor') {
        //             $whereDSDMO['pengajuan.is_approved1'] = NULL;
        //         }
        //     } else if ($request->id_status == '1') {//diterima
        //         $whereKU['pengajuan.is_approved0'] = 1;
        //         $whereDSDMO['pengajuan.is_approved1'] = 1;
        //     } else if ($request->id_status == '2') { //revisi
        //         $whereKU['pengajuan.is_approved0'] = 2; 
        //         $whereDSDMO['pengajuan.is_approved1'] = 2;
        //     } else if ($request->id_status == '3') {//ditolak
        //         $whereKU['pengajuan.is_approved0'] = 0;
        //         $whereDSDMO['pengajuan.is_approved1'] = 0;
        //         // $where['pengajuan.is_approved1'] = '0';
        //     }
        // }

        // diterima 1, revisi 2, ditolak 0, menunggu null
        if ($request->id_status != null) {
            if ($request->id_status == '1') { //Menunggu Persetujuan Pimpinan Unit

                $where['pengajuan.is_approved0'] = NULL;
                $where['pengajuan.is_approved1'] = NULL;

            } elseif ($request->id_status == '2') {//Menunggu Persetujuan DSDMO(Disetujui Pimpinan Unit)

                $where['pengajuan.is_approved0'] = 1;
                $where['pengajuan.is_approved1'] = NULL;

            } elseif ($request->id_status == '3') {//Disetujui DSDMO

                // $where['pengajuan.is_approved0'] = 1;
                $where['pengajuan.is_approved1'] = 1;

            } elseif ($request->id_status == '4') {//Revisi Pimpinan Unit

                $where['pengajuan.is_approved0'] = 2;
                $where['pengajuan.is_approved1'] = NULL;

            } elseif ($request->id_status == '5') {//Revisi DSDMO

                // $where['pengajuan.is_approved0'] = 1;
                $where['pengajuan.is_approved1'] = 2;

            } elseif ($request->id_status == '6') {//Ditolak Pimpinan Unit

                $where['pengajuan.is_approved0'] = 0;
                $where['pengajuan.is_approved1'] = NULL;

            } elseif ($request->id_status == '7') {//Ditolak DSDMO

                // $where['pengajuan.is_approved0'] = 1;
                $where['pengajuan.is_approved1'] = 0;

            }
        }

        // <option value="0">Menunggu Persetujuan Pimpinan Unit</option>
        // <option value="1">Menunggu Persetujuan DSDMO</option>
        // <option value="2">Disetujui Pimpinan Unit</option>
        // <option value="3">Disetujui DSDMO</option>
        // <option value="4">Revisi Pimpinan Unit</option>
        // <option value="5">Revisi DSDMO</option>
        // <option value="6">Ditolak Pimpinan Unit</option>
        // <option value="7">Ditolak DSDMO</option>

        if ($server_side) {
            if (count($where) > 0) {
                $data = DB::table('pengajuan')
                    ->leftJoin('pegawai', 'pengajuan.nik', 'pegawai.nik')
                    ->leftJoin('unit', 'pengajuan.id_unit', 'unit.id_unit')
                    ->leftJoin('jenis_pengajuan', 'pengajuan.id_jenis_pengajuan', 'jenis_pengajuan.id_jenis_pengajuan')
                    ->select('pegawai.nik', 'pengajuan.no_surat', 'pengajuan.is_approved0', 'pengajuan.is_approved1', 'id_pegawai', 'pegawai.nama', 'jenis_kelamin', 'unit.nama as unit', 'jenis_pengajuan.nama as jenis_pengajuan', 'pengajuan.id_pengajuan', 'pengajuan.berkas','pengajuan.date_approved0','pengajuan.date_approved1', 'pengajuan.updated_at', 'pegawai.tgl_nonaktif','pengajuan.tgl_akhir_kerja')
                    ->where($where)
                    ->get();
            } else {
                $data = DB::table('pengajuan')
                    ->join('pegawai', 'pengajuan.nik', 'pegawai.nik')
                    ->join('unit', 'pengajuan.id_unit', 'unit.id_unit')
                    ->join('jenis_pengajuan', 'pengajuan.id_jenis_pengajuan', 'jenis_pengajuan.id_jenis_pengajuan')
                    ->select('pegawai.nik', 'pengajuan.no_surat', 'pengajuan.is_approved0', 'pengajuan.is_approved1', 'id_pegawai', 'pegawai.nama', 'jenis_kelamin', 'unit.nama as unit', 'jenis_pengajuan.nama as jenis_pengajuan', 'pengajuan.id_pengajuan', 'pengajuan.berkas','pengajuan.date_approved0','pengajuan.date_approved1', 'pengajuan.updated_at', 'pegawai.tgl_nonaktif','pengajuan.tgl_akhir_kerja')
                    ->get();
            }

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('aksi', function ($data) {
                    $detail = '<a href="/pegawai/pengajuan/show/' . $data->id_pengajuan . '" class="btn btn-xs btn-white btn-block"><i class="fa fa-list"></i> Detail</a>';
                    
                    if (is_null($data->tgl_nonaktif)) {
                        if (sso()->user()->getActiveRole()->getName() == 'Administrator') {
                            if ($data->is_approved1 == '1') {
                                $detail = $detail . '<a href="/pegawai/pengajuan/draft/' . $data->id_pengajuan . '" class="btn btn-xs btn-its btn-block"><i class="fa fa-download"></i> Unduh Draft</a>';
                            }
                            if ($data->is_approved0 == '2' || $data->is_approved1 == '2') {
                                $detail = $detail . '<a href="/pegawai/pengajuan/edit/' . $data->id_pengajuan . '" class="btn btn-xs btn-warning btn-block"><i class="fa fa-edit"></i> Ubah</a>';
                            }
                            if ($data->berkas != NULL) {
                                $detail = $detail . '<a href="https://storage-api-dev.its.ac.id/' . $data->berkas . '" class="btn btn-xs btn-its btn-block"><i class="fa fa-download"></i> Unduh Berkas</a>';
                            }
                        }
                        if (sso()->user()->getActiveRole()->getName() == 'Super Administrator') {
                            if ($data->is_approved1 == '1') {
                                $detail = $detail . '<a href="/pegawai/pengajuan/draft/' . $data->id_pengajuan . '" class="btn btn-xs btn-its btn-block"><i class="fa fa-download"></i> Unduh Draft</a>';
                            }
                            if ($data->is_approved0 == '2' || $data->is_approved1 == '2') {
                                $detail = $detail . '<a href="/pegawai/pengajuan/edit/' . $data->id_pengajuan . '" class="btn btn-xs btn-warning btn-block"><i class="fa fa-edit"></i> Ubah</a>';
                            }
                            if ($data->berkas != NULL) {
                                $detail = $detail . '<a href="https://storage-api-dev.its.ac.id/' . $data->berkas . '" class="btn btn-xs btn-its btn-block"><i class="fa fa-download"></i> Unduh Berkas</a>';
                                $detail = $detail . '<button type="button" data-id="' . $data->id_pengajuan . '" data-toggle="modal" data-target="#hapus" class="btn btn-xs btn-danger btn-block"><i class="fa fa-trash"></i> Hapus Berkas</a>';
                            } else {
                                if ($data->is_approved1 == '1') {
                                $detail = $detail . '<button type="button" data-id="' . $data->id_pengajuan . '" data-toggle="modal" data-target="#upload" class="btn btn-xs btn-secondary btn-block"><i class="fa fa-upload"></i> Unggah Berkas</a>';
                                }
                            }
                        }
                    }

                    return $detail;
                })
                ->addColumn('nomor_surat', function ($data) {
                    if ($data->no_surat == NULL) {
                        return "-";
                    }
                    return $data->no_surat;
                })
                ->addColumn('status', function ($data) {
                    if (is_null($data->tgl_nonaktif)) { //jika pegawai masih aktif
                        if ($data->is_approved0 == NULL) {
                            $status = '<small>' . $data->jenis_pengajuan . '</small><br><span class="badge badge-secondary"> Menunggu Approval Unit</span>';
                        } else if ($data->is_approved1 == NULL && $data->is_approved0 != NULL) { //belum masuk validasi SDMO atau masih kepala unit
                            if ($data->is_approved0 == '2') {
                                if (intval(strtotime($data->updated_at)-strtotime($data->date_approved0))/60 > 2) { // 2 adalah 2 menit. estimasi kolom updated_at terupdate saat memvalidasi ajuan. kalau kurang dari 2 menit berarti kolom updated_at terupdate saat validasi
                                    $status = '<small>' . $data->jenis_pengajuan . '</small><br><span class="badge badge-warning"> Revisi Kepala Unit(Telah dilakukan revisi)</span>';
                                } else {
                                    $status = '<small>' . $data->jenis_pengajuan . '</small><br><span class="badge badge-warning"> Revisi Kepala Unit</span>';
                                }
                            } else if ($data->is_approved0 == '0') {
                                $status = '<small>' . $data->jenis_pengajuan . '</small><br><span class="badge badge-danger"> Ditolak Kepala Unit</span>';
                            } else {
                                $status = '<small>' . $data->jenis_pengajuan . '</small><br><span class="badge badge-primary"> Menunggu Approval Pimpinan SDMO</span>';
                            }
                        } else { //masuk SDMO
                            if ($data->is_approved1 == '2') {
                                if (intval(strtotime($data->updated_at)-strtotime($data->date_approved1))/60 > 2) { // 2 adalah 2 menit. estimasi kolom updated_at terupdate saat memvalidasi ajuan. kalau kurang dari 2 menit berarti kolom updated_at terupdate saat validasi
                                    $status = '<small>' . $data->jenis_pengajuan . '</small><br><span class="badge badge-warning"> Revisi Pimpinan SDMO(Telah dilakukan revisi)</span>';
                                } else {
                                    $status = '<small>' . $data->jenis_pengajuan . '</small><br><span class="badge badge-warning"> Revisi Pimpinan SDMO</span>';
                                }
                            } else if ($data->is_approved1 == '0') {
                                $status = '<small>' . $data->jenis_pengajuan . '</small><br><span class="badge badge-danger"> Ditolak Pimpinan SDMO</span>';
                            } else {
                                $status = '<small>' . $data->jenis_pengajuan . '</small><br><span class="badge badge-success"> Disetujui</span>';
                            }
                            if(Carbon::now() > $data->tgl_akhir_kerja){
                                $status = '<small>' . $data->jenis_pengajuan . '</small><br><span class="badge badge-warning"> Pegawai tidak aktif</span>';
                            }
                        }
                    } else {
                        $status = '<small>' . $data->jenis_pengajuan . '</small><br><span class="badge badge-warning"> Pegawai tidak aktif</span>';
                    }
                    return $status;
                })
                ->rawColumns(['aksi', 'status'])
                ->make(true);
        } else {
            $data = DB::table('pegawai')->get();
            return $data;
        }
    }

    public function getById($id)
    {
        $data = DB::table('pengajuan')
            ->join('pegawai', 'pengajuan.nik', 'pegawai.nik')
            ->join('unit', 'pengajuan.id_unit', 'unit.id_unit')
            ->join('jenis_pengajuan', 'pengajuan.id_jenis_pengajuan', 'jenis_pengajuan.id_jenis_pengajuan')
            ->select('pegawai.nama', 'pegawai.tgl_lahir', 'pegawai.jenis_kelamin', 'pegawai.id_pendidikan', 'pegawai.alamat', 'pegawai.email', 'pegawai.hp', 'pegawai.public_link_pas_foto','pegawai.id_status_bpjs','pegawai.nomor_bpjs', 'pegawai.public_link_ktp', 'pegawai.public_link_ijazah', 'pengajuan.*', 'unit.nama as unit', 'jenis_pengajuan.nama as jenis_pengajuan', 'pegawai.tgl_nonaktif','pegawai.id_ijazah','pegawai.id_ktp','pegawai.id_pas_foto','pegawai.mime_type_ijazah','pegawai.mime_type_ktp','pegawai.mime_type_pas_foto')
            ->where('pengajuan.id_pengajuan', $id)->first();

        return $data;
    }
}
