<?php

namespace App\Modules\Pegawai\Repositories;

use App\Modules\Pegawai\Interfaces\DosenInterface;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class DosenRepository implements DosenInterface
{

    public function getAll()
    {
        $data = DB::table('dosen')->get();

        return $data;
    }

    public function getArrayWhere($request, $server_side = false)
    {
        $where = [];
        if ($request->id_unit != null) {
            $where['pengajuan_dosen.id_unit'] = $request->id_unit;
        }
        if ($request->id_status != null) {
            $where['pengajuan_dosen.id_jenis_pengajuan_dosen'] = $request->id_status;
        }

        if ($server_side) {
            if (count($where) > 0) {
                $data = DB::table('dosen')
                    ->join('pengajuan_dosen', 'pengajuan_dosen.nik', 'dosen.nik')
                    ->join('unit', 'pengajuan_dosen.id_unit', 'unit.id_unit')
                    ->join('jenis_pengajuan_dosen', 'pengajuan_dosen.id_jenis_pengajuan_dosen', 'pengajuan_dosen.id_jenis_pengajuan_dosen')
                    ->select('dosen.nik', 'pengajuan_dosen.id_status', 'dosen.nama', 'jenis_kelamin', 'unit.nama as unit', 'jenis_pengajuan_dosen.nama as jenis_pengajuan')
                    ->where($where)
                    ->where('pengajuan_dosen.is_approved1','1')
                    ->get();
            } else {
                $data = DB::table('dosen')
                    ->join('pengajuan_dosen', 'pengajuan_dosen.nik', 'dosen.nik')
                    ->join('unit', 'pengajuan_dosen.id_unit', 'unit.id_unit')
                    ->join('jenis_pengajuan_dosen', 'pengajuan_dosen.id_jenis_pengajuan_dosen', 'jenis_pengajuan_dosen.id_jenis_pengajuan_dosen')
                    ->where('pengajuan_dosen.is_approved1','1')
                    ->select('dosen.nik', 'dosen.nama', 'jenis_kelamin', 'unit.nama as unit', 'jenis_pengajuan_dosen.nama as jenis_pengajuan')->get();
            }

            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('detail', function ($data) {
                    // 
                })
                ->addColumn('status', function ($data) {
                    $status = '<span class="small">'.$data->jenis_pengajuan . '</span><br><span class="badge badge-primary">Aktif</span>';
                    return $status;
                })
                ->rawColumns(['detail', 'status'])
                ->make(true);
        } else {
            $data = DB::table('pegawai')->get();
            return $data;
        }
    }

    public function getByNik($nik)
    {
        $data = DB::table('dosen')->where('nik', $nik)->first();

        return $data;
    }

    public function getOnlyAvailable() // hanya yang tidak sedang aktif pengajuannya
    {
        $dosen = DB::table('dosen')
                ->select('dosen.nik', 'nama', 'npp')
                ->distinct()
                ->join('pengajuan_dosen', 'pengajuan_dosen.nik', 'dosen.nik')
                ->where('pengajuan_dosen.tanggal_akhir_kerja', '<',  Carbon::now())
                ->get();
        
        return $dosen;
    }
}
