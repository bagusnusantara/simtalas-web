<?php

namespace App\Modules\Pegawai\Repositories;

use App\Modules\Pegawai\Interfaces\PengajuanDosenInterface;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class PengajuanDosenRepository implements PengajuanDosenInterface
{

    public function getAll()
    {
        $data = DB::table('pengajuan_dosen')->get();

        return $data;
    }

    public function getArrayWhere($request, $server_side = false)
    {
        $where = [];
        $id_unit = sso()->user()->getActiveRole()->getOrgId();
        if (sso()->user()->getActiveRole()->getName() != 'Super Administrator') {
            $where['pengajuan_dosen.id_unit'] = $id_unit;
        }
        if ($request->id_unit != null) {
            $where['pengajuan_dosen.id_unit'] = $request->id_unit;
        }
        if ($request->id_status != null) {
            if ($request->id_status == '0') {
                $where['pengajuan_dosen.is_approved0'] = NULL;
                if (sso()->user()->getActiveRole()->getName() != 'Kepala Kantor') {
                    $where['pengajuan_dosen.is_approved1'] = NULL;
                }
            } else {
                // diterima / direvisi / ditolak
                $where['pengajuan_dosen.is_approved0'] = $request->id_status;
                $where['pengajuan_dosen.is_approved1'] = $request->id_status;
            }
        }

        if(!$server_side) {
            $data = DB::table('dosen')->get();
            return $data;
        }

        if (count($where) > 0) {
            $data = DB::table('pengajuan_dosen')
                ->leftJoin('dosen', 'pengajuan_dosen.nik', 'dosen.nik')
                ->leftJoin('unit', 'pengajuan_dosen.id_unit', 'unit.id_unit')
                ->select('dosen.nik', 'pengajuan_dosen.no_surat', 'pengajuan_dosen.is_approved0', 'pengajuan_dosen.is_approved1', 'dosen.nama', 'jenis_kelamin', 'unit.nama as unit', 'pengajuan_dosen.id_pengajuan_dosen', 'pengajuan_dosen.berkas','pengajuan_dosen.date_approved0','pengajuan_dosen.date_approved1', 'pengajuan_dosen.updated_at', 'pengajuan_dosen.id_unit')
                ->where($where)
                ->get();

        } else {
            $data = DB::table('pengajuan_dosen')
                ->join('dosen', 'pengajuan_dosen.nik', 'dosen.nik')
                ->join('unit', 'pengajuan_dosen.id_unit', 'unit.id_unit')
                ->select('dosen.nik', 'pengajuan_dosen.no_surat', 'pengajuan_dosen.is_approved0', 'pengajuan_dosen.is_approved1', 'dosen.nama', 'jenis_kelamin', 'unit.nama as unit', 'pengajuan_dosen.id_pengajuan_dosen', 'pengajuan_dosen.berkas','pengajuan_dosen.date_approved0','pengajuan_dosen.date_approved1', 'pengajuan_dosen.updated_at', 'pengajuan_dosen.id_unit')
                ->get();
        }

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                $detail = '<a href="/pegawai/pengajuan-dosen/' . $data->id_pengajuan_dosen . '" class="btn btn-xs btn-white btn-block"><i class="fa fa-list"></i> Detail</a>';
                
                if (sso()->user()->getActiveRole()->getName() == 'Administrator') {
                    if ($data->is_approved1 == '1') {
                        $detail = $detail . '<a href="/pegawai/pengajuan-dosen/draft/' . $data->id_pengajuan_dosen . '" class="btn btn-xs btn-its btn-block"><i class="fa fa-download"></i> Unduh Draft</a>';
                    }
                    if ($data->is_approved0 == '2' || $data->is_approved1 == '2') {
                        $detail = $detail . '<a href="/pegawai/pengajuan-dosen/'. $data->id_pengajuan_dosen . '/edit/' . '" class="btn btn-xs btn-warning btn-block"><i class="fa fa-edit"></i> Revisi</a>';
                    }
                    if ($data->berkas != NULL) {
                        $detail = $detail . '<a href="https://storage-api-dev.its.ac.id/' . $data->berkas . '" class="btn btn-xs btn-its btn-block"><i class="fa fa-download"></i> Unduh Berkas</a>';
                    }
                    if ($data->is_approved0 == NULL && sso()->user()->getActiveRole()->getOrgId() == $data->id_unit) {
                        $detail = $detail . '<a href="/pegawai/pengajuan-dosen/'. $data->id_pengajuan_dosen . '/edit/' . '" class="btn btn-xs btn-warning btn-block"><i class="fa fa-edit"></i> Ubah</a>';
                    }
                    if (sso()->user()->getActiveRole()->getOrgId() == $data->id_unit && !$data->is_approved1) {
                        $detail .= '<button class="btn btn-xs btn-danger btn-block" data-target="#hapusPengajuanModal" data-toggle="modal" data-id-pengajuan-dosen="' . $data->id_pengajuan_dosen . '"><i class="fa fa-trash"></i> Hapus</button>';
                    }
                }
                if (sso()->user()->getActiveRole()->getName() == 'Super Administrator') {
                    if ($data->is_approved1 == '1') {
                        $detail = $detail . '<a href="/pegawai/pengajuan-dosen/draft/' . $data->id_pengajuan_dosen . '" class="btn btn-xs btn-its btn-block"><i class="fa fa-download"></i> Unduh Draft</a>';
                    }
                    if ($data->is_approved0 == '2' || $data->is_approved1 == '2') {
                        $detail = $detail . '<a href="/pegawai/pengajuan-dosen/'. $data->id_pengajuan_dosen . '/edit/' . '" class="btn btn-xs btn-warning btn-block"><i class="fa fa-edit"></i> Revisi</a>';
                    }
                    if ($data->berkas != NULL) {
                        $detail = $detail . '<a href="https://storage-api-dev.its.ac.id/' . $data->berkas . '" class="btn btn-xs btn-its btn-block"><i class="fa fa-download"></i> Unduh Berkas</a>';
                        $detail = $detail . '<button type="button" data-id="' . $data->id_pengajuan_dosen . '" data-toggle="modal" data-target="#hapus" class="btn btn-xs btn-danger btn-block"><i class="fa fa-trash"></i> Hapus Berkas</a>';
                    } else {
                        if ($data->is_approved1 == '1') {
                        $detail = $detail . '<button type="button" data-id="' . $data->id_pengajuan_dosen . '" data-toggle="modal" data-target="#upload" class="btn btn-xs btn-secondary btn-block"><i class="fa fa-upload"></i> Unggah Berkas</a>';
                        }
                    }
                    if ($data->is_approved0 == NULL && sso()->user()->getActiveRole()->getOrgId() == $data->id_unit) {
                        $detail = $detail . '<a href="/pegawai/pengajuan-dosen/'. $data->id_pengajuan_dosen . '/edit/' . '" class="btn btn-xs btn-warning btn-block"><i class="fa fa-edit"></i> Ubah</a>';
                    }
                    if(!$data->is_approved1) {
                        $detail .= '<button class="btn btn-xs btn-danger btn-block" data-target="#hapusPengajuanModal" data-toggle="modal" data-id-pengajuan-dosen="' . $data->id_pengajuan_dosen . '"><i class="fa fa-trash"></i> Hapus</button>';
                    }
                }
                return $detail;
            })
            ->addColumn('nomor_surat', function ($data) {
                if ($data->no_surat == NULL) {
                    return "-";
                }
                return $data->no_surat;
            })
            ->addColumn('status', function ($data) {
                if ($data->is_approved0 == NULL) {
                    $status = '<span class="badge badge-secondary"> Menunggu Approval Unit</span>';
                } else if ($data->is_approved1 == NULL && $data->is_approved0 != NULL) { //belum masuk validasi SDMO atau masih kepala unit
                    if ($data->is_approved0 == '2') {
                        if (intval(strtotime($data->updated_at)-strtotime($data->date_approved0))/60 > 2) { // 2 adalah 2 menit. estimasi kolom updated_at terupdate saat memvalidasi ajuan. kalau kurang dari 2 menit berarti kolom updated_at terupdate saat validasi
                            $status = '<span class="badge badge-warning"> Revisi Kepala Unit(Telah dilakukan revisi)</span>';
                        } else {
                            $status = '<span class="badge badge-warning"> Revisi Kepala Unit</span>';
                        }
                    } else if ($data->is_approved0 == '0') {
                        $status = '<span class="badge badge-danger"> Ditolak Kepala Unit</span>';
                    } else {
                        $status = '<span class="badge badge-primary"> Menunggu Approval Pimpinan SDMO</span>';
                    }
                } else { //masuk SDMO
                    if ($data->is_approved1 == '2') {
                        if (intval(strtotime($data->updated_at)-strtotime($data->date_approved1))/60 > 2) { // 2 adalah 2 menit. estimasi kolom updated_at terupdate saat memvalidasi ajuan. kalau kurang dari 2 menit berarti kolom updated_at terupdate saat validasi
                            $status = '<span class="badge badge-warning"> Revisi Pimpinan SDMO(Telah dilakukan revisi)</span>';
                        } else {
                            $status = '<span class="badge badge-warning"> Revisi Pimpinan SDMO</span>';
                        }
                    } else if ($data->is_approved1 == '0') {
                        $status = '<span class="badge badge-danger"> Ditolak Pimpinan SDMO</span>';
                    } else {
                        $status = '<span class="badge badge-success"> Disetujui</span>';
                    }
                }
                return $status;
            })
            ->rawColumns(['aksi', 'status'])
            ->make(true);
    }
}
