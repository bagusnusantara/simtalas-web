<li class="nav-label mg-t-15">Tenaga Harian Kontrak</li>
<li class="nav-item {{ (Request::is('/pegawai/pengajuan/create') || Request::is('pegawai/pengajuan/create')) ? 'active' : '' }}">
    <a href="{{ url('/pegawai/pengajuan/create') }}" class="nav-link">
        <i data-feather="file-plus"></i>
        <span>Pengajuan Baru</span>
    </a>
</li>
<li class="nav-item {{ (Request::is('/pegawai/pengajuan') || Request::is('pegawai/pengajuan')) ? 'active' : '' }}">
    <a href="{{ url('/pegawai/pengajuan') }}" class="nav-link">
        <i data-feather="list"></i>
        <span>Daftar Pengajuan</span>
    </a>
</li>
<li class="nav-item {{ (Request::is('/pegawai') || Request::is('pegawai')) ? 'active' : '' }}">
    <a href="{{ url('/pegawai') }}" class="nav-link">
        <i data-feather="users"></i>
        <span>Data THL</span>
    </a>
</li>

<li class="nav-label mg-t-15">Dosen LB</li>
<li class="nav-item {{ (Request::is('/pegawai/pengajuan-dosen/create') || Request::is('pegawai/pengajuan-dosen/create')) ? 'active' : '' }}">
    <a href="{{ url('/pegawai/pengajuan-dosen/create') }}" class="nav-link">
        <i data-feather="file-plus"></i>
        <span>Pengajuan Baru</span>
    </a>
</li>
<li class="nav-item {{ (Request::is('/pegawai/pengajuan-dosen') || Request::is('pegawai/pengajuan-dosen')) ? 'active' : '' }}">
    <a href="{{ url('/pegawai/pengajuan-dosen') }}" class="nav-link">
        <i data-feather="list"></i>
        <span>Daftar Pengajuan</span>
    </a>
</li>
@php
use Dptsi\Sso\Facade\Sso;
$user = Sso::user();
$current_role=$user->getActiveRole()->getName();
@endphp
@if($current_role == 'Super Administrator')
<li class="nav-label mg-t-15">Nomor Surat THL</li>
<li class="nav-item {{ (Request::is('/pegawai/nomor-surat') || Request::is('pegawai/nomor-surat')) ? 'active' : '' }}">
    <a href="{{ url('/pegawai/nomor-surat') }}" class="nav-link">
        <i data-feather="folder-minus"></i>
        <span>Input Nomor Surat</span>
    </a>
</li>
@endif