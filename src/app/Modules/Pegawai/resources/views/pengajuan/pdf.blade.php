<!DOCTYPE html>
<html lang="en">

<head>
    <title>Draft Perjanjian Kerja</title>
    <style>
        .text-center {
            text-align: center;
        }

        .font-weight-bold {
            font-weight: bold;
        }

        body {
            text-align: justify;
        }

        td {
            vertical-align: text-top;
        }

        .table {
            /* border: 1px solid grey; */
            padding: 15px;
        }

        /* Create two equal columns that floats next to each other */
        .column {
            float: left;
            width: 40%;
            padding: 10px;
            height: 30px;
            /* Should be removed. Only for demonstration */
        }

        .column2 {
            float: left;
            width: 10%;
            padding: 10px;
            height: 300px;
            /* Should be removed. Only for demonstration */
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        .page_break {
            page-break-before: always;
        }

    </style>
</head>

<body>

    <h3 class="text-center" style="margin-bottom: 0px;"><strong>PERJANJIAN KERJA</strong></h3>
    <p class="text-center font-weight-bold" style="margin-top: 0px;">Nomor: {{ $data['no_surat'] }}</p>
    <br>
    <p>Perjanjian ini dibuat dan ditandatangani pada hari {{ $data['hari'] }} tanggal
        {{ $data['tanggal'] }} bulan {{ $data['bulan'] }} tahun {{ $data['tahun_terbilang'] }} antara:
    </p>
    <table>
        <tr>
            <td style="width: 20px;" class="text-center">1.</td>
            <td style="width: 80px;">Nama</td>
            <td>: Dr. Ir. Sri Gunani Partiwi, M.T.</td>
        </tr>
        <tr>
            <td></td>
            <td>NIP</td>
            <td>: 196605311990022001</td>
        </tr>
        <tr>
            <td></td>
            <td>Jabatan</td>
            <td>: Direktur Sumber Daya Manusia dan Organisasi</td>
        </tr>
        <tr>
            <td></td>
            <td>Alamat</td>
            <td>: Kampus ITS Sukolilo Surabaya</td>
        </tr>
    </table>
    <p style="margin-left:30px">Dalam hal ini bertindak untuk dan atas nama Institut Teknologi Sepuluh Nopember,
        berkedudukan di Surabaya,
        selanjutnya disebut sebagai <strong>Pihak Pertama</strong>.</p>
    <table>
        <tr>
            <td style="width: 20px;" class="text-center">2.</td>
            <td>Nama</td>
            <td>: {{ $data['nama'] }}</td>
        </tr>
        <tr>
            <td></td>
            <td>NIK / Umur</td>
            <td>: {{ $data['nik'] }} / {{ $data['umur'] }}</td>
        </tr>
        <tr>
            <td></td>
            <td>Alamat</td>
            <td>: {{ $data['alamat'] }}</td>
        </tr>
    </table>
    <p style="margin-left:30px">Dalam hal ini bertindak selaku diri sendiri, selanjutnya disebut sebagai <strong>Pihak
            Kedua</strong>.</p>
    <p>Kedua belah pihak sepakat untuk mengadakan Perjanjian Kerja Pegawai sebagai @if ($data['id_jenis_pengajuan'] == 'thl') Tenaga Harian Lepas @elseif($data['id_jenis_pengajuan'] == 'kontrak') Tenaga Kontrak @elseif($data['id_jenis_pengajuan'] == 'magang') Tenaga Magang @endif dengan
        ketentuan
        dan syarat sebagai berikut:</p>
    <p class="text-center" style="margin-bottom: 0px;">Pasal 1</p>
    <p class="text-center" style="margin-top: 0px;">Kewajiban Pihak Pertama</p>
    @if ($data['id_jenis_pengajuan'] == 'thl')
        <p style="margin-bottom: 0px;">Pihak Pertama memiliki kewajiban sebagai berikut:</p>

        <table style="margin-left: 30px">
            <tr>
                <td style="width: 20px;">a.</td>

                <td>Memberikan upah kepada Pihak Kedua sebesar Rp {{ $data['honor'] }}
                    ({{ $data['terbilang_honor'] }} rupiah)
                    per 8 (delapan) jam per
                    hari;</td>
            </tr>
            <tr>
                <td>b.</td>
                <td>Memberikan uang makan kepada Pihak Kedua sebesar Rp {{ $data['uang_makan'] }}
                    ({{ $data['terbilang_uang_makan'] }} rupiah) per kehadiran; dan</td>
            </tr>
            <tr>
                <td>c.</td>
                <td>Memberikan uang lembur kepada Pihak Kedua apabila ada pekerjaan lembur.</td>
            </tr>
        </table>
    @else
        <p>Pihak Pertama memiliki kewajiban memberikan upah kepada Pihak Kedua sebesar Rp {{ $data['honor'] }}
            ({{ $data['terbilang_honor'] }} rupiah)
            per 1 (satu) jam.</p>
    @endif

    <p class="text-center" style="margin-bottom: 0px;">Pasal 2</p>
    <p class="text-center" style="margin-top: 0px;">Kewajiban Pihak Kedua</p>
    <p style="margin-bottom: 0px;">Pihak Kedua memiliki kewajiban sebagai berikut:</p>

    <table style="margin-left: 30px">
        <tr>
            <td style="width: 20px;vertical-align: text-top;">a.</td>
            <td>Melaksanakan tugas dan tanggung jawab sebagai @if ($data['id_jenis_pengajuan'] == 'thl') Tenaga Harian Lepas @elseif($data['id_jenis_pengajuan'] == 'kontrak') Tenaga Kontrak @elseif($data['id_jenis_pengajuan'] == 'magang') Tenaga Magang @endif di
                {{ $data['unit'] }}
                ITS dengan tugas sebagai berikut:<br>
                @foreach ($rincian_tugas as $item)
                    <li style="margin-left: 20px;">{{ $item->deskripsi }}</li>
                @endforeach
            </td>
        </tr>
        <tr>
            <td>b.</td>
            <td>Melaksanakan tugas dan tanggung jawab sesuai arahan dari pimpinan unit; dan</td>
        </tr>
        <tr>
            <td>c.</td>
            <td>Mematuhi hari dan jam kerja.</td>
        </tr>
    </table>
    <div class="page_break"></div>
    <p class="text-center" style="margin-bottom: 0px;">Pasal 3</p>
    <p class="text-center" style="margin-top: 0px;">Hak Pihak Pertama</p>

    <p style="margin-bottom: 0px;">Pihak Pertama memiliki hak sebagai berikut:</p>
    <table style="margin-left: 30px;margin-top:0px;">
        <tr>
            <td style="width: 20px;vertical-align: text-top;">a.</td>
            <td>Memberikan saran dan/ atau teguran terhadap Pihak Kedua apabila menurut penilaiannya Pihak Kedua kurang
                baik dalam melaksanakan kewajiban sebagaimana disebutkan dalam perjanjian ini; dan</td>
        </tr>
        <tr>
            <td>b.</td>
            <td>Memberikan penilaian dan/ atau evaluasi pelaksanaan pekerjaan Pihak Kedua.</td>
        </tr>
    </table>

    <p class="text-center" style="margin-bottom: 0px;">Pasal 4</p>
    <p class="text-center" style="margin-top: 0px;">Hak Pihak Kedua</p>
    @if ($data['id_jenis_pengajuan'] == 'thl')
        <p style="margin-bottom: 0px;">Pihak Kedua memiliki hak sebagai berikut:</p>
        <table style="margin-left: 30px">
            <tr>
                <td style="width: 20px;">a.</td>
                <td>Menerima upah;</td>
            </tr>
            <tr>
                <td>b.</td>
                <td>Menerima uang makan; dan</td>
            </tr>
            <tr>
                <td>c.</td>
                <td>Menerima uang lembur.</td>
            </tr>
        </table>
    @else
        <p>Pihak Kedua memiliki hak untuk menerima upah dari Pihak Pertama.</p>
    @endif

    <p class="text-center" style="margin-bottom: 0px;">Pasal 5</p>
    <p class="text-center" style="margin-top: 0px;">Tata Cara Pembayaran Upah, Uang Makan, dan Uang Lembur</p>
    <p>Pembayaran upah, uang makan, dan uang lembur sebagaimana dimaksud dalam Pasal 1 (satu) dibebankan pada {{$data['id_unit'] == '5CF36B70-105D-41AF-AF27-B44884397FA1' ? 'Dana Non PNBP' : $data['unit']}} ITS.</p>

    <p class="text-center" style="margin-bottom: 0px;">Pasal 6</p>
    <p class="text-center" style="margin-top: 0px;">Jangka Waktu dan Pengakhiran Perjanjian</p>
    <p style="margin-bottom: 0px;">1. Jangka waktu perjanjian kerja ini adalah mulai tanggal
        {{ $data['tgl_mulai_kerja_d'] }} {{ $data['tgl_mulai_kerja_m'] }} sampai dengan
        {{ $data['tgl_akhir_kerja_d'] }} {{ $data['tgl_akhir_kerja_m'] }} {{ $data['tgl_akhir_kerja_y'] }}.
    </p>
    <p style="margin-top: 0px;margin-bottom: 0px;">2. Perjanjian ini berakhir apabila:</p>
    <table style="margin-left: 30px">
        <tr>
            <td style="width: 20px;">a.</td>
            <td>Masa berlaku perjanjian kerja ini sudah berakhir;</td>
        </tr>
        <tr>
            <td>b.</td>
            <td>Pihak Kedua berhalangan tetap;</td>
        </tr>
        <tr>
            <td>c.</td>
            <td>Pihak Kedua mengundurkan diri; atau</td>
        </tr>
        <tr>
            <td>d.</td>
            <td>Pihak Kedua telah ditetapkan sebagai tersangka dalam kasus pidana.</td>
        </tr>
    </table>
    <p style="margin-top: 0px;margin-bottom:0px;">3. Pihak Pertama dapat memutuskan kontrak kerja secara sepihak,
        apabila:</p>
    <table style="margin-left: 30px">
        <tr>
            <td style="width: 20px;">a.</td>
            <td>Pihak Kedua tidak memenuhi kewajibannya seperti tersebut pada Pasal 2, dan tidak mematuhi setiap
                ketentuan dalam perjanjian ini; atau</td>
        </tr>
        <tr>
            <td>b.</td>
            <td>Pihak Kedua karena sesuatu hal dipandang tidak mampu lagi melaksanakan kewajiban sebagaimana ditetapkan
                dalam perjanjian kerja ini.</td>
        </tr>
    </table>

    <p class="text-center" style="margin-bottom: 0px;">Pasal 7</p>
    <p class="text-center" style="margin-top: 0px;">Penyelesaian Perselisihan</p>
    <table>
        <tr>
            <td style="width: 20px;" class="text-center">1.</td>
            <td>Penyelesaian perselisihan dalam Perjanjian Kerja ini akan diselesaikan Para Pihak dengan sistem
                musyawarah untuk mufakat; dan</td>
        </tr>
        <tr>
            <td class="text-center">2.</td>
            <td>Dalam hal musyawarah tidak tercapai, maka Para Pihak akan menyelesaikan secara hukum dengan memilih
                domisili hukum di wilayah Pengadilan Negeri Surabaya.</td>
        </tr>
    </table>

    <p class="text-center" style="margin-bottom: 0px;">Pasal 8</p>
    <p class="text-center" style="margin-top: 0px;">Addendum</p>
    <p>Perubahan atas ketentuan serta pengaturan yang belum cukup diatur dalam perjanjian ini hanya dapat dilakukan
        dengan suatu addendum yang disepakati oleh para pihak dan merupakan bagian yang tidak terpisahkan dari
        perjanjian ini.</p>

    <p class="text-center" style="margin-bottom: 0px;">Pasal 9</p>
    <p class="text-center" style="margin-top: 0px;">Penutup</p>
    <p>Demikian Perjanjian Kerja ini dibuat dalam 2 (dua) rangkap asli di atas kertas bermaterai cukup, ditandatangani
        oleh para pihak pada hari, tanggal, bulan dan tahun sebagaimana tersebut di atas, serta masing-masing pihak
        memegang 1 (satu) asli Perjanjian kerja ini dan mempunyai kekuatan hukum yang sama bagi Pihak Pertama dan Pihak
        Kedua.</p>
    <div class="row">
        <div class="column">
            <table class="table text-center" style="width: 100%">
                <tr class="font-weight-bold">
                    <th>PIHAK PERTAMA</th>
                </tr>
                <tr>
                    <td style="text-align: left;padding:10px; width:5px;"><img
                            src="https://drive.google.com/uc?id=1AdujD5RIpbHQN4UWvgkIAdziLv8d9uXf"></td>
                </tr>
                <tr>
                    <td>Dr. Ir. Sri Gunani Partiwi, M.T.</td>
                </tr>
                <tr>
                    <td>NIP. 196605311990022001</td>
                </tr>
            </table>
        </div>
        <div class="column2">
        </div>
        <div class="column">
            <table class="table text-center" style="width: 100%;">
                <tr class="font-weight-bold">
                    <th>PIHAK KEDUA</th>
                </tr>
                <tr>
                    <td style="text-align: left; "><br><br><br><br><br><br></td>
                </tr>
                <tr>
                    <td>{{ $data['nama'] }}</td>
                </tr>
                <tr>
                    <td><br></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="page_break"></div>


    <h3 class="text-center" style="margin-bottom: 0px;"><strong>PERJANJIAN KERJA</strong></h3>
    <p class="text-center font-weight-bold" style="margin-top: 0px;">Nomor: {{ $data['no_surat'] }}</p>
    <br>
    <p>Perjanjian ini dibuat dan ditandatangani pada hari {{ $data['hari'] }} tanggal
        {{ $data['tanggal'] }} bulan {{ $data['bulan'] }} tahun {{ $data['tahun_terbilang'] }} antara:
    </p>
    <table>
        <tr>
            <td style="width: 20px;" class="text-center">1.</td>
            <td style="width: 80px;">Nama</td>
            <td>: Dr. Ir. Sri Gunani Partiwi, M.T.</td>
        </tr>
        <tr>
            <td></td>
            <td>NIP</td>
            <td>: 196605311990022001</td>
        </tr>
        <tr>
            <td></td>
            <td>Jabatan</td>
            <td>: Direktur Sumber Daya Manusia dan Organisasi</td>
        </tr>
        <tr>
            <td></td>
            <td>Alamat</td>
            <td>: Kampus ITS Sukolilo Surabaya</td>
        </tr>
    </table>
    <p style="margin-left:30px">Dalam hal ini bertindak untuk dan atas nama Institut Teknologi Sepuluh Nopember,
        berkedudukan di Surabaya,
        selanjutnya disebut sebagai <strong>Pihak Pertama</strong>.</p>
    <table>
        <tr>
            <td style="width: 20px;" class="text-center">2.</td>
            <td>Nama</td>
            <td>: {{ $data['nama'] }}</td>
        </tr>
        <tr>
            <td></td>
            <td>NIK / Umur</td>
            <td>: {{ $data['nik'] }} / {{ $data['umur'] }}</td>
        </tr>
        <tr>
            <td></td>
            <td>Alamat</td>
            <td>: {{ $data['alamat'] }}</td>
        </tr>
    </table>
    <p style="margin-left:30px">Dalam hal ini bertindak selaku diri sendiri, selanjutnya disebut sebagai <strong>Pihak
            Kedua</strong>.</p>
    <p>Kedua belah pihak sepakat untuk mengadakan Perjanjian Kerja Pegawai sebagai @if ($data['id_jenis_pengajuan'] == 'thl') Tenaga Harian Lepas @elseif($data['id_jenis_pengajuan'] == 'kontrak') Tenaga Kontrak @elseif($data['id_jenis_pengajuan'] == 'magang') Tenaga Magang @endif dengan
        ketentuan
        dan syarat sebagai berikut:</p>
    <p class="text-center" style="margin-bottom: 0px;">Pasal 1</p>
    <p class="text-center" style="margin-top: 0px;">Kewajiban Pihak Pertama</p>
    @if ($data['id_jenis_pengajuan'] == 'thl')
        <p style="margin-bottom: 0px;">Pihak Pertama memiliki kewajiban sebagai berikut:</p>

        <table style="margin-left: 30px">
            <tr>
                <td style="width: 20px;">a.</td>

                <td>Memberikan upah kepada Pihak Kedua sebesar Rp {{ $data['honor'] }}
                    ({{ $data['terbilang_honor'] }} rupiah)
                    per 8 (delapan) jam per
                    hari;</td>
            </tr>
            <tr>
                <td>b.</td>
                <td>Memberikan uang makan kepada Pihak Kedua sebesar Rp {{ $data['uang_makan'] }}
                    ({{ $data['terbilang_uang_makan'] }} rupiah) per kehadiran; dan</td>
            </tr>
            <tr>
                <td>c.</td>
                <td>Memberikan uang lembur kepada Pihak Kedua apabila ada pekerjaan lembur.</td>
            </tr>
        </table>
    @else
        <p>Pihak Pertama memiliki kewajiban memberikan upah kepada Pihak Kedua sebesar Rp {{ $data['honor'] }}
            ({{ $data['terbilang_honor'] }} rupiah)
            per 1 (satu) jam.</p>
    @endif

    <p class="text-center" style="margin-bottom: 0px;">Pasal 2</p>
    <p class="text-center" style="margin-top: 0px;">Kewajiban Pihak Kedua</p>
    <p style="margin-bottom: 0px;">Pihak Kedua memiliki kewajiban sebagai berikut:</p>

    <table style="margin-left: 30px">
        <tr>
            <td style="width: 20px;vertical-align: text-top;">a.</td>
            <td>Melaksanakan tugas dan tanggung jawab sebagai @if ($data['id_jenis_pengajuan'] == 'thl') Tenaga Harian Lepas @elseif($data['id_jenis_pengajuan'] == 'kontrak') Tenaga Kontrak @elseif($data['id_jenis_pengajuan'] == 'magang') Tenaga Magang @endif di
                {{ $data['unit'] }}
                ITS dengan tugas sebagai berikut:<br>
                @foreach ($rincian_tugas as $item)
                    <li style="margin-left: 20px;">{{ $item->deskripsi }}</li>
                @endforeach
            </td>
        </tr>
        <tr>
            <td>b.</td>
            <td>Melaksanakan tugas dan tanggung jawab sesuai arahan dari pimpinan unit; dan</td>
        </tr>
        <tr>
            <td>c.</td>
            <td>Mematuhi hari dan jam kerja.</td>
        </tr>
    </table>
    <div class="page_break"></div>
    <p class="text-center" style="margin-bottom: 0px;">Pasal 3</p>
    <p class="text-center" style="margin-top: 0px;">Hak Pihak Pertama</p>

    <p style="margin-bottom: 0px;">Pihak Pertama memiliki hak sebagai berikut:</p>
    <table style="margin-left: 30px;margin-top:0px;">
        <tr>
            <td style="width: 20px;vertical-align: text-top;">a.</td>
            <td>Memberikan saran dan/ atau teguran terhadap Pihak Kedua apabila menurut penilaiannya Pihak Kedua kurang
                baik dalam melaksanakan kewajiban sebagaimana disebutkan dalam perjanjian ini; dan</td>
        </tr>
        <tr>
            <td>b.</td>
            <td>Memberikan penilaian dan/ atau evaluasi pelaksanaan pekerjaan Pihak Kedua.</td>
        </tr>
    </table>

    <p class="text-center" style="margin-bottom: 0px;">Pasal 4</p>
    <p class="text-center" style="margin-top: 0px;">Hak Pihak Kedua</p>
    @if ($data['id_jenis_pengajuan'] == 'thl')
        <p style="margin-bottom: 0px;">Pihak Kedua memiliki hak sebagai berikut:</p>
        <table style="margin-left: 30px">
            <tr>
                <td style="width: 20px;">a.</td>
                <td>Menerima upah;</td>
            </tr>
            <tr>
                <td>b.</td>
                <td>Menerima uang makan; dan</td>
            </tr>
            <tr>
                <td>c.</td>
                <td>Menerima uang lembur.</td>
            </tr>
        </table>
    @else
        <p>Pihak Kedua memiliki hak untuk menerima upah dari Pihak Pertama.</p>
    @endif

    <p class="text-center" style="margin-bottom: 0px;">Pasal 5</p>
    <p class="text-center" style="margin-top: 0px;">Tata Cara Pembayaran Upah, Uang Makan, dan Uang Lembur</p>
    <p>Pembayaran upah, uang makan, dan uang lembur sebagaimana dimaksud dalam Pasal 1 (satu) dibebankan pada {{$data['id_unit'] == '5CF36B70-105D-41AF-AF27-B44884397FA1' ? 'Dana Non PNBP' : $data['unit']}} ITS.</p>

    <p class="text-center" style="margin-bottom: 0px;">Pasal 6</p>
    <p class="text-center" style="margin-top: 0px;">Jangka Waktu dan Pengakhiran Perjanjian</p>
    <p style="margin-bottom: 0px;">1. Jangka waktu perjanjian kerja ini adalah mulai tanggal
        {{ $data['tgl_mulai_kerja_d'] }} {{ $data['tgl_mulai_kerja_m'] }} sampai dengan
        {{ $data['tgl_akhir_kerja_d'] }} {{ $data['tgl_akhir_kerja_m'] }} {{ $data['tgl_akhir_kerja_y'] }}.
    </p>
    <p style="margin-top: 0px;margin-bottom: 0px;">2. Perjanjian ini berakhir apabila:</p>
    <table style="margin-left: 30px">
        <tr>
            <td style="width: 20px;">a.</td>
            <td>Masa berlaku perjanjian kerja ini sudah berakhir;</td>
        </tr>
        <tr>
            <td>b.</td>
            <td>Pihak Kedua berhalangan tetap;</td>
        </tr>
        <tr>
            <td>c.</td>
            <td>Pihak Kedua mengundurkan diri; atau</td>
        </tr>
        <tr>
            <td>d.</td>
            <td>Pihak Kedua telah ditetapkan sebagai tersangka dalam kasus pidana.</td>
        </tr>
    </table>
    <p style="margin-top: 0px;margin-bottom:0px;">3. Pihak Pertama dapat memutuskan kontrak kerja secara sepihak,
        apabila:</p>
    <table style="margin-left: 30px">
        <tr>
            <td style="width: 20px;">a.</td>
            <td>Pihak Kedua tidak memenuhi kewajibannya seperti tersebut pada Pasal 2, dan tidak mematuhi setiap
                ketentuan dalam perjanjian ini; atau</td>
        </tr>
        <tr>
            <td>b.</td>
            <td>Pihak Kedua karena sesuatu hal dipandang tidak mampu lagi melaksanakan kewajiban sebagaimana ditetapkan
                dalam perjanjian kerja ini.</td>
        </tr>
    </table>

    <p class="text-center" style="margin-bottom: 0px;">Pasal 7</p>
    <p class="text-center" style="margin-top: 0px;">Penyelesaian Perselisihan</p>
    <table>
        <tr>
            <td style="width: 20px;" class="text-center">1.</td>
            <td>Penyelesaian perselisihan dalam Perjanjian Kerja ini akan diselesaikan Para Pihak dengan sistem
                musyawarah untuk mufakat; dan</td>
        </tr>
        <tr>
            <td class="text-center">2.</td>
            <td>Dalam hal musyawarah tidak tercapai, maka Para Pihak akan menyelesaikan secara hukum dengan memilih
                domisili hukum di wilayah Pengadilan Negeri Surabaya.</td>
        </tr>
    </table>

    <p class="text-center" style="margin-bottom: 0px;">Pasal 8</p>
    <p class="text-center" style="margin-top: 0px;">Addendum</p>
    <p>Perubahan atas ketentuan serta pengaturan yang belum cukup diatur dalam perjanjian ini hanya dapat dilakukan
        dengan suatu addendum yang disepakati oleh para pihak dan merupakan bagian yang tidak terpisahkan dari
        perjanjian ini.</p>

    <p class="text-center" style="margin-bottom: 0px;">Pasal 9</p>
    <p class="text-center" style="margin-top: 0px;">Penutup</p>
    <p>Demikian Perjanjian Kerja ini dibuat dalam 2 (dua) rangkap asli di atas kertas bermaterai cukup, ditandatangani
        oleh para pihak pada hari, tanggal, bulan dan tahun sebagaimana tersebut di atas, serta masing-masing pihak
        memegang 1 (satu) asli Perjanjian kerja ini dan mempunyai kekuatan hukum yang sama bagi Pihak Pertama dan Pihak
        Kedua.</p>
    
    <div class="row">
        <div class="column">
            <table class="table text-center" style="width: 100%">
                <tr class="font-weight-bold">
                    <th>PIHAK PERTAMA</th>
                </tr>
                <tr>
                    <td><br><br><br><br><br><br></td>
                </tr>
                <tr>
                    <td>Dr. Ir. Sri Gunani Partiwi, M.T.</td>
                </tr>
                <tr>
                    <td>NIP. 196605311990022001</td>
                </tr>
            </table>
        </div>
        <div class="column2">
        </div>
        <div class="column">
            <table class="table text-center" style="width: 100%;">
                <tr class="font-weight-bold">
                    <th>PIHAK KEDUA</th>
                </tr>
                <tr>
                    <td style="text-align: left;padding:10px; width:5px;"><img
                        src="https://drive.google.com/uc?id=1AdujD5RIpbHQN4UWvgkIAdziLv8d9uXf"></td>
                    
                </tr>
                <tr>
                    <td>{{ $data['nama'] }}</td>
                </tr>
                <tr>
                    <td><br></td>
                </tr>
            </table>
        </div>
    </div>
</body>

</html>
