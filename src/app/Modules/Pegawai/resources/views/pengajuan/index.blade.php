@extends('Ui::base')

@section('title')
    @if (isset($page['halaman'])){{ ucwords($page['halaman']) }}@endif
@endsection
@section('prestyles')
    <link href="{{ asset('lib/datatables.net-dt/css/jquery.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/select2/css/select2.min.css') }}" rel="stylesheet">
    <style type="text/css">
        /*table row hover color*/
        table#mydatatable.dataTable tbody tr:hover {
            background-color: #FFFDE5;
        }

        table#mydatatable.dataTable tbody tr:hover>.sorting_1 {
            background-color: #FFFDE5;
        }

    </style>
    <style type="text/css">
        .irs-disabled {
            opacity: 1;
        }

        .progress-pengajuan {
            width: 250%;
            margin: 0 auto;
        }

    </style>
@endsection
@section('header_title')
    @if (isset($page['halaman'])){{ ucwords($page['halaman']) }}@endif
@endsection
@section('breadcrumbs')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-style1 mg-b-10">
            <li class="breadcrumb-item"><a href="#">
                    @if (isset($page['menu'])){{ ucwords($page['menu']) }}@endif
                </a></li>
            <li class="breadcrumb-item active" aria-current="page">
                @if (isset($page['halaman'])){{ ucwords($page['halaman']) }}@endif
            </li>
        </ol>
    </nav>
@endsection
@section('content')
    <div class="d-flex col-lg-12 flex-row justify-content-start mg-b-20">

        <div class="card card-body bg-gray-200">
            <div class="form-row align-items-center">
                <label class="col-form-label" for="filter_unit">Unit:&nbsp;</label>
                <div class="col-md-4">
                    <select class="form-control" id="filter_unit">
                        <option value="">Semua unit</option>
                        @foreach ($data['unit'] as $item)
                            <option value="{{ $item->id_unit }}">{{ $item->nama }} </option>
                        @endforeach
                    </select>
                </div>
                <label class="col-form-label" for="filter_status">Status Pengajuan:&nbsp;</label>
                <div class="col-md-3">
                    <select class="form-control" id="filter_status">
                        <option value="0">Semua status</option>
                        <option value="1">Menunggu Persetujuan Pimpinan Unit</option>
                        <option value="2">Menunggu Persetujuan DSDMO/Disetujui Pimpinan Unit</option>
                        <option value="3">Disetujui DSDMO</option>
                        <option value="4">Revisi Pimpinan Unit</option>
                        <option value="5">Revisi DSDMO</option>
                        <option value="6">Ditolak Pimpinan Unit</option>
                        <option value="7">Ditolak DSDMO</option>


                        {{-- @foreach ($data['jenis_pengajuan'] as $item)
                            <option value="{{ $item->id_jenis_pengajuan }}">{{ $item->nama }} </option>
                        @endforeach --}}
                    </select>
                </div>
                <button id="btn-filter" class="btn d-block btn-sm btn-its tx-montserrat tx-semibold">
                    Tampilkan
                </button>
                <button id="btn-reset" class="btn btn-sm btn-white tx-montserrat tx-semibold ml-1">
                    Reset Filter
                </button>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-lg-12">
        @if (session('success'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ session('success') }}
            </div>
        @elseif(session('failed'))
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ session('failed') }}
            </div>
        @endif
        <div class="card card-body">
            <div data-label="Example" class="df-example demo-table">
                <div class="table-responsive">
                    <table id="mydatatable" class="table">
                        <thead>
                            <tr class="text-center">
                                <th class="wd-5p">No</th>
                                <th class="wd-20p">No. Surat</th>
                                <th class="wd-20p">NIK</th>
                                <th class="wd-20p">Nama</th>
                                <th>Unit</th>
                                <th>Status</th>
                                <th class="wd-20p">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- df-example -->
    </div>

    {{-- Modal setuju --}}
    <div class="modal fade" id="upload" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content tx-14">
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Unggah Dokumen</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ url('pegawai/pengajuan/upload') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" id="id" name="id_pengajuan">
                        <div class="custom-file">
                            <input type="file" name="file" class="custom-file-input" id="customFile" required>
                            <label class="custom-file-label" for="customFile">Pilih file</label>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white tx-montserrat tx-semibold"
                            data-dismiss="modal">Batal</button>
                        <input type="submit" class="btn btn-its tx-montserrat tx-semibold" value="Ya">
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- Modal hapus --}}
    <div class="modal fade" id="hapus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content tx-14">
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Konfirmasi</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ url('pegawai/pengajuan/hapus-berkas') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" id="id" name="id_pengajuan">
                        <p>Apakah Anda yakin ingin menghapus berkas ini?</p>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white tx-montserrat tx-semibold"
                            data-dismiss="modal">Batal</button>
                        <input type="submit" class="btn btn-its tx-montserrat tx-semibold" value="Ya">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
    <script src="{{ asset('lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
    <script src="{{ asset('lib/select2/js/select2.min.js') }}"></script>
    <script src="{{ asset('lib/ion-rangeslider/js/ion.rangeSlider.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            let table = $('#mydatatable').DataTable({
                processing: true,
                serverSide: true,
                language: {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Indonesian.json"
                },
                ajax: {
                    url: "{{ url('pegawai/pengajuan/server-side') }}",
                    data: function(data) {
                        data.id_unit = $('#filter_unit').val();
                        data.id_status = $('#filter_status').val();
                    }
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                        class: 'text-center'
                    },
                    {
                        data: 'nomor_surat',
                        name: 'nomor_surat',
                        class: 'text-center'
                    },
                    {
                        data: 'nik',
                        name: 'nik'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'unit',
                        name: 'unit'
                    },
                    {
                        data: 'status',
                        name: 'status',
                        class: 'text-center'
                    },
                    {
                        data: 'aksi',
                        name: 'aksi',
                        class: 'text-center'
                    },
                ],
            });

            $('#btn-filter').click(function() {
                console.log($('#filter_unit').val());
                table.ajax.reload();
            });
            $('#btn-reset').click(function() {
                document.getElementById("filter_unit").value = '';
                document.getElementById("filter_status").value = '';
                table.ajax.reload();
            });

            $('#filter_unit').select2({
                placeholder: 'Pilih Unit',
                searchInputPlaceholder: 'Search options'
            });
            $('#filter_status').select2({
                placeholder: 'Pilih Status',
                searchInputPlaceholder: 'Search options'
            });

            $('#upload').on('show.bs.modal', function(event) {
                let button = $(event.relatedTarget) // Button that triggered the modal
                let id = button.data('id')
                var modal = $(this)
                modal.find('.modal-content #id').val(id);
            });
            $('#hapus').on('show.bs.modal', function(event) {
                let button = $(event.relatedTarget) // Button that triggered the modal
                let id = button.data('id')
                var modal = $(this)
                modal.find('.modal-content #id').val(id);
            });



        });
    </script>
@endsection
