@extends('Ui::base')

@section('title')
    Pengajuan Pegawai
@endsection

@section('header_title')
    Pengajuan Pegawai
@endsection
@section('breadcrumbs')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-style1 mg-b-10">
            <li class="breadcrumb-item"><a href="#">
                    @if (isset($page['menu'])){{ ucwords($page['menu']) }}@endif
                </a></li>
            <li class="breadcrumb-item"><a href="#">
                    @if (isset($page['submenu'])){{ ucwords($page['submenu']) }}
                    @endif
                </a></li>
            <li class="breadcrumb-item active" aria-current="page">
                @if (isset($page['halaman'])){{ ucwords($page['halaman']) }}@endif
            </li>
        </ol>
    </nav>
@endsection
@section('header_right')
    <div class="d-md-block">
        <a class="btn btn-white tx-montserrat tx-semibold" href="{{ url('pegawai/pengajuan') }}"><i
                data-feather="arrow-left" class="wd-10 mg-r-5"></i> Kembali</a>
    </div>
@endsection
@section('content')
    <div class="col-sm-12 col-lg-12">
        @if (session('success'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ session('success') }}
            </div>
        @elseif(session('failed'))
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ session('failed') }}
            </div>
        @endif
        <div class="card card-body">
            <div class="row">
                <div class="col-lg-9 mg-t-5">
                    <div class="col-lg-12 pd-x-0">
                        <div class="card card-body">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                        aria-controls="home" aria-selected="true">Data Diri</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#tanggungan"
                                        role="tab" aria-controls="profile" aria-selected="false">Tanggungan Keluarga</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                                        aria-controls="profile" aria-selected="false">Riwayat Kontrak Pekerjaan</a>
                                </li>
                            </ul>
                            <div class="tab-content bd bd-gray-300 bd-t-0 pd-20" id="myTabContent">
                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    {{-- <h6>Data Diri</h6> --}}
                                    <div class="row justify-content-center">
                                        <div class="col-12 mg-b-30 mg-t-20">
                                            <div class="avatar avatar-xxl avatar-online text-center"><img
                                                    src="https://storage-api.its.ac.id/{{ $data?->public_link_pas_foto }}"
                                                    class="rounded-circle" alt="">
                                            </div>
                                        </div><!-- col -->
                                    </div>
                                    <div id="baru">
                                        <div class="form-row">
                                            <div class="form-group col-md-8">
                                                <label for="nama">Nama Lengkap</label>
                                                <input type="text" class="form-control" id="nama" name="nama"
                                                    value="{{ $data->nama }}" readonly placeholder="Nama lengkap">
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="nik">NIK</label>
                                                <input type="text" class="form-control" id="nik" name="nik"
                                                    value="{{ $data->nik }}" readonly placeholder="NIK">
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-4">
                                                <label for="nama">Tanggal Lahir</label>
                                                <input type="text" class="form-control" id="tgl_lahir" name="tgl_lahir"
                                                    value="{{ date('d-m-Y', strtotime($data->tgl_lahir)) }}" readonly>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="jenis_kelamin">Jenis Kelamin</label>
                                                <input type="text" class="form-control" id="" name="tgl_lahir"
                                                    value="@if ($data->jenis_kelamin == 'L')Laki-laki @else Perempuan @endif" readonly>

                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="pendidikan_terakhir">Pendidikan Terakhir</label>
                                                <input type="text" class="form-control tx-uppercase" id="pendidikan_id"
                                                    name="tgl_lahir" value="{{ $data->id_pendidikan }}" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="alamat">Alamat</label>
                                            <input type="text" class="form-control" id="alamat" name="alamat"
                                                value="{{ $data->alamat }}" readonly>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="email">Email</label>
                                                <input type="email" class="form-control" id="email" name="email"
                                                    value="{{ $data->email }}" readonly placeholder="Email">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="hp">Nomor HP</label>
                                                <input type="text" class="form-control" id="hp" name="hp"
                                                    value="{{ $data->hp }}" readonly placeholder="Nomor HP">
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="email">Unit</label>
                                                <input type="text" class="form-control" id="email" name="email"
                                                    value="{{ $data->unit }}" readonly placeholder="Email">
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="honor">Honor</label>
                                                <input type="number" class="form-control" id="honor" name="honor"
                                                    value="{{ $data->honor }}" readonly>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="uang_makan">Uang Makan</label>
                                                <input type="number" class="form-control" id="uang_makan"
                                                    value="{{ $data->uang_makan }}" readonly name="uang_makan"
                                                    placeholder="Uang Makan">
                                            </div>

                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="hp">Jenis Pengajuan</label>
                                                <input type="text" class="form-control" id="hp" name="hp"
                                                    value="{{ $data->jenis_pengajuan }}" readonly>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="hp">Status Pengajuan</label>
                                                <input type="text" class="form-control" id="hp" name="hp"
                                                    value="@if ($data->id_status_pengajuan == 'O')Baru @else Perpanjangan @endif" readonly>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-4">
                                                <label for="tanggal_pengajuan">Tanggal Pengajuan</label>
                                                <input type="text" class="form-control" id="tgl_pengajuan"
                                                    name="tgl_pengajuan"
                                                    value="{{ date('d-m-Y', strtotime($data->tgl_pengajuan)) }}"
                                                    readonly>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="email">Tanggal Mulai Kerja</label>
                                                <input type="text" class="form-control" id="honor" name="honor"
                                                    value="{{ date('d-m-Y', strtotime($data->tgl_mulai_kerja)) }}"
                                                    placeholder="Tanggal Mulai Kerja" readonly>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="hp">Tanggal Berakhir Kontrak Kerja</label>
                                                <input type="text" class="form-control" id="uang_makan"
                                                    value="{{ date('d-m-Y', strtotime($data->tgl_akhir_kerja)) }}"
                                                    readonly>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <label for="tanggal_pengajuan">Rincian Tugas / Deskripsi Tugas</label>
                                                @foreach ($rincian_tugas as $item)
                                                    <li>{{ $item->deskripsi }}</li>
                                                @endforeach
                                                <p class="mg-l-30"></p>
                                            </div>
                                        </div>
                                        <hr class="mg-t-10">

                                        <h4 id="section2" class="mg-b-30">Berkas</h4>
                                        <div class="form-row">
                                            @if($data->id_ijazah != NULL)
                                            <div class="col-md-6 mg-t-10">
                                                <div
                                                    class="card card-body bd pd-y-15 pd-x-15 pd-sm-x-20 rounded-its-10 d-flex justify-content-between">
                                                    <div class="media d-flex align-items-center">
                                                        <img src="@if($data->mime_type_ijazah == 'application/pdf') {{ asset('assets/img/format-pdf.svg') }} @else {{ asset('assets/img/format-gambar.svg') }} @endif"
                                                            class="wd-40 wd-md-40 wd-lg-40 mg-r-15">
                                                        <div class="media-body">
                                                            <a href="@if($data->mime_type_ijazah == 'application/pdf') {{url('pegawai/show_pdf')}}/{{$data->id_ijazah}} @else{{ route('preview-berkas-pendukung', ['id_berkas' => $data->id_ijazah]) }} @endif"
                                                                target="_blank"
                                                                class="tx-poppins tx-medium tx-color-01 mg-b-0 crop-text-1"
                                                                title="File berkas dsfsdf dsfsfse dfsg.pdf">Unduh Ijazah</a>
                                                            <p class="tx-13 tx-color-03 mg-b-0 crop-text-1"
                                                                title="Muncul keterangan di sini.">
                                                                {{ $data?->created_at }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 mg-t-10">
                                                <div
                                                    class="card card-body bd pd-y-15 pd-x-15 pd-sm-x-20 rounded-its-10 d-flex justify-content-between">
                                                    <div class="media d-flex align-items-center">
                                                        <img src="@if($data->mime_type_ktp == 'application/pdf') {{ asset('assets/img/format-pdf.svg') }} @else {{ asset('assets/img/format-gambar.svg') }} @endif"
                                                            class="wd-40 wd-md-40 wd-lg-40 mg-r-15">
                                                        <div class="media-body">
                                                            <a href="@if($data->mime_type_ktp == 'application/pdf') {{url('pegawai/show_pdf')}}/{{$data->id_ktp}} @else{{ route('preview-berkas-pendukung', ['id_berkas' => $data->id_ktp]) }} @endif"
                                                                target="_blank"
                                                                class="tx-poppins tx-medium tx-color-01 mg-b-0 crop-text-1"
                                                                title="File berkas dsfsdf dsfsfse dfsg.pdf">Unduh KTP/KTM</a>
                                                            <p class="tx-13 tx-color-03 mg-b-0 crop-text-1"
                                                                title="Muncul keterangan di sini.">
                                                                {{ $data?->created_at }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 mg-t-10">
                                                <div
                                                    class="card card-body bd pd-y-15 pd-x-15 pd-sm-x-20 rounded-its-10 d-flex justify-content-between">
                                                    <div class="media d-flex align-items-center">
                                                        <img src="@if($data->mime_type_pas_foto == 'application/pdf') {{ asset('assets/img/format-pdf.svg') }} @else {{ asset('assets/img/format-gambar.svg') }} @endif"
                                                            class="wd-40 wd-md-40 wd-lg-40 mg-r-15">
                                                        <div class="media-body">
                                                            <a href="@if($data->mime_type_pas_foto == 'application/pdf') {{url('pegawai/show_pdf')}}/{{$data->id_pas_foto}} @else{{ route('preview-berkas-pendukung', ['id_berkas' => $data->id_pas_foto]) }} @endif"
                                                                target="_blank"
                                                                class="tx-poppins tx-medium tx-color-01 mg-b-0 crop-text-1"
                                                                title="File berkas dsfsdf dsfsfse dfsg.pdf">Unduh Pas Foto</a>
                                                            <p class="tx-13 tx-color-03 mg-b-0 crop-text-1"
                                                                title="Muncul keterangan di sini.">
                                                                {{ $data?->created_at }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @else
                                            <div class="col-md-6 mg-t-10">
                                                <div
                                                    class="card card-body bd pd-y-15 pd-x-15 pd-sm-x-20 rounded-its-10 d-flex justify-content-between">
                                                    <div class="media d-flex align-items-center">
                                                        <img src="{{ asset('assets/img/format-gambar.svg') }}"
                                                            class="wd-40 wd-md-40 wd-lg-40 mg-r-15">
                                                        <div class="media-body">
                                                            <a href="{{ route('unduh-berkas-pendukung', ['id_pengajuan' => $data->id_pengajuan, 'kategori' => 'public_link_ijazah']) }}"
                                                                target="_blank"
                                                                class="tx-poppins tx-medium tx-color-01 mg-b-0 crop-text-1"
                                                                title="File berkas dsfsdf dsfsfse dfsg.pdf">Unduh Ijazah</a>
                                                            <p class="tx-13 tx-color-03 mg-b-0 crop-text-1"
                                                                title="Muncul keterangan di sini.">
                                                                {{ $data?->created_at }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 mg-t-10">
                                                <div
                                                    class="card card-body bd pd-y-15 pd-x-15 pd-sm-x-20 rounded-its-10 d-flex justify-content-between">
                                                    <div class="media d-flex align-items-center">
                                                        <img src="{{ asset('assets/img/format-gambar.svg') }}"
                                                            class="wd-40 wd-md-40 wd-lg-40 mg-r-15">
                                                        <div class="media-body">
                                                            <a href="{{ route('unduh-berkas-pendukung', ['id_pengajuan' => $data->id_pengajuan, 'kategori' => 'public_link_ktp']) }}"
                                                                target="_blank"
                                                                class="tx-poppins tx-medium tx-color-01 mg-b-0 crop-text-1"
                                                                title="File berkas dsfsdf dsfsfse dfsg.pdf">Unduh KTP/KTM</a>
                                                            <p class="tx-13 tx-color-03 mg-b-0 crop-text-1"
                                                                title="Muncul keterangan di sini.">
                                                                {{ $data?->created_at }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 mg-t-10">
                                                <div
                                                    class="card card-body bd pd-y-15 pd-x-15 pd-sm-x-20 rounded-its-10 d-flex justify-content-between">
                                                    <div class="media d-flex align-items-center">
                                                        <img src="{{ asset('assets/img/format-gambar.svg') }}"
                                                            class="wd-40 wd-md-40 wd-lg-40 mg-r-15">
                                                        <div class="media-body">
                                                            <a href="{{ route('unduh-berkas-pendukung', ['id_pengajuan' => $data->id_pengajuan, 'kategori' => 'public_link_pas_foto']) }}"
                                                                target="_blank"
                                                                class="tx-poppins tx-medium tx-color-01 mg-b-0 crop-text-1"
                                                                title="File berkas dsfsdf dsfsfse dfsg.pdf">Unduh Pas Foto</a>
                                                            <p class="tx-13 tx-color-03 mg-b-0 crop-text-1"
                                                                title="Muncul keterangan di sini.">
                                                                {{ $data?->created_at }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif

                                        </div>

                                    </div>
                                </div>
                                <div class="tab-pane fade" id="tanggungan" role="tabpanel" aria-labelledby="profile-tab">
                                    <h6>Tanggungan Keluarga</h6>
                                    <table class="table table-bordered">
                                        <tr class="text-center">
                                            <th>No</th>
                                            <th>Nama</th>
                                            <th>Status</th>
                                            <th>Tanggal Lahir</th>
                                        </tr>
                                        @php
                                            $i = 1;
                                        @endphp
                                        @foreach ($tanggungan as $item)
                                            <tr class="tx-center">

                                                <td>{{ $i++ }}</td>
                                                <td class="tx-left">{{ $item->nama }}</td>
                                                <td>{{ $item->status }}</th>
                                                <td>{{ date('d-m-Y', strtotime($item->tgl_lahir)) }}</td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                    <h6>Riwayat Pekerjaan</h6>
                                    <p class="tx-italic mg-t-20">Belum ada riwayat kontrak pekerjaan</p>
                                    <table class="table table-bordered">
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 mg-t-10">
                    <div class="card card-body pd-x-5 pd-y-5">
                        <div class="card-content">
                            <div class="card-header">
                                <label class="tx-montserrat tx-medium tx-uppercase tx-center">Status
                                    Pengajuan</label>
                            </div>
                            <div class="card-body">

                                <div class="row form-row mg-t-7">
                                    <div class="col-md-12">
                                        @if (is_null($data->tgl_nonaktif))
                                            {{-- Administrator --}}
                                            @if (sso()->user()->getActiveRole()->getName() == 'Administrator' && $data->is_approved0 != '1')
                                                <p class="font-italic text-danger">Menunggu persetujuan dari Kepala Unit
                                                </p>
                                            @elseif(sso()->user()->getActiveRole()->getName() == 'Administrator' && $data->is_approved0 == '1')
                                                @if (sso()->user()->getActiveRole()->getName() == 'Administrator' && $data->is_approved1 == '1')
                                                    <p class="font-italic">Telah disetujui oleh : <br><span
                                                            class="font-weight-bold">{{ $data->name_approved0 }}</span><br><small
                                                            class="tx-secondary">pada :
                                                            {{ date('d-m-Y', strtotime($data->date_approved0)) }}</small>
                                                    </p>
                                                    <p class="font-italic">dan <br><span
                                                            class="font-weight-bold">{{ $data->name_approved1 }}</span><br><small
                                                            class="tx-secondary">pada :
                                                            {{ date('d-m-Y', strtotime($data->date_approved1)) }}</small>
                                                    </p>
                                                    <hr class="mg-t-10">
                                                    <br>
                                                    <label class="tx-montserrat tx-medium tx-uppercase tx-center">Riwayat Perbaikan</label>
                                                    @if(!is_null($data->alasan0) || !is_null($data->alasan1))
                                                        <p class="font-italic">Instruksi untuk melakukan perbaikan oleh :
                                                            @if(!is_null($data->alasan0))
                                                                <br><span class="font-weight-bold">{{ $data->name_approved0 }}</span><br><small class="tx-secondary">pada :{{ date('d-m-Y', strtotime($data->date_approved0)) }}</small><br><small>Alasan:<br> {{ $data->alasan0 }}</small>
                                                            @endif
                                                            @if(!is_null($data->alasan1))
                                                                <br><span class="font-weight-bold">{{ $data->name_approved1 }}</span><br><small class="tx-secondary">pada :{{ date('d-m-Y', strtotime($data->date_approved1)) }}</small><br><small>Alasan:<br> {{ $data->alasan1 }}</small>
                                                            @endif
                                                        </p>
                                                    @else
                                                        <p class="font-italic">Ajuan tidak pernah direvisi</p> 
                                                    @endif
                                                @elseif(sso()->user()->getActiveRole()->getName() == 'Administrator' && $data->is_approved1 == '0')
                                                    <p class="font-italic">Permohonan ditolak oleh : <br><span
                                                            class="font-weight-bold">{{ $data->name_approved1 }}</span><br><small
                                                            class="tx-secondary">pada :
                                                            {{ date('d-m-Y', strtotime($data->date_approved1)) }}</small><br><br><small>Alasan
                                                            :<br> {{ $data->alasan1 }}</small>
                                                    </p>
                                                @elseif(sso()->user()->getActiveRole()->getName() == 'Administrator' && $data->is_approved1 == '2')
                                                    <p class="font-italic">Instruksi untuk melakukan perbaikan oleh :
                                                        <br><span
                                                            class="font-weight-bold">{{ $data->name_approved1 }}</span><br><small
                                                            class="tx-secondary">pada :
                                                            {{ date('d-m-Y', strtotime($data->date_approved1)) }}</small><br><br><small>Alasan
                                                            :<br> {{ $data->alasan1 }}</small>
                                                    </p>
                                                @endif
                                            @endif

                                            {{-- Kepala Kantor --}}
                                            @if (Session::get('is_validator') && $data->is_approved0 == null)
                                                <!-- belum divalidasi-->
                                                <div class="form-group row">
                                                    <a href="#setuju" data-toggle="modal"
                                                        class="btn btn-xs btn-its tx-montserrat tx-semibold btn-block">Setuju</a>
                                                    <a href="#perbaikan" data-toggle="modal"
                                                        class="btn btn-xs btn-warning tx-semibold tx-montserrat btn-block">
                                                        Perbaikan</a>
                                                    <a href="#tidak_setuju" data-toggle="modal"
                                                        class="btn btn-xs btn-danger tx-semibold tx-montserrat btn-block">Tidak
                                                        Setuju</a>
                                                </div>
                                            @else
                                                <!--validator beraksi -->
                                                @if (Session::get('is_validator') && $data->is_approved0 == '1')
                                                    <!--disetujui -->
                                                    <p class="font-italic">Telah disetujui oleh : <br><span
                                                            class="font-weight-bold">{{ $data->name_approved0 }}</span><br><small
                                                            class="tx-secondary">pada :
                                                            {{ date('d-m-Y', strtotime($data->date_approved0)) }}</small>
                                                    </p>
                                                    <p class="font-italic">dan <br><span
                                                            class="font-weight-bold">{{ $data->name_approved1 }}</span><br><small
                                                            class="tx-secondary">pada :
                                                            {{ date('d-m-Y', strtotime($data->date_approved1)) }}</small>
                                                    </p>
                                                    <hr class="mg-t-10">
                                                    <br>
                                                    <label class="tx-montserrat tx-medium tx-uppercase tx-center">Riwayat Perbaikan</label>
                                                    @if(!is_null($data->alasan0) || !is_null($data->alasan1))
                                                        <p class="font-italic">Instruksi untuk melakukan perbaikan oleh :
                                                            @if(!is_null($data->alasan0))
                                                                <br><span class="font-weight-bold">{{ $data->name_approved0 }}</span><br><small class="tx-secondary">pada :{{ date('d-m-Y', strtotime($data->date_approved0)) }}</small><br><small>Alasan:<br> {{ $data->alasan0 }}</small>
                                                            @endif
                                                            @if(!is_null($data->alasan1))
                                                                <br><span class="font-weight-bold">{{ $data->name_approved1 }}</span><br><small class="tx-secondary">pada :{{ date('d-m-Y', strtotime($data->date_approved1)) }}</small><br><small>Alasan:<br> {{ $data->alasan1 }}</small>
                                                            @endif
                                                        </p>
                                                    @else
                                                        <p class="font-italic">Ajuan tidak pernah direvisi</p> 
                                                    @endif
                                                @elseif(Session::get('is_validator') && $data->is_approved0 == '0')
                                                    <!-- tidak disetujui / butuh revisi admin unit -->
                                                    <p class="font-italic">Permohonan ditolak oleh : <br>
                                                        <span
                                                            class="font-weight-bold">{{ $data->name_approved0 }}</span><br>
                                                        <small class="tx-secondary">pada :
                                                            {{ date('d-m-Y', strtotime($data->date_approved0)) }}</small><br><br>
                                                        <small>Alasan :<br> {{ $data->alasan0 }}</small>
                                                    </p>
                                                    @if ($data->is_approved1 == null)
                                                        <a href="#batalkan" data-toggle="modal"
                                                            class="btn btn-xs btn-danger tx-semibold tx-montserrat btn-block">Batalkan</a>
                                                    @endif
                                                @elseif(Session::get('is_validator') && $data->is_approved0 == '2')
                                                    <!-- revisi-->

                                                    <p class="font-italic">Instruksi untuk melakukan perbaikan oleh :
                                                        <br><span
                                                            class="font-weight-bold">{{ $data->name_approved0 }}</span><br>
                                                        <small class="tx-secondary">pada :
                                                            {{ date('d-m-Y', strtotime($data->date_approved0)) }}</small><br><br>
                                                        <small>Alasan :<br> {{ $data->alasan0 }}</small>

                                                    </p>

                                                    @if ($data->alasan0 != null && $data->date_approved0 < $data->updated_at)
                                                        <small>Telah dilakukan perbaikan</small>
                                                        <small class="tx-secondary">pada :
                                                            {{ date('d-m-Y H:i', strtotime($data->updated_at)) }}</small><br><br>
                                                        </p>
                                                    @endif

                                                    <a href="#batalkan" data-toggle="modal"
                                                        class="btn btn-xs btn-danger tx-semibold tx-montserrat btn-block">Batalkan</a>
                                                    <a href="#setuju" data-toggle="modal"
                                                        class="btn btn-xs btn-its tx-montserrat tx-semibold btn-block">Setuju</a>

                                                @endif
                                            @endif
                                        @else
                                            <p class="font-italic">Telah dinonaktifkan pada : <br>
                                                <small class="tx-secondary">pada :
                                                    {{ date('d-m-Y', strtotime($data->tgl_nonaktif)) }}</small>
                                            </p>
                                        @endif

                                        {{-- Super Administrator --}}
                                        @if (sso()->user()->getActiveRole()->getName() == 'Super Administrator' && $data->is_approved0 != '1')
                                            <p class="font-italic text-danger">Menunggu persetujuan dari Kepala Unit</p>
                                        @elseif(sso()->user()->getActiveRole()->getName() == 'Super Administrator' && $data->is_approved0 == '1')
                                            @if (sso()->user()->getActiveRole()->getName() == 'Super Administrator' && $data->is_approved1 != '1')

                                                <div class="form-group row">
                                                    <a href="#setuju" data-toggle="modal"
                                                        class="btn btn-xs btn-its tx-montserrat tx-semibold btn-block">Setuju</a>
                                                    <a href="#perbaikan" data-toggle="modal"
                                                        class="btn btn-xs btn-warning tx-semibold tx-montserrat btn-block">
                                                        Perbaikan</a>
                                                    <a href="#tidak_setuju" data-toggle="modal"
                                                        class="btn btn-xs btn-danger tx-semibold tx-montserrat btn-block">Tidak
                                                        Setuju</a>
                                                </div>

                                            @endif
                                            @if (sso()->user()->getActiveRole()->getName() == 'Super Administrator' && $data->is_approved1 == '1')
                                                <p class="font-italic">Telah disetujui oleh : <br><span
                                                        class="font-weight-bold">{{ $data->name_approved0 }}</span><br><small
                                                        class="tx-secondary">pada :
                                                        {{ date('d-m-Y', strtotime($data->date_approved0)) }}</small>
                                                </p>
                                                <p class="font-italic">dan <br><span
                                                        class="font-weight-bold">{{ $data->name_approved1 }}</span><br><small
                                                        class="tx-secondary">pada :
                                                        {{ date('d-m-Y', strtotime($data->date_approved1)) }}</small>
                                                </p>
                                                <a href="#batalkan" data-toggle="modal" class="btn btn-xs btn-danger tx-semibold tx-montserrat btn-block">Batalkan</a>
                                                <hr class="mg-t-10">
                                                <br>
                                                <label class="tx-montserrat tx-medium tx-uppercase tx-center">Riwayat Perbaikan</label>
                                                @if(!is_null($data->alasan0) || !is_null($data->alasan1))
                                                    <p class="font-italic">Instruksi untuk melakukan perbaikan oleh :
                                                        @if(!is_null($data->alasan0))
                                                            <br><span class="font-weight-bold">{{ $data->name_approved0 }}</span><br><small class="tx-secondary">pada :{{ date('d-m-Y', strtotime($data->date_approved0)) }}</small><br><small>Alasan:<br> {{ $data->alasan0 }}</small>
                                                        @endif
                                                        @if(!is_null($data->alasan1))
                                                            <br><span class="font-weight-bold">{{ $data->name_approved1 }}</span><br><small class="tx-secondary">pada :{{ date('d-m-Y', strtotime($data->date_approved1)) }}</small><br><small>Alasan:<br> {{ $data->alasan1 }}</small>
                                                        @endif
                                                    </p>
                                                @else
                                                    <p class="font-italic">Ajuan tidak pernah direvisi</p> 
                                                @endif
                                            @elseif(sso()->user()->getActiveRole()->getName() == 'Super Administrator' && $data->is_approved1 == '0')
                                                <p class="font-italic">Permohonan ditolak oleh : <br><span
                                                        class="font-weight-bold">{{ $data->name_approved1 }}</span><br><small
                                                        class="tx-secondary">pada :
                                                        {{ date('d-m-Y', strtotime($data->date_approved1)) }}</small><br><br><small>Alasan
                                                        :<br> {{ $data->alasan1 }}</small>
                                                </p>
                                                <a href="#batalkan" data-toggle="modal" class="btn btn-xs btn-danger tx-semibold tx-montserrat btn-block">Batalkan</a>
                                            @elseif(sso()->user()->getActiveRole()->getName() == 'Super Administrator' && $data->is_approved1 == '2')
                                                <p class="font-italic">Instruksi untuk melakukan perbaikan oleh :
                                                    <br><span
                                                        class="font-weight-bold">{{ $data->name_approved1 }}</span><br><small
                                                        class="tx-secondary">pada :
                                                        {{ date('d-m-Y', strtotime($data->date_approved1)) }}</small><br><br><small>Alasan
                                                        :<br> {{ $data->alasan1 }}</small>
                                                </p>
                                                <a href="#batalkan" data-toggle="modal"
                                                    class="btn btn-xs btn-danger tx-semibold tx-montserrat btn-block">Batalkan</a>
                                            @endif
                                        @endif

                                        {{-- Viewer --}}
                                        @if (session('is_viewer') && $data->is_approved0 != '1')
                                            <p class="font-italic text-danger">Menunggu persetujuan dari Kepala Unit</p>
                                        @elseif(session('is_viewer') && $data->is_approved0 == '1')
                                           
                                            @if (session('is_viewer') && $data->is_approved1 == '1')
                                                <p class="font-italic">Telah disetujui oleh : <br><span
                                                        class="font-weight-bold">{{ $data->name_approved0 }}</span><br><small
                                                        class="tx-secondary">pada :
                                                        {{ date('d-m-Y', strtotime($data->date_approved0)) }}</small>
                                                </p>
                                                <p class="font-italic">dan <br><span
                                                        class="font-weight-bold">{{ $data->name_approved1 }}</span><br><small
                                                        class="tx-secondary">pada :
                                                        {{ date('d-m-Y', strtotime($data->date_approved1)) }}</small>
                                                </p>
                                                <hr class="mg-t-10">
                                                <br>
                                                <label class="tx-montserrat tx-medium tx-uppercase tx-center">Riwayat Perbaikan</label>
                                                @if(!is_null($data->alasan0) || !is_null($data->alasan1))
                                                    <p class="font-italic">Instruksi untuk melakukan perbaikan oleh :
                                                        @if(!is_null($data->alasan0))
                                                            <br><span class="font-weight-bold">{{ $data->name_approved0 }}</span><br><small class="tx-secondary">pada :{{ date('d-m-Y', strtotime($data->date_approved0)) }}</small><br><small>Alasan:<br> {{ $data->alasan0 }}</small>
                                                        @endif
                                                        @if(!is_null($data->alasan1))
                                                            <br><span class="font-weight-bold">{{ $data->name_approved1 }}</span><br><small class="tx-secondary">pada :{{ date('d-m-Y', strtotime($data->date_approved1)) }}</small><br><small>Alasan:<br> {{ $data->alasan1 }}</small>
                                                        @endif
                                                    </p>
                                                @else
                                                    <p class="font-italic">Ajuan tidak pernah direvisi</p> 
                                                @endif
                                            @elseif(session('is_viewer') && $data->is_approved1 == '0')
                                                <p class="font-italic">Permohonan ditolak oleh : <br><span
                                                        class="font-weight-bold">{{ $data->name_approved1 }}</span><br><small
                                                        class="tx-secondary">pada :
                                                        {{ date('d-m-Y', strtotime($data->date_approved1)) }}</small><br><br><small>Alasan
                                                        :<br> {{ $data->alasan1 }}</small>
                                                </p>
                                               
                                            @elseif(session('is_viewer') && $data->is_approved1 == '2')
                                                <p class="font-italic">Instruksi untuk melakukan perbaikan oleh :
                                                    <br><span
                                                        class="font-weight-bold">{{ $data->name_approved1 }}</span><br><small
                                                        class="tx-secondary">pada :
                                                        {{ date('d-m-Y', strtotime($data->date_approved1)) }}</small><br><br><small>Alasan
                                                        :<br> {{ $data->alasan1 }}</small>
                                                </p>
                                               
                                            @endif
                                        @endif

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    @if (sso()->user()->getActiveRole()->getName() == 'Super Administrator')
                        <div class="card mg-t-10">
                            <div class="card-content">
                                <div class="card-header">
                                    <label class="tx-montserrat tx-medium tx-uppercase tx-center">Draft</label>
                                </div>

                                <div class="card-body">
                                    @if ($data->no_surat == null)
                                        <small class="font-italic text-danger">Isi nomor surat untuk mengunduh
                                            draft.</small>
                                        <a href="#isi_nomor" data-toggle="modal"
                                            class="btn btn-xs btn-its tx-semibold tx-montserrat btn-block mg-t-10"><i
                                                class="fa fa-edit"></i> Isi Nomor Surat</a>
                                    @else
                                        <a class="btn btn-xs btn-block btn-its tx-montserrat tx-semibold"
                                            href="{{ url('pegawai/pengajuan/draft') }}/{{ $data->id_pengajuan }}"><i
                                                class="fa fa-download"></i> Unduh Draft</a>
                                    @endif
                                </div>

                            </div>
                        </div>
                    @endif
                </div>


            </div>
        </div>

    </div>
    {{-- Modal setuju --}}
    <div class="modal fade" id="setuju" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content tx-14">
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Konfirmasi</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ url('pegawai/pengajuan/approve') }}">
                    @csrf
                    <div class="modal-body">
                        <p class="mg-b-0">Apakah Anda yakin untuk menyetujui pengajuan ini? </p>
                        <input type="hidden" value="{{ $data->id_pengajuan }}" name="id_pengajuan">
                        <input type="hidden" value="setuju" name="status">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white tx-montserrat tx-semibold"
                            data-dismiss="modal">Batal</button>
                        <input type="submit" class="btn btn-its tx-montserrat tx-semibold" value="Ya">
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- Modal perbaikan --}}
    <div class="modal fade" id="perbaikan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content tx-14">
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Konfirmasi</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ url('pegawai/pengajuan/approve') }}">
                    @csrf
                    <div class="modal-body">
                        <p class="mg-b-20 tx-semibold">Apakah Anda yakin untuk menyuruh melakukan perbaikan pada pengajuan
                            ini? </p>
                        <input type="hidden" value="{{ $data->id_pengajuan }}" name="id_pengajuan">
                        <input type="hidden" value="perbaikan" name="status">
                        <div class="form-group">
                            <label for="alasan" class="d-block">Masukkan Alasan</label>
                            <textarea name="alasan" class="form-control" placeholder="Harap jelaskan secara detail perbaikan yang harus dilakukan" required></textarea>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white tx-montserrat tx-semibold"
                            data-dismiss="modal">Batal</button>
                        <input type="submit" class="btn btn-its tx-montserrat tx-semibold" value="Ya">
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- Modal tidak setuju --}}
    <div class="modal fade" id="tidak_setuju" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content tx-14">
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Konfirmasi</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ url('pegawai/pengajuan/approve') }}">
                    @csrf
                    <div class="modal-body">
                        <p class="mg-b-20 tx-semibold">Apakah Anda yakin untuk tidak menyetujui pengajuan ini? </p>
                        <input type="hidden" value="{{ $data->id_pengajuan }}" name="id_pengajuan">
                        <input type="hidden" value="tidak_setuju" name="status">
                        <div class="form-group">
                            <label for="alasan" class="d-block">Masukkan Alasan</label>
                            <textarea name="alasan" class="form-control" required></textarea>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white tx-montserrat tx-semibold"
                            data-dismiss="modal">Batal</button>
                        <input type="submit" class="btn btn-its tx-montserrat tx-semibold" value="Ya">
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- Modal batalkan --}}
    <div class="modal fade" id="batalkan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content tx-14">
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Konfirmasi</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ url('pegawai/pengajuan/approve') }}">
                    @csrf
                    <div class="modal-body">
                        <p class="mg-b-0">Apakah Anda yakin untuk membatalkan persetujuan ini? </p>
                        <input type="hidden" value="{{ $data->id_pengajuan }}" name="id_pengajuan">
                        <input type="hidden" value="batalkan" name="status">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white tx-montserrat tx-semibold"
                            data-dismiss="modal">Batal</button>
                        <input type="submit" class="btn btn-its tx-montserrat tx-semibold" value="Ya">
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- Isi Nomor Surat --}}
    <div class="modal fade" id="isi_nomor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content tx-14">
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Masukkan Nomor Surat</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ url('pegawai/pengajuan/isi-nomor-surat') }}">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" value="{{ $data->id_pengajuan }}" name="id_pengajuan">
                        <select class="custom-select" id="nomor_surat" name="nomor_surat" required>
                            <option value="">--- Pilih Nomor Surat ---</option>
                            @php
                                $nomor_surat = DB::table('nomor_surat')
                                    ->where('is_used', 0)
                                    ->get();
                            @endphp
                            @foreach ($nomor_surat as $item)
                                <option value="{{ $item->nomor_surat }}">{{ $item->nomor_surat }}</option>
                            @endforeach
                        </select>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white tx-montserrat tx-semibold"
                            data-dismiss="modal">Batal</button>
                        <input type="submit" class="btn btn-its tx-montserrat tx-semibold" value="Ya">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

@endsection
