@extends('Ui::base')

@section('title')
    Pengajuan Pegawai
@endsection

@section('header_title')
    Pengajuan Pegawai
@endsection
@section('breadcrumbs')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-style1 mg-b-10">
            <li class="breadcrumb-item"><a href="#">
                    @if (isset($page['menu'])){{ ucwords($page['menu']) }}@endif
                </a></li>
            <li class="breadcrumb-item"><a href="#">
                    @if (isset($page['submenu'])){{ ucwords($page['submenu']) }}
                    @endif
                </a></li>
            <li class="breadcrumb-item active" aria-current="page">
                @if (isset($page['halaman'])){{ ucwords($page['halaman']) }}@endif
            </li>
        </ol>
    </nav>
@endsection
@section('header_right')
    <div class="d-md-block">
        <a class="btn btn-white tx-montserrat tx-semibold" href="{{ url('pegawai/pengajuan') }}"><i
                data-feather="arrow-left" class="wd-10 mg-r-5"></i> Kembali</a>
    </div>
@endsection
@section('content')
    <div class="col-sm-12 col-lg-12">
        @if (session('success'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ session('success') }}
            </div>
        @elseif(session('failed'))
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ session('failed') }}
            </div>
        @endif
        <div class="card card-body">
            <div class="row">
                <div class="col-lg-12 mg-t-5">
                    <div class="col-lg-12 pd-x-0">
                        <div class="card card-body col-md-12">


                            <h3 class="tx-center">Data Diri</h3>
                            <div class="row justify-content-center">
                                <div class="col-12 mg-b-30 mg-t-20">
                                    <div class="avatar avatar-xxl avatar-online text-center"><img
                                            src="https://storage-api-dev.its.ac.id/{{ $data->public_link_pas_foto }}"
                                            class="rounded-circle" alt="">
                                    </div>
                                </div><!-- col -->
                            </div>
                            <form method="post" action="{{ url('pegawai/pengajuan/update') }}"
                                enctype="multipart/form-data" id="form" class="parsley-style-1" data-parsley-validate
                                novalidate>
                                @csrf
                                <div id="baru">
                                    <input type="hidden" class="form-control" id="id_pengajuan" name="id_pengajuan"
                                        value="{{ $data->id_pengajuan }}">
                                    <input type="hidden" class="form-control" id="nik_lama" name="nik_lama"
                                        value="{{ $data->nik }}">
                                    <div class="form-row">
                                        <div class="form-group col-md-8">
                                            <label for="nama">Nama Lengkap</label>
                                            <input type="text" class="form-control" id="nama" name="nama"
                                                value="{{ $data->nama }}" placeholder="Nama lengkap">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="nik">NIK</label>
                                            <input type="text" class="form-control" id="nik" name="nik"
                                                value="{{ $data->nik }}" placeholder="NIK">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="nama">Tanggal Lahir</label>
                                            <input type="text" class="form-control" id="tgl_lahir" name="tgl_lahir"
                                                value="{{ date('d-m-Y', strtotime($data->tgl_lahir)) }}">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="jenis_kelamin">Jenis Kelamin</label>
                                            <select class="custom-select" id="jenis_kelamin" name="jenis_kelamin" required>
                                                <option value="L" {{ $data->jenis_kelamin == 'L' ? 'selected' : '' }}>
                                                    Laki-laki</option>
                                                <option value="P" {{ $data->jenis_kelamin == 'P' ? 'selected' : '' }}>
                                                    Perempuan</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="pendidikan_terakhir">Pendidikan Terakhir</label>
                                            <select class="custom-select" id="id_pendidikan" name="id_pendidikan" required>
                                                <option value="sd" {{ $data->id_pendidikan == 'sd' ? 'selected' : '' }}>
                                                    SD</option>
                                                <option value="smp"
                                                    {{ $data->id_pendidikan == 'smp' ? 'selected' : '' }}>
                                                    SMP</option>
                                                <option value="sma"
                                                    {{ $data->id_pendidikan == 'sma' ? 'selected' : '' }}>
                                                    SMA/MA/SMK</option>
                                                <option value="d1" {{ $data->id_pendidikan == 'd1' ? 'selected' : '' }}>
                                                    D1</option>
                                                <option value="d2" {{ $data->id_pendidikan == 'd2' ? 'selected' : '' }}>
                                                    D2</option>
                                                <option value="d3" {{ $data->id_pendidikan == 'd3' ? 'selected' : '' }}>
                                                    D3</option>
                                                <option value="d4" {{ $data->id_pendidikan == 'd4' ? 'selected' : '' }}>
                                                    D4</option>
                                                <option value="s1" {{ $data->id_pendidikan == 's1' ? 'selected' : '' }}>
                                                    S1</option>
                                                <option value="s2" {{ $data->id_pendidikan == 's2' ? 'selected' : '' }}>
                                                    S2</option>
                                                <option value="s3" {{ $data->id_pendidikan == 's3' ? 'selected' : '' }}>
                                                    S3</option>


                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="alamat">Alamat</label>
                                        <input type="text" class="form-control" id="alamat" name="alamat"
                                            value="{{ $data->alamat }}">
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="email">Email</label>
                                            <input type="email" class="form-control" id="email" name="email"
                                                value="{{ $data->email }}" placeholder="Email">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="hp">Nomor HP</label>
                                            <input type="text" class="form-control" id="hp" name="hp"
                                                value="{{ $data->hp }}" placeholder="Nomor HP">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="email">Unit</label>
                                            <input type="text" class="form-control" id="id_unit" name="id_unit"
                                                value="{{ $data->unit }}" readonly>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="honor">Honor</label>
                                            <input type="number" class="form-control" id="honor" name="honor"
                                                value="{{ $data->honor }}">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="uang_makan">Uang Makan</label>
                                            <input type="number" class="form-control" id="uang_makan"
                                                value="{{ $data->uang_makan }}" name="uang_makan"
                                                placeholder="Uang Makan">
                                        </div>

                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="hp">Jenis Pengajuan</label>
                                            <select class="custom-select" id="id_jenis_pengajuan" name="id_jenis_pengajuan"
                                                required>
                                                <option value="kontrak"
                                                    {{ $data->id_jenis_pengajuan == 'kontrak' ? 'selected' : '' }}>
                                                    Tenaga Kontrak</option>
                                                <option value="magang"
                                                    {{ $data->id_jenis_pengajuan == 'magang' ? 'selected' : '' }}>
                                                    Magang Berbayar</option>
                                                <option value="thl"
                                                    {{ $data->id_jenis_pengajuan == 'thl' ? 'selected' : '' }}>
                                                    THL (Tenaga Harian Lepas)</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="hp">Status Pengajuan</label>
                                            <select class="custom-select" id="id_status_pengajuan"
                                                name="id_status_pengajuan" required>
                                                <option value="0"
                                                    {{ $data->id_status_pengajuan == '0' ? 'selected' : '' }}>
                                                    Baru</option>
                                                <option value="1"
                                                    {{ $data->id_status_pengajuan == '1' ? 'selected' : '' }}>
                                                    Perpanjangan</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="tanggal_pengajuan">Status BPJS</label>
                                            <select class="custom-select" id="id_status_bpjs" name="id_status_bpjs">
                                                <option value="0" {{ $data->id_status_bpjs == '0' ? 'selected' : '' }}>
                                                    BPJS PBI (Penerima Bantuan Pemerintah)</option>
                                                <option value="1" {{ $data->id_status_bpjs == '1' ? 'selected' : '' }}>
                                                    BPJS NON PBI (Mandiri)</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="nomor_bpjs">Nomor BPJS</label>
                                            <input type="number" class="form-control" id="nomor_bpjs" name="nomor_bpjs"
                                                value="{{ $data->nomor_bpjs }}">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="tanggal_pengajuan">Tanggal Pengajuan</label>
                                            <input type="text" class="form-control" id="tgl_pengajuan"
                                                name="tgl_pengajuan"
                                                value="{{ date('d-m-Y', strtotime($data->tgl_pengajuan)) }}">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="email">Tanggal Mulai Kerja</label>
                                            <input type="text" class="form-control" id="tgl_mulai_kerja"
                                                name="tgl_mulai_kerja"
                                                value="{{ date('d-m-Y', strtotime($data->tgl_mulai_kerja)) }}"
                                                placeholder="Tanggal Mulai Kerja">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="hp">Tanggal Berakhir Kontrak Kerja</label>
                                            <input type="text" class="form-control" id="tgl_akhir_kerja"
                                                name="tgl_akhir_kerja"
                                                value="{{ date('d-m-Y', strtotime($data->tgl_akhir_kerja)) }}">
                                        </div>
                                    </div>
                                    <div class="form-group row col-md-9">
                                        <label for="inputEmail3" class="col-sm-3 col-form-label">Ijazah</label>
                                        <div class="col-sm-9">
                                            <input type="file" class="form-control" id="ijazah" name="ijazah"
                                                required><br>
                                            <a href="{{ route('unduh-berkas-pendukung', ['id_pengajuan' => $data->id_pengajuan, 'kategori' => 'public_link_ijazah']) }}"
                                                target="_blank"><i class="fa fa-download"></i> Unduh Ijazah</a>
                                        </div>

                                    </div>
                                    <div class="form-group row col-md-9">
                                        <label for="inputEmail3" class="col-sm-3 col-form-label">KTP / KTM</label>
                                        <div class="col-sm-9">
                                            <input type="file" class="form-control" id="ktp" name="ktp" required><br>
                                            <a href="{{ route('unduh-berkas-pendukung', ['id_pengajuan' => $data->id_pengajuan, 'kategori' => 'public_link_ktp']) }}"
                                                target="_blank"><i class="fa fa-download"></i> Unduh KTP / KTM</a>
                                        </div>

                                    </div>
                                    <div class="form-group row col-md-9">
                                        <label for="inputEmail3" class="col-sm-3 col-form-label">Pas Foto</label>
                                        <div class="col-sm-9">
                                            <input type="file" class="form-control" id="pas_foto" name="pas_foto"
                                                required><br>
                                            <a href="{{ route('unduh-berkas-pendukung', ['id_pengajuan' => $data->id_pengajuan, 'kategori' => 'public_link_pas_foto']) }}"
                                                target="_blank"><i class="fa fa-download"></i> Unduh Pas Foto</a>
                                        </div>

                                    </div>



                                </div>
                                <input class="btn btn-its text-center" type="submit" value="Simpan">
                            </form>
                            <h3 class="tx-center mg-t-20 mg-b-20">Rincian Tugas</h3>
                            <div class="col-md-12 text-right mg-b-20">
                                <button href="#tambah-tugas" data-toggle="modal"
                                    class="btn btn-xs btn-its tx-semibold tx-montserrat">Tambahkan</button>
                            </div>
                            <table class="table table-bordered">
                                <tr class="text-center">
                                    <th>No</th>
                                    <th>Deskripsi Tugas</th>
                                    <th>Aksi</th>
                                </tr>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach ($rincian_tugas as $item)
                                    <tr class="tx-center">
                                        <td>{{ $i++ }}</td>
                                        <td class="tx-left">{{ $item->deskripsi }}</td>
                                        <td>
                                            <button data-id="{{ $item->id }}"
                                                data-deskripsi="{{ $item->deskripsi }}" data-toggle="modal"
                                                data-target="#edit-tugas" class="btn btn-xs btn-warning"><i
                                                    class="fa fa-edit"></i> Ubah</button>
                                            <button data-id="{{ $item->id }}" data-toggle="modal"
                                                data-target="#hapus-tugas" class="btn btn-xs btn-danger"><i
                                                    class="fa fa-trash"></i> Hapus</button>
                                        </td>
                                    </tr>
                                @endforeach
                                @if ($rincian_tugas->count() == null)
                                    <tr class="tx-center">
                                        <td colspan="3" class="tx-italic tx-danger">Belum ada data</td>
                                    </tr>
                                @endif
                            </table>
                            <h3 class="tx-center mg-t-20 mg-b-20">Tanggungan Keluarga</h3>
                            <div class="col-md-12 text-right mg-b-20">
                                <button href="#tambah-keluarga" data-toggle="modal"
                                    class="btn btn-xs btn-its tx-semibold tx-montserrat">Tambahkan</button>
                            </div>

                            <table class="table table-bordered">
                                <tr class="text-center">
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Status</th>
                                    <th>Tanggal Lahir</th>
                                    <th>Aksi</th>
                                </tr>
                                @php
                                    $i = 1;
                                @endphp
                                @foreach ($tanggungan as $item)
                                    <tr class="tx-center">
                                        <td>{{ $i++ }}</td>
                                        <td class="tx-left">{{ $item->nama }}</td>
                                        <td>{{ $item->status }}</th>
                                        <td>{{ date('d-m-Y', strtotime($item->tgl_lahir)) }}</td>
                                        <td>
                                            <button data-id="{{ $item->id_pegawai_tanggungan }}"
                                                data-nama="{{ $item->nama }}" data-toggle="modal"
                                                data-target="#edit-keluarga" class="btn btn-xs btn-warning"><i
                                                    class="fa fa-edit"></i> Ubah</button>
                                            <button data-id="{{ $item->id_pegawai_tanggungan }}" data-toggle="modal"
                                                data-target="#hapus-keluarga" class="btn btn-xs btn-danger"><i
                                                    class="fa fa-trash"></i> Hapus</button>
                                        </td>
                                    </tr>
                                @endforeach
                                @if ($tanggungan->count() == null)
                                    <tr class="tx-center">
                                        <td colspan="5" class="tx-italic tx-danger">Belum ada data</td>
                                    </tr>
                                @endif
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    {{-- Modal Tambah Tugas --}}
    <div class="modal fade" id="tambah-tugas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content tx-14">
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Tambah Deskripsi Tugas</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ url('pegawai/pengajuan/tambah-tugas') }}">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" value="{{ $data->id_pengajuan }}" name="id_pengajuan">
                        <div class="form-group">
                            <label for="deskripsi" class="d-block">Deskripsi Tugas</label>
                            <textarea name="deskripsi" class="form-control" required></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white tx-montserrat tx-semibold"
                            data-dismiss="modal">Batal</button>
                        <input type="submit" class="btn btn-its tx-montserrat tx-semibold" value="Ya">
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- Modal Hapus Tugas --}}
    <div class="modal fade" id="hapus-tugas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content tx-14">
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Konfirmasi</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ url('pegawai/pengajuan/hapus-tugas') }}">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" id="id-hapus-tugas" name="id">
                        <p>Apakah Anda yakin menghapus data ini?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white tx-montserrat tx-semibold"
                            data-dismiss="modal">Batal</button>
                        <input type="submit" class="btn btn-its tx-montserrat tx-semibold" value="Ya">
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- Modal Edit Tugas --}}
    <div class="modal fade" id="edit-tugas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content tx-14">
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Edit Deskripsi Tugas</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ url('pegawai/pengajuan/edit-tugas') }}">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" id="id-edit-tugas" name="id">

                        <div class="form-group">
                            <label for="deskripsi" class="d-block">Deskripsi Tugas</label>
                            <textarea type="text" id="deskripsi" class="form-control" name="deskripsi"></textarea>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white tx-montserrat tx-semibold"
                            data-dismiss="modal">Batal</button>
                        <input type="submit" class="btn btn-its tx-montserrat tx-semibold" value="Ya">
                    </div>
                </form>
            </div>
        </div>
    </div>

    {{-- Modal Tambah Keluarga --}}
    <div class="modal fade" id="tambah-keluarga" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content tx-14">
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Tambah Tanggungan Keluarga</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ url('pegawai/pengajuan/tambah-keluarga') }}">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" value="{{ $data->nik }}" name="id_pegawai">
                        <div class="form-group">
                            <label for="nama" class="d-block">Nama</label>
                            <input type="text" name="nama" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="tgl_lahir" class="d-block">Tanggal Lahir</label>
                            <input type="date" name="tgl_lahir" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="status" class="d-block">Hubungan Keluarga</label>
                            <select class="custom-select" id="status" name="status" required>
                                <option value="">--- Pilih Hubungan Keluarga ---</option>
                                <option value="istri">Istri</option>
                                <option value="suami">Suami</option>
                                <option value="anak">Anak</option>
                                <option value="ibu">Ibu</option>
                                <option value="bapak">Bapak</option>
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white tx-montserrat tx-semibold"
                            data-dismiss="modal">Batal</button>
                        <input type="submit" class="btn btn-its tx-montserrat tx-semibold" value="Ya">
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- Modal Hapus Keluarga --}}
    <div class="modal fade" id="hapus-keluarga" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content tx-14">
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Konfirmasi</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ url('pegawai/pengajuan/hapus-keluarga') }}">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" id="id-hapus-keluarga" name="id">
                        <p>Apakah Anda yakin menghapus data ini?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white tx-montserrat tx-semibold"
                            data-dismiss="modal">Batal</button>
                        <input type="submit" class="btn btn-its tx-montserrat tx-semibold" value="Ya">
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- Modal Edit Keluarga --}}
    <div class="modal fade" id="edit-keluarga" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content tx-14">
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Edit Tanggungan Keluarga</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ url('pegawai/pengajuan/edit-keluarga') }}">
                    @csrf
                    <div class="modal-body">
                        <input type="hidden" id="id-edit-keluarga" name="id">

                        <div class="form-group">
                            <label for="nama" class="d-block">Nama</label>
                            <input type="text" id="nama" name="nama" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="tgl_lahir" class="d-block">Tanggal Lahir</label>
                            <input type="date" name="tgl_lahir" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="status" class="d-block">Hubungan Keluarga</label>
                            <select class="custom-select" id="status" name="status" required>
                                <option value="">--- Pilih Hubungan Keluarga ---</option>
                                <option value="istri">Istri</option>
                                <option value="suami">Suami</option>
                                <option value="anak">Anak</option>
                                <option value="ibu">Ibu</option>
                                <option value="bapak">Bapak</option>
                            </select>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white tx-montserrat tx-semibold"
                            data-dismiss="modal">Batal</button>
                        <input type="submit" class="btn btn-its tx-montserrat tx-semibold" value="Ya">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            $('#edit-tugas').on('show.bs.modal', function(event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var id = button.data('id')
                var deskripsi = button.data('deskripsi')
                console.log(deskripsi)
                var modal = $(this)
                modal.find('.modal-body #id-edit-tugas').val(id);
                modal.find('.modal-body #deskripsi').val(deskripsi);
            });
            $('#hapus-tugas').on('show.bs.modal', function(event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var id = button.data('id')
                var modal = $(this)
                modal.find('.modal-body #id-hapus-tugas').val(id);
            });

            $('#edit-keluarga').on('show.bs.modal', function(event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var id = button.data('id')
                var nama = button.data('nama')
                var modal = $(this)
                modal.find('.modal-body #id-edit-keluarga').val(id);
                modal.find('.modal-body #nama').val(nama);
            });
            $('#hapus-keluarga').on('show.bs.modal', function(event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var id = button.data('id')
                var modal = $(this)
                modal.find('.modal-body #id-hapus-keluarga').val(id);
            });
        });
    </script>
@endsection
