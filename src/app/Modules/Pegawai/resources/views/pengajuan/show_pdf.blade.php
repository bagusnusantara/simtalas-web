<div class="col-sm-12 col-lg-12">
    <iframe
        src="data:{{ $response->info->file_mimetype }};base64,<?= base64_encode(file_get_contents('https://storage-api.its.ac.id' . $response->info->public_link)) ?>"
        width="100%" height="600%">
    </iframe>
</div>
