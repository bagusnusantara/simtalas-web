@extends('Ui::base')

@section('title')
    Pengajuan Tenaga Lepas
@endsection

@section('header_title')
    Pengajuan Tenaga Lepas
@endsection
@section('prestyles')
    <link href="{{ asset('lib/select2/css/select2.min.css') }}" rel="stylesheet">
    <style>
        .select2-container {
            width: 100% !important;
            padding: 0;
        }

    </style>
    <link href="{{ asset('lib/quill/quill.core.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/quill/quill.snow.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/quill/quill.bubble.css') }}" rel="stylesheet">
@endsection

@section('breadcrumbs')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-style1 mg-b-10">
            <li class="breadcrumb-item"><a href="#">@if (isset($page['menu'])){{ ucwords($page['menu']) }}@endif</a></li>
            <li class="breadcrumb-item"><a href="#">@if (isset($page['submenu'])){{ ucwords($page['submenu']) }}@endif</a></li>
            <li class="breadcrumb-item active" aria-current="page">@if (isset($page['halaman'])){{ ucwords($page['halaman']) }}@endif</li>
        </ol>
    </nav>
@endsection
@section('content')

    @if(count( $errors ) > 0)
        <div data-label="Example" class="df-example">
            <div class="alert alert-dark mg-b-0" role="alert">
                @foreach ($errors->all() as $error)
                    {{ $error }}
                @endforeach
            </div>
        </div>
    @endif


    <div class="col-12">
        <div class="alert alert-solid alert-warning d-flex align-items-center" role="alert">
            <i data-feather="alert-circle" class="mg-r-10"></i> Pastikan Anda akan membuat pengajuan untuk Tenaga Lepas&nbsp; <b> (Bukan Dosen LB)</b>
        </div>
        <form method="post" action="{{ url('pegawai/pengajuan/store') }}" enctype="multipart/form-data" id="form"
            class="parsley-style-1" data-parsley-validate novalidate>
            @csrf
            <div id="wizard1">
                <h3>Jenis Pengajuan</h3>
                <section>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label>Jenis Pengajuan</label>
                            <select class="custom-select" id="id_jenis_pengajuan" name="id_jenis_pengajuan" required>
                                <option value="">--- Pilih Jenis Pengajuan ---</option>
                                @foreach ($data['jenis_pengajuan'] as $item)
                                    <option value="{{ $item->id_jenis_pengajuan }}">{{ $item->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Status Pengajuan</label>
                            <select class="custom-select" id="id_status_pengajuan" name="id_status_pengajuan" required>
                                <option value="">--- Pilih Status Pengajuan ---</option>
                                <option value="0">Baru</option>
                                <option value="1">Perpanjangan</option>
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="honor">Honor</label>
                            <input type="number" class="form-control" id="honor" name="honor" placeholder="Honor"
                                required>
                        </div>

                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-8">
                            <label>Unit</label>
                            <select class="custom-select" id="id_unit" name="id_unit" required>
                                <option value=""></option>
                                @foreach ($data['unit'] as $item)
                                    <option value="{{ $item->id_unit }}">{{ $item->nama }} </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="uang_makan">Uang Makan</label>
                            <input type="number" class="form-control" id="uang_makan" name="uang_makan"
                                placeholder="Uang Makan">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="tgl_pengajuan">Tanggal Pengajuan</label>
                            <input type="date" class="form-control" id="tgl_pengajuan" name="tgl_pengajuan"
                                placeholder="Tanggal Pengajuan" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="tgl_mulai_kerja">Tanggal Mulai Kerja</label>
                            <input type="date" class="form-control" id="tgl_mulai_kerja" name="tgl_mulai_kerja"
                                data-parsley-min="6" min="6" placeholder="Tanggal Mulai Kerja" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="tgl_akhir_kerja">Tanggal Berakhir Kontrak Kerja</label>
                            <input type="date" class="form-control" id="tgl_akhir_kerja" name="tgl_akhir_kerja"
                                placeholder="Tanggal Berakhir Kontrak Kerja" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="rincian_tugas">Rincian Tugas / Deskripsi Tugas</label><br>
                            <button class="btn btn-xs btn-its btn-tambah2 tx-montserrat tx-semibold mg-b-20"
                                type="button"><i class="fa fa-plus"></i> Tambah</button>
                            <div class="col-12 increment2">

                            </div>
                            <div class="clone2 hide mg-t-30" style="display: none;">
                                <div class="form-row control-group">
                                    <div class="form-group col-md-8">
                                        <small>Deskripsi Tugas</small><br>
                                        <input type="text" class="form-control" id="deskripsi[]" name="deskripsi[]"
                                            placeholder="Masukkan deskripsi tugas">
                                    </div>
                                    <div class="input-group-btn col-md-2">
                                        <small></small><br>
                                        <button class="btn btn-sm tx-montserrat tx-semibold btn-danger btn-hapus2"
                                            type="button"><i class="glyphicon glyphicon-remove"></i> Hapus</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </section>
                <h3>Data Pegawai</h3>
                <section>
                    <div id="baru" class="d-none">
                        <div class="form-row">
                            <div class="form-group col-md-8">
                                <label for="nama">Nama Lengkap</label>
                                <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama lengkap"
                                    required>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="nik">NIK</label>
                                <input type="number" class="form-control" id="nik" name="nik" placeholder="NIK" minlength="16" maxlength="16" required>
                                <span class="font-italic" style="display: none;" id="alert16"></span>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="nama">Tanggal Lahir</label>
                                <input type="date" class="form-control" id="tgl_lahir" name="tgl_lahir" required>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="jenis_kelamin">Jenis Kelamin</label>
                                <select class="custom-select" id="jenis_kelamin" name="jenis_kelamin" required>
                                    <option value="">Pilih Jenis Kelamin</option>
                                    <option value="L">Laki-laki</option>
                                    <option value="P">Perempuan</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="pendidikan_terakhir">Pendidikan Terakhir</label>
                                <select class="custom-select" id="id_pendidikan" name="id_pendidikan" required>
                                    <option value="">Pilih Pendidikan Terakhir</option>
                                    @foreach ($data['pendidikan'] as $item)
                                        <option value="{{ $item->id_pendidikan }}">{{ $item->nama }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="alamat">Alamat</label>
                            <input type="text" class="form-control" id="alamat" name="alamat"
                                placeholder="Masukkan alamat" required>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="hp">Nomor HP</label>
                                <input type="number" class="form-control" id="hp" name="hp" placeholder="Nomor HP">
                            </div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="tanggal_pengajuan">Status BPJS</label>
                                <select class="custom-select" id="id_status_bpjs" name="id_status_bpjs">
                                    <option value="">Pilih Status BPJS</option>
                                    <option value="0">BPJS PBI (Penerima Bantuan Pemerintah)</option>
                                    <option value="1">BPJS NON PBI (Mandiri)</option>
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="email">Nomor BPJS</label>
                                <input type="number" class="form-control" id="nomor_bpjs" name="nomor_bpjs"
                                    placeholder="Nomor BPJS">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="pas_foto">Upload Pas Foto</label>
                                <input type="file" class="form-control" id="pas_foto" name="pas_foto" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="ktp">Upload KTP/KTM</label>
                                <input type="file" class="form-control" id="ktp" name="ktp" required>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="ijazah">Upload Ijazah</label>
                                <input type="file" class="form-control" id="ijazah" name="ijazah" required>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-12">
                                <label for="tanggal_pengajuan">Nama Anggota keluarga yang masuk tanggungan</label><br>
                                <button class="btn btn-xs btn-its btn-tambah tx-montserrat tx-semibold mg-b-30"
                                    type="button"><i class="fa fa-plus"></i> Tambah</button>
                                <div class="col-12 increment">

                                </div>
                                <div class="clone hide mg-t-30" style="display: none;">
                                    <div class="form-row control-group">
                                        <div class="form-group col-md-4">
                                            <small>Nama</small>
                                            <input type="text" class="form-control" id="nama_anggota[]"
                                                name="nama_anggota[]" placeholder="Nama Lengkap">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <small>Hubungan Keluarga</small>
                                            <select class="custom-select" id="status" name="status_anggota[]" required>
                                                <option value="">--- Pilih Hubungan Keluarga ---</option>
                                                <option value="istri">Istri</option>
                                                <option value="suami">Suami</option>
                                                <option value="anak">Anak</option>
                                                <option value="ibu">Ibu</option>
                                                <option value="bapak">Bapak</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <small>Tanggal Lahir</small>
                                            <input type="date" class="form-control tgl_lahir" name="tgl_lahir_anggota[]">

                                        </div>
                                        <div class="input-group-btn col-md-2">
                                            <label for="tanggal_pengajuan"></label><br>
                                            <button class="btn btn-sm tx-montserrat tx-semibold btn-danger btn-hapus"
                                                type="button"><i class="glyphicon glyphicon-remove"></i> Hapus</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div id="lama" class="d-none">
                        <div class="form-group col-md-12">
                            <p for="nik">Masukkan NIK</p>
                        </div>
                        <div class="col-md-8">
                            <select class="custom-select" id="cari_nik" name="cari_nik">
                                <option value=""></option>
                                @foreach ($data['pegawai_perpanjangan'] as $item)
                                    <option value="{{ $item->id_pegawai }}" data-nik="{{$item->nik}}">{{ $item->nik }} - {{ $item->nama }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </section>
            </div>
        </form>
    </div>


@endsection
@section('scripts')
    <script src="{{ asset('lib/parsleyjs/parsley.min.js') }}"></script>
    <script src="{{ asset('lib/jquery-steps/build/jquery.steps.min.js') }}"></script>
    <script src="{{ asset('lib/select2/js/select2.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            'use strict'

            $('#wizard1').steps({
                headerTag: 'h3',
                bodyTag: 'section',
                autoFocus: true,
                titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
                onStepChanging: function(event, currentIndex, newIndex) {
                    if (currentIndex < newIndex) {
                        // Step 1 form validation
                        if (currentIndex === 0) {

                            let v_jenis_pengajuan = $('#id_jenis_pengajuan').parsley();
                            let v_status_pengajuan = $('#id_status_pengajuan').parsley();
                            let v_unit = $('#id_unit').parsley();
                            let v_tgl_pengajuan = $('#tgl_pengajuan').parsley();
                            let v_tgl_mulai_kerja = $('#tgl_mulai_kerja').parsley();
                            let v_tgl_akhir_kerja = $('#tgl_akhir_kerja').parsley();
                            let v_honor = $('#honor').parsley();

                            if (v_status_pengajuan.isValid() && v_jenis_pengajuan.isValid() && v_unit
                                .isValid() && v_tgl_pengajuan.isValid() && v_tgl_mulai_kerja
                                .isValid() && v_tgl_akhir_kerja.isValid() && v_honor.isValid()) {
                                let status_pengajuan = document.getElementById("id_status_pengajuan")
                                    .value;
                                if (status_pengajuan == '0') {

                                    let baru = document.getElementById("baru");
                                    let lama = document.getElementById("lama");
                                    baru.classList.remove("d-none");
                                    lama.classList.add("d-none");
                                } else {
                                    let baru = document.getElementById("baru");
                                    let lama = document.getElementById("lama");
                                    lama.classList.remove("d-none");
                                    baru.classList.add("d-none");
                                }
                                return true;
                            } else {
                                v_jenis_pengajuan.validate();
                                v_status_pengajuan.validate();
                                v_unit.validate();
                                v_tgl_pengajuan.validate();
                                v_tgl_akhir_kerja.validate();
                                v_tgl_mulai_kerja.validate();
                                v_honor.validate();
                            }
                        }
                        // Always allow step back to the previous step even if the current step is not valid.
                    } else {
                        return true;
                    }
                },

                onFinishing: function(event, currentIndex) {
                    let status_pengajuan = document.getElementById("id_status_pengajuan").value;
                    if (status_pengajuan == '0') {
                        let v_nik = $('#nik').parsley();
                        let v_tgl_lahir = $('#tgl_lahir').parsley();
                        let v_nama = $('#nama').parsley();
                        let v_jenis_kelamin = $('#jenis_kelamin').parsley();
                        let v_id_pendidikan = $('#id_pendidikan').parsley();
                        let v_hp = $('#hp').parsley();
                        let v_alamat = $('#alamat').parsley();
                        let v_pas_foto = $('#pas_foto').parsley();
                        let v_ijazah = $('#ijazah').parsley();
                        let v_ktp = $('#ktp').parsley();
                        if (v_nik.isValid() && v_tgl_lahir.isValid() && v_nama.isValid() &&
                            v_jenis_kelamin.isValid() && v_id_pendidikan.isValid() && v_hp.isValid() &&
                            v_alamat.isValid() && v_pas_foto.isValid() && v_ijazah.isValid() && v_ktp
                            .isValid()) {
                            return true;
                        } else {
                            v_nik.validate();
                            v_tgl_lahir.validate();
                            v_nama.validate();
                            v_jenis_kelamin.validate();
                            v_id_pendidikan.validate();
                            v_hp.validate();
                            v_alamat.validate();
                            v_pas_foto.validate();
                            v_ijazah.validate();
                            v_ktp.validate();
                        }
                    } else {
                        $("#form")[0].submit();
                    }


                },

                onFinished: function(event, currentIndex) {

                    $("#form")[0].submit();
                }
            });


            var daftarNik = {!! $data['nik'] !!};

            $('#id_unit').select2({
                placeholder: 'Pilih Unit',
                searchInputPlaceholder: 'Search options',
                allowClear: true

            });
            $('#id_unit').on("select2:selecting", function(e) {
                $('#id_unit').parsley().reset();
            });
            $('#cari_nik').select2({
                placeholder: 'Masukkan NIK Pegawai',
                searchInputPlaceholder: 'Search options'
            });

            $('#cari_nik').on('select2:select', function(e){
                var data = e.params.data;
                var nik = data.element.dataset.nik;
                $('#nik').val(nik);
            });

            $('#nik').on('input', function(evt) {

                var value = evt.target.value;

                if (value.length === 16) {
                    $('#alert16').html('').removeClass('tx-danger').hide();

                    if (jQuery.inArray(value, daftarNik) !== -1) { //check if an item is in the array
                        $('#alert16').html('<i class="fa fa-times"></i> NIK sudah terdaftar').addClass(
                            'tx-danger').show();

                        $('#nama').prop('disabled', true);
                        $('#jenis_kelamin').prop('disabled', true);
                        $('#id_pendidikan').prop('disabled', true);
                        $('#tgl_lahir').prop('disabled', true);
                        $('#alamat').prop('disabled', true);
                        $('#email').prop('disabled', true);
                        $('#hp').prop('disabled', true);
                        $('#id_status_bpjs').prop('disabled', true);
                        $('#nomor_bpjs').prop('disabled', true);
                        $('#pas_foto').prop('disabled', true);
                        $('#ktp').prop('disabled', true);
                        $('#ijazah').prop('disabled', true);

                    } else {
                        $('#alert16').html('<i class="fa fa-check"></i> NIK tersedia').addClass(
                            'tx-success').show();

                        $('#nama').prop('disabled', false);
                        $('#jenis_kelamin').prop('disabled', false);
                        $('#id_pendidikan').prop('disabled', false);
                        $('#tgl_lahir').prop('disabled', false);
                        $('#alamat').prop('disabled', false);
                        $('#email').prop('disabled', false);
                        $('#hp').prop('disabled', false);
                        $('#id_status_bpjs').prop('disabled', false);
                        $('#nomor_bpjs').prop('disabled', false);
                        $('#pas_foto').prop('disabled', false);
                        $('#ktp').prop('disabled', false);
                        $('#ijazah').prop('disabled', false);
                    }

                }

            });

            $(".btn-tambah").click(function() {
                var html = $(".clone").html();
                $(".increment").after(html);
            });
            $("body").on("click", ".btn-hapus", function() {
                $(this).parents(".control-group").remove();
            });

            $(".btn-tambah2").click(function() {
                var html = $(".clone2").html();
                $(".increment2").after(html);
            });
            $("body").on("click", ".btn-hapus2", function() {
                $(this).parents(".control-group").remove();
            });

            $('#id_jenis_pengajuan').change(function() {
                if ($(this).val() == 'thl') {
                    document.getElementById("honor").placeholder = "Masukkan nilai honor per hari";
                    document.getElementById("uang_makan").value = '';
                    document.getElementById("uang_makan").readOnly = false;

                } else {
                    document.getElementById("honor").placeholder = "Masukkan nilai honor per jam";
                    document.getElementById("uang_makan").value = 0;
                    document.getElementById("uang_makan").readOnly = true;
                }
            });


        });
    </script>
@endsection
