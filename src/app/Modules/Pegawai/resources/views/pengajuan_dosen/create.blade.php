@extends('Ui::base')

@section('title')
    Pengajuan Dosen Luar Biasa
@endsection

@section('header_title')
    Pengajuan Dosen Luar Biasa
@endsection
@section('prestyles')
    <link href="{{ asset('lib/select2/css/select2.min.css') }}" rel="stylesheet">
    <style>
        .select2-container {
            width: 100% !important;
            padding: 0;
        }

    </style>
    <link href="{{ asset('lib/quill/quill.core.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/quill/quill.snow.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/quill/quill.bubble.css') }}" rel="stylesheet">
@endsection


@section('breadcrumbs')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-style1 mg-b-10">
            <li class="breadcrumb-item"><a href="#">@if (isset($page['menu'])){{ ucwords($page['menu']) }}@endif</a></li>
            <li class="breadcrumb-item"><a href="#">@if (isset($page['submenu'])){{ ucwords($page['submenu']) }}@endif</a></li>
            <li class="breadcrumb-item active" aria-current="page">@if (isset($page['halaman'])){{ ucwords($page['halaman']) }}@endif</li>
        </ol>
    </nav>
@endsection
@section('content')

    @if(count( $errors ) > 0)
        <div data-label="Example" class="df-example">
            <div class="alert alert-dark mg-b-0" role="alert">
                @foreach ($errors->all() as $error)
                    {{ $error }}
                @endforeach
            </div>
        </div>
    @endif


    <div class="col-12">
        <div class="alert alert-solid alert-warning d-flex align-items-center" role="alert">
            <i data-feather="alert-circle" class="mg-r-10"></i> Pastikan Anda akan membuat pengajuan untuk Dosen LB &nbsp;<b>(bukan Tenaga Lepas)</b>
        </div>
        <form method="post" action="{{ url('pegawai/pengajuan-dosen') }}" enctype="multipart/form-data" id="form"
            class="parsley-style-1" data-parsley-validate novalidate>
            @csrf
            <div id="wizard1">
                <h3>Jenis Pengajuan</h3>
                <section>
                    <div class="form-row">
                        <div class="form-group col-12">
                            <label>Status Pengajuan *</label>
                            <select class="custom-select" id="id_status_pengajuan_dosen" name="id_status_pengajuan_dosen" required>
                                <option value="">Pilih Status Pengajuan</option>
                                <option value="0">Baru</option>
                                <option value="1">Perpanjangan</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-12">
                            <label>Unit *</label>
                            <select class="custom-select" id="id_unit" name="id_unit" required>
                                <option value=""></option>
                                @foreach ($data['unit'] as $item)
                                    <option value="{{ $item->id_unit }}">{{ $item->nama }} </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-12">
                            <label>Jenis Pengajuan *</label>
                            @foreach ($data['jenis_pengajuan_dosen'] as $jenis_pengajuan_dosen)
                            <div class="form-check form-row">
                                <input class="form-check-input" type="radio" name="id_jenis_pengajuan_dosen" id="{{$jenis_pengajuan_dosen->id_jenis_pengajuan_dosen}}" value="{{$jenis_pengajuan_dosen->id_jenis_pengajuan_dosen}}" checked>
                                <label class="form-check-label" for="{{$jenis_pengajuan_dosen->id_jenis_pengajuan_dosen}}">
                                    {{$jenis_pengajuan_dosen->nama}}
                                </label>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label for="tanggal_pengajuan">Tanggal Pengajuan *</label>
                            <input type="date" class="form-control" id="tanggal_pengajuan" name="tanggal_pengajuan"
                                placeholder="Tanggal Pengajuan" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="tanggal_mulai_kerja">Tanggal Mulai Kerja *</label>
                            <input type="date" class="form-control" id="tanggal_mulai_kerja" name="tanggal_mulai_kerja"
                                data-parsley-min="6" min="6" placeholder="Tanggal Mulai Kerja" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="tanggal_akhir_kerja">Tanggal Berakhir Kontrak Kerja *</label>
                            <input type="date" class="form-control" id="tanggal_akhir_kerja" name="tanggal_akhir_kerja"
                                placeholder="Tanggal Berakhir Kontrak Kerja" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="beban_kerja">Beban Kerja</label><br>
                            <button class="btn btn-xs btn-its btn-tambah2 tx-montserrat tx-semibold mg-b-20"
                                type="button"><i class="fa fa-plus"></i> Tambah</button>
                            <div class="text-danger d-none" id="error-beban-kerja">
                                Mohon menambahkan beban kerja
                            </div>
                            <div class="col-12 increment2">

                            </div>
                            <div class="mt-3 d-flex flex-column justify-content-center align-items-end">
                                <p>Total SKS Pengajaran: <span id="total-sks">0 SKS</span></p>
                                <p >Batas SKS Pengajaran: <span id="maks-sks">0 SKS</span></p>
                            </div>
                        </div>
                    </div>

                </section>

                <h3>Data Dosen</h3>
                <section>
                    <div id="baru" class="d-none">
                        <div class="alert alert-info" role="alert">
                            Pastikan isian <strong>NIK</strong> sudah benar karena <strong>tidak bisa</strong> diubah setelah dimasukkan.
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-2">
                                <label for="gelar_depan">Gelar Depan</label>
                                <input type="text" class="form-control" id="gelar_depan" name="gelar_depan" placeholder="Gelar depan">
                            </div>
                            <div class="form-group col-md-8">
                                <label for="nama">Nama Lengkap *</label>
                                <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama lengkap"
                                    required>
                            </div>
                            <div class="form-group col-md-2">
                                <label for="gelar_belakang">Gelar Belakang</label>
                                <input type="text" class="form-control" id="gelar_belakang" name="gelar_belakang" placeholder="Gelar belakang">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="nik">NIK *</label>
                                <input type="number" class="form-control" id="nik" name="nik" placeholder="NIK" minlength="16" maxlength="16" required>
                                <span class="font-italic" style="display: none;" id="alert16"></span>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="nidn">NIDN / NIDK / NUP</label>
                                <input type="number" class="form-control" id="nidn" name="nidn" minlength="10" maxlength="10" placeholder="NIDN / NIDK / NUP">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="nama">Tanggal Lahir *</label>
                                <input type="date" class="form-control" id="tgl_lahir" name="tgl_lahir" required>
                            </div>
                            <div class="form-group col-md-5">
                                <label for="tempat_lahir">Tempat Lahir *</label>
                                <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir"
                                    placeholder="Tempat lahir" required>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="jenis_kelamin">Jenis Kelamin *</label>
                                <select class="custom-select" id="jenis_kelamin" name="jenis_kelamin" required>
                                    <option value="">Pilih Jenis Kelamin</option>
                                    <option value="L">Laki-laki</option>
                                    <option value="P">Perempuan</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="kepakaran">Kepakaran *</label>
                                <input type="text" class="form-control" id="kepakaran" name="kepakaran"
                                    placeholder="Kepakaran" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="id_pendidikan">Pendidikan Terakhir *</label>
                                <select class="custom-select" id="id_pendidikan" name="id_pendidikan" required>
                                    <option value="">Pilih Pendidikan Terakhir</option>
                                    @foreach ($data['pendidikan'] as $pendidikan)
                                        <option value="{{ $pendidikan->id_pendidikan }}">{{ $pendidikan->nama }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="alamat">Alamat *</label>
                            <input type="text" class="form-control" id="alamat" name="alamat"
                                placeholder="Masukkan alamat" required>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="email">Email *</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label for="nomor_hp">Nomor HP</label>
                                <input type="number" class="form-control" id="nomor_hp" name="nomor_hp" placeholder="Nomor HP">
                            </div>
                        </div>


                    </div>
                    <div id="lama" class="d-none">
                        <div class="form-row">
                            <div class="col-md-8">
                                <label for="nik">Masukkan NIK *</label>
                                <select class="custom-select" id="cari_nik" name="cari_nik" required>
                                    <option value=""></option>
                                    @foreach ($data['dosen_perpanjangan'] as $item)
                                        <option value="{{ $item->nik }}" data-nik="{{$item->nik}}" data-npp="{{$item->npp}}">{{ $item->nik }} - {{ $item->nama }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-4">
                                <label for="npp">NPP</label>
                                <input type="number" class="form-control" id="npp" name="npp"
                                    placeholder="Masukkan NPP" minlength="13" maxlength="16">
                            </div>
                        </div>
                    </div>
                </section>
                <h3>Berkas Dosen</h3>
                <section>
                    <div class="form-row">
                        <div class="form-group col-12">
                            <label for="pas_foto">Upload Pas Foto *</label>
                            <input type="file" accept=".jpeg,.jpg,.pdf" class="form-control berkas-dosen" id="pas_foto" name="pas_foto" required>
                        </div>
                        <div class="form-group col-12">
                            <label for="ktp">Upload KTP *</label>
                            <input type="file" accept=".jpeg,.jpg,.pdf" class="form-control berkas-dosen" id="ktp" name="ktp" required>
                        </div>
                        <div class="form-group col-12">
                            <label for="riwayat_hidup">Upload Berkas Riwayat Hidup</label>
                            <input type="file" accept=".jpeg,.jpg,.pdf" class="form-control berkas-dosen" id="riwayat_hidup" name="riwayat_hidup">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-12">
                            <label for="ijazah_s1">Upload Ijazah S1 *</label>
                            <input type="file" accept=".jpeg,.jpg,.pdf" class="form-control berkas-dosen" id="ijazah_s1" name="ijazah_s1" required>
                        </div>
                        <div class="form-group col-12">
                            <label for="transkrip_s1">Upload Transkrip S1 *</label>
                            <input type="file" accept=".jpeg,.jpg,.pdf" class="form-control berkas-dosen" id="transkrip_s1" name="transkrip_s1" required>
                        </div>
                        <div class="form-group col-12">
                            <label for="penyetaraan_s1">Upload Berkas Penyetaraan S1</label>
                            <input type="file" accept=".jpeg,.jpg,.pdf" class="form-control berkas-dosen" id="penyetaraan_s1" name="penyetaraan_s3">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-12">
                            <label for="ijazah_s2">Upload Ijazah S2 *</label>
                            <input type="file" accept=".jpeg,.jpg,.pdf" class="form-control berkas-dosen" id="ijazah_s2" name="ijazah_s2" required>
                        </div>
                        <div class="form-group col-12">
                            <label for="transkrip_s2">Upload Transkrip S2 *</label>
                            <input type="file" accept=".jpeg,.jpg,.pdf" class="form-control berkas-dosen" id="transkrip_s2" name="transkrip_s2" required>
                        </div>
                        <div class="form-group col-12">
                            <label for="penyetaraan_s2">Upload Berkas Penyetaraan S2</label>
                            <input type="file" accept=".jpeg,.jpg,.pdf" class="form-control berkas-dosen" id="penyetaraan_s2" name="penyetaraan_s2">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-12">
                            <label for="ijazah_s3">Upload Ijazah S3</label>
                            <input type="file" accept=".jpeg,.jpg,.pdf" class="form-control berkas-dosen" id="ijazah_s3" name="ijazah_s3">
                        </div>
                        <div class="form-group col-12">
                            <label for="transkrip_s3">Upload Transkrip S3</label>
                            <input type="file" accept=".jpeg,.jpg,.pdf" class="form-control berkas-dosen" id="transkrip_s3" name="transkrip_s3">
                        </div>
                        <div class="form-group col-12">
                            <label for="penyetaraan_s3">Upload Berkas Penyetaraan S3</label>
                            <input type="file" accept=".jpeg,.jpg,.pdf" class="form-control berkas-dosen" id="penyetaraan_s3" name="penyetaraan_s3">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-12">
                            <label for="analisis_bkd">Upload Analisis Beban Kerja Dosen *</label>
                            <input type="file" accept=".jpeg,.jpg,.pdf" class="form-control berkas-dosen" id="analisis_bkd" name="analisis_bkd" required>
                        </div>
                        <div class="form-group col-12">
                            <label for="surat_izin_institusi_asal">Upload Surat Izin Institusi Asal *</label>
                            <input type="file" accept=".jpeg,.jpg,.pdf" class="form-control berkas-dosen" id="surat_izin_institusi_asal" name="surat_izin_institusi_asal" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-12">
                            <label for="sehat_rohani">Upload Berkas Sehat Rohani</label>
                            <input type="file" accept=".jpeg,.jpg,.pdf" class="form-control berkas-dosen" id="sehat_rohani" name="sehat_rohani">
                        </div>
                        <div class="form-group col-12">
                            <label for="sehat_jasmani">Upload Berkas Sehat Jasmani</label>
                            <input type="file" accept=".jpeg,.jpg,.pdf" class="form-control berkas-dosen" id="sehat_jasmani" name="sehat_jasmani">
                        </div>
                        <div class="form-group col-12">
                            <label for="bebas_narkotika">Upload Berkas Bebas Narkotika</label>
                            <input type="file" accept=".jpeg,.jpg,.pdf" class="form-control berkas-dosen" id="bebas_narkotika" name="bebas_narkotika">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-12">
                            <label for="pernyataan_dekan">Upload Berkas Pernyataan Dekan</label>
                            <input type="file" accept=".jpeg,.jpg,.pdf" class="form-control berkas-dosen" id="pernyataan_dekan" name="pernyataan_dekan">
                        </div>
                        <div class="form-group col-12">
                            <label for="minimal_mengajar">Upload Berkas Minimal Mengajar dari Dekan</label>
                            <input type="file" accept=".jpeg,.jpg,.pdf" class="form-control berkas-dosen" id="minimal_mengajar" name="minimal_mengajar">
                        </div>
                    </div>
                </section>
            </div>
        </form>
    </div>


@endsection
@section('scripts')
    <script src="{{ asset('lib/parsleyjs/parsley.min.js') }}"></script>
    <script src="{{ asset('lib/jquery-steps/build/jquery.steps.min.js') }}"></script>
    <script src="{{ asset('lib/select2/js/select2.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            'use strict'
            let beban_kerja_cnt = 0;
            let id_jenis_pengajuan_dosen;
            let sks_count = 0;
            let numeric = true;
            let batas_sks = 6;
            
            $('#wizard1').steps({
                headerTag: 'h3',
                bodyTag: 'section',
                autoFocus: true,
                titleTemplate: '<span class="number">#index#</span> <span class="title">#title#</span>',
                onStepChanging: function(event, currentIndex, newIndex) {
                    if (currentIndex < newIndex) {
                        // Step 1 form validation
                        if (currentIndex === 0) {
                            let v_status_pengajuan = $('#id_status_pengajuan_dosen').parsley();
                            let v_unit = $('#id_unit').parsley();
                            let v_tgl_pengajuan = $('#tanggal_pengajuan').parsley();
                            let v_tanggal_mulai_kerja = $('#tanggal_mulai_kerja').parsley();
                            let tanggal_akhir_kerja = $('#tanggal_akhir_kerja').parsley();
                            const v_input_beban_kerja_dosen = $('.input-beban-kerja-dosen')?.parsley()

                            let inputBebanKerjaValidationStatus = true
                            for(let i=0; i<v_input_beban_kerja_dosen?.length; i++)
                                inputBebanKerjaValidationStatus &= v_input_beban_kerja_dosen[i].isValid()
                            if(!v_input_beban_kerja_dosen?.length)
                                document.getElementById('error-beban-kerja').classList.remove('d-none')
                            else document.getElementById('error-beban-kerja').classList.add('d-none')
                            if (v_status_pengajuan.isValid() && v_unit
                                .isValid() && v_tgl_pengajuan.isValid() && v_tanggal_mulai_kerja
                                .isValid() && tanggal_akhir_kerja.isValid() && inputBebanKerjaValidationStatus && sks_count <= batas_sks
                                && v_input_beban_kerja_dosen?.length > 0) {
                                let status_pengajuan = document.getElementById("id_status_pengajuan_dosen")
                                    .value;
                                if (status_pengajuan == '0') {

                                    let baru = document.getElementById("baru");
                                    let lama = document.getElementById("lama");
                                    baru.classList.remove("d-none");
                                    lama.classList.add("d-none");
                                } else {
                                    let baru = document.getElementById("baru");
                                    let lama = document.getElementById("lama");
                                    lama.classList.remove("d-none");
                                    baru.classList.add("d-none");
                                }
                                return true;
                            } else {
                                v_status_pengajuan.validate();
                                v_unit.validate();
                                v_tgl_pengajuan.validate();
                                tanggal_akhir_kerja.validate();
                                v_tanggal_mulai_kerja.validate();
                                for(let i=0; i<v_input_beban_kerja_dosen?.length; i++)
                                    v_input_beban_kerja_dosen[i].validate()
                            }
                        } else if (currentIndex === 1) {
                            let status_pengajuan = document.getElementById("id_status_pengajuan_dosen").value;
                            if (status_pengajuan == '0') {
                                let v_nik = $('#nik').parsley();
                                let v_tgl_lahir = $('#tgl_lahir').parsley();
                                let v_tempat_lahir = $('#tempat_lahir').parsley();
                                let v_nama = $('#nama').parsley();
                                let v_jenis_kelamin = $('#jenis_kelamin').parsley();
                                let v_pendidikan_terakhir = $('#id_pendidikan').parsley();
                                let v_nidn = $('#nidn').parsley();
                                let v_hp = $('#nomor_hp').parsley();
                                let v_alamat = $('#alamat').parsley();
                                let v_kepakaran = $('#kepakaran').parsley();
                                let v_email = $('#email').parsley();
                                
                                if (v_nik.isValid() && v_tgl_lahir.isValid() && v_nama.isValid() &&
                                    v_jenis_kelamin.isValid() && v_nidn.isValid() && v_hp.isValid() &&
                                    v_alamat.isValid() && v_pendidikan_terakhir.isValid()
                                    && v_kepakaran.isValid() && v_tempat_lahir.isValid() && v_email.isValid()) {
                                    return true;
                                } else {
                                    v_nik.validate();
                                    v_tgl_lahir.validate();
                                    v_nama.validate();
                                    v_jenis_kelamin.validate();
                                    v_nidn.validate();
                                    v_hp.validate();
                                    v_kepakaran.validate();
                                    v_alamat.validate();
                                    v_tempat_lahir.validate();
                                    v_pendidikan_terakhir.validate();
                                    v_email.validate();
                                }
                            } else {
                                let v_nik = $('#cari_nik').parsley();
                                let v_npp = $('#npp').parsley();
                                if(v_nik.isValid() && v_npp.isValid()) {
                                    $("#form")[0].submit();
                                }
                                v_nik.validate()
                                v_npp.validate()
                            }
                        }
                        // Always allow step back to the previous step even if the current step is not valid.
                    } else {
                        return true;
                    }
                },

                onFinishing: function(event, currentIndex) {
                    const v_berkas_dosen = $('.berkas-dosen').parsley()

                    let validationStatus = true
                    for(let i=0; i<v_berkas_dosen.length; i++)
                        validationStatus &= v_berkas_dosen[i].isValid()

                    if (validationStatus)
                        return true

                    for(let i=0; i<v_berkas_dosen.length; i++)
                        v_berkas_dosen[i].validate()
                },

                onFinished: function(event, currentIndex) {
                    $("#form")[0].submit();
                }
            });


            var daftarNik = {!! $data['nik'] !!};

            $('#id_unit').select2({
                placeholder: 'Pilih Unit',
                searchInputPlaceholder: 'Search options',
                allowClear: true

            });
            $('#id_unit').on("select2:selecting", function(e) {
                $('#id_unit').parsley().reset();
            });
            $('#cari_nik').select2({
                placeholder: 'Masukkan NIK Pegawai',
                searchInputPlaceholder: 'Search options'
            });

            $('#cari_nik').on('select2:select', function(e){
                var nik = e.params.data.element.dataset.nik;
                var npp = e.params.data.element.dataset.npp;
                $('#nik').val(nik);
                $('#npp').val(npp);
                if (npp !== '') {
                    $('#npp').attr('disabled', true);
                } else {
                    $('#npp').attr('disabled', false);
                    $('#npp').attr('required', true);
                }
            });

            $('#nik').on('input', function(evt) {

                var value = evt.target.value;

                if (value.length === 16) {
                    $('#alert16').html('').removeClass('tx-danger').hide();

                    if (jQuery.inArray(value, daftarNik) !== -1) { //check if an item is in the array
                        $('#alert16').html('<i class="fa fa-times"></i> NIK sudah terdaftar').addClass(
                            'tx-danger').show();

                        $('#gelar_depan').prop('disabled', true);
                        $('#nama').prop('disabled', true);
                        $('#gelar_belakang').prop('disabled', true);
                        $('#tempat_lahir').prop('disabled', true);
                        $('#Kepakaran').prop('disabled', true);
                        $('#jenis_kelamin').prop('disabled', true);
                        $('#nidn').prop('disabled', true);
                        $('#tgl_lahir').prop('disabled', true);
                        $('#alamat').prop('disabled', true);
                        $('#email').prop('disabled', true);
                        $('#nomor_hp').prop('disabled', true);
                        $('#id_status_bpjs').prop('disabled', true);
                        $('#nomor_bpjs').prop('disabled', true);
                        $('#pas_foto').prop('disabled', true);
                        $('#ktp').prop('disabled', true);
                        $('#ijazah').prop('disabled', true);

                    } else {
                        $('#alert16').html('<i class="fa fa-check"></i> NIK tersedia').addClass(
                            'tx-success').show();

                        $('#gelar_depan').prop('disabled', false);
                        $('#nama').prop('disabled', false);
                        $('#gelar_belakang').prop('disabled', false);
                        $('#tempat_lahir').prop('disabled', false);
                        $('#Kepakaran').prop('disabled', false);
                        $('#jenis_kelamin').prop('disabled', false);
                        $('#nidn').prop('disabled', false);
                        $('#tgl_lahir').prop('disabled', false);
                        $('#alamat').prop('disabled', false);
                        $('#email').prop('disabled', false);
                        $('#nomor_hp').prop('disabled', false);
                        $('#id_status_bpjs').prop('disabled', false);
                        $('#nomor_bpjs').prop('disabled', false);
                        $('#pas_foto').prop('disabled', false);
                        $('#ktp').prop('disabled', false);
                        $('#ijazah').prop('disabled', false);
                    }

                }

            });

            $(".btn-tambah").click(function() {
                var html = $(".clone").html();
                $(".increment").after(html);
            });
            $("body").on("click", ".btn-hapus", function() {
                $(this).parents(".control-group").remove();
            });

            $(".btn-tambah2").click(function() {
                var html = `
                <div class="form-row control-group mt-3">
                    <div class="form-group col-sm-6 col-md-4">
                        <small>Jenis Beban Kerja *</small><br>
                        <select class="custom-select input-beban-kerja-dosen" id="id-jenis-beban-kerja-${beban_kerja_cnt}" name="id_jenis_beban_kerja[]" required>
                            <option value="">Pilih Jenis Beban Kerja</option>
                            @foreach ($data['jenis_beban_kerja'] as $jenis_beban_kerja)
                                <option value="{{ $jenis_beban_kerja->id_jenis_beban_kerja_dosen }}">{{ $jenis_beban_kerja->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-6 col-md-4" id="form-uraian-${beban_kerja_cnt}">
                        <small id="label-deskripsi-${beban_kerja_cnt}">Deskripsi / Mata Kuliah *</small><br>
                        <input type="text" class="form-control input-beban-kerja-dosen uraian" id="uraian-${beban_kerja_cnt}" name="uraian[]"
                            placeholder="Masukkan deskripsi / mata kuliah" required>
                    </div>
                    <div class="form-group col-sm-6 col-md-4" id="form-id-mkb-wajib-${beban_kerja_cnt}">
                        <small>Mata Kuliah Bersama Wajib *</small><br>
                        <select class="custom-select input-beban-kerja-dosen id-mkb-wajib" name="id_mkb_wajib[]" id="id-mkb-wajib-${beban_kerja_cnt}">
                            <option value="">Pilih Mata Kuliah Bersama Wajib</option>
                            @foreach ($data['mata_kuliah_bersama_wajib'] as $mata_kuliah_bersama_wajib)
                                <option value="{{ $mata_kuliah_bersama_wajib->id_mkb_wajib }}">{{ $mata_kuliah_bersama_wajib->nama }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-6 col-md-4">
                        <small>Kelas</small><br>
                        <input type="text" class="form-control input-beban-kerja-dosen" id="kelas-${beban_kerja_cnt}" name="kelas[]" maxlength="50"
                            placeholder="Masukkan kelas">
                    </div>
                    <div class="form-group col-sm-6">
                        <small id="label-jumlah-${beban_kerja_cnt}">Jumlah *</small><br>
                        <input type="number" step="0.1" class="form-control input-beban-kerja-dosen jumlah-beban-kerja" id="jumlah-beban-kerja-${beban_kerja_cnt}" name="jumlah[]"
                            placeholder="Masukkan jumlah" required>
                    </div>
                    <div class="form-group col-sm-6">
                        <small id="label-upah-${beban_kerja_cnt}">Upah *</small><br>
                        <input type="number" class="form-control input-beban-kerja-dosen" id="upah[]" name="upah[]"
                            placeholder="Masukkan upah" required>
                    </div>
                    <div class="col-12">
                        <button class="btn btn-sm tx-montserrat tx-semibold btn-danger btn-hapus2 w-100"
                            type="button"><i class="fa fa-trash"></i> Hapus</button>
                    </div>
                </div>
                `;
                $(".increment2").after(html);

                $('input[name=id_jenis_pengajuan_dosen]').on('change', function () {
                    countSks()
                });

                $(`#jumlah-beban-kerja-${beban_kerja_cnt}`).on('input', function () {
                    countSks()
                });

                $(`#id-jenis-beban-kerja-${beban_kerja_cnt}`).on('input', function (e) {
                    countSks()
                    const bebanKerjaIdx = e.target.id.match(/id-jenis-beban-kerja-(\w+)/)[1]
                    const uraianElem = document.getElementById(`uraian-${bebanKerjaIdx}`)
                    uraianElem.value = ''
                });

                $(`#uraian-${beban_kerja_cnt}`).on('input', function () {
                    countSks()
                });

                $(`#id-mkb-wajib-${beban_kerja_cnt}`).on('input', function (e) {
                    countSks()
                    if(e.target.value) return
                    const bebanKerjaIdx = e.target.id.match(/id-mkb-wajib-(\w+)/)[1]
                    const uraianElem = document.getElementById(`uraian-${bebanKerjaIdx}`)
                    uraianElem.value = ''
                });
                beban_kerja_cnt++
                countSks()
            });

            $("body").on("click", ".btn-hapus2", function() {
                $(this).parents(".control-group").remove();
                countSks()
            });

            $('#nidn').on('input', function(evt) {
                var value = evt.target.value.trim();
            });

            function countSks() {
                const jmlBebanKerjaElem = document.getElementsByClassName('jumlah-beban-kerja')
                const jenisMkElem = document.querySelector(`input[name=id_jenis_pengajuan_dosen]:checked`)
                sks_count = 0
                numeric = true

                let isMengambilMkbWajib = jenisMkElem.value === 'mkbwajib'
                batas_sks = isMengambilMkbWajib ? 10 : 6

                for(let i=0; i<jmlBebanKerjaElem?.length; i++) {
                    let sks
                    const bebanKerjaIdx = jmlBebanKerjaElem[i].id.match(/jumlah-beban-kerja-(\w+)/)[1]
                    const jenisBebanKerjaElem = document.getElementById(`id-jenis-beban-kerja-${bebanKerjaIdx}`)
                    const mkbElem = document.getElementById(`id-mkb-wajib-${bebanKerjaIdx}`)
                    const mkbFormGroupElem = document.getElementById(`form-id-mkb-wajib-${bebanKerjaIdx}`)
                    const kelasElem = document.getElementById(`kelas-${bebanKerjaIdx}`)
                    const uraianGroupElem = document.getElementById(`form-uraian-${bebanKerjaIdx}`)
                    const uraianElem = document.getElementById(`uraian-${bebanKerjaIdx}`)
                    uraianElem.readOnly = false
                    mkbElem.disabled = false
                    kelasElem.readOnly = false

                    const isPengajaran = jenisBebanKerjaElem.value === '2' || jenisBebanKerjaElem.value === '1'
                                || jenisBebanKerjaElem.value == '3' || jenisBebanKerjaElem.value == '4'
                    if(!isPengajaran || !isMengambilMkbWajib) {
                        mkbElem.required = false
                        mkbElem.value = ''
                        mkbFormGroupElem.classList.add('d-none')
                        uraianGroupElem.classList.remove('d-none')
                    } else if(isMengambilMkbWajib) {
                        mkbElem.required = true
                        mkbFormGroupElem.classList.remove('d-none')
                        uraianGroupElem.classList.add('d-none')
                    }

                    const labelDeskripsi = document.getElementById(`label-deskripsi-${bebanKerjaIdx}`)
                    const labelJumlah = document.getElementById(`label-jumlah-${bebanKerjaIdx}`)
                    const labelUpah = document.getElementById(`label-upah-${bebanKerjaIdx}`)

                    if(!isPengajaran) {
                        kelasElem.readOnly = true
                        labelJumlah.innerText = 'Jumlah Mahasiswa'
                        labelDeskripsi.innerText = 'Deskripsi'
                        labelUpah.innerText = 'Upah per mahasiswa'
                        continue
                    }

                    labelJumlah.innerText = 'Jumlah SKS'
                    labelDeskripsi.innerText = 'Mata Kuliah'
                    labelUpah.innerText = 'Upah per sks/tatap muka'

                    if(isMengambilMkbWajib && mkbElem.value) {
                        uraianElem.value = mkbElem[mkbElem.selectedIndex].text
                        uraianElem.readOnly = true
                    }

                    try {
                        sks = JSON.parse(jmlBebanKerjaElem[i].value)
                        if (typeof sks !== 'number') {
                            numeric = false
                            continue
                        }
                    } catch (e) {
                        numeric = false
                        continue
                    }
                    sks_count += sks
                }

                const mkbElem = document.getElementsByClassName(`id-mkb-wajib`)
                updateMaksSKS()

                const totalSKSElem = document.getElementById('total-sks')
                totalSKSElem.classList.remove('text-danger')
                if(!numeric) {
                    totalSKSElem.classList.add('text-danger')
                    totalSKSElem.innerText = `${sks_count} SKS (Masukkan angka pada kolom jumlah)`
                    return
                }

                if(sks_count > batas_sks) {
                    totalSKSElem.classList.add('text-danger')
                    totalSKSElem.innerText = `${sks_count} SKS (Jumlah SKS melebihi batas yang diperbolehkan)`
                    return
                }
                totalSKSElem.innerText = `${sks_count} SKS`
            }

            function updateMaksSKS() {
                document.getElementById('maks-sks').innerText = `${batas_sks} SKS`
            }

            updateMaksSKS()
        });
    </script>
@endsection
