<!DOCTYPE html>
<html lang="en">

<head>
    <title>DRAFT PERJANJIAN KERJA DOSEN TIDAK TETAP</title>
    <style>
        .text-center {
            text-align: center;
        }

        .font-weight-bold {
            font-weight: bold;
        }

        body {
            text-align: justify;
        }

        td {
            vertical-align: text-top;
        }

        .table {
            /* border: 1px solid grey; */
            padding: 15px;
        }

        /* Create two equal columns that floats next to each other */
        .column {
            float: left;
            width: 40%;
            padding: 10px;
            height: 30px;
            /* Should be removed. Only for demonstration */
        }

        .column2 {
            float: left;
            width: 10%;
            padding: 10px;
            height: 300px;
            /* Should be removed. Only for demonstration */
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        .page_break {
            page-break-before: always;
        }

    </style>
</head>

<body>

    <h3 class="text-center" style="margin-bottom: 0px;"><strong>PERJANJIAN KERJA DOSEN TIDAK TETAP</strong></h3>
    <p class="text-center font-weight-bold" style="margin-top: 0px;">Nomor: {{ $data['no_surat'] }}</p>
    <br>
    <p>Perjanjian ini dibuat dan ditandatangani pada hari {{ $data['hari_pengesahan'] }} tanggal
        {{ $data['tgl_pengesahan_d'] }} bulan {{ $data['tgl_pengesahan_m'] }} tahun
        {{ $data['tgl_pengesahan_y'] }} antara:
    </p>
    <table>
        <tr>
            <td style="width: 25px;" class="text-center">1.</td>
            <td style="width: 150px;">Nama</td>
            <td>: Dr. Eng. Ir. Ahmad Rusdiansyah, M. Eng.</td>
        </tr>
        <tr>
            <td></td>
            <td>NIP</td>
            <td>: 196811091995031003</td>
        </tr>
        <tr>
            <td></td>
            <td>Jabatan</td>
            <td>: Wakil Rektor Bidang Sumber Daya Manusia,
                Organisasi, dan Teknologi dan <span style="margin-left: 10px;">Sistem Informasi
                    Institut Teknologi Sepuluh Nopember.</span></td>
        </tr>
        <tr>
            <td></td>
            <td>Alamat</td>
            <td>: Kampus ITS Sukolilo Surabaya</td>
        </tr>
    </table>
    <p style="margin-left:30px">Dalam hal ini bertindak untuk dan atas nama Institut Teknologi Sepuluh Nopember,
        berkedudukan di Surabaya,
        selanjutnya disebut sebagai <strong>Pihak Pertama</strong>.</p>
    <table>
        <tr>
            <td style="width: 20px;" class="text-center">2.</td>
            <td style="width: 150px;">Nama</td>
            <td>: {{ $data['nama'] }}</td>
        </tr>
        <tr>
            <td></td>
            <td>Tanggal Lahir</td>
            <td>: {{ $data['tgl_lahir'] }}</td>
        </tr>
        <tr>
            <td></td>
            <td>Jenis Kelamin</td>
            <td>: @if ($data['jenis_kelamin'] == 'L')Laki-laki @elseif($data['jenis_kelamin'] == 'P') Perempuan @endif</td>
        </tr>
        <tr>
            <td></td>
            <td>Pendidikan</td>
            <td>: {{ $data['pendidikan_terakhir'] }}</td>
        </tr>
        <tr>
            <td></td>
            <td>Kepakaran/keahlian</td>
            <td>: {{ $data['kepakaran'] }}</td>
        </tr>
        <tr>
            <td></td>
            <td>No. KTP</td>
            <td>: {{ $data['nik'] }}</td>
        </tr>
        <tr>
            <td></td>
            <td>Alamat</td>
            <td>: {{ $data['alamat'] }}</td>
        </tr>
    </table>
    <p style="margin-left:27px">Dalam hal ini bertindak selaku diri sendiri, selanjutnya disebut sebagai <strong>Pihak
            Kedua</strong>.</p>
    <p>Selanjutnya PIHAK PERTAMA dan PIHAK KEDUA secara bersama-sama disebut PARA PIHAK dan secara sendiri disebut
        PIHAK. </p>
    <p style="margin-bottom: 0px;">Menerangkan terlebih dahulu:</p>

    <table style="margin-left: 30px">
        <tr>
            <td style="width: 20px;vertical-align: text-top;">1.</td>
            <td>Bahwa, PIHAK PERTAMA adalah Perguruan Tinggi Negeri yang menerapkan Pola Pengelolaan Keuangan PTN Badan
                Hukum;
            </td>
        </tr>
        <tr>
            <td>2.</td>
            <td>Bahwa, PIHAK PERTAMA membutuhkan tenaga/kepakaran PIHAK KEDUA untuk ditetapkan sebagai dosen tidak tetap
                pada {{ $data['departemen'] }}, {{ $data['fakultas'] }}, Institut Teknologi Sepuluh Nopember;</td>
        </tr>
        <tr>
            <td>3.</td>
            <td>Bahwa, PIHAK KEDUA menyatakaan bersedia ditunjuk sebagai dosen tidak tetap pada
                {{ $data['departemen'] }}, {{ $data['fakultas'] }}, Institut Teknologi Sepuluh Nopember;</td>
        </tr>
    </table>
    <p>Sehubungan dengan perihal tersebut di atas, para pihak sepakat mengadakan Perjanjian Kerja dengan ketentuan dan
        syarat-syarat sebagai berikut.</p>
    <p class="text-center" style="margin-bottom: 0px;">KEWAJIBAN PARA PIHAK</p>
    <p class="text-center" style="margin-top: 0px;">Pasal 1</p>
    <p style="margin-bottom: 0px;"><span style="margin-right: 15px">(1)</span>PIHAK PERTAMA mempunyai kewajiban:</p>

    <table style="margin-left: 30px">
        <tr>
            <td style="width: 20px;">1.</td>
            <td>Menyediakan fasilitas pendukung baik berupa ruang kerja dan peralatan kerja;</td>
        </tr>
        <tr>
            <td>2.</td>
            <td>Membayar honorarium kepada PIHAK KEDUA sesuai beban kerja.</td>
        </tr>
    </table>
    
    <p style="margin-bottom: 0px;"><span style="margin-right: 15px">(2)</span>PIHAK KEDUA mempunyai kewajiban: </p>

    <table style="margin-left: 30px">
        <tr>
            <td style="width: 20px;">1.</td>
            <td>Melaksanakan tugas dengan beban mengajar pada {{ $data['departemen'] }}, {{ $data['fakultas'] }}
                ITS: </td>
        </tr>
    </table>
    <table class="text-center" style="border-collapse: collapse;width:95%;margin-left:30px">
        <tr>
            <th style="border: 1px solid black;">No</th>
            <th style="border: 1px solid black;">Mata Kuliah</th>
            <th style="border: 1px solid black;">Kelas</th>
            <th style="border: 1px solid black;">Jumlah SKS</th>
        </tr>
        @php
            $i = 1;
        @endphp
        @foreach ($beban_kerja_dosen_mengajar as $item)
            <tr>
                <td style="border: 1px solid black;">{{ $i++ }}</td>
                <td style="border: 1px solid black;" class="text-left">{{ $item->deskripsi }}</td>
                <td style="border: 1px solid black;">{{ $item->kelas }}</td>
                <td style="border: 1px solid black;">{{ $item->jumlah }}</td>
            </tr>
        @endforeach


    </table>
    <table style="margin-left: 30px">
        <tr>
            <td style="width: 20px;">2.</td>
            <td>Mematuhi dan/atau menjalankan peraturan dan tata tertib yang ditetapkan oleh PIHAK PERTAMA.</td>
        </tr>
    </table>
    <p class="text-center" style="margin-bottom: 0px;">HAK-HAK PARA PIHAK</p>
    <p class="text-center" style="margin-top: 0px;">Pasal 2</p>
    <p>PIHAK PERTAMA berhak memberikan saran dan / atau teguran terhadap PIHAK KEDUA apabila menurut penilaiannya PIHAK
        KEDUA kurang baik dalam melaksanakan kewajibannya, baik sebagian atau seluruhnya, sebagaimana disebutkan dalam
        pasal 1 perjanjian ini; </p>
    <p class="text-center" style="margin-top: 0px;">Pasal 3</p>
    @foreach ($beban_kerja_dosen_mengajar as $item)
        <p style="margin-top: 0px;margin-bottom: 0px">PIHAK KEDUA berhak menerima honorarium mengajar
            {{ $item->deskripsi }} sebesar
            Rp. {{ number_format($item->upah, 0, ',', '.') }}
            per SKS/tatap muka.</p>
    @endforeach

    @if ($beban_kerja_dosen_ta->count() >= 1)
        @foreach ($beban_kerja_dosen_ta as $item)
            @php
                $beban = DB::table('jenis_beban_kerja_dosen')
                    ->where('id_jenis_beban_kerja_dosen', $item->id_jenis_beban_kerja_dosen)
                    ->first();
            @endphp
            <p style="margin-top: 0px;margin-bottom: 0px">PIHAK KEDUA berhak menerima honorarium
                {{ $beban->nama }} sebesar Rp. {{ number_format($item->upah, 0, ',', '.') }} per mahasiswa.</p>
        @endforeach
    @endif


    <p class="text-center" style="margin-bottom: 0px;">JANGKA WAKTU</p>
    <p class="text-center" style="margin-top: 0px;">Pasal 4</p>
    <p>Jangka waktu perjanjian kerja ini adalah terhitung sejak tanggal {{ $data['tgl_mulai_d'] }}
        {{ $data['tgl_mulai_m'] }} {{ $data['tgl_mulai_y'] }} sampai dengan tanggal {{ $data['tgl_akhir_d'] }}
        {{ $data['tgl_akhir_m'] }} {{ $data['tgl_akhir_y'] }} .</p>

    <p class="text-center" style="margin-bottom: 0px;">PEMUTUSAN HUBUNGAN KERJA </p>
    <p class="text-center" style="margin-top: 0px;">Pasal 5</p>
    <table style="margin-left: 30px">
        <tr>
            <td style="width: 20px;">a.</td>
            <td>Menurut penilaian PIHAK PERTAMA, PIHAK KEDUA tidak memenuhi kewajibannya seperti tersebut pada pasal 1,
                dan tidak mematuhi setiap ketentuan dalam perjanjian ini.</td>
        </tr>
        <tr>
            <td>b.</td>
            <td>Menurut penilaian PIHAK PERTAMA, PIHAK KEDUA karena sesuatu hal dipandang tidak mampu lagi melaksanakan
                kewajiban sebagaimana ditetapkan dalam perjanjian kerja ini.</td>
        </tr>
    </table>

    <p class="text-center" style="margin-bottom: 0px;">PENYELESAIAN PERSELISIHAN</p>
    <p class="text-center" style="margin-top: 0px;">Pasal 6</p>
    <p>Perselisihan dalam pelaksanaan perjanjian kerja ini akan diselesaikan secara musyawarah untuk mufakat oleh PARA
        PIHAK. Apabila tidak dapat diselesaikan dengan musyawarah maka PARA PIHAK sepakat untuk menyelesaikan melalui
        Pengadilan Negeri Surabaya.
    </p>
    <div class="page_break"></div>
    <p class="text-center" style="margin-bottom: 0px;">PENUTUP</p>
    <p class="text-center" style="margin-top: 0px;">Pasal 7</p>
    <p>Hal-hal yang belum diatur dan/atau belum cukup diatur dalam Perjanjian Kerja ini, maka PARA PIHAK sepakat akan
        mengatur dalam adendum Perjanjian Kerja yang merupakan bagian tak terpisahkan dari Perjanjian Kerja ini.</p>
    <p class="text-center" style="margin-bottom: 0px;">Pasal 8</p>
    <p class="text-center" style="margin-top: 0px;">Addendum</p>


    <p>Perjanjian Kerja ini dibuat dalam rangkap 2 (dua) dengan 2 (dua) eksemplar diantaranya bermeterai cukup dan
        mempunyai kekuatan hukum yang sama, masing-masing pihak memperoleh 1 (satu) eksemplar.
    </p>
    <div class="row">
        <div class="column">
            <table class="table text-center" style="width: 120%">
                <tr class="font-weight-bold">
                    <th>PIHAK PERTAMA</th>
                </tr>
                <tr>
                    <td><br><br><br><br><br><br><br></td>
                </tr>
                <tr>
                    <td>Dr. Eng. Ir. Ahmad Rusdiansyah, M. Eng.</td>
                </tr>
                <tr>
                    <td>NIP 196811091995031003</td>
                </tr>
            </table>
        </div>
        <div class="column2">
        </div>
        <div class="column">
            <table class="table text-center" style="width: 100%;">
                <tr class="font-weight-bold">
                    <th>PIHAK KEDUA</th>
                </tr>
                <tr>
                    <td style="text-align: left;padding:10px; width:5px;"><img
                            src="https://drive.google.com/uc?id=1AdujD5RIpbHQN4UWvgkIAdziLv8d9uXf" style="width: 127px"></td>

                </tr>
                <tr>
                    <td>{{ ucwords($data['nama']) }}</td>
                </tr>
                <tr>
                    <td><br></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="page_break"></div>
    <h3 class="text-center" style="margin-bottom: 0px;"><strong>PERJANJIAN KERJA DOSEN TIDAK TETAP</strong></h3>
    <p class="text-center font-weight-bold" style="margin-top: 0px;">Nomor: {{ $data['no_surat'] }}</p>
    <br>
    <p>Perjanjian ini dibuat dan ditandatangani pada hari {{ $data['hari_pengesahan'] }} tanggal
        {{ $data['tgl_pengesahan_d'] }} bulan {{ $data['tgl_pengesahan_m'] }} tahun
        {{ $data['tgl_pengesahan_y'] }} antara:
    </p>
    <table>
        <tr>
            <td style="width: 25px;" class="text-center">1.</td>
            <td style="width: 150px;">Nama</td>
            <td>: Dr. Eng. Ir. Ahmad Rusdiansyah, M. Eng.</td>
        </tr>
        <tr>
            <td></td>
            <td>NIP</td>
            <td>: 196811091995031003</td>
        </tr>
        <tr>
            <td></td>
            <td>Jabatan</td>
            <td>: Wakil Rektor Bidang Sumber Daya Manusia,
                Organisasi, dan Teknologi dan <span style="margin-left: 10px;">Sistem Informasi
                    Institut Teknologi Sepuluh Nopember.</span></td>
        </tr>
        <tr>
            <td></td>
            <td>Alamat</td>
            <td>: Kampus ITS Sukolilo Surabaya</td>
        </tr>
    </table>
    <p style="margin-left:30px">Dalam hal ini bertindak untuk dan atas nama Institut Teknologi Sepuluh Nopember,
        berkedudukan di Surabaya,
        selanjutnya disebut sebagai <strong>Pihak Pertama</strong>.</p>
    <table>
        <tr>
            <td style="width: 20px;" class="text-center">2.</td>
            <td style="width: 150px;">Nama</td>
            <td>: {{ $data['nama'] }}</td>
        </tr>
        <tr>
            <td></td>
            <td>Tanggal Lahir</td>
            <td>: {{ $data['tgl_lahir'] }}</td>
        </tr>
        <tr>
            <td></td>
            <td>Jenis Kelamin</td>
            <td>: @if ($data['jenis_kelamin'] == 'L')Laki-laki @elseif($data['jenis_kelamin'] == 'P') Perempuan @endif</td>
        </tr>
        <tr>
            <td></td>
            <td>Pendidikan</td>
            <td>: {{ $data['pendidikan_terakhir'] }}</td>
        </tr>
        <tr>
            <td></td>
            <td>Kepakaran/keahlian</td>
            <td>: {{ $data['kepakaran'] }}</td>
        </tr>
        <tr>
            <td></td>
            <td>No. KTP</td>
            <td>: {{ $data['nik'] }}</td>
        </tr>
        <tr>
            <td></td>
            <td>Alamat</td>
            <td>: {{ $data['alamat'] }}</td>
        </tr>
    </table>
    <p style="margin-left:27px">Dalam hal ini bertindak selaku diri sendiri, selanjutnya disebut sebagai <strong>Pihak
            Kedua</strong>.</p>
    <p>Selanjutnya PIHAK PERTAMA dan PIHAK KEDUA secara bersama-sama disebut PARA PIHAK dan secara sendiri disebut
        PIHAK. </p>
    <p style="margin-bottom: 0px;">Menerangkan terlebih dahulu:</p>

    <table style="margin-left: 30px">
        <tr>
            <td style="width: 20px;vertical-align: text-top;">1.</td>
            <td>Bahwa, PIHAK PERTAMA adalah Perguruan Tinggi Negeri yang menerapkan Pola Pengelolaan Keuangan PTN Badan
                Hukum;
            </td>
        </tr>
        <tr>
            <td>2.</td>
            <td>Bahwa, PIHAK PERTAMA membutuhkan tenaga/kepakaran PIHAK KEDUA untuk ditetapkan sebagai dosen tidak tetap
                pada {{ $data['departemen'] }}, {{ $data['fakultas'] }}, Institut Teknologi Sepuluh Nopember;</td>
        </tr>
        <tr>
            <td>3.</td>
            <td>Bahwa, PIHAK KEDUA menyatakaan bersedia ditunjuk sebagai dosen tidak tetap pada
                {{ $data['departemen'] }}, {{ $data['fakultas'] }}, Institut Teknologi Sepuluh Nopember;</td>
        </tr>
    </table>
    <p>Sehubungan dengan perihal tersebut di atas, para pihak sepakat mengadakan Perjanjian Kerja dengan ketentuan dan
        syarat-syarat sebagai berikut.</p>
    <p class="text-center" style="margin-bottom: 0px;">KEWAJIBAN PARA PIHAK</p>
    <p class="text-center" style="margin-top: 0px;">Pasal 1</p>
    <p style="margin-bottom: 0px;"><span style="margin-right: 15px">(1)</span>PIHAK PERTAMA mempunyai kewajiban:</p>

    <table style="margin-left: 30px">
        <tr>
            <td style="width: 20px;">1.</td>
            <td>Menyediakan fasilitas pendukung baik berupa ruang kerja dan peralatan kerja;</td>
        </tr>
        <tr>
            <td>2.</td>
            <td>Membayar honorarium kepada PIHAK KEDUA sesuai beban kerja.</td>
        </tr>
    </table>
    
    <p style="margin-bottom: 0px;"><span style="margin-right: 15px">(2)</span>PIHAK KEDUA mempunyai kewajiban: </p>

    <table style="margin-left: 30px">
        <tr>
            <td style="width: 20px;">1.</td>
            <td>Melaksanakan tugas dengan beban mengajar pada {{ $data['departemen'] }}, {{ $data['fakultas'] }}
                ITS: </td>
        </tr>
    </table>
    <table class="text-center" style="border-collapse: collapse;width:95%;margin-left:30px">
        <tr>
            <th style="border: 1px solid black;">No</th>
            <th style="border: 1px solid black;">Mata Kuliah</th>
            <th style="border: 1px solid black;">Kelas</th>
            <th style="border: 1px solid black;">Jumlah SKS</th>
        </tr>
        @php
            $i = 1;
        @endphp
        @foreach ($beban_kerja_dosen_mengajar as $item)
            <tr>
                <td style="border: 1px solid black;">{{ $i++ }}</td>
                <td style="border: 1px solid black;" class="text-left">{{ $item->deskripsi }}</td>
                <td style="border: 1px solid black;">{{ $item->kelas }}</td>
                <td style="border: 1px solid black;">{{ $item->jumlah }}</td>
            </tr>
        @endforeach


    </table>
    <table style="margin-left: 30px">
        <tr>
            <td style="width: 20px;">2.</td>
            <td>Mematuhi dan/atau menjalankan peraturan dan tata tertib yang ditetapkan oleh PIHAK PERTAMA.</td>
        </tr>
    </table>
    <p class="text-center" style="margin-bottom: 0px;">HAK-HAK PARA PIHAK</p>
    <p class="text-center" style="margin-top: 0px;">Pasal 2</p>
    <p>PIHAK PERTAMA berhak memberikan saran dan / atau teguran terhadap PIHAK KEDUA apabila menurut penilaiannya PIHAK
        KEDUA kurang baik dalam melaksanakan kewajibannya, baik sebagian atau seluruhnya, sebagaimana disebutkan dalam
        pasal 1 perjanjian ini; </p>
    <p class="text-center" style="margin-top: 0px;">Pasal 3</p>
    @foreach ($beban_kerja_dosen_mengajar as $item)
        <p style="margin-top: 0px;margin-bottom: 0px">PIHAK KEDUA berhak menerima honorarium mengajar
            {{ $item->deskripsi }} sebesar
            Rp. {{ number_format($item->upah, 0, ',', '.') }}
            per SKS/tatap muka.</p>
    @endforeach

    @if ($beban_kerja_dosen_ta->count() >= 1)
        @foreach ($beban_kerja_dosen_ta as $item)
            @php
                $beban = DB::table('jenis_beban_kerja_dosen')
                    ->where('id_jenis_beban_kerja_dosen', $item->id_jenis_beban_kerja_dosen)
                    ->first();
            @endphp
            <p style="margin-top: 0px;margin-bottom: 0px">PIHAK KEDUA berhak menerima honorarium
                {{ $beban->nama }} sebesar Rp. {{ number_format($item->upah, 0, ',', '.') }} per mahasiswa.</p>
        @endforeach
    @endif


    <p class="text-center" style="margin-bottom: 0px;">JANGKA WAKTU</p>
    <p class="text-center" style="margin-top: 0px;">Pasal 4</p>
    <p>Jangka waktu perjanjian kerja ini adalah terhitung sejak tanggal {{ $data['tgl_mulai_d'] }}
        {{ $data['tgl_mulai_m'] }} {{ $data['tgl_mulai_y'] }} sampai dengan tanggal {{ $data['tgl_akhir_d'] }}
        {{ $data['tgl_akhir_m'] }} {{ $data['tgl_akhir_y'] }} .</p>

    <p class="text-center" style="margin-bottom: 0px;">PEMUTUSAN HUBUNGAN KERJA </p>
    <p class="text-center" style="margin-top: 0px;">Pasal 5</p>
    <table style="margin-left: 30px">
        <tr>
            <td style="width: 20px;">a.</td>
            <td>Menurut penilaian PIHAK PERTAMA, PIHAK KEDUA tidak memenuhi kewajibannya seperti tersebut pada pasal 1,
                dan tidak mematuhi setiap ketentuan dalam perjanjian ini.</td>
        </tr>
        <tr>
            <td>b.</td>
            <td>Menurut penilaian PIHAK PERTAMA, PIHAK KEDUA karena sesuatu hal dipandang tidak mampu lagi melaksanakan
                kewajiban sebagaimana ditetapkan dalam perjanjian kerja ini.</td>
        </tr>
    </table>

    <p class="text-center" style="margin-bottom: 0px;">PENYELESAIAN PERSELISIHAN</p>
    <p class="text-center" style="margin-top: 0px;">Pasal 6</p>
    <p>Perselisihan dalam pelaksanaan perjanjian kerja ini akan diselesaikan secara musyawarah untuk mufakat oleh PARA
        PIHAK. Apabila tidak dapat diselesaikan dengan musyawarah maka PARA PIHAK sepakat untuk menyelesaikan melalui
        Pengadilan Negeri Surabaya.
    </p>
    <div class="page_break"></div>
    <p class="text-center" style="margin-bottom: 0px;">PENUTUP</p>
    <p class="text-center" style="margin-top: 0px;">Pasal 7</p>
    <p>Hal-hal yang belum diatur dan/atau belum cukup diatur dalam Perjanjian Kerja ini, maka PARA PIHAK sepakat akan
        mengatur dalam adendum Perjanjian Kerja yang merupakan bagian tak terpisahkan dari Perjanjian Kerja ini.</p>
    <p class="text-center" style="margin-bottom: 0px;">Pasal 8</p>
    <p class="text-center" style="margin-top: 0px;">Addendum</p>


    <p>Perjanjian Kerja ini dibuat dalam rangkap 2 (dua) dengan 2 (dua) eksemplar diantaranya bermeterai cukup dan
        mempunyai kekuatan hukum yang sama, masing-masing pihak memperoleh 1 (satu) eksemplar.
    </p>
    <div class="row">
        <div class="column">
            <table class="table text-center" style="width: 120%">
                <tr class="font-weight-bold">
                    <th>PIHAK PERTAMA</th>
                </tr>
                <tr>
                    <td style="text-align: left;padding:10px; width:5px;"><img
                        src="https://drive.google.com/uc?id=1AdujD5RIpbHQN4UWvgkIAdziLv8d9uXf" style="width: 127px"></td>
                    
                </tr>
                <tr>
                    <td>Dr. Eng. Ir. Ahmad Rusdiansyah, M. Eng.</td>
                </tr>
                <tr>
                    <td>NIP 196811091995031003</td>
                </tr>
            </table>
        </div>
        <div class="column2">
        </div>
        <div class="column">
            <table class="table text-center" style="width: 100%;">
                <tr class="font-weight-bold">
                    <th>PIHAK KEDUA</th>
                </tr>
                <tr>
                    <td><br><br><br><br><br><br><br></td>
                </tr>
                <tr>
                    <td>{{ ucwords($data['nama']) }}</td>
                </tr>
                <tr>
                    <td><br></td>
                </tr>
            </table>
        </div>
    </div>
</body>

</html>
