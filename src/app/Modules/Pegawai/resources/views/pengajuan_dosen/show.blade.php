@extends('Ui::base')

@section('title')
    Pengajuan Dosen
@endsection

@section('header_title')
    Pengajuan Dosen
@endsection
@section('breadcrumbs')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-style1 mg-b-10">
            <li class="breadcrumb-item"><a href="#">
                    @if (isset($page['menu'])){{ ucwords($page['menu']) }}@endif
                </a></li>
            <li class="breadcrumb-item"><a href="#">
                    @if (isset($page['submenu'])){{ ucwords($page['submenu']) }}
                    @endif
                </a></li>
            <li class="breadcrumb-item active" aria-current="page">
                @if (isset($page['halaman'])){{ ucwords($page['halaman']) }}@endif
            </li>
        </ol>
    </nav>
@endsection
@section('header_right')
    <div class="d-md-block">
        <a class="btn btn-white tx-montserrat tx-semibold" href="{{ url('pegawai/pengajuan-dosen') }}"><i
                data-feather="arrow-left" class="wd-10 mg-r-5"></i> Kembali</a>
    </div>
@endsection
@section('content')
    <div class="col-sm-12 col-lg-12">
        @if (session('success'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ session('success') }}
            </div>
        @elseif(session('failed'))
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ session('failed') }}
            </div>
        @endif
        <div class="card card-body">
            <div class="row">
                <div class="col-lg-9 mg-t-5">
                    <div class="col-lg-12 pd-x-0">
                        <div class="card card-body">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                        aria-controls="home" aria-selected="true">Data Diri</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                                        aria-controls="profile" aria-selected="false">Riwayat Kontrak Pekerjaan</a>
                                </li>
                            </ul>
                            <div class="tab-content bd bd-gray-300 bd-t-0 pd-20" id="myTabContent">
                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    {{-- <h6>Data Diri</h6> --}}
                                    <div class="row justify-content-center">
                                        <div class="col-12 mg-b-30 mg-t-20">
                                            <div class="avatar avatar-xxl avatar-online text-center"><img
                                                    src="{{ route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_berkas_pas_foto ?? '-']) }}"
                                                    class="rounded-circle" alt="Pas Foto">
                                            </div>
                                        </div><!-- col -->
                                    </div>
                                    <div id="baru">
                                        <div class="form-row">
                                            <div class="form-group col-md-2">
                                                <label for="gelar_depan">Gelar Depan</label>
                                                <input type="text" class="form-control" id="gelar_depan" 
                                                value="{{ $data['dosen']->gelar_depan }}" name="gelar_depan" placeholder="-" disabled>
                                            </div>
                                            <div class="form-group col-md-8">
                                                <label for="nama">Nama Lengkap</label>
                                                <input type="text" class="form-control" id="nama" name="nama"
                                                    value="{{ $data['dosen']->nama }}" placeholder="-" disabled>
                                            </div>
                                            <div class="form-group col-md-2">
                                                <label for="gelar_belakang">Gelar Belakang</label>
                                                <input type="text" class="form-control" id="gelar_belakang" 
                                                value="{{ $data['dosen']->gelar_belakang }}" name="gelar_belakang" placeholder="-" disabled>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-4">
                                                <label for="nik">NIK</label>
                                                <input type="text" class="form-control" id="nik" name="nik"
                                                    value="{{ $data['dosen']->nik }}" placeholder="-" disabled>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="npp">NPP</label>
                                                <input type="number" class="form-control" id="npp" name="npp"
                                                value="{{ $data['dosen']->npp }}" placeholder="-" disabled>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="nidn">NIDN / NIDK / NUP</label>
                                                <input type="number" class="form-control" id="nidn" name="nidn"
                                                value="{{ $data['dosen']->nidn }}" placeholder="-" disabled>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-4">
                                                <label for="nama">Tanggal Lahir</label>
                                                <input type="date" class="form-control" id="tgl_lahir" name="tgl_lahir"
                                                    value="{{ date('Y-m-d', strtotime($data['dosen']->tgl_lahir)) }}" disabled>
                                            </div>
                                            <div class="form-group col-md-5">
                                                <label for="tempat_lahir">Tempat Lahir</label>
                                                <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir"
                                                    value="{{ $data['dosen']->tempat_lahir }}" placeholder="-" disabled>
                                            </div>
                                            <div class="form-group col-md-3">
                                                <label for="jenis_kelamin">Jenis Kelamin</label>
                                                <input type="text" class="form-control" id="jenis_kelamin" name="jenis_kelamin"
                                                    value="{{ $data['dosen']->jenis_kelamin == 'L' ? 'Laki-laki' : 'Perempuan' }}" disabled>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="kepakaran">Kepakaran</label>
                                                <input type="text" class="form-control" id="kepakaran" name="kepakaran"
                                                    value="{{ $data['dosen']->kepakaran }}" placeholder="-" disabled>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="pendidikan_terakhir">Pendidikan Terakhir</label>
                                                <input type="text" class="form-control" id="pendidikan_terakhir" name="pendidikan_terakhir"
                                                    value="{{ $data['pendidikan']->nama }}" placeholder="-" disabled>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="alamat">Alamat</label>
                                            <input type="text" class="form-control" id="alamat" name="alamat"
                                                value="{{ $data['dosen']->alamat }}" disabled>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="email">Email</label>
                                                <input type="email" class="form-control" id="email" name="email"
                                                    value="{{ $data['dosen']->email }}" placeholder="-" disabled>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="nomor_hp">Nomor HP</label>
                                                <input type="number" class="form-control" id="nomor_hp" name="nomor_hp"
                                                    value="{{ $data['dosen']->nomor_hp }}" placeholder="-" disabled>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-12">
                                                <label for="unit">Unit</label>
                                                <input type="text" class="form-control" id="unit" name="unit"
                                                    value="{{ $data['unit']->nama }}" placeholder="Unit" disabled>
                                            </div>
                                        </div>
    
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="id_status_pengajuan_dosen">Status Pengajuan</label>
                                                <select class="custom-select" id="id_status_pengajuan_dosen"
                                                    name="id_status_pengajuan_dosen" disabled>
                                                    <option value="0"
                                                        {{ $data['pengajuan_dosen']->id_status_pengajuan == '0' ? 'selected' : '' }}>
                                                        Baru</option>
                                                    <option value="1"
                                                        {{ $data['pengajuan_dosen']->id_status_pengajuan == '1' ? 'selected' : '' }}>
                                                        Perpanjangan</option>
                                                </select>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="id_jenis_pengajuan_dosen">Jenis Pengajuan</label>
                                                <select class="custom-select" id="id_jenis_pengajuan_dosen"
                                                    name="id_jenis_pengajuan_dosen" disabled>
                                                    @foreach ($data['jenis_pengajuan_dosen'] as $jenis_pengajuan_dosen)
                                                    <option value="{{ $jenis_pengajuan_dosen->id_jenis_pengajuan_dosen }}"
                                                        {{ $data['pengajuan_dosen']->id_jenis_pengajuan_dosen == $jenis_pengajuan_dosen->id_jenis_pengajuan_dosen ? 'selected' : '' }}>
                                                        {{ $jenis_pengajuan_dosen->nama }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-4">
                                                <label for="tanggal_pengajuan">Tanggal Pengajuan</label>
                                                <input type="date" class="form-control" id="tanggal_pengajuan"
                                                    name="tanggal_pengajuan"
                                                    value="{{ date('Y-m-d', strtotime($data['pengajuan_dosen']->tanggal_pengajuan)) }}" disabled>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="tanggal_mulai_kerja">Tanggal Mulai Kerja</label>
                                                <input type="date" class="form-control" id="tanggal_mulai_kerja"
                                                    name="tanggal_mulai_kerja"
                                                    value="{{ date('Y-m-d', strtotime($data['pengajuan_dosen']->tanggal_mulai_kerja)) }}"
                                                    placeholder="Tanggal Mulai Kerja" disabled>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="tanggal_akhir_kerja">Tanggal Berakhir Kontrak Kerja</label>
                                                <input type="date" class="form-control" id="tanggal_akhir_kerja"
                                                    name="tanggal_akhir_kerja"
                                                    value="{{ date('Y-m-d', strtotime($data['pengajuan_dosen']->tanggal_akhir_kerja)) }}" disabled>
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-12">
                                                <label for="tanggal_pengajuan">Beban Kerja (Total SKS Pengajaran : {{ $data['sks_ajar'] }} SKS)</label>
                                                <table class="table table-bordered">
                                                    <thead>
                                                      <tr>
                                                        <th scope="col">No</th>
                                                        <th scope="col">Jenis</th>
                                                        <th scope="col">Deskripsi / Mata Kuliah</th>
                                                        <th scope="col">Kelas</th>
                                                        <th scope="col">Jumlah</th>
                                                        <th scope="col">Upah</th>
                                                      </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $beban_kerja_dosen_idx = 1;
                                                        @endphp
                                                        @foreach ($data['beban_kerja_dosen'] as $beban_kerja_dosen)
                                                        <tr>
                                                          <th scope="row">{{ $beban_kerja_dosen_idx++ }}</th>
                                                          <td>{{ $beban_kerja_dosen->jenis_beban_kerja_dosen->nama }}</td>
                                                          <td>{{ $beban_kerja_dosen->deskripsi }}</td>
                                                          <td>{{ $beban_kerja_dosen->kelas ?? '-' }}</td>
                                                          <td>{{ round($beban_kerja_dosen->jumlah, 1) }}</td>
                                                          <td>{{ $beban_kerja_dosen->upah }}</td>
                                                        </tr>
                                                        @endforeach
                                                        @if (!$data['beban_kerja_dosen']->count())
                                                            <tr class="tx-center">
                                                                <td colspan="6" class="tx-italic tx-danger">Belum ada data</td>
                                                            </tr>
                                                        @endif
                                                    </tbody>
                                                  </table>
                                                <p class="mg-l-30"></p>
                                            </div>
                                        </div>
                                        <hr class="mg-t-10">

                                        <h4 id="section2" class="">Lihat Berkas</h4>
                                        <span class="tx-10 tx-spacing-1 tx-color-03 tx-uppercase tx-semibold">Pas foto, KTP, dan berkas riwayat hidup</span>
                                        <div class="form-row mt-2">
                                            <div class="form-group col-md-4">
                                                <a class="btn btn-block btn-its btn-xs {{ !$data['dosen']->id_berkas_pas_foto ? 'disabled' : '' }}"
                                                    href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_berkas_pas_foto ?? '-'])}}"
                                                    target="_blank"><i class="fa fa-download"></i> Pas Foto</a>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <a class="btn btn-block btn-its btn-xs {{ !$data['dosen']->id_berkas_ktp ? 'disabled' : '' }}"
                                                    href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_berkas_ktp ?? '-'])}}"
                                                    target="_blank"><i class="fa fa-download"></i> Kartu Tanda Penduduk</a>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <a class="btn btn-block btn-its btn-xs {{ !$data['dosen']->id_berkas_riwayat_hidup ? 'disabled' : '' }}"
                                                    href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_berkas_riwayat_hidup ?? '-'])}}"
                                                    target="_blank"><i class="fa fa-download"></i> Berkas Riwayat Hidup</a>
                                            </div>
                                        </div>
                                        <span class="tx-10 tx-spacing-1 tx-color-03 tx-uppercase tx-semibold">Berkas S1</span>
                                        <div class="form-row mt-2">
                                            <div class="form-group col-md-4">
                                                <a class="btn btn-block btn-its btn-xs {{ !$data['dosen']->id_ijazah_s1 ? 'disabled' : '' }}"
                                                    href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_ijazah_s1 ?? '-'])}}"
                                                    target="_blank"><i class="fa fa-download"></i> Ijazah S1</a>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <a class="btn btn-block btn-its btn-xs {{ !$data['dosen']->id_transkrip_s1 ? 'disabled' : '' }}"
                                                    href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_transkrip_s1 ?? '-'])}}"
                                                    target="_blank"><i class="fa fa-download"></i> Transkrip S1</a>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <a class="btn btn-block btn-its btn-xs {{ !$data['dosen']->id_penyetaraan_s1 ? 'disabled' : '' }}"
                                                    href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_penyetaraan_s1 ?? '-'])}}"
                                                    target="_blank"><i class="fa fa-download"></i> Penyetaraan S1</a>
                                            </div>
                                        </div>
                                        <span class="tx-10 tx-spacing-1 tx-color-03 tx-uppercase tx-semibold">Berkas S2</span>
                                        <div class="form-row mt-2">
                                            <div class="form-group col-md-4">
                                                <a class="btn btn-block btn-its btn-xs {{ !$data['dosen']->id_ijazah_s2 ? 'disabled' : '' }}"
                                                    href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_ijazah_s2 ?? '-'])}}"
                                                    target="_blank"><i class="fa fa-download"></i> Ijazah S2</a>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <a class="btn btn-block btn-its btn-xs {{ !$data['dosen']->id_transkrip_s2 ? 'disabled' : '' }}"
                                                    href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_transkrip_s2 ?? '-'])}}"
                                                    target="_blank"><i class="fa fa-download"></i> Transkrip S2</a>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <a class="btn btn-block btn-its btn-xs {{ !$data['dosen']->id_penyetaraan_s2 ? 'disabled' : '' }}"
                                                    href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_penyetaraan_s2 ?? '-'])}}"
                                                    target="_blank"><i class="fa fa-download"></i> Penyetaraan S2</a>
                                            </div>
                                        </div>
                                        <span class="tx-10 tx-spacing-1 tx-color-03 tx-uppercase tx-semibold">Berkas S3</span>
                                        <div class="form-row mt-2">
                                            <div class="form-group col-md-4">
                                                <a class="btn btn-block btn-its btn-xs {{ !$data['dosen']->id_ijazah_s3 ? 'disabled' : '' }}"
                                                    href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_ijazah_s3 ?? '-'])}}"
                                                    target="_blank"><i class="fa fa-download"></i> Ijazah S3</a>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <a class="btn btn-block btn-its btn-xs {{ !$data['dosen']->id_transkrip_s3 ? 'disabled' : '' }}"
                                                    href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_transkrip_s3 ?? '-'])}}"
                                                    target="_blank"><i class="fa fa-download"></i> Transkrip S3</a>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <a class="btn btn-block btn-its btn-xs {{ !$data['dosen']->id_penyetaraan_s3 ? 'disabled' : '' }}"
                                                    href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_penyetaraan_s3 ?? '-'])}}"
                                                    target="_blank"><i class="fa fa-download"></i> Penyetaraan S3</a>
                                            </div>
                                        </div>
                                        <span class="tx-10 tx-spacing-1 tx-color-03 tx-uppercase tx-semibold">Analisis BKD dan Surat Institusi Asal</span>
                                        <div class="form-row mt-2">
                                            <div class="form-group col-md-6">
                                                <a class="btn btn-block btn-its btn-xs {{ !$data['dosen']->id_berkas_analisis_bkd ? 'disabled' : '' }}"
                                                    href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_berkas_analisis_bkd ?? '-'])}}"
                                                    target="_blank"><i class="fa fa-download"></i> Analisis Beban Kerja Dosen</a>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <a class="btn btn-block btn-its btn-xs {{ !$data['dosen']->id_surat_izin_institusi_asal ? 'disabled' : '' }}"
                                                    href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_surat_izin_institusi_asal ?? '-'])}}"
                                                    target="_blank"><i class="fa fa-download"></i> Surat Izin Institusi Asal</a>
                                            </div>
                                        </div>
                                        <span class="tx-10 tx-spacing-1 tx-color-03 tx-uppercase tx-semibold">Berkas Kesehatan</span>
                                        <div class="form-row mt-2">
                                            <div class="form-group col-md-4">
                                                <a class="btn btn-block btn-its btn-xs {{ !$data['dosen']->id_berkas_sehat_rohani ? 'disabled' : '' }}"
                                                    href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_berkas_sehat_rohani ?? '-'])}}"
                                                    target="_blank"><i class="fa fa-download"></i> Sehat Rohani</a>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <a class="btn btn-block btn-its btn-xs {{ !$data['dosen']->id_berkas_sehat_jasmani ? 'disabled' : '' }}"
                                                    href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_berkas_sehat_jasmani ?? '-'])}}"
                                                    target="_blank"><i class="fa fa-download"></i> Sehat Jasmani</a>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <a class="btn btn-block btn-its btn-xs {{ !$data['dosen']->id_berkas_bebas_narkotika ? 'disabled' : '' }}"
                                                    href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_berkas_bebas_narkotika ?? '-'])}}"
                                                    target="_blank"><i class="fa fa-download"></i> Bebas Narkotika</a>
                                            </div>
                                        </div>
                                        <span class="tx-10 tx-spacing-1 tx-color-03 tx-uppercase tx-semibold">Berkas dari dekan</span>
                                        <div class="form-row mt-2">
                                            <div class="form-group col-md-6">
                                                <a class="btn btn-block btn-its btn-xs {{ !$data['dosen']->id_berkas_pernyataan_dekan ? 'disabled' : '' }}"
                                                    href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_berkas_pernyataan_dekan ?? '-'])}}"
                                                    target="_blank"><i class="fa fa-download"></i> Pernyataan Dekan</a>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <a class="btn btn-block btn-its btn-xs {{ !$data['dosen']->id_berkas_minimal_mengajar ? 'disabled' : '' }}"
                                                    href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_berkas_minimal_mengajar ?? '-'])}}"
                                                    target="_blank"><i class="fa fa-download"></i> Berkas Minimal Mengajar dari Dekan</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                    <h6>Riwayat Pekerjaan</h6>
                                    <table class="table table-bordered">
                                        <thead>
                                          <tr>
                                            <th scope="col">No</th>
                                            <th scope="col">No. Surat</th>
                                            <th scope="col">Unit</th>
                                            <th scope="col">Status</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $riwayat_pekerjaan_idx = 1;
                                            @endphp
                                            @foreach ($data['riwayat_pekerjaan'] as $riwayat_pekerjaan)
                                            <tr>
                                              <th scope="row">{{ $riwayat_pekerjaan_idx++ }}</th>
                                              <td>{{ $riwayat_pekerjaan->no_surat ?? '-' }}</td>
                                              <td>{{ $riwayat_pekerjaan->unit }}</td>
                                              <td>
                                                  @if ($riwayat_pekerjaan->is_approved0 == NULL)
                                                  <span class="badge badge-secondary"> Menunggu Approval Unit</span>
                                                  @elseif($riwayat_pekerjaan->is_approved1 == NULL && $riwayat_pekerjaan->is_approved0 != NULL)
                                                    @if ($riwayat_pekerjaan->is_approved0 == '2')
                                                        @if ($riwayat_pekerjaan->date_approved0 < $riwayat_pekerjaan->updated_at)
                                                        <span class="badge badge-warning"> Revisi Kepala Unit (Telah dilakukan revisi)</span>
                                                        @else
                                                        <span class="badge badge-warning"> Revisi Kepala Unit</span>
                                                        @endif
                                                    @elseif($riwayat_pekerjaan->is_approved0 == '0')
                                                    <span class="badge badge-danger"> Ditolak Kepala Unit</span>
                                                    @else
                                                    <span class="badge badge-primary"> Menunggu Approval Pimpinan SDMO</span>
                                                    @endif
                                                  @else
                                                    @if ($riwayat_pekerjaan->is_approved1 == '2')
                                                        @if ($riwayat_pekerjaan->date_approved1 < $riwayat_pekerjaan->updated_at)
                                                        <span class="badge badge-warning"> Revisi Pimpinan SDMO (Telah dilakukan revisi)</span>
                                                        @else
                                                        <span class="badge badge-warning"> Revisi Pimpinan SDMO</span>
                                                        @endif
                                                    @elseif($riwayat_pekerjaan->is_approved1 == '0')
                                                    <span class="badge badge-danger"> Ditolak Pimpinan SDMO</span>
                                                    @else
                                                    <span class="badge badge-success"> Disetujui</span>
                                                    @endif
                                                  @endif
                                              </td>
                                            </tr>
                                            @endforeach
                                            @if (!$data['riwayat_pekerjaan']->count())
                                                <tr class="tx-center">
                                                    <td colspan="4"><p class="tx-italic mg-t-20">Belum ada riwayat kontrak pekerjaan</p></td>
                                                </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 mg-t-10">
                    <div class="card card-body pd-x-5 pd-y-5">
                        <div class="card-content">
                            <div class="card-header">
                                <label class="tx-montserrat tx-medium tx-uppercase tx-center">Status
                                    Pengajuan</label>
                            </div>
                            <div class="card-body">

                                <div class="row form-row mg-t-7">
                                    <div class="col-md-12">
                                        {{-- Kepala Kantor --}}
                                        @if (Session::get('is_validator') && $data['pengajuan_dosen']->is_approved0 == null)<!-- belum divalidasi-->
                                            <div class="form-group row">
                                                <a href="#setuju" data-toggle="modal"
                                                    class="btn btn-xs btn-its tx-montserrat tx-semibold btn-block">Setuju</a>
                                                <a href="#perbaikan" data-toggle="modal"
                                                    class="btn btn-xs btn-warning tx-semibold tx-montserrat btn-block">
                                                    Perbaikan</a>
                                                <a href="#tidak_setuju" data-toggle="modal"
                                                    class="btn btn-xs btn-danger tx-semibold tx-montserrat btn-block">Tidak
                                                    Setuju</a>
                                            </div>
                                        @else <!--validator beraksi -->
                                            @if (Session::get('is_validator') && $data['pengajuan_dosen']->is_approved0 == '1')  <!--disetujui -->
                                                <p class="font-italic">Telah disetujui oleh : <br><span class="font-weight-bold">{{ $data['pengajuan_dosen']->name_approved0 }}</span><br>
                                                    <small class="tx-secondary">pada : {{ date('d-m-Y', strtotime($data['pengajuan_dosen']->date_approved0)) }}</small>
                                                </p>
                                                @if ($data['pengajuan_dosen']->is_approved1 == '1')
                                                <p class="font-italic">dan <br><span
                                                        class="font-weight-bold">{{ $data['pengajuan_dosen']->name_approved1 }}</span><br><small
                                                        class="tx-secondary">pada :
                                                        {{ date('d-m-Y', strtotime($data['pengajuan_dosen']->date_approved1)) }}</small>
                                                </p>
                                                @endif
                                                @if ($data['pengajuan_dosen']->is_approved1 == null)
                                                    <a href="#batalkan" data-toggle="modal" class="btn btn-xs btn-danger tx-semibold tx-montserrat btn-block">Batalkan</a>
                                                @endif
                                                <br>
                                                <label class="tx-montserrat tx-medium tx-uppercase tx-center">Riwayat Perbaikan</label>
                                                @if(!is_null($data['pengajuan_dosen']->alasan0) || !is_null($data['pengajuan_dosen']->alasan1))
                                                    <p class="font-italic">Instruksi untuk melakukan perbaikan oleh :
                                                        @if(!is_null($data['pengajuan_dosen']->alasan0))
                                                            <br><span class="font-weight-bold">{{ $data['pengajuan_dosen']->name_approved0 }}</span><br><small class="tx-secondary">pada :{{ date('d-m-Y', strtotime($data['pengajuan_dosen']->date_approved0)) }}</small><br><small>Alasan:<br> {{ $data['pengajuan_dosen']->alasan0 }}</small>
                                                        @endif
                                                        @if(!is_null($data['pengajuan_dosen']->alasan1))
                                                            <br><span class="font-weight-bold">{{ $data['pengajuan_dosen']->name_approved1 }}</span><br><small class="tx-secondary">pada :{{ date('d-m-Y', strtotime($data['pengajuan_dosen']->date_approved1)) }}</small><br><small>Alasan:<br> {{ $data['pengajuan_dosen']->alasan1 }}</small>
                                                        @endif
                                                    </p>
                                                @else
                                                    <p class="font-italic">Ajuan tidak pernah direvisi</p> 
                                                @endif
                                            @elseif(Session::get('is_validator') && $data['pengajuan_dosen']->is_approved0 == '0') <!-- tidak disetujui / butuh revisi admin unit -->
                                                <p class="font-italic">Permohonan ditolak oleh : <br>
                                                    <span class="font-weight-bold">{{ $data['pengajuan_dosen']->name_approved0 }}</span><br>
                                                    <small class="tx-secondary">pada : {{ date('d-m-Y', strtotime($data['pengajuan_dosen']->date_approved0)) }}</small><br><br>
                                                    <small>Alasan :<br> {{ $data['pengajuan_dosen']->alasan0 }}</small>
                                                </p>
                                                @if ($data['pengajuan_dosen']->is_approved1 == null)
                                                    <a href="#batalkan" data-toggle="modal" class="btn btn-xs btn-danger tx-semibold tx-montserrat btn-block">Batalkan</a>
                                                @endif
                                            @elseif(Session::get('is_validator') && $data['pengajuan_dosen']->is_approved0 == '2') <!-- revisi-->
                                               
                                                <p class="font-italic">Instruksi untuk melakukan perbaikan oleh :
                                                    <br><span class="font-weight-bold">{{ $data['pengajuan_dosen']->name_approved0 }}</span><br>
                                                    <small class="tx-secondary">pada : {{ date('d-m-Y', strtotime($data['pengajuan_dosen']->date_approved0)) }}</small><br><br>
                                                    <small>Alasan :<br> {{ $data['pengajuan_dosen']->alasan0 }}</small>

                                                </p>

                                                @if($data['pengajuan_dosen']->alasan0 != null && $data['pengajuan_dosen']->date_approved0 < $data['pengajuan_dosen']->updated_at)
                                                    <small>Telah dilakukan perbaikan</small>
                                                    <small class="tx-secondary">pada : {{ date('d-m-Y H:i', strtotime($data['pengajuan_dosen']->updated_at)) }}</small><br><br>
                                                </p>
                                                @endif

                                                <a href="#batalkan" data-toggle="modal" class="btn btn-xs btn-danger tx-semibold tx-montserrat btn-block">Batalkan</a>
                                                <a href="#setuju" data-toggle="modal" class="btn btn-xs btn-its tx-montserrat tx-semibold btn-block">Setuju</a>

                                            @endif
                                        @endif

                                        {{-- Super Administrator --}}
                                        @if (sso()->user()->getActiveRole()->getName() == 'Super Administrator' && $data['pengajuan_dosen']->is_approved0 != '1')
                                            <p class="font-italic text-danger">Menunggu persetujuan dari Kepala Unit</p>
                                        @elseif(sso()->user()->getActiveRole()->getName() == 'Super Administrator' && $data['pengajuan_dosen']->is_approved0 == '1')
                                            @if (sso()->user()->getActiveRole()->getName() == 'Super Administrator' && $data['pengajuan_dosen']->is_approved1 != '1')

                                                <div class="form-group row">
                                                    <a href="#setuju" data-toggle="modal"
                                                        class="btn btn-xs btn-its tx-montserrat tx-semibold btn-block">Setuju</a>
                                                    <a href="#perbaikan" data-toggle="modal"
                                                        class="btn btn-xs btn-warning tx-semibold tx-montserrat btn-block">
                                                        Perbaikan</a>
                                                    <a href="#tidak_setuju" data-toggle="modal"
                                                        class="btn btn-xs btn-danger tx-semibold tx-montserrat btn-block">Tidak
                                                        Setuju</a>
                                                </div>

                                            @endif
                                            @if (sso()->user()->getActiveRole()->getName() == 'Super Administrator' && $data['pengajuan_dosen']->is_approved1 == '1')
                                                <p class="font-italic">Telah disetujui oleh : <br><span
                                                        class="font-weight-bold">{{ $data['pengajuan_dosen']->name_approved0 }}</span><br><small
                                                        class="tx-secondary">pada :
                                                        {{ date('d-m-Y', strtotime($data['pengajuan_dosen']->date_approved0)) }}</small>
                                                </p>
                                                <p class="font-italic">dan <br><span
                                                        class="font-weight-bold">{{ $data['pengajuan_dosen']->name_approved1 }}</span><br><small
                                                        class="tx-secondary">pada :
                                                        {{ date('d-m-Y', strtotime($data['pengajuan_dosen']->date_approved1)) }}</small>
                                                </p>
                                                <a href="#batalkan" data-toggle="modal"
                                                    class="btn btn-xs btn-danger tx-semibold tx-montserrat btn-block">Batalkan</a>
                                                <br>
                                                <label class="tx-montserrat tx-medium tx-uppercase tx-center">Riwayat Perbaikan</label>
                                                @if(!is_null($data['pengajuan_dosen']->alasan0) || !is_null($data['pengajuan_dosen']->alasan1))
                                                    <p class="font-italic">Instruksi untuk melakukan perbaikan oleh :
                                                        @if(!is_null($data['pengajuan_dosen']->alasan0))
                                                            <br><span class="font-weight-bold">{{ $data['pengajuan_dosen']->name_approved0 }}</span><br><small class="tx-secondary">pada :{{ date('d-m-Y', strtotime($data['pengajuan_dosen']->date_approved0)) }}</small><br><small>Alasan:<br> {{ $data['pengajuan_dosen']->alasan0 }}</small>
                                                        @endif
                                                        @if(!is_null($data['pengajuan_dosen']->alasan1))
                                                            <br><span class="font-weight-bold">{{ $data['pengajuan_dosen']->name_approved1 }}</span><br><small class="tx-secondary">pada :{{ date('d-m-Y', strtotime($data['pengajuan_dosen']->date_approved1)) }}</small><br><small>Alasan:<br> {{ $data['pengajuan_dosen']->alasan1 }}</small>
                                                        @endif
                                                    </p>
                                                @else
                                                    <p class="font-italic">Ajuan tidak pernah direvisi</p> 
                                                @endif
                                            @elseif(sso()->user()->getActiveRole()->getName() == 'Super Administrator' && $data['pengajuan_dosen']->is_approved1 == '0')
                                                <p class="font-italic">Permohonan ditolak oleh : <br><span
                                                        class="font-weight-bold">{{ $data['pengajuan_dosen']->name_approved1 }}</span><br><small
                                                        class="tx-secondary">pada :
                                                        {{ date('d-m-Y', strtotime($data['pengajuan_dosen']->date_approved1)) }}</small><br><br><small>Alasan
                                                        :<br> {{ $data['pengajuan_dosen']->alasan1 }}</small>
                                                </p>
                                                <a href="#batalkan" data-toggle="modal"
                                                    class="btn btn-xs btn-danger tx-semibold tx-montserrat btn-block">Batalkan</a>
                                            @elseif(sso()->user()->getActiveRole()->getName() == 'Super Administrator' && $data['pengajuan_dosen']->is_approved1 == '2')
                                                <p class="font-italic">Instruksi untuk melakukan perbaikan oleh :
                                                    <br><span
                                                        class="font-weight-bold">{{ $data['pengajuan_dosen']->name_approved1 }}</span><br><small
                                                        class="tx-secondary">pada :
                                                        {{ date('d-m-Y', strtotime($data['pengajuan_dosen']->date_approved1)) }}</small><br><br><small>Alasan
                                                        :<br> {{ $data['pengajuan_dosen']->alasan1 }}</small>
                                                </p>
                                                <a href="#batalkan" data-toggle="modal"
                                                    class="btn btn-xs btn-danger tx-semibold tx-montserrat btn-block">Batalkan</a>
                                            @endif
                                        @endif


                                        {{-- Administrator --}}
                                        @if (sso()->user()->getActiveRole()->getName() == 'Administrator' && $data['pengajuan_dosen']->is_approved0 != '1')
                                            <p class="font-italic text-danger">Menunggu persetujuan dari Kepala Unit</p>
                                        @elseif(sso()->user()->getActiveRole()->getName() == 'Administrator' && $data['pengajuan_dosen']->is_approved0 == '1')
                                            @if (sso()->user()->getActiveRole()->getName() == 'Administrator' && $data['pengajuan_dosen']->is_approved1 == '1')
                                                <p class="font-italic">Telah disetujui oleh : <br><span
                                                        class="font-weight-bold">{{ $data['pengajuan_dosen']->name_approved0 }}</span><br><small
                                                        class="tx-secondary">pada :
                                                        {{ date('d-m-Y', strtotime($data['pengajuan_dosen']->date_approved0)) }}</small>
                                                </p>
                                                <p class="font-italic">dan <br><span
                                                        class="font-weight-bold">{{ $data['pengajuan_dosen']->name_approved1 }}</span><br><small
                                                        class="tx-secondary">pada :
                                                        {{ date('d-m-Y', strtotime($data['pengajuan_dosen']->date_approved1)) }}</small>
                                                </p>
                                                <br>
                                                <label class="tx-montserrat tx-medium tx-uppercase tx-center">Riwayat Perbaikan</label>
                                                @if(!is_null($data['pengajuan_dosen']->alasan0) || !is_null($data['pengajuan_dosen']->alasan1))
                                                    <p class="font-italic">Instruksi untuk melakukan perbaikan oleh :
                                                        @if(!is_null($data['pengajuan_dosen']->alasan0))
                                                            <br><span class="font-weight-bold">{{ $data['pengajuan_dosen']->name_approved0 }}</span><br><small class="tx-secondary">pada :{{ date('d-m-Y', strtotime($data['pengajuan_dosen']->date_approved0)) }}</small><br><small>Alasan:<br> {{ $data['pengajuan_dosen']->alasan0 }}</small>
                                                        @endif
                                                        @if(!is_null($data['pengajuan_dosen']->alasan1))
                                                            <br><span class="font-weight-bold">{{ $data['pengajuan_dosen']->name_approved1 }}</span><br><small class="tx-secondary">pada :{{ date('d-m-Y', strtotime($data['pengajuan_dosen']->date_approved1)) }}</small><br><small>Alasan:<br> {{ $data['pengajuan_dosen']->alasan1 }}</small>
                                                        @endif
                                                    </p>
                                                @else
                                                    <p class="font-italic">Ajuan tidak pernah direvisi</p> 
                                                @endif
                                            @elseif(sso()->user()->getActiveRole()->getName() == 'Administrator'
                                                &&
                                                $data['pengajuan_dosen']->is_approved1 == '0')
                                                <p class="font-italic">Permohonan ditolak oleh : <br><span
                                                        class="font-weight-bold">{{ $data['pengajuan_dosen']->name_approved1 }}</span><br><small
                                                        class="tx-secondary">pada :
                                                        {{ date('d-m-Y', strtotime($data['pengajuan_dosen']->date_approved1)) }}</small><br><br><small>Alasan
                                                        :<br> {{ $data['pengajuan_dosen']->alasan1 }}</small>
                                                </p>
                                            @elseif(sso()->user()->getActiveRole()->getName() == 'Administrator'
                                                &&
                                                $data['pengajuan_dosen']->is_approved1 == '2')
                                                <p class="font-italic">Instruksi untuk melakukan perbaikan oleh :
                                                    <br><span
                                                        class="font-weight-bold">{{ $data['pengajuan_dosen']->name_approved1 }}</span><br><small
                                                        class="tx-secondary">pada :
                                                        {{ date('d-m-Y', strtotime($data['pengajuan_dosen']->date_approved1)) }}</small><br><br><small>Alasan
                                                        :<br> {{ $data['pengajuan_dosen']->alasan1 }}</small>
                                                </p>
                                            @endif
                                        @endif


                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    @if (sso()->user()->getActiveRole()->getName() == 'Super Administrator')
                        <div class="card mg-t-10">
                            <div class="card-content">
                                <div class="card-header">
                                    <label class="tx-montserrat tx-medium tx-uppercase tx-center">Draft</label>
                                </div>

                                <div class="card-body">
                                    @if ($data['pengajuan_dosen']->no_surat == null && $data['pengajuan_dosen']->tanggal_pengesahan == null)
                                        <div class="alert alert-danger" role="alert">Isi nomor surat dan tanggal pengesahan untuk mengunduh
                                            draft.</div>
                                        <form
                                            class="needs-validation mt-1"
                                            novalidate
                                            action="{{ route('isi-nomor-surat-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen]) }}"
                                            method="post">
                                            @method('PUT')
                                            @csrf
                                            <div class="form-row px-2">
                                                <label for="tanggal_pengesahan">Tanggal Pengesahan</label>
                                                <input type="date" class="form-control" id="tanggal_pengesahan" name="tanggal_pengesahan" {{$data['pengajuan_dosen']->is_approved1 != 1 ? 'disabled' : ''}} required>
                                            </div>
                                            <br>
                                            <div class="form-row px-2">
                                                <label for="no_surat" class="tx-10 tx-spacing-1 tx-color-03 tx-uppercase tx-semibold">Nomor surat</label>
                                                <input type="text" name="no_surat" id="no_surat" class="form-control" {{$data['pengajuan_dosen']->is_approved1 != 1 ? 'disabled' : ''}} required>
                                                <div class="invalid-feedback">
                                                    Mohon isi nomor surat
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-xs btn-its tx-semibold tx-montserrat btn-block mg-t-10" {{$data['pengajuan_dosen']->is_approved1 != 1 ? 'disabled' : ''}} >
                                                <i class="fa fa-edit"></i> Isi Nomor Surat</button>
                                        </form>
                                    @else
                                        <a class="btn btn-xs btn-block btn-its tx-montserrat tx-semibold"
                                            href="{{ url('pegawai/pengajuan-dosen/draft') }}/{{ $data['pengajuan_dosen']->id_pengajuan_dosen }}"><i
                                                class="fa fa-download"></i> Unduh Draft</a>
                                    @endif
                                </div>

                            </div>
                        </div>
                    @endif
                </div>


            </div>
        </div>

    </div>
    {{-- Modal setuju --}}
    <div class="modal fade" id="setuju" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content tx-14">
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Konfirmasi</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ route('approve-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen]) }}">
                    @csrf
                    <div class="modal-body">
                        <p class="mg-b-0">Apakah Anda yakin untuk menyetujui pengajuan ini? </p>
                        <input type="hidden" value="setuju" name="status">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white tx-montserrat tx-semibold"
                            data-dismiss="modal">Batal</button>
                        <input type="submit" class="btn btn-its tx-montserrat tx-semibold" value="Ya">
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- Modal perbaikan --}}
    <div class="modal fade" id="perbaikan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content tx-14">
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Konfirmasi</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ route('approve-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen]) }}">
                    @csrf
                    <div class="modal-body">
                        <p class="mg-b-20 tx-semibold">Apakah Anda yakin untuk menyuruh melakukan perbaikan pada pengajuan
                            ini? </p>
                        <input type="hidden" value="perbaikan" name="status">
                        <div class="form-group">
                            <label for="alasan" class="d-block">Masukkan Alasan</label>
                            <textarea name="alasan" class="form-control" placeholder="Harap jelaskan secara detail perbaikan yang harus dilakukan" required></textarea>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white tx-montserrat tx-semibold"
                            data-dismiss="modal">Batal</button>
                        <input type="submit" class="btn btn-its tx-montserrat tx-semibold" value="Ya">
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- Modal tidak setuju --}}
    <div class="modal fade" id="tidak_setuju" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content tx-14">
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Konfirmasi</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ route('approve-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen]) }}">
                    @csrf
                    <div class="modal-body">
                        <p class="mg-b-20 tx-semibold">Apakah Anda yakin untuk tidak menyetujui pengajuan ini? </p>
                        <input type="hidden" value="tidak_setuju" name="status">
                        <div class="form-group">
                            <label for="alasan" class="d-block">Masukkan Alasan</label>
                            <textarea name="alasan" class="form-control" required></textarea>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white tx-montserrat tx-semibold"
                            data-dismiss="modal">Batal</button>
                        <input type="submit" class="btn btn-its tx-montserrat tx-semibold" value="Ya">
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- Modal batalkan --}}
    <div class="modal fade" id="batalkan" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content tx-14">
                <div class="modal-header">
                    <h6 class="modal-title" id="exampleModalLabel">Konfirmasi</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ route('approve-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen]) }}">
                    @csrf
                    <div class="modal-body">
                        <p class="mg-b-0">Apakah Anda yakin untuk membatalkan persetujuan ini? </p>
                        <input type="hidden" value="batalkan" name="status">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white tx-montserrat tx-semibold"
                            data-dismiss="modal">Batal</button>
                        <input type="submit" class="btn btn-its tx-montserrat tx-semibold" value="Ya">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        (function() {
            'use strict';
            window.addEventListener('load', function() {
                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
                });
            }, false);
        })();
    </script>
@endsection
