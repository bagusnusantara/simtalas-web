@extends('Ui::base')

@section('title')
    Pengajuan Dosen
@endsection

@section('header_title')
    Pengajuan Dosen
@endsection
@section('breadcrumbs')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-style1 mg-b-10">
            <li class="breadcrumb-item"><a href="#">
                    @if (isset($page['menu'])){{ ucwords($page['menu']) }}@endif
                </a></li>
            <li class="breadcrumb-item"><a href="#">
                    @if (isset($page['submenu'])){{ ucwords($page['submenu']) }}
                    @endif
                </a></li>
            <li class="breadcrumb-item active" aria-current="page">
                @if (isset($page['halaman'])){{ ucwords($page['halaman']) }}@endif
            </li>
        </ol>
    </nav>
@endsection
@section('header_right')
    <div class="d-md-block">
        <a class="btn btn-white tx-montserrat tx-semibold" href="{{ url('pegawai/pengajuan-dosen') }}"><i
                data-feather="arrow-left" class="wd-10 mg-r-5"></i> Kembali</a>
    </div>
@endsection
@section('content')
    <div class="col-sm-12 col-lg-12">        
        @if(count( $errors ) > 0)
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger alert-dismissable fade show" role="alert">
                    {{ $error }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endforeach
        @elseif (session('failed'))
        <div class="alert alert-danger alert-dismissable fade show" role="alert">
            {{ session('failed') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @elseif (session('success'))
        <div class="alert alert-success alert-dismissable fade show" role="alert">
            {{ session('success') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        <div class="card card-body">
            <div class="row">
                <div class="col-lg-12 mg-t-5">
                    <div class="col-lg-12 pd-x-0">
                        <div class="card card-body col-md-12">


                            <h3 class="tx-center">Data Diri</h3>
                            <div class="row justify-content-center">
                                <div class="col-12 mg-b-30 mg-t-20">
                                    <div class="avatar avatar-xxl avatar-online text-center"><img
                                            src="{{ route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_berkas_pas_foto ?? '-']) }}"
                                            class="rounded-circle" alt="">
                                    </div>
                                </div><!-- col -->
                            </div>
                            <form class="needs-validation" novalidate method="post" action="{{ route('pengajuan-dosen.update', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen]) }}"
                                enctype="multipart/form-data" id="form" class="parsley-style-1" data-parsley-validate
                                novalidate>
                                @csrf
                                @method('PUT')
                                <div id="baru">
                                    <input type="hidden" class="form-control" id="id_pengajuan" name="id_pengajuan"
                                        value="{{ $data['pengajuan_dosen']->id_pengajuan }}">
                                    <input type="hidden" class="form-control" id="nik_lama" name="nik_lama"
                                        value="{{ $data['pengajuan_dosen']->nik }}">
                                    <div class="form-row">
                                        <div class="form-group col-md-2">
                                            <label for="gelar_depan">Gelar Depan</label>
                                            <input type="text" class="form-control" id="gelar_depan" 
                                            value="{{ $data['dosen']->gelar_depan }}" name="gelar_depan" placeholder="Gelar depan">
                                        </div>
                                        <div class="form-group col-md-8">
                                            <label for="nama">Nama Lengkap *</label>
                                            <input type="text" class="form-control" id="nama" name="nama"
                                                value="{{ $data['dosen']->nama }}" placeholder="Nama lengkap" required>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <label for="gelar_belakang">Gelar Belakang</label>
                                            <input type="text" class="form-control" id="gelar_belakang" 
                                            value="{{ $data['dosen']->gelar_belakang }}" name="gelar_belakang" placeholder="Gelar belakang">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="nik">NIK</label>
                                            <input type="text" class="form-control" id="nik" name="nik"
                                                value="{{ $data['dosen']->nik }}" placeholder="NIK" disabled>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="npp">NPP</label>
                                            <input type="number" class="form-control" id="npp" name="npp"
                                            value="{{ $data['dosen']->npp }}" placeholder="NPP">
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="nidn">NIDN / NIDK / NUP</label>
                                            <input type="number" class="form-control" id="nidn" name="nidn"
                                            value="{{ $data['dosen']->nidn }}" placeholder="NIDN / NIDK / NUP">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="nama">Tanggal Lahir *</label>
                                            <input type="date" class="form-control" id="tgl_lahir" name="tgl_lahir"
                                                value="{{ date('Y-m-d', strtotime($data['dosen']->tgl_lahir)) }}" required>
                                        </div>
                                        <div class="form-group col-md-5">
                                            <label for="tempat_lahir">Tempat Lahir</label>
                                            <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir"
                                                value="{{ $data['dosen']->tempat_lahir }}" placeholder="Tempat lahir">
                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="jenis_kelamin">Jenis Kelamin *</label>
                                            <select class="custom-select" id="jenis_kelamin" name="jenis_kelamin" required>
                                                <option value="L" {{ $data['dosen']->jenis_kelamin == 'L' ? 'selected' : '' }}>
                                                    Laki-laki</option>
                                                <option value="P" {{ $data['dosen']->jenis_kelamin == 'P' ? 'selected' : '' }}>
                                                    Perempuan</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="kepakaran">Kepakaran *</label>
                                            <input type="text" class="form-control" id="kepakaran" name="kepakaran"
                                                value="{{ $data['dosen']->kepakaran }}" placeholder="Kepakaran" required>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="pendidikan_terakhir">Pendidikan Terakhir *</label>
                                            <select class="custom-select" id="id_pendidikan" name="id_pendidikan" required>
                                                @foreach ($data['pendidikan'] as $pendidikan)
                                                    <option value="{{ $pendidikan->id_pendidikan }}"
                                                        @if ($pendidikan->id_pendidikan === $data['dosen']->id_pendidikan)
                                                            selected
                                                        @endif
                                                        >{{ $pendidikan->nama }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="alamat">Alamat *</label>
                                        <input type="text" class="form-control" id="alamat" name="alamat"
                                            value="{{ $data['dosen']->alamat }}" required>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="email">Email *</label>
                                            <input type="email" class="form-control" id="email" name="email"
                                                value="{{ $data['dosen']->email }}" placeholder="Email" required>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="nomor_hp">Nomor HP</label>
                                            <input type="number" class="form-control" id="nomor_hp" name="nomor_hp"
                                                value="{{ $data['dosen']->nomor_hp }}" placeholder="Nomor HP">
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-12">
                                            <label for="email">Unit</label>
                                            <select class="custom-select" disabled>
                                                <option value="{{ $data['unit']->id_unit }}" selected>{{ $data['unit']->nama }}</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-row">
                                        <div class="form-group col-md-6">
                                            <label for="id_status_pengajuan_dosen">Status Pengajuan *</label>
                                            <select class="custom-select" id="id_status_pengajuan_dosen"
                                                name="id_status_pengajuan_dosen" required>
                                                <option value="0"
                                                    {{ $data['pengajuan_dosen']->id_status_pengajuan == '0' ? 'selected' : '' }}>
                                                    Baru</option>
                                                <option value="1"
                                                    {{ $data['pengajuan_dosen']->id_status_pengajuan == '1' ? 'selected' : '' }}>
                                                    Perpanjangan</option>
                                            </select>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="id_jenis_pengajuan_dosen">Jenis Pengajuan *</label>
                                            <select class="custom-select" id="id_jenis_pengajuan_dosen"
                                                name="id_jenis_pengajuan_dosen" required>
                                                @foreach ($data['jenis_pengajuan_dosen'] as $jenis_pengajuan_dosen)
                                                <option value="{{ $jenis_pengajuan_dosen->id_jenis_pengajuan_dosen }}"
                                                    {{ $data['pengajuan_dosen']->id_jenis_pengajuan_dosen == $jenis_pengajuan_dosen->id_jenis_pengajuan_dosen ? 'selected' : '' }}>
                                                    {{ $jenis_pengajuan_dosen->nama }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="tanggal_pengajuan">Tanggal Pengajuan *</label>
                                            <input type="date" class="form-control" id="tanggal_pengajuan"
                                                name="tanggal_pengajuan"
                                                value="{{ date('Y-m-d', strtotime($data['pengajuan_dosen']->tanggal_pengajuan)) }}" required>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="tanggal_mulai_kerja">Tanggal Mulai Kerja *</label>
                                            <input type="date" class="form-control" id="tanggal_mulai_kerja"
                                                name="tanggal_mulai_kerja"
                                                value="{{ date('Y-m-d', strtotime($data['pengajuan_dosen']->tanggal_mulai_kerja)) }}"
                                                placeholder="Tanggal Mulai Kerja" required>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="tanggal_akhir_kerja">Tanggal Berakhir Kontrak Kerja *</label>
                                            <input type="date" class="form-control" id="tanggal_akhir_kerja"
                                                name="tanggal_akhir_kerja"
                                                value="{{ date('Y-m-d', strtotime($data['pengajuan_dosen']->tanggal_akhir_kerja)) }}" required>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="form-group row col-md-9">
                                        <label for="pas_foto" class="col-sm-3 col-form-label">Pas Foto</label>
                                        <div class="col-sm-9">
                                            <input type="file" class="form-control" id="pas_foto" name="pas_foto"
                                                ><br>
                                            @if ($data['dosen']->id_berkas_pas_foto)
                                            <a href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_berkas_pas_foto ?? '-'])}}"
                                                target="_blank"><i class="fa fa-download"></i> Unduh Pas Foto</a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row col-md-9">
                                        <label for="ktp" class="col-sm-3 col-form-label">KTP</label>
                                        <div class="col-sm-9">
                                            <input type="file" class="form-control" id="ktp" name="ktp"><br>
                                            @if ($data['dosen']->id_berkas_ktp)
                                            <a href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_berkas_ktp ?? '-'])}}"
                                                target="_blank"><i class="fa fa-download"></i> Unduh KTP</a>
                                            @endif
                                        </div>

                                    </div>
                                    <div class="form-group row col-md-9">
                                        <label for="riwayat_hidup" class="col-sm-3 col-form-label">Berkas Riwayat Hidup</label>
                                        <div class="col-sm-9">
                                            <input type="file" class="form-control" id="riwayat_hidup" name="riwayat_hidup"><br>
                                            @if ($data['dosen']->id_berkas_riwayat_hidup)
                                            <a href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_berkas_riwayat_hidup ?? '-'])}}"
                                                target="_blank"><i class="fa fa-download"></i> Unduh Berkas Riwayat Hidup</a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row col-md-9">
                                        <label for="ijazah_s2" class="col-sm-3 col-form-label">Ijazah S2</label>
                                        <div class="col-sm-9">
                                            <input type="file" class="form-control" id="ijazah_s2" name="ijazah_s2"
                                                ><br>
                                            @if ($data['dosen']->id_ijazah_s2)
                                            <a href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_ijazah_s2 ?? '-'])}}"
                                                target="_blank"><i class="fa fa-download"></i> Unduh Ijazah S2</a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row col-md-9">
                                        <label for="transkrip_s2" class="col-sm-3 col-form-label">Transkrip S2</label>
                                        <div class="col-sm-9">
                                            <input type="file" class="form-control" id="transkrip_s2" name="transkrip_s2"
                                                ><br>
                                            @if ($data['dosen']->id_transkrip_s2)
                                            <a href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_transkrip_s2 ?? '-'])}}"
                                                target="_blank"><i class="fa fa-download"></i> Unduh Transkrip S2</a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row col-md-9">
                                        <label for="penyetaraan_s2" class="col-sm-3 col-form-label">Penyetaraan S2</label>
                                        <div class="col-sm-9">
                                            <input type="file" class="form-control" id="penyetaraan_s2" name="penyetaraan_s2"
                                                ><br>
                                            @if ($data['dosen']->id_penyetaraan_s2)
                                            <a href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_penyetaraan_s2 ?? '-'])}}"
                                                target="_blank"><i class="fa fa-download"></i> Unduh Penyetaraan S2</a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row col-md-9">
                                        <label for="ijazah_s3" class="col-sm-3 col-form-label">Ijazah S3</label>
                                        <div class="col-sm-9">
                                            <input type="file" class="form-control" id="ijazah_s3" name="ijazah_s3"
                                                ><br>
                                            @if ($data['dosen']->id_ijazah_s3)
                                            <a href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_ijazah_s3 ?? '-'])}}"
                                                target="_blank"><i class="fa fa-download"></i> Unduh Ijazah S3</a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row col-md-9">
                                        <label for="transkrip_s3" class="col-sm-3 col-form-label">Transkrip S3</label>
                                        <div class="col-sm-9">
                                            <input type="file" class="form-control" id="transkrip_s3" name="transkrip_s3"
                                                ><br>
                                            @if ($data['dosen']->id_transkrip_s3)
                                            <a href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_transkrip_s3 ?? '-'])}}"
                                                target="_blank"><i class="fa fa-download"></i> Unduh Transkrip S3</a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row col-md-9">
                                        <label for="penyetaraan_s3" class="col-sm-3 col-form-label">Penyetaraan S3</label>
                                        <div class="col-sm-9">
                                            <input type="file" class="form-control" id="penyetaraan_s3" name="penyetaraan_s3"
                                                ><br>
                                            @if ($data['dosen']->id_penyetaraan_s3)
                                            <a href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_penyetaraan_s3 ?? '-'])}}"
                                                target="_blank"><i class="fa fa-download"></i> Unduh Penyetaraan S3</a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row col-md-9">
                                        <label for="analisis_bkd" class="col-sm-3 col-form-label">Analisis Beban Kerja Dosen</label>
                                        <div class="col-sm-9">
                                            <input type="file" class="form-control" id="analisis_bkd" name="analisis_bkd"
                                                ><br>
                                            @if ($data['dosen']->id_berkas_analisis_bkd)
                                            <a href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_berkas_analisis_bkd ?? '-'])}}"
                                                target="_blank"><i class="fa fa-download"></i> Unduh Analisis Beban Kerja Dosen</a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row col-md-9">
                                        <label for="surat_izin_institusi_asal" class="col-sm-3 col-form-label">Surat Izin Institusi Asal</label>
                                        <div class="col-sm-9">
                                            <input type="file" class="form-control" id="surat_izin_institusi_asal" name="surat_izin_institusi_asal"
                                                ><br>
                                            @if ($data['dosen']->id_surat_izin_institusi_asal)
                                            <a href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_surat_izin_institusi_asal ?? '-'])}}"
                                                target="_blank"><i class="fa fa-download"></i> Unduh Surat Izin Institusi Asal</a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row col-md-9">
                                        <label for="ijazah_s1" class="col-sm-3 col-form-label">Ijazah S1</label>
                                        <div class="col-sm-9">
                                            <input type="file" class="form-control" id="ijazah_s1" name="ijazah_s1"
                                                ><br>
                                            @if ($data['dosen']->id_ijazah_s1)
                                            <a href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_ijazah_s1 ?? '-'])}}"
                                                target="_blank"><i class="fa fa-download"></i> Unduh Ijazah S1</a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row col-md-9">
                                        <label for="transkrip_s1" class="col-sm-3 col-form-label">Transkrip S1</label>
                                        <div class="col-sm-9">
                                            <input type="file" class="form-control" id="transkrip_s1" name="transkrip_s1"
                                                ><br>
                                            @if ($data['dosen']->id_transkrip_s1)
                                            <a href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_transkrip_s1 ?? '-'])}}"
                                                target="_blank"><i class="fa fa-download"></i> Unduh Transkrip S1</a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row col-md-9">
                                        <label for="penyetaraan_s1" class="col-sm-3 col-form-label">Penyetaraan S1</label>
                                        <div class="col-sm-9">
                                            <input type="file" class="form-control" id="penyetaraan_s1" name="penyetaraan_s1"
                                                ><br>
                                            @if ($data['dosen']->id_penyetaraan_s1)
                                            <a href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_penyetaraan_s1 ?? '-'])}}"
                                                target="_blank"><i class="fa fa-download"></i> Unduh Penyetaraan S1</a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row col-md-9">
                                        <label for="sehat_rohani" class="col-sm-3 col-form-label">Berkas Sehat Rohani</label>
                                        <div class="col-sm-9">
                                            <input type="file" class="form-control" id="sehat_rohani" name="sehat_rohani"
                                                ><br>
                                            @if ($data['dosen']->id_berkas_sehat_rohani)
                                            <a href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_berkas_sehat_rohani ?? '-'])}}"
                                                target="_blank"><i class="fa fa-download"></i> Unduh Berkas Sehat Rohani</a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row col-md-9">
                                        <label for="sehat_jasmani" class="col-sm-3 col-form-label">Berkas Sehat Jasmani</label>
                                        <div class="col-sm-9">
                                            <input type="file" class="form-control" id="sehat_jasmani" name="sehat_jasmani"
                                                ><br>
                                            @if ($data['dosen']->id_berkas_sehat_jasmani)
                                            <a href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_berkas_sehat_jasmani ?? '-'])}}"
                                                target="_blank"><i class="fa fa-download"></i> Unduh Berkas Sehat Jasmani</a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row col-md-9">
                                        <label for="bebas_narkotika" class="col-sm-3 col-form-label">Berkas Bebas Narkotika</label>
                                        <div class="col-sm-9">
                                            <input type="file" class="form-control" id="bebas_narkotika" name="bebas_narkotika"
                                                ><br>
                                            @if ($data['dosen']->id_berkas_bebas_narkotika)
                                            <a href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_berkas_bebas_narkotika ?? '-'])}}"
                                                target="_blank"><i class="fa fa-download"></i> Unduh Berkas Bebas Narkotika</a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row col-md-9">
                                        <label for="pernyataan_dekan" class="col-sm-3 col-form-label">Berkas Pernyataan Dekan</label>
                                        <div class="col-sm-9">
                                            <input type="file" class="form-control" id="pernyataan_dekan" name="pernyataan_dekan"
                                                ><br>
                                            @if ($data['dosen']->id_berkas_pernyataan_dekan)
                                            <a href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_berkas_pernyataan_dekan ?? '-'])}}"
                                                target="_blank"><i class="fa fa-download"></i> Unduh Berkas Pernyataan Dekan</a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row col-md-9">
                                        <label for="minimal_mengajar" class="col-sm-3 col-form-label">Berkas Minimal Mengajar dari Dekan</label>
                                        <div class="col-sm-9">
                                            <input type="file" class="form-control" id="minimal_mengajar" name="minimal_mengajar"
                                                ><br>
                                            @if ($data['dosen']->id_berkas_minimal_mengajar)
                                            <a href="{{route('berkas-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen, 'id_berkas' => $data['dosen']->id_berkas_minimal_mengajar ?? '-'])}}"
                                                target="_blank"><i class="fa fa-download"></i> Unduh Berkas Minimal Mengajar dari Dekan</a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-end">
                                    <input class="btn btn-its text-center mt-3" type="submit" value="Simpan">
                                </div>
                            </form>
                            <h3 class="tx-center mg-t-20 mg-b-20">Beban Kerja</h3>
                            <h6 class="tx-center">Total SKS Pengajaran: {{$data['sks_ajar']}} SKS</h6>
                            <div class="col-md-12 text-right mg-b-20">
                                <button href="#tambah-tugas" data-toggle="modal"
                                    class="btn btn-xs btn-its tx-semibold tx-montserrat">Tambahkan</button>
                            </div>
                            <table class="table table-bordered">
                                <thead>
                                  <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Jenis</th>
                                    <th scope="col">Deskripsi / Mata Kuliah</th>
                                    <th scope="col">Kelas</th>
                                    <th scope="col">Jumlah</th>
                                    <th scope="col">Upah</th>
                                    <th scope="col">Aksi</th>
                                </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $beban_kerja_dosen_idx = 1;
                                    @endphp
                                    @foreach ($data['beban_kerja_dosen'] as $beban_kerja_dosen)
                                    <tr>
                                      <th scope="row">{{ $beban_kerja_dosen_idx++ }}</th>
                                      <td>{{ $beban_kerja_dosen->jenis_beban_kerja_dosen->nama }}</td>
                                      <td>{{ $beban_kerja_dosen->deskripsi }}</td>
                                      <td>{{ $beban_kerja_dosen->kelas ?? '-' }}</td>
                                      <td>{{ round($beban_kerja_dosen->jumlah, 1) }}</td>
                                      <td>{{ $beban_kerja_dosen->upah }}</td>
                                      <td>
                                        <button data-id="{{ $beban_kerja_dosen->id_beban_kerja_dosen }}"
                                            data-id_jenis_beban_kerja_dosen="{{ $beban_kerja_dosen->id_jenis_beban_kerja_dosen }}"
                                            data-deskripsi="{{ $beban_kerja_dosen->deskripsi }}"
                                            data-kelas="{{ $beban_kerja_dosen->kelas }}"
                                            data-jumlah="{{ round($beban_kerja_dosen->jumlah, 1) }}"
                                            data-upah="{{ $beban_kerja_dosen->upah }}"
                                            data-id_mkb_wajib="{{ $beban_kerja_dosen->id_mkb_wajib }}"
                                            data-toggle="modal"
                                            data-target="#edit-tugas" class="btn btn-xs btn-warning"><i
                                                class="fa fa-edit"></i> Ubah</button>
                                        <button data-id="{{ $beban_kerja_dosen->id_beban_kerja_dosen }}" data-toggle="modal"
                                            data-target="#hapus-tugas" class="btn btn-xs btn-danger"><i
                                                class="fa fa-trash"></i> Hapus</button>
                                      </td>
                                    </tr>
                                    @endforeach
                                    @if (!$data['beban_kerja_dosen']->count())
                                        <tr class="tx-center">
                                            <td colspan="6" class="tx-italic tx-danger">Belum ada data</td>
                                        </tr>
                                    @endif
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    {{-- Modal Tambah Tugas --}}
    <div class="modal fade" id="tambah-tugas" tabindex="-1" role="dialog" aria-labelledby="tambahTugasModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content tx-14">
                <div class="modal-header">
                    <h6 class="modal-title" id="tambahTugasModalLabel">Tambah Tugas</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="needs-validation" novalidate method="POST" action="{{ route('tambah-beban-kerja-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen]) }}">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="id_jenis_beban_kerja_dosen" class="d-block">Jenis Beban Kerja *</label>
                            <select class="custom-select" id="id_jenis_beban_kerja_dosen" name="id_jenis_beban_kerja_dosen" required>
                                <option value="">Pilih Jenis Beban Kerja</option>
                                @foreach ($data['jenis_beban_kerja'] as $jenis_beban_kerja)
                                    <option value="{{ $jenis_beban_kerja->id_jenis_beban_kerja_dosen }}">{{ $jenis_beban_kerja->nama }}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Mohon pilih jenis beban kerja
                            </div>
                        </div>
                        <div class="form-group" id="add-form-id-mkb-wajib">
                            <label for="id_mkb_wajib" class="d-block">Mata Kuliah *</label>
                            <select class="custom-select" name="id_mkb_wajib" id="add_id_mkb_wajib">
                                <option value="">Pilih Mata Kuliah</option>
                                @foreach ($data['mata_kuliah_bersama_wajib'] as $mata_kuliah_bersama_wajib)
                                    <option value="{{ $mata_kuliah_bersama_wajib->id_mkb_wajib }}">{{ $mata_kuliah_bersama_wajib->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group" id="add-form-uraian">
                            <label id="add-label-deskripsi" for="deskripsi" class="d-block">Deskripsi / Mata Kuliah *</label>
                            <input type="text" name="deskripsi" id="add_deskripsi" class="form-control" placeholder="Masukkan deskripsi / mata kuliah" required></input>
                            <div class="invalid-feedback">
                                Mohon masukkan deskripsi
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="kelas" class="d-block">Kelas</label>
                            <input type="text" id="add-kelas" name="kelas" class="form-control" placeholder="Masukkan kelas" maxlength="50"></input>
                        </div>
                        <div class="form-group">
                            <label id="add-label-jumlah" for="jumlah" class="d-block">Jumlah *</label>
                            <input type="number" step="0.1" id="add_jumlah" name="jumlah" class="form-control" placeholder="Masukkan jumlah" required></input>
                            <div class="invalid-feedback">
                                Mohon masukkan jumlah
                            </div>
                        </div>
                        <div class="form-group">
                            <label id="add-label-upah" for="upah" class="d-block">Upah *</label>
                            <input type="number" name="upah" class="form-control" placeholder="Masukkan upah" required></input>
                            <div class="invalid-feedback">
                                Mohon masukkan upah
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white tx-montserrat tx-semibold"
                            data-dismiss="modal">Batal</button>
                        <input type="submit" class="btn btn-its tx-montserrat tx-semibold" value="Tambahkan">
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- Modal Hapus Tugas --}}
    <div class="modal fade" id="hapus-tugas" tabindex="-1" role="dialog" aria-labelledby="hapusTugasModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content tx-14">
                <div class="modal-header">
                    <h6 class="modal-title" id="hapusTugasModalLabel">Konfirmasi</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="{{ route('hapus-beban-kerja-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen]) }}">
                    @csrf
                    @method('DELETE')
                    <div class="modal-body">
                        <input type="hidden" id="id-hapus-tugas" name="id">
                        <p>Apakah Anda yakin menghapus data ini?</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white tx-montserrat tx-semibold"
                            data-dismiss="modal">Batal</button>
                        <input type="submit" class="btn btn-its tx-montserrat tx-semibold" value="Ya">
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- Modal Edit Tugas --}}
    <div class="modal fade" id="edit-tugas" tabindex="-1" role="dialog" aria-labelledby="editTugasModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content tx-14">
                <div class="modal-header">
                    <h6 class="modal-title" id="editTugasModalLabel">Edit Deskripsi Tugas</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form class="needs-validation" novalidate method="POST" action="{{ route('hapus-beban-kerja-pengajuan-dosen', ['pengajuan_dosen' => $data['pengajuan_dosen']->id_pengajuan_dosen]) }}">
                    @csrf
                    @method('PUT')
                    <div class="modal-body">
                        <input type="hidden" id="id-edit-tugas" name="id">
                        <div class="form-group">
                            <label for="id_jenis_beban_kerja_dosen" class="d-block">Jenis Beban Kerja *</label>
                            <select class="custom-select" id="edit_tugas_id_jenis_beban_kerja_dosen" name="id_jenis_beban_kerja_dosen" required>
                                <option value="">Pilih Jenis Beban Kerja</option>
                                @foreach ($data['jenis_beban_kerja'] as $jenis_beban_kerja)
                                    <option value="{{ $jenis_beban_kerja->id_jenis_beban_kerja_dosen }}">{{ $jenis_beban_kerja->nama }}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Mohon pilih jenis beban kerja
                            </div>
                        </div>
                        <div class="form-group" id="edit-form-id-mkb-wajib">
                            <label for="id_mkb_wajib" class="d-block">Mata Kuliah *</label>
                            <select class="custom-select" name="id_mkb_wajib" id="edit_id_mkb_wajib">
                                <option value="">Pilih Mata Kuliah</option>
                                @foreach ($data['mata_kuliah_bersama_wajib'] as $mata_kuliah_bersama_wajib)
                                    <option value="{{ $mata_kuliah_bersama_wajib->id_mkb_wajib }}">{{ $mata_kuliah_bersama_wajib->nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group" id="edit-form-uraian">
                            <label id="edit-label-deskripsi" for="deskripsi" class="d-block">Deskripsi / Mata Kuliah *</label>
                            <input type="text" id="edit_tugas_deskripsi" name="deskripsi" class="form-control" placeholder="Masukkan deskripsi" required></input>
                            <div class="invalid-feedback">
                                Mohon masukkan deskripsi / mata kuliah
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="kelas" class="d-block">Kelas</label>
                            <input type="text" id="edit_tugas_kelas" name="kelas" class="form-control" placeholder="Masukkan kelas" maxlength="50"></input>
                        </div>
                        <div class="form-group">
                            <label id="edit-label-jumlah" for="jumlah" class="d-block">Jumlah *</label>
                            <input type="number" step="0.1" id="edit_tugas_jumlah" name="jumlah" class="form-control" placeholder="Masukkan jumlah" required></input>
                            <div class="invalid-feedback">
                                Mohon masukkan jumlah
                            </div>
                        </div>
                        <div class="form-group">
                            <label id="edit-label-upah" for="upah" class="d-block">Upah *</label>
                            <input type="number" id="edit_tugas_upah" name="upah" class="form-control" placeholder="Masukkan upah" required></input>
                            <div class="invalid-feedback">
                                Mohon masukkan upah
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-white tx-montserrat tx-semibold"
                            data-dismiss="modal">Batal</button>
                        <input type="submit" class="btn btn-its tx-montserrat tx-semibold" value="Ya">
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        $(document).ready(function() {
            $('#edit-tugas').on('show.bs.modal', function(event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var id = button.data('id')
                var id_jenis_beban_kerja_dosen = button.data('id_jenis_beban_kerja_dosen')
                var deskripsi = button.data('deskripsi')
                var id_mkb_wajib = button.data('id_mkb_wajib')
                var kelas = button.data('kelas')
                var jumlah = button.data('jumlah')
                var upah = button.data('upah')
                var modal = $(this)
                modal.find('.modal-body #id-edit-tugas').val(id);
                modal.find('.modal-body #edit_tugas_id_jenis_beban_kerja_dosen').val(id_jenis_beban_kerja_dosen);
                modal.find('.modal-body #edit_id_mkb_wajib').val(id_mkb_wajib);
                modal.find('.modal-body #edit_tugas_deskripsi').val(deskripsi);
                modal.find('.modal-body #edit_tugas_kelas').val(kelas);
                modal.find('.modal-body #edit_tugas_jumlah').val(jumlah);
                modal.find('.modal-body #edit_tugas_upah').val(upah);

                const idJenisBebanKerja = document.getElementById('edit_tugas_id_jenis_beban_kerja_dosen')
                const idMkbWajib = document.getElementById('edit_id_mkb_wajib')
                const deskripsiElem = document.getElementById('edit_tugas_deskripsi')
                const jumlahElem = document.getElementById('edit_tugas_jumlah')
                const mkbFormGroupElem = document.getElementById('edit-form-id-mkb-wajib')
                const uraianGroupElem = document.getElementById('edit-form-uraian')
                const kelasElem = document.getElementById('edit_tugas_kelas')
                const label_deskripsi = document.getElementById('edit-label-deskripsi')
                const label_jumlah = document.getElementById('edit-label-jumlah')
                const label_upah = document.getElementById('edit-label-upah')

                idJenisBebanKerja.addEventListener('change', () => onModalFormChange(idJenisBebanKerja, idMkbWajib, deskripsiElem, jumlahElem, mkbFormGroupElem, uraianGroupElem, label_deskripsi, label_jumlah, label_upah, kelasElem))
                idMkbWajib.addEventListener('change', () => onModalFormChange(idJenisBebanKerja, idMkbWajib, deskripsiElem, jumlahElem, mkbFormGroupElem, uraianGroupElem, label_deskripsi, label_jumlah, label_upah, kelasElem))
                onModalFormChange(idJenisBebanKerja, idMkbWajib, deskripsiElem, jumlahElem, mkbFormGroupElem, uraianGroupElem, label_deskripsi, label_jumlah, label_upah, kelasElem)
            });
            $('#hapus-tugas').on('show.bs.modal', function(event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var id = button.data('id')
                var modal = $(this)
                modal.find('.modal-body #id-hapus-tugas').val(id);
            });
        });
    </script>
    <script>
        (function() {
            'use strict';
            window.addEventListener('load', function() {
                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
                });
            }, false);
        })();

        const nidnInput = document.getElementById('nidn')

        function nidnHandler() {
            return
        }

        nidnInput.addEventListener('input', nidnHandler)
        nidnHandler()

        function onModalFormChange(idJenisBebanKerja, idMkbWajib, deskripsi, jumlah, mkbFormGroupElem, uraianGroupElem, labelDeskripsi, labelJumlah, labelUpah, kelasElem) {
            const isPengajaran = idJenisBebanKerja.value == '1' || idJenisBebanKerja.value == '2'
                                || idJenisBebanKerja.value == '3' || idJenisBebanKerja.value == '4'
            const jenisPengajuan = "{{ $data['pengajuan_dosen']->id_jenis_pengajuan_dosen }}"
            const isMengajarMkbWajib = jenisPengajuan === "mkbwajib"
            idMkbWajib.disabled = false
            deskripsi.readOnly = false
            kelasElem.readOnly = false

            if(!isPengajaran || !isMengajarMkbWajib) {
                idMkbWajib.required = false
                idMkbWajib.value = ''
                mkbFormGroupElem.classList.add('d-none')
                uraianGroupElem.classList.remove('d-none')
            } else if(isMengajarMkbWajib) {
                idMkbWajib.required = true
                mkbFormGroupElem.classList.remove('d-none')
                uraianGroupElem.classList.add('d-none')
            }
            
            if(!isPengajaran) {
                kelasElem.readOnly = true
                labelJumlah.innerText = 'Jumlah Mahasiswa'
                labelDeskripsi.innerText = 'Deskripsi'
                labelUpah.innerText = 'Upah per mahasiswa'
                return
            }

            labelJumlah.innerText = 'Jumlah SKS'
            labelDeskripsi.innerText = 'Mata Kuliah'
            labelUpah.innerText = 'Upah per sks/tatap muka'

            if(isMengajarMkbWajib && idMkbWajib.value) {
                deskripsi.value = idMkbWajib[idMkbWajib.selectedIndex].text
                deskripsi.readOnly = true
            }
        }

        const idJenisBebanKerja = document.getElementById('id_jenis_beban_kerja_dosen')
        const idMkbWajib = document.getElementById('add_id_mkb_wajib')
        const deskripsi = document.getElementById('add_deskripsi')
        const jumlah = document.getElementById('add_jumlah')
        const mkbFormGroupElem = document.getElementById('add-form-id-mkb-wajib')
        const uraianGroupElem = document.getElementById('add-form-uraian')
        const label_deskripsi = document.getElementById('add-label-deskripsi')
        const label_jumlah = document.getElementById('add-label-jumlah')
        const label_upah = document.getElementById('add-label-upah')
        const kelasElem = document.getElementById('add-kelas')

        idJenisBebanKerja.addEventListener('change', () => onModalFormChange(idJenisBebanKerja, idMkbWajib, deskripsi, jumlah, mkbFormGroupElem, uraianGroupElem, label_deskripsi, label_jumlah, label_upah, kelasElem))
        idMkbWajib.addEventListener('change', () => onModalFormChange(idJenisBebanKerja, idMkbWajib, deskripsi, jumlah, mkbFormGroupElem, uraianGroupElem, label_deskripsi, label_jumlah, label_upah, kelasElem))

        onModalFormChange(idJenisBebanKerja, idMkbWajib, deskripsi, null, mkbFormGroupElem, uraianGroupElem, label_deskripsi, label_jumlah, label_upah, kelasElem)
    </script>
@endsection
