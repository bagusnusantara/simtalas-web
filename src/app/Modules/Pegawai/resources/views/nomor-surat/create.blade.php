@extends('Ui::base')

@section('title')
    @if (isset($page['halaman'])){{ ucwords($page['halaman']) }}@endif
@endsection
@section('header_title')
    @if (isset($page['halaman'])){{ ucwords($page['halaman']) }}@endif
@endsection
@section('breadcrumbs')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-style1 mg-b-10">
            <li class="breadcrumb-item"><a href="#">
                    @if (isset($page['menu'])){{ ucwords($page['menu']) }}@endif
                </a></li>
            <li class="breadcrumb-item active" aria-current="page">
                @if (isset($page['halaman'])){{ ucwords($page['halaman']) }}@endif
            </li>
        </ol>
    </nav>
@endsection
@section('header_right')
    <div class="d-md-block">
        <a class="btn btn-white tx-montserrat tx-semibold" href="{{ url('pegawai/nomor-surat') }}"><i data-feather="arrow-left" class="wd-10 mg-r-5"></i> Kembali</a>
    </div>
@endsection
@section('content')
    <div class="col-sm-12 col-lg-12">
        <div class="alert alert-primary mg-b-20" role="alert">
            <span class="font-weight-bold">Cara pengisian :</span> Pisahkan antar nomor surat dengan tanda koma (,)
        </div>
        <div class="card card-body">
            <form method="post" action="{{ url('pegawai/nomor-surat/store') }}" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="nomor_surat" class="d-block">Nomor Surat</label>
                    <textarea class="form-control" rows="10" name="nomor_surat" placeholder="Nomor Surat"></textarea>
                </div>
                <button class="btn btn-primary" type="submit">Simpan</button>
            </form>
        </div><!-- df-example -->
    </div>
@endsection
