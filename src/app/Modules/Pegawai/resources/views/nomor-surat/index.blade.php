@extends('Ui::base')

@section('title')
    @if (isset($page['halaman'])){{ ucwords($page['halaman']) }}@endif
@endsection
@section('prestyles')
    <link href="{{ asset('lib/datatables.net-dt/css/jquery.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css') }}" rel="stylesheet">
@endsection
@section('header_title')
    @if (isset($page['halaman'])){{ ucwords($page['halaman']) }}@endif
@endsection
@section('breadcrumbs')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-style1 mg-b-10">
            <li class="breadcrumb-item"><a href="#">
                    @if (isset($page['menu'])){{ ucwords($page['menu']) }}@endif
                </a></li>
            <li class="breadcrumb-item active" aria-current="page">
                @if (isset($page['halaman'])){{ ucwords($page['halaman']) }}@endif
            </li>
        </ol>
    </nav>
@endsection
@section('header_right')
    <div class="d-md-block">
        <a class="btn btn-sm pd-x-15 btn-its tx-montserrat tx-semibold mg-l-5"
            href="{{ url('pegawai/nomor-surat/create') }}"><i data-feather="plus" class="wd-10 mg-r-5 tx-color-its2"></i>
            Tambah</a>
    </div>
@endsection
@section('content')
    <div class="col-sm-12 col-lg-12">
        @if (session('success'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ session('success') }}
            </div>
        @endif
        <div class="card card-body">
            <div data-label="Example" class="df-example demo-table">
                <div class="table-responsive">
                    <table id="mydatatable" class="table">
                        <thead>
                            <tr class="text-center">
                                <th class="wd-5p">No</th>
                                <th class="wd-50p">No. Surat</th>
                                <th>Status</th>
                                <th class="wd-10p"></th>
                                <th class="wd-10p"></th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- df-example -->
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
    <script src="{{ asset('lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>

    <script>
        $('#mydatatable').DataTable({
            processing: true,
            serverSide: true,
            language: {
                "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Indonesian.json"
            },
            ajax: "{{ url('pegawai/nomor-surat/server-side') }}",
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    class: 'text-center'
                },
                {
                    data: 'nomor_surat',
                    name: 'nomor_surat'
                },
                {
                    data: 'status',
                    name: 'status',
                    class: 'text-center'
                },
                {
                    data: 'edit',
                    name: 'edit',
                    class: 'text-center'
                },
                {
                    data: 'delete',
                    name: 'delete',
                    class: 'text-center'
                }
            ]
        });
    </script>
@endsection
