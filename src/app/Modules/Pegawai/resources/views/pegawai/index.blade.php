@extends('Ui::base')

@section('title')
    @if (isset($page['halaman'])){{ ucwords($page['halaman']) }}@endif
@endsection
@section('prestyles')
    <link href="{{ asset('lib/datatables.net-dt/css/jquery.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/datatables.net-responsive-dt/css/responsive.dataTables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('lib/select2/css/select2.min.css') }}" rel="stylesheet">
@endsection
@section('header_title')
    @if (isset($page['halaman'])){{ ucwords($page['halaman']) }}@endif
@endsection
@section('breadcrumbs')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-style1 mg-b-10">
            <li class="breadcrumb-item"><a href="#">
                    @if (isset($page['menu'])){{ ucwords($page['menu']) }}@endif
                </a></li>
            <li class="breadcrumb-item active" aria-current="page">
                @if (isset($page['halaman'])){{ ucwords($page['halaman']) }}@endif
            </li>
        </ol>
    </nav>
@endsection
@section('content')
    {{-- <div class="d-flex col-lg-12 flex-row justify-content-start mg-b-20">
        <div class="card card-body bg-gray-200">
            <div class="form-row align-items-center">
                <label class="col-form-label" for="filter_unit">Unit:&nbsp;</label>
                <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <select class="form-control" id="filter_unit">
                        <option value="">Semua unit</option>
                        @foreach ($data['unit'] as $item)
                            <option value="{{ $item->id_unit }}">{{ $item->nama }} </option>
                        @endforeach
                    </select>
                </div>
                <label class="col-form-label" for="filter_status">Status Pegawai:&nbsp;</label>
                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <select class="form-control" id="filter_status">
                        <option value="">Semua status</option>
                        @foreach ($data['jenis_pengajuan'] as $item)
                            <option value="{{ $item->id_jenis_pengajuan }}">{{ $item->nama }} </option>
                        @endforeach
                    </select>
                </div>
                <button id="btn-filter" class="btn d-block btn-sm btn-its tx-montserrat tx-semibold ml-1 col-xs-12">
                    Tampilkan
                </button>
                <button id="btn-reset" class="btn btn-sm btn-white tx-semibold ml-1">
                    Reset
                </button>
            </div>
        </div>
    </div> --}}
    <div class="col-sm-12 col-lg-12">
        <div class="card card-body">
            <div data-label="Example" class="df-example demo-table">
                <div class="table-responsive">
                    <table id="mydatatable" class="table">
                        <thead>
                            <tr class="text-center">
                                <th class="wd-5p">No</th>
                                <th class="wd-20p">NIK</th>
                                <th class="wd-20p">Nama</th>
                                <th>JK</th>
                                <th>Unit</th>
                                <th>Status</th>
                                <th class="wd-20p">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div><!-- df-example -->
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
    <script src="{{ asset('lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
    <script src="{{ asset('lib/select2/js/select2.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            let table = $('#mydatatable').DataTable({
                processing: true,
                serverSide: true,
                language: {
                    "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Indonesian.json"
                },
                ajax: {
                    url: "{{ url('pegawai/server-side') }}",
                    data: function(data) {
                        data.id_unit = $('#filter_unit').val();
                        data.id_status = $('#filter_status').val();
                    }
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                        class: 'text-center'
                    },
                    {
                        data: 'nik',
                        name: 'nik'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'jenis_kelamin',
                        name: 'jenis_kelamin',
                        class: 'text-center small'
                    },
                    {
                        data: 'unit',
                        name: 'unit',
                        class: 'small'
                    },
                    {
                        data: 'status',
                        name: 'status',
                        // class: 'text-center'
                    },
                    {
                        data: 'detail',
                        name: 'detail',
                        class: 'text-center'
                    }
                ],
            });


            $('#btn-filter').click(function() {
                console.log($('#filter_unit').val());
                table.ajax.reload();
            });
            $('#btn-reset').click(function() {
                document.getElementById("filter_unit").value = '';
                document.getElementById("filter_status").value = '';
                table.ajax.reload();
            });

            $('#filter_unit').select2({
                placeholder: 'Pilih Unit',
                searchInputPlaceholder: 'Search options'
            });

        });
    </script>
@endsection
