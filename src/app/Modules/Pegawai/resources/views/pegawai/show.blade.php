@extends('Ui::base')

@section('title')
    Pengajuan Pegawai
@endsection

@section('header_title')
    Pengajuan Pegawai
@endsection
@section('breadcrumbs')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-style1 mg-b-10">
            <li class="breadcrumb-item"><a href="#">
                    @if (isset($page['menu'])){{ ucwords($page['menu']) }}@endif
                </a></li>
            <li class="breadcrumb-item"><a href="#">
                    @if (isset($page['submenu'])){{ ucwords($page['submenu']) }}
                    @endif
                </a></li>
            <li class="breadcrumb-item active" aria-current="page">
                @if (isset($page['halaman'])){{ ucwords($page['halaman']) }}@endif
            </li>
        </ol>
    </nav>
@endsection
@section('content')
    <div class="col-sm-12 col-lg-12">
        @if (session('success'))
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ session('success') }}
            </div>
        @elseif(session('failed'))
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ session('failed') }}
            </div>
        @endif
        <div class="card card-body">
            <div class="row">
                <div class="col-lg-9 mg-t-5">
                    <div class="col-lg-12 pd-x-0">
                        <div class="card card-body">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                        aria-controls="home" aria-selected="true">Data Diri</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                                        aria-controls="profile" aria-selected="false">Riwayat Kontrak Pekerjaan</a>
                                </li>
                            </ul>
                            <div class="tab-content bd bd-gray-300 bd-t-0 pd-20" id="myTabContent">
                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    {{-- <h6>Data Diri</h6> --}}
                                    <div class="row justify-content-center">
                                        <div class="col-12 mg-b-30 mg-t-20">
                                            <div class="avatar avatar-xxl avatar-online text-center"><img
                                                    src="https://iskconofescondido.com/wp-content/uploads/2019/08/Download-Template-Kemeja-Putih-Untuk-Pas-Photo.jpg" class="rounded-circle" alt="">
                                            </div>
                                        </div><!-- col -->
                                    </div>
                                    <div id="baru">
                                        <div class="form-row">
                                            <div class="form-group col-md-8">
                                                <label class="font-weight-bold" for="nama">Nama Lengkap</label>
                                                <input type="text" class="form-control" id="nama" name="nama" value="{{$data->nama}}" readonly
                                                    placeholder="Nama lengkap">
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="nik">NIK</label>
                                                <input type="text" class="form-control" id="nik" name="nik" value="{{$data->nik}}" readonly
                                                    placeholder="NIK">
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-4">
                                                <label for="nama">Tanggal Lahir</label>
                                                <input type="text" class="form-control" value="{{ date('d-m-Y', strtotime($data->tgl_lahir)) }}" readonly>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <label for="jenis_kelamin">Jenis Kelamin</label>
                                                <input type="text" class="form-control" value="{{$data->jenis_kelamin == 'L' ? 'Laki-laki' : 'Wanita'}}" readonly>
                                                
                                            </div>
                                            <div class="form-group col-md-4">
                                                @php
                                                $daftarPendidikan = [
                                                                    'd1'=>"D1",
                                                                    'd2'=>"D2",
                                                                    'd3'=>"D3",
                                                                    'd4'=>"D4",
                                                                    's1'=>"S1",
                                                                    's2'=>"S2",
                                                                    's3'=>"S3",
                                                                    'sd'=>"SD",
                                                                    'sma'=>"SMA/MA/SMK",
                                                                    'smp'=>"SMP"
                                                                    ];
                                                @endphp

                                                <label for="pendidikan_terakhir">Pendidikan Terakhir</label>
                                                <input type="text" class="form-control" value="{{$daftarPendidikan[$data->id_pendidikan]}}" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="alamat">Alamat</label>
                                            <input type="text" class="form-control" value="{{$data->alamat}}" readonly
                                                placeholder="Masukkan alamat">
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col-md-6">
                                                <label for="email">Email</label>
                                                <input type="email" class="form-control" value="{{$data->email}}" readonly
                                                    placeholder="Email">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label for="hp">Nomor HP</label>
                                                <input type="text" class="form-control" value="{{$data->hp}}" readonly
                                                    placeholder="Nomor HP">
                                            </div>
                                        </div>
                                        <div class="form-group row col-md-12">
                                            <div class="form-group col-md-4">
                                                <a href="{{ route('unduh-berkas-pendukung', ['id_pengajuan' => $data->id_pengajuan, 'kategori' =>'pas_foto']) }}" target="_blank"><i class="fa fa-download"></i>Unduh Pas foto</a>
                                            </div>
                                            <div class="form-group col-md-4">
                                                <a href="{{ route('unduh-berkas-pendukung', ['id_pengajuan' => $data->id_pengajuan, 'kategori' =>'ktp']) }}" target="_blank"><i class="fa fa-download"></i> Unduh KTP</a>
                                                
                                            </div>
                                            <div class="form-group col-md-4">
                                                <a href="{{ route('unduh-berkas-pendukung', ['id_pengajuan' => $data->id_pengajuan, 'kategori' =>'ijazah']) }}" target="_blank"><i class="fa fa-download"></i> Unduh Ijazah</a>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                    <h6>Riwayat Pekerjaan</h6>
                                    <p>...</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 mg-t-10">
                    <div class="col-lg-12 pd-x-5">
                        <div class="card card-body pd-x-5 pd-y-5">
                            @if(is_null($data->tgl_nonaktif))
                                <div class="alert alert-danger" role="alert">Harap isi tanggal dan alasan menonaktifkan pegawai.</div>
                                <form class="needs-validation mt-1" action="{{ route('pegawai.menonaktifkan', ['pegawai' => $data->id_pegawai]) }}" method="post">
                                    @method('PUT')
                                    @csrf
                                    <div class="form-row px-2">
                                        <label for="tgl_nonaktif">Tanggal Dinonaktifkan</label>
                                        <input type="date" class="form-control" id="tgl_nonaktif" name="tgl_nonaktif" required>
                                    </div>
                                    <br>
                                    <div class="form-row px-2">
                                        <label for="alasan_nonaktif" class="tx-10 tx-spacing-1 tx-color-03 tx-uppercase tx-semibold">Alasan</label>
                                        <textarea name="alasan_nonaktif" id="alasan_nonaktif" class="form-control" required></textarea>
                                    </div>
                                    <button type="submit" class="btn btn-xs btn-its tx-semibold tx-montserrat btn-block mg-t-10">
                                        <i class="fa fa-edit"></i> Nonaktifkan Pegawai</button>
                                </form>
                            @else
                                <div class="alert alert-warning" role="alert">Pegawai tidak aktif sejak tanggal {{date('d-m-Y', strtotime($data->tgl_nonaktif))}} dengan alasan <b>{{$data->alasan_nonaktif}}</b></div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

@endsection