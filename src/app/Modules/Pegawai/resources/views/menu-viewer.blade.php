<li class="nav-label mg-t-15">Daftar Pengajuan</li>

<li class="nav-item {{ (Request::is('/pegawai/pengajuan') || Request::is('pegawai/pengajuan')) ? 'active' : '' }}">
    <a href="{{ url('/pegawai/pengajuan') }}" class="nav-link">
        <i data-feather="list"></i>
        <span>THL</span>
    </a>
</li>
