<?php

namespace App\Modules\Beranda\Providers;

use Dptsi\Modular\Facade\Messaging;
use Illuminate\Support\ServiceProvider;

class MessagingServiceProvider extends ServiceProvider
{
    protected string $module_name = 'beranda';

    public function register()
    {
    }

    public function boot()
    {
        Messaging::setChannel('beranda');
//        Messaging::listenTo();
    }
}