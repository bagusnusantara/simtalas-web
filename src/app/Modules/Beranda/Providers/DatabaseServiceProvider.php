<?php

namespace App\Modules\Beranda\Providers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;

class DatabaseServiceProvider extends ServiceProvider
{
    protected string $module_name = 'beranda';

    public function register()
    {
    }

    public function boot()
    {
        Config::set(
            'database.connections.' . $this->module_name,
            config($this->module_name . '.database')
        );
    }
}