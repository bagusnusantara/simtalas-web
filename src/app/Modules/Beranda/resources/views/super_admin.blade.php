@extends('Ui::base')

@section('title')
    Dashboard
@endsection

@section('header_title')
    Dashboard
@endsection
@section('prestyles')
@endsection
@section('breadcrumbs')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-style1 mg-b-10">
            <li class="breadcrumb-item"><a href="#">@if (isset($page['menu'])){{ ucwords($page['menu']) }}@endif</a></li>
        </ol>
    </nav>
@endsection
@section('content')
    
    <div class="col-12">
        <p class="mg-b-30"><span class="font-weight-bold">Selamat datang,
                {{ sso()->user()->getName() }}</span><br>{{ sso()->user()->getActiveRole()->getName() }}
            {{ sso()->user()->getActiveRole()->getOrgName() }}</p>

        <div class="accordion" id="accordionExample">
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                    <a class="text-primary text-left tx-montserrat tx-medium h4" type="button" data-toggle="collapse" data-target="#collapseOne"  aria-controls="collapseOne" id="title-accordion-1" aria-expanded="true">
                        <span style="height: 20px; width: 20px; display:inline-flex; margin-bottom: -2px; justify-content: center; align-items: center">
                            <div class="bg-primary" style="height: 4px; width: 18px; position: absolute; border-radius: 20%"></div>
                            <div class="bg-primary" id="icon-title-1" style="height: 18px; width: 4px; position: absolute; border-radius: 20%; transform: scaleY(0);transition-property: transform; transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1); transition-duration: 150ms;"></div>
                        </span>
                        Data Status Pengajuan Pegawai
                    </a>
                    </h2>
                </div>
            
                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <div class="card bg-white mg-b-10">
                            <div class="card-body">
                                <p>Status Approval Kepala SDMO</p>
                                <div class="row">
                                    <div class="col-md-3">
                                        <a href="{{ url('pegawai/pengajuan') }}">
                                            <div class="card bg-white mg-b-10 text-truncate shadow-none">
                                                <div class="card-body card-link">
                                                    <div class="media d-flex align-items-center">
                                                        <div
                                                            class="wd-60 ht-60 bg-its-icon tx-color-its mg-r-15 mg-md-r-15 d-flex align-items-center justify-content-center rounded-its">
                                                            <i class="fa fa-hourglass-half"></i>
                                                        </div>
                                                        <div class="media-body text-truncate">
                                                            <h6 class="tx-montserrat mg-b-0 text-truncate">Menunggu Approval</h6>
                                                            <h3 class="tx-left">{{ $data['perlu_disetujui'] }}</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-3">
                                        <a href="{{ url('pegawai/pengajuan') }}">
                                            <div class="card bg-white mg-b-10 text-truncate shadow-none">
                                                <div class="card-body card-link">
                                                    <div class="media d-flex align-items-center">
                                                        <div
                                                            class="wd-60 ht-60 bg-its-icon tx-success mg-r-15 mg-md-r-15 d-flex align-items-center justify-content-center rounded-its">
                                                            <i data-feather="check-circle"></i>
                                                        </div>
                                                        <div class="media-body text-truncate">
                                                            <h6 class="tx-montserrat mg-b-0 text-truncate">Diterima</h6>
                                                            <h3 class="tx-left">{{ $data['diterima'] }}</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-3">
                                        <a href="#">
                                            <div class="card bg-white mg-b-10 text-truncate shadow-none">
                                                <div class="card-body card-link">
                                                    <div class="media d-flex align-items-center">
                                                        <div
                                                            class="wd-60 ht-60 bg-its-icon tx-info mg-r-15 mg-md-r-15 d-flex align-items-center justify-content-center rounded-its">
                                                            <i data-feather="refresh-cw"></i>
                                                        </div>
                                                        <div class="media-body text-truncate">
                                                            <h6 class="tx-montserrat mg-b-0 text-truncate">Revisi</h6>
                                                            <h3 class="tx-left">{{ $data['revisi_sdmo'] }}</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-3">
                                        <a href="{{ url('pegawai/pengajuan') }}">
                                            <div class="card bg-white mg-b-10 text-truncate shadow-none">
                                                <div class="card-body card-link">
                                                    <div class="media d-flex align-items-center">
                                                        <div
                                                            class="wd-60 ht-60 bg-its-icon tx-danger mg-r-15 mg-md-r-15 d-flex align-items-center justify-content-center rounded-its">
                                                            <i data-feather="x-circle"></i>
                                                        </div>
                                                        <div class="media-body text-truncate">
                                                            <h6 class="tx-montserrat mg-b-0 text-truncate">Ditolak</h6>
                                                            <h3 class="tx-left">{{ $data['ditolak'] }}</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mg-b-10">
                            <div class="col-md-4">
                                <div class="card bg-white">
                                    <div class="card-body">
                                        <p>Total</p>
                                        <div class="row">
                                            <div class="col-md-12 mg-b-10">
                                                <a href="{{ url('pegawai/pengajuan') }}">
                                                    <div class="card bg-white mg-b-10 text-truncate shadow-none">
                                                        <div class="card-body card-link">
                                                            <div class="media d-flex align-items-center">
                                                                <div
                                                                    class="wd-60 ht-60 bg-its-icon tx-color-its mg-r-15 mg-md-r-15 d-flex align-items-center justify-content-center rounded-its">
                                                                    <i data-feather="file"></i>
                                                                </div>
                                                                <div class="media-body text-truncate">
                                                                    <h6 class="tx-montserrat mg-b-0 text-truncate">Total Pengajuan</h6>
                                                                    <h3 class="tx-left">{{ $data['total_pengajuan'] }}</h3>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="col-md-12 mg-b-45">
                                                <a href="{{ url('pegawai/pengajuan') }}">
                                                    <div class="card bg-white mg-b-10 text-truncate shadow-none">
                                                        <div class="card-body card-link">
                                                            <div class="media d-flex align-items-center">
                                                                <div
                                                                    class="wd-60 ht-60 bg-its-icon tx-color-its mg-r-15 mg-md-r-15 d-flex align-items-center justify-content-center rounded-its">
                                                                    <i data-feather="users"></i>
                                                                </div>
                                                                <div class="media-body text-truncate">
                                                                    <h6 class="tx-montserrat mg-b-0 text-truncate">Total Pegawai</h6>
                                                                    <h3 class="tx-left">{{ $data['total_pegawai'] }}</h3>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="card bg-white">
                                    <p class="mg-l-20 mg-t-20">Statistik Jumlah Pegawai di Tiap Unit</p>
                                    <div data-label="Example" class="df-example">
                                        <div class="ht-250 ht-lg-300"><canvas id="chartBar5"></canvas></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mg-b-10">
                            <div class="col-md-12">
                                <div class="card bg-white">
                                    <p class="pd-l-20 pd-t-20">Jumlah Pegawai Tiap Unit</p>
                                    <div data-label="Example" class="df-example demo-table">
                                        <div class="table-responsive">
                                            <table id="mydatatable" class="table">
                                                <thead>
                                                    <tr class="text-center">
                                                        <th class="wd-5p">No</th>
                                                        <th class="wd-20p">Unit</th>
                                                        <th class="wd-20p">THL</th>
                                                        <th>Magang</th>
                                                        <th>Kontrak</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php
                                                    $i=1;    
                                                    @endphp
                                                    @foreach ($data['unit'] as $item)
                                                        <tr>
                                                            <td class="text-center">{{ $i++ }}</td>
                                                            <td>{{ $item->nama }}</td>
                                                            <td class="text-center">{{ DB::table('pengajuan')->where('id_jenis_pengajuan','thl')->where('id_unit',$item->id_unit)->count() }}</td>
                                                            <td class="text-center">{{ DB::table('pengajuan')->where('id_jenis_pengajuan','magang')->where('id_unit',$item->id_unit)->count() }}</td>
                                                            <td class="text-center">{{ DB::table('pengajuan')->where('id_jenis_pengajuan','kontrak')->where('id_unit',$item->id_unit)->count() }}</td>
                                                        </tr>
                                                    @endforeach
                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingTwo">
                    <h2 class="mb-0">
                        <a class="text-primary text-left tx-montserrat tx-medium h4" type="button" data-toggle="collapse" data-target="#collapseTwo"  aria-controls="collapseTwo" id="title-accordion-2" aria-expanded="false">
                            <span style="height: 20px; width: 20px; display:inline-flex; margin-bottom: -2px; justify-content: center; align-items: center">
                                <div class="bg-primary" style="height: 4px; width: 18px; position: absolute; border-radius: 20%"></div>
                                <div class="bg-primary" id="icon-title-2" style="height: 18px; width: 4px; position: absolute; border-radius: 20%; transform: scaleY(1);transition-property: transform; transition-timing-function: cubic-bezier(0.4, 0, 0.2, 1); transition-duration: 150ms;"></div>
                            </span>
                            Data Status Pengajuan Dosen
                        </a>
                    </h2>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="card-body">
                        <div class="card bg-white mg-b-10">
                            <div class="card-body">
                                <p>Status Approval Kepala SDMO</p>
                                <div class="row">
                                    <div class="col-md-3">
                                        <a href="{{ url('pegawai/pengajuan-dosen') }}">
                                            <div class="card bg-white mg-b-10 text-truncate shadow-none">
                                                <div class="card-body card-link">
                                                    <div class="media d-flex align-items-center">
                                                        <div
                                                            class="wd-60 ht-60 bg-its-icon tx-color-its mg-r-15 mg-md-r-15 d-flex align-items-center justify-content-center rounded-its">
                                                            <i class="fa fa-hourglass-half"></i>
                                                        </div>
                                                        <div class="media-body text-truncate">
                                                            <h6 class="tx-montserrat mg-b-0 text-truncate">Menunggu Approval</h6>
                                                            <h3 class="tx-left">{{ $pengajuan_dosen['perlu_disetujui'] }}</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-3">
                                        <a href="{{ url('pegawai/pengajuan-dosen') }}">
                                            <div class="card bg-white mg-b-10 text-truncate shadow-none">
                                                <div class="card-body card-link">
                                                    <div class="media d-flex align-items-center">
                                                        <div
                                                            class="wd-60 ht-60 bg-its-icon tx-success mg-r-15 mg-md-r-15 d-flex align-items-center justify-content-center rounded-its">
                                                            <i data-feather="check-circle"></i>
                                                        </div>
                                                        <div class="media-body text-truncate">
                                                            <h6 class="tx-montserrat mg-b-0 text-truncate">Diterima</h6>
                                                            <h3 class="tx-left">{{ $pengajuan_dosen['diterima'] }}</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-3">
                                        <a href="{{ url('pegawai/pengajuan-dosen') }}">
                                            <div class="card bg-white mg-b-10 text-truncate shadow-none">
                                                <div class="card-body card-link">
                                                    <div class="media d-flex align-items-center">
                                                        <div
                                                            class="wd-60 ht-60 bg-its-icon tx-info mg-r-15 mg-md-r-15 d-flex align-items-center justify-content-center rounded-its">
                                                            <i data-feather="refresh-cw"></i>
                                                        </div>
                                                        <div class="media-body text-truncate">
                                                            <h6 class="tx-montserrat mg-b-0 text-truncate">Revisi</h6>
                                                            <h3 class="tx-left">{{ $pengajuan_dosen['revisi_sdmo'] }}</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-3">
                                        <a href="{{ url('pegawai/pengajuan-dosen') }}">
                                            <div class="card bg-white mg-b-10 text-truncate shadow-none">
                                                <div class="card-body card-link">
                                                    <div class="media d-flex align-items-center">
                                                        <div
                                                            class="wd-60 ht-60 bg-its-icon tx-danger mg-r-15 mg-md-r-15 d-flex align-items-center justify-content-center rounded-its">
                                                            <i data-feather="x-circle"></i>
                                                        </div>
                                                        <div class="media-body text-truncate">
                                                            <h6 class="tx-montserrat mg-b-0 text-truncate">Ditolak</h6>
                                                            <h3 class="tx-left">{{ $pengajuan_dosen['ditolak'] }}</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mg-b-10">
                            <div class="col-md-4">
                                <div class="card bg-white">
                                    <div class="card-body">
                                        <p>Total</p>
                                        <div class="row">
                                            <div class="col-md-12 mg-b-10">
                                                <a href="{{ url('pegawai/pengajuan-dosen') }}">
                                                    <div class="card bg-white mg-b-10 text-truncate shadow-none">
                                                        <div class="card-body card-link">
                                                            <div class="media d-flex align-items-center">
                                                                <div
                                                                    class="wd-60 ht-60 bg-its-icon tx-color-its mg-r-15 mg-md-r-15 d-flex align-items-center justify-content-center rounded-its">
                                                                    <i data-feather="file"></i>
                                                                </div>
                                                                <div class="media-body text-truncate">
                                                                    <h6 class="tx-montserrat mg-b-0 text-truncate">Total Pengajuan</h6>
                                                                    <h3 class="tx-left">{{ $pengajuan_dosen['total_pengajuan'] }}</h3>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="col-md-12 mg-b-45">
                                                <a href="{{ url('pegawai/pengajuan-dosen') }}">
                                                    <div class="card bg-white mg-b-10 text-truncate shadow-none">
                                                        <div class="card-body card-link">
                                                            <div class="media d-flex align-items-center">
                                                                <div
                                                                    class="wd-60 ht-60 bg-its-icon tx-color-its mg-r-15 mg-md-r-15 d-flex align-items-center justify-content-center rounded-its">
                                                                    <i data-feather="users"></i>
                                                                </div>
                                                                <div class="media-body text-truncate">
                                                                    <h6 class="tx-montserrat mg-b-0 text-truncate">Total Pegawai</h6>
                                                                    <h3 class="tx-left">{{ $pengajuan_dosen['total_pegawai'] }}</h3>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="card bg-white">
                                    <p class="mg-l-20 mg-t-20">Statistik Jumlah Dosen di Tiap Unit</p>
                                    <div data-label="Statistik" class="df-dosen">
                                        <div class="ht-250 ht-lg-300"><canvas id="chartBarDosen"></canvas></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mg-b-10">
                            <div class="col-md-12">
                                <div class="card bg-white">
                                    <p class="pd-l-20 pd-t-20">Jumlah Dosen Tiap Unit</p>
                                    <div data-label="Example" class="df-example demo-table">
                                        <div class="table-responsive">
                                            <table id="pengajuan-dosen-data-table" class="table">
                                                <thead>
                                                    <tr class="text-center">
                                                        <th class="wd-5p">No</th>
                                                        <th class="wd-20p">Unit</th>
                                                        <th>MKB Wajib</th>
                                                        <th>Non MKB Wajib</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @php
                                                    $i=1;    
                                                    @endphp
                                                    @foreach ($pengajuan_dosen['unit'] as $item)
                                                        <tr>
                                                            <td class="text-center">{{ $i++ }}</td>
                                                            <td>{{ $item->nama }}</td>
                                                            <td class="text-center">{{ $item->jumlah_mkbwajib ?? '-' }}</td>
                                                            <td class="text-center">{{ $item->jumlah_nonmkbwajib ?? '-' }}</td>
                                                        </tr>
                                                    @endforeach
                    
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('prescripts')
    <script src="{{ asset('lib/chart.js/Chart.bundle.min.js') }}"></script>
    <script src="{{ asset('lib/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('lib/datatables.net-dt/js/dataTables.dataTables.min.js') }}"></script>
    <script src="{{ asset('lib/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('lib/datatables.net-responsive-dt/js/responsive.dataTables.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            
            // START kondisi awal 
                // $('#title-accordion-1').prepend('<i class="fa fa-minus"></i>');
                // $('#title-accordion-2').prepend('<i class="fa fa-plus"></i>');
            // END kondisi awal 

            $('#accordionExample').on('shown.bs.collapse', function (e) {
                // $('#title-accordion-1').children().remove();
                // $('#title-accordion-2').children().remove();
                
                if ($('#title-accordion-1').attr('aria-expanded') === 'true') {
                    document.getElementById('icon-title-1').style.transform = 'scaleY(0)'
                    document.getElementById('icon-title-2').style.transform = 'scaleY(1)'
                    // console.log(document.getElementById('icon-title-1').style)
                    // $('#title-accordion-1').prepend('<i class="fa fa-minus"></i>');
                    // $('#title-accordion-2').prepend('<i class="fa fa-plus"></i>');
                }
                if($('#title-accordion-2').attr('aria-expanded') === 'true') {
                    document.getElementById('icon-title-1').style.transform = 'scaleY(1)'
                    document.getElementById('icon-title-2').style.transform = 'scaleY(0)'
                    // $('#title-accordion-2').prepend('<i class="fa fa-minus"></i>');
                    // $('#title-accordion-1').prepend('<i class="fa fa-plus"></i>');
                }
            });

            $('#accordionExample').on('hidden.bs.collapse', function () {
                // $('#title-accordion-1').children().remove();
                // $('#title-accordion-2').children().remove();

                if ($('#title-accordion-1').attr('aria-expanded') === 'false' && $('#title-accordion-2').attr('aria-expanded') === 'false') {
                    document.getElementById('icon-title-1').style.transform = 'scaleY(1)'
                    document.getElementById('icon-title-2').style.transform = 'scaleY(1)'
                    // $('#title-accordion-1').prepend('<i class="fa fa-plus"></i>');
                    // $('#title-accordion-2').prepend('<i class="fa fa-plus"></i>');
                } else {
                    if ($('#title-accordion-1').attr('aria-expanded') === 'false') {
                        document.getElementById('icon-title-1').style.transform = 'scaleY(1)'
                        document.getElementById('icon-title-2').style.transform = 'scaleY(0)'
                        // $('#title-accordion-1').prepend('<i class="fa fa-plus"></i>');
                        // $('#title-accordion-2').prepend('<i class="fa fa-minus"></i>');
                    }

                    if($('#title-accordion-2').attr('aria-expanded') === 'false') {
                        document.getElementById('icon-title-1').style.transform = 'scaleY(0)'
                        document.getElementById('icon-title-2').style.transform = 'scaleY(1)'
                        $('#title-accordion-2').prepend('<i class="fa fa-plus"></i>');
                        $('#title-accordion-1').prepend('<i class="fa fa-minus"></i>');
                    }
                }


            });
        });

        $(function() {
            let table = $('#mydatatable').DataTable();
            'use strict'
            let thl = <?= $data['thl'] ?>;
            let magang = <?= $data['kontrak'] ?>;
            let kontrak = <?= $data['magang'] ?>;

            var ctxLabel = ['THL', 'Magang', 'Kontrak'];
            var ctxData1 = [thl, kontrak, magang];
            var ctxColor1 = '#001737';

            // With gradient
            var ctx7 = document.getElementById('chartBar5').getContext('2d');

            var gradient1 = ctx7.createLinearGradient(0, 350, 0, 0);
            gradient1.addColorStop(0, '#001737');
            gradient1.addColorStop(1, '#0168fa');



            new Chart(ctx7, {
                type: 'bar',
                data: {
                    labels: ctxLabel,
                    datasets: [{
                        data: ctxData1,
                        backgroundColor: gradient1
                    }]
                },
                options: {
                    maintainAspectRatio: false,
                    responsive: true,
                    legend: {
                        display: false,
                        labels: {
                            display: false
                        }
                    },
                    scales: {
                        yAxes: [{
                            gridLines: {
                                color: '#e5e9f2'
                            },
                            ticks: {
                                beginAtZero: true,
                                fontSize: 10,
                                fontColor: '#182b49',
                                max: <?= $data['kontrak']+$data['thl']+$data['magang'] ?>
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                display: false
                            },
                            barPercentage: 0.6,
                            ticks: {
                                beginAtZero: true,
                                fontSize: 11,
                                fontColor: '#182b49'
                            }
                        }]
                    }
                }
            });

        })
        $(function() {
            'use strict'
            let table = $('#pengajuan-dosen-data-table').DataTable();
            let nonmkbwajib = <?= $pengajuan_dosen['nonmkbwajib'] ?>;
            let mkbwajib = <?= $pengajuan_dosen['mkbwajib'] ?>;
            console.table({nonmkbwajib, mkbwajib})

            var ctxLabel = ['Non MKB Wajib', 'MKB Wajib'];
            var ctxData1 = [nonmkbwajib, mkbwajib];
            var ctxColor1 = '#001737';

            // With gradient
            var ctx7 = document.getElementById('chartBarDosen').getContext('2d');

            var gradient1 = ctx7.createLinearGradient(0, 350, 0, 0);
            gradient1.addColorStop(0, '#001737');
            gradient1.addColorStop(1, '#0168fa');



            new Chart(ctx7, {
                type: 'bar',
                data: {
                    labels: ctxLabel,
                    datasets: [{
                        data: ctxData1,
                        backgroundColor: gradient1
                    }]
                },
                options: {
                    maintainAspectRatio: false,
                    responsive: true,
                    legend: {
                        display: false,
                        labels: {
                            display: false
                        }
                    },
                    scales: {
                        yAxes: [{
                            gridLines: {
                                color: '#e5e9f2'
                            },
                            ticks: {
                                beginAtZero: true,
                                fontSize: 10,
                                fontColor: '#182b49',
                                max: <?= $pengajuan_dosen['nonmkbwajib'] +  $pengajuan_dosen['mkbwajib'] ?>
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                display: false
                            },
                            barPercentage: 0.6,
                            ticks: {
                                beginAtZero: true,
                                fontSize: 11,
                                fontColor: '#182b49'
                            }
                        }]
                    }
                }
            });

        })
    </script>
@endsection
