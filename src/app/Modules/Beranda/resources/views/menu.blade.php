@if(env('BERANDA_DB_HOST') == '10.199.16.69')
	<li class="nav-label mg-t-15">Dashboard || DB: DEV</li>
@else
	<li class="nav-label mg-t-15">Dashboard</li>
@endif

<li class="nav-item {{ (Request::is('/') || Request::is('beranda')) ? 'active' : '' }}">
    <a href="{{ url('/') }}" class="nav-link">
        <i data-feather="home"></i>
        <span>Dashboard</span>
    </a>
</li>

