@extends('Ui::base')

@section('title')
    Dashboard
@endsection

@section('header_title')
    Dashboard
@endsection
@section('prestyles')
@endsection
@section('breadcrumbs')
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb breadcrumb-style1 mg-b-10">
            <li class="breadcrumb-item"><a href="#">@if (isset($page['menu'])){{ ucwords($page['menu']) }}@endif</a></li>
        </ol>
    </nav>
@endsection
@section('content')
    <div class="col-12">
        <p class="mg-b-30"><span class="font-weight-bold">Selamat datang,
                {{ sso()->user()->getName() }}</span><br>{{ sso()->user()->getActiveRole()->getName() }}
            {{ sso()->user()->getActiveRole()->getOrgName() }}</p>

        <div class="accordion" id="accordionExample">
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                    <a class="text-primary text-left tx-montserrat tx-medium h4" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        Data Status Pengajuan Pegawai
                    </a>
                    </h2>
                </div>
            
                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <div class="card bg-white mg-b-10">
                            <div class="card-body">
                                <p>Status Approval Kepala Unit</p>
                                <div class="row">
                                    <div class="col-md-3">
                                        <a href="#">
                                            <div class="card bg-white mg-b-10 text-truncate shadow-none">
                                                <div class="card-body card-link">
                                                    <div class="media d-flex align-items-center">
                                                        <div
                                                            class="wd-60 ht-60 bg-its-icon tx-color-its mg-r-15 mg-md-r-15 d-flex align-items-center justify-content-center rounded-its">
                                                            <i class="fa fa-hourglass-half"></i>
                                                        </div>
                                                        <div class="media-body text-truncate">
                                                            <h6 class="tx-montserrat mg-b-0 text-truncate">Menunggu Approval</h6>
                                                            <h3 class="tx-left">{{ $data['approval_unit'] }}</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-3">
                                        <a href="#">
                                            <div class="card bg-white mg-b-10 text-truncate shadow-none">
                                                <div class="card-body card-link">
                                                    <div class="media d-flex align-items-center">
                                                        <div
                                                            class="wd-60 ht-60 bg-its-icon tx-success mg-r-15 mg-md-r-15 d-flex align-items-center justify-content-center rounded-its">
                                                            <i data-feather="check-circle"></i>
                                                        </div>
                                                        <div class="media-body text-truncate">
                                                            <h6 class="tx-montserrat mg-b-0 text-truncate">Diterima</h6>
                                                            <h3 class="tx-left">{{ $data['diterima_unit'] }}</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-3">
                                        <a href="#">
                                            <div class="card bg-white mg-b-10 text-truncate shadow-none">
                                                <div class="card-body card-link">
                                                    <div class="media d-flex align-items-center">
                                                        <div
                                                            class="wd-60 ht-60 bg-its-icon tx-info mg-r-15 mg-md-r-15 d-flex align-items-center justify-content-center rounded-its">
                                                            <i data-feather="refresh-cw"></i>
                                                        </div>
                                                        <div class="media-body text-truncate">
                                                            <h6 class="tx-montserrat mg-b-0 text-truncate">Revisi</h6>
                                                            <h3 class="tx-left">{{ $data['revisi_unit'] }}</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-3">
                                        <a href="#">
                                            <div class="card bg-white mg-b-10 text-truncate shadow-none">
                                                <div class="card-body card-link">
                                                    <div class="media d-flex align-items-center">
                                                        <div
                                                            class="wd-60 ht-60 bg-its-icon tx-danger mg-r-15 mg-md-r-15 d-flex align-items-center justify-content-center rounded-its">
                                                            <i data-feather="x-circle"></i>
                                                        </div>
                                                        <div class="media-body text-truncate">
                                                            <h6 class="tx-montserrat mg-b-0 text-truncate">Ditolak</h6>
                                                            <h3 class="tx-left">{{ $data['ditolak_unit'] }}</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- Approval SDMO --}}
                        <div class="card bg-white mg-b-10">
                            <div class="card-body">
                                <p>Status Approval Kepala SDMO</p>
                                <div class="row">
                                    <div class="col-md-3">
                                        <a href="#">
                                            <div class="card bg-white mg-b-10 text-truncate shadow-none">
                                                <div class="card-body card-link">
                                                    <div class="media d-flex align-items-center">
                                                        <div
                                                            class="wd-60 ht-60 bg-its-icon tx-color-its mg-r-15 mg-md-r-15 d-flex align-items-center justify-content-center rounded-its">
                                                            <i class="fa fa-hourglass-half"></i>
                                                        </div>
                                                        <div class="media-body text-truncate">
                                                            <h6 class="tx-montserrat mg-b-0 text-truncate">Menunggu Approval</h6>
                                                            <h3 class="tx-left">{{ $data['approval_sdmo'] }}</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-3 mg-b-10">
                                        <a href="#">
                                            <div class="card bg-white mg-b-10 text-truncate shadow-none">
                                                <div class="card-body card-link">
                                                    <div class="media d-flex align-items-center">
                                                        <div
                                                            class="wd-60 ht-60 bg-its-icon tx-success mg-r-15 mg-md-r-15 d-flex align-items-center justify-content-center rounded-its">
                                                            <i data-feather="check-circle"></i>
                                                        </div>
                                                        <div class="media-body text-truncate">
                                                            <h6 class="tx-montserrat mg-b-0 text-truncate">Diterima</h6>
                                                            <h3 class="tx-left">{{ $data['diterima_sdmo'] }}</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-3">
                                        <a href="#">
                                            <div class="card bg-white mg-b-10 text-truncate shadow-none">
                                                <div class="card-body card-link">
                                                    <div class="media d-flex align-items-center">
                                                        <div
                                                            class="wd-60 ht-60 bg-its-icon tx-info mg-r-15 mg-md-r-15 d-flex align-items-center justify-content-center rounded-its">
                                                            <i data-feather="refresh-cw"></i>
                                                        </div>
                                                        <div class="media-body text-truncate">
                                                            <h6 class="tx-montserrat mg-b-0 text-truncate">Revisi</h6>
                                                            <h3 class="tx-left">{{ $data['revisi_sdmo'] }}</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-3">
                                        <a href="#">
                                            <div class="card bg-white mg-b-10 text-truncate shadow-none">
                                                <div class="card-body card-link">
                                                    <div class="media d-flex align-items-center">
                                                        <div
                                                            class="wd-60 ht-60 bg-its-icon tx-danger mg-r-15 mg-md-r-15 d-flex align-items-center justify-content-center rounded-its">
                                                            <i data-feather="x-circle"></i>
                                                        </div>
                                                        <div class="media-body text-truncate">
                                                            <h6 class="tx-montserrat mg-b-0 text-truncate">Ditolak</h6>
                                                            <h3 class="tx-left">{{ $data['ditolak_sdmo'] }}</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="card bg-white">
                                    <div class="card-body">
                                        <p>Total</p>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <a href="#">
                                                    <div class="card bg-white mg-b-10 text-truncate shadow-none">
                                                        <div class="card-body card-link">
                                                            <div class="media d-flex align-items-center">
                                                                <div
                                                                    class="wd-60 ht-60 bg-its-icon tx-color-its mg-r-15 mg-md-r-15 d-flex align-items-center justify-content-center rounded-its">
                                                                    <i data-feather="file"></i>
                                                                </div>
                                                                <div class="media-body">
                                                                    <h6 class="tx-montserrat mg-b-0 text-truncate">Total Pengajuan</h6>
                                                                    <h3 class="tx-left">{{ $data['total'] }}</h3>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="card bg-white">
                                    <p class="mg-l-20 mg-t-20">Statistik Jumlah Pegawai di Unit</p>
                                    <div data-label="Example" class="df-example">
                                        <div class="ht-250 ht-lg-300"><canvas id="chartBar5"></canvas></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header" id="headingTwo">
                    <h2 class="mb-0">
                        <a class="text-primary text-left tx-montserrat tx-medium h4" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                            Data Status Pengajuan Dosen
                        </a>
                    </h2>
                </div>
                <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="card-body">
                        <div class="card bg-white mg-b-10">
                            <div class="card-body">
                                <p>Status Approval Kepala Unit</p>
                                <div class="row">
                                    <div class="col-md-3">
                                        <a href="{{ url('pegawai/pengajuan-dosen') }}">
                                            <div class="card bg-white mg-b-10 text-truncate shadow-none">
                                                <div class="card-body card-link">
                                                    <div class="media d-flex align-items-center">
                                                        <div
                                                            class="wd-60 ht-60 bg-its-icon tx-color-its mg-r-15 mg-md-r-15 d-flex align-items-center justify-content-center rounded-its">
                                                            <i class="fa fa-hourglass-half"></i>
                                                        </div>
                                                        <div class="media-body text-truncate">
                                                            <h6 class="tx-montserrat mg-b-0 text-truncate">Menunggu Approval</h6>
                                                            <h3 class="tx-left">{{ $pengajuan_dosen['approval_unit'] }}</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-3">
                                        <a href="{{ url('pegawai/pengajuan-dosen') }}">
                                            <div class="card bg-white mg-b-10 text-truncate shadow-none">
                                                <div class="card-body card-link">
                                                    <div class="media d-flex align-items-center">
                                                        <div
                                                            class="wd-60 ht-60 bg-its-icon tx-success mg-r-15 mg-md-r-15 d-flex align-items-center justify-content-center rounded-its">
                                                            <i data-feather="check-circle"></i>
                                                        </div>
                                                        <div class="media-body text-truncate">
                                                            <h6 class="tx-montserrat mg-b-0 text-truncate">Diterima</h6>
                                                            <h3 class="tx-left">{{ $pengajuan_dosen['diterima_unit'] }}</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-3">
                                        <a href="{{ url('pegawai/pengajuan-dosen') }}">
                                            <div class="card bg-white mg-b-10 text-truncate shadow-none">
                                                <div class="card-body card-link">
                                                    <div class="media d-flex align-items-center">
                                                        <div
                                                            class="wd-60 ht-60 bg-its-icon tx-info mg-r-15 mg-md-r-15 d-flex align-items-center justify-content-center rounded-its">
                                                            <i data-feather="refresh-cw"></i>
                                                        </div>
                                                        <div class="media-body text-truncate">
                                                            <h6 class="tx-montserrat mg-b-0 text-truncate">Revisi</h6>
                                                            <h3 class="tx-left">{{ $pengajuan_dosen['revisi_unit'] }}</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-3">
                                        <a href="{{ url('pegawai/pengajuan-dosen') }}">
                                            <div class="card bg-white mg-b-10 text-truncate shadow-none">
                                                <div class="card-body card-link">
                                                    <div class="media d-flex align-items-center">
                                                        <div
                                                            class="wd-60 ht-60 bg-its-icon tx-danger mg-r-15 mg-md-r-15 d-flex align-items-center justify-content-center rounded-its">
                                                            <i data-feather="x-circle"></i>
                                                        </div>
                                                        <div class="media-body text-truncate">
                                                            <h6 class="tx-montserrat mg-b-0 text-truncate">Ditolak</h6>
                                                            <h3 class="tx-left">{{ $pengajuan_dosen['ditolak_unit'] }}</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{-- Approval SDMO --}}
                        <div class="card bg-white mg-b-10">
                            <div class="card-body">
                                <p>Status Approval Kepala SDMO</p>
                                <div class="row">
                                    <div class="col-md-3">
                                        <a href="{{ url('pegawai/pengajuan-dosen') }}">
                                            <div class="card bg-white mg-b-10 text-truncate shadow-none">
                                                <div class="card-body card-link">
                                                    <div class="media d-flex align-items-center">
                                                        <div
                                                            class="wd-60 ht-60 bg-its-icon tx-color-its mg-r-15 mg-md-r-15 d-flex align-items-center justify-content-center rounded-its">
                                                            <i class="fa fa-hourglass-half"></i>
                                                        </div>
                                                        <div class="media-body text-truncate">
                                                            <h6 class="tx-montserrat mg-b-0 text-truncate">Menunggu Approval</h6>
                                                            <h3 class="tx-left">{{ $pengajuan_dosen['approval_sdmo'] }}</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-3 mg-b-10">
                                        <a href="{{ url('pegawai/pengajuan-dosen') }}">
                                            <div class="card bg-white mg-b-10 text-truncate shadow-none">
                                                <div class="card-body card-link">
                                                    <div class="media d-flex align-items-center">
                                                        <div
                                                            class="wd-60 ht-60 bg-its-icon tx-success mg-r-15 mg-md-r-15 d-flex align-items-center justify-content-center rounded-its">
                                                            <i data-feather="check-circle"></i>
                                                        </div>
                                                        <div class="media-body text-truncate">
                                                            <h6 class="tx-montserrat mg-b-0 text-truncate">Diterima</h6>
                                                            <h3 class="tx-left">{{ $pengajuan_dosen['diterima_sdmo'] }}</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-3">
                                        <a href="{{ url('pegawai/pengajuan-dosen') }}">
                                            <div class="card bg-white mg-b-10 text-truncate shadow-none">
                                                <div class="card-body card-link">
                                                    <div class="media d-flex align-items-center">
                                                        <div
                                                            class="wd-60 ht-60 bg-its-icon tx-info mg-r-15 mg-md-r-15 d-flex align-items-center justify-content-center rounded-its">
                                                            <i data-feather="refresh-cw"></i>
                                                        </div>
                                                        <div class="media-body text-truncate">
                                                            <h6 class="tx-montserrat mg-b-0 text-truncate">Revisi</h6>
                                                            <h3 class="tx-left">{{ $pengajuan_dosen['revisi_sdmo'] }}</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="col-md-3">
                                        <a href="{{ url('pegawai/pengajuan-dosen') }}">
                                            <div class="card bg-white mg-b-10 text-truncate shadow-none">
                                                <div class="card-body card-link">
                                                    <div class="media d-flex align-items-center">
                                                        <div
                                                            class="wd-60 ht-60 bg-its-icon tx-danger mg-r-15 mg-md-r-15 d-flex align-items-center justify-content-center rounded-its">
                                                            <i data-feather="x-circle"></i>
                                                        </div>
                                                        <div class="media-body text-truncate">
                                                            <h6 class="tx-montserrat mg-b-0 text-truncate">Ditolak</h6>
                                                            <h3 class="tx-left">{{ $pengajuan_dosen['ditolak_sdmo'] }}</h3>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="card bg-white">
                                    <div class="card-body">
                                        <p>Total</p>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <a href="{{ url('pegawai/pengajuan-dosen') }}">
                                                    <div class="card bg-white mg-b-10 text-truncate shadow-none">
                                                        <div class="card-body card-link">
                                                            <div class="media d-flex align-items-center">
                                                                <div
                                                                    class="wd-60 ht-60 bg-its-icon tx-color-its mg-r-15 mg-md-r-15 d-flex align-items-center justify-content-center rounded-its">
                                                                    <i data-feather="file"></i>
                                                                </div>
                                                                <div class="media-body">
                                                                    <h6 class="tx-montserrat mg-b-0 text-truncate">Total Pengajuan</h6>
                                                                    <h3 class="tx-left">{{ $pengajuan_dosen['total'] }}</h3>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="card bg-white">
                                    <p class="mg-l-20 mg-t-20">Statistik Jumlah Dosen di Unit</p>
                                    <div data-label="Statistik" class="df-dosen">
                                        <div class="ht-250 ht-lg-300"><canvas id="chartBarDosen"></canvas></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        
@endsection
@section('prescripts')
    <script src="{{ asset('lib/chart.js/Chart.bundle.min.js') }}"></script>
    <script>
        $(function() {
            'use strict'
            let thl = <?= $data['thl'] ?>;
            let magang = <?= $data['kontrak'] ?>;
            let kontrak = <?= $data['magang'] ?>;

            var ctxLabel = ['THL', 'Magang', 'Kontrak'];
            var ctxData1 = [thl, kontrak, magang];
            var ctxColor1 = '#001737';

            // With gradient
            var ctx7 = document.getElementById('chartBar5').getContext('2d');

            var gradient1 = ctx7.createLinearGradient(0, 350, 0, 0);
            gradient1.addColorStop(0, '#001737');
            gradient1.addColorStop(1, '#0168fa');



            new Chart(ctx7, {
                type: 'bar',
                data: {
                    labels: ctxLabel,
                    datasets: [{
                        data: ctxData1,
                        backgroundColor: gradient1
                    }]
                },
                options: {
                    maintainAspectRatio: false,
                    responsive: true,
                    legend: {
                        display: false,
                        labels: {
                            display: false
                        }
                    },
                    scales: {
                        yAxes: [{
                            gridLines: {
                                color: '#e5e9f2'
                            },
                            ticks: {
                                beginAtZero: true,
                                fontSize: 10,
                                fontColor: '#182b49',
                                max: <?= $data['thl']+$data['kontrak']+$data['magang'] ?>
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                display: false
                            },
                            barPercentage: 0.6,
                            ticks: {
                                beginAtZero: true,
                                fontSize: 11,
                                fontColor: '#182b49'
                            }
                        }]
                    }
                }
            });

        })
        $(function() {
            'use strict'
            let nonmkbwajib = <?= $pengajuan_dosen['nonmkbwajib'] ?>;
            let mkbwajib = <?= $pengajuan_dosen['mkbwajib'] ?>;

            var ctxLabel = ['Non MKB Wajib', 'MKB Wajib'];
            var ctxData1 = [nonmkbwajib, mkbwajib];
            var ctxColor1 = '#001737';

            // With gradient
            var ctx7 = document.getElementById('chartBarDosen').getContext('2d');

            var gradient1 = ctx7.createLinearGradient(0, 350, 0, 0);
            gradient1.addColorStop(0, '#001737');
            gradient1.addColorStop(1, '#0168fa');



            new Chart(ctx7, {
                type: 'bar',
                data: {
                    labels: ctxLabel,
                    datasets: [{
                        data: ctxData1,
                        backgroundColor: gradient1
                    }]
                },
                options: {
                    maintainAspectRatio: false,
                    responsive: true,
                    legend: {
                        display: false,
                        labels: {
                            display: false
                        }
                    },
                    scales: {
                        yAxes: [{
                            gridLines: {
                                color: '#e5e9f2'
                            },
                            ticks: {
                                beginAtZero: true,
                                fontSize: 10,
                                fontColor: '#182b49',
                                max: <?= $pengajuan_dosen['mkbwajib']+$pengajuan_dosen['nonmkbwajib'] ?>
                            }
                        }],
                        xAxes: [{
                            gridLines: {
                                display: false
                            },
                            barPercentage: 0.6,
                            ticks: {
                                beginAtZero: true,
                                fontSize: 11,
                                fontColor: '#182b49'
                            }
                        }]
                    }
                }
            });

        })
    </script>
@endsection
