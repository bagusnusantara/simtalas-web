<?php

namespace App\Modules\Beranda\Controllers;

use App\Modules\Pegawai\Repositories\UnitRepository;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;


class BerandaController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function index(Request $request)
    {
        // dd(Session::all(), sso()->user()->getActiveRole()->getName());
        $page['menu'] = "dashboard";
        $data['nama_unit'] = sso()->user()->getActiveRole()->getName();
        $data['id_unit'] = sso()->user()->getActiveRole()->getOrgId();
        // dd($data['id_unit'] );

        if ($data['nama_unit'] = sso()->user()->getActiveRole()->getName() == 'Super Administrator' || sso()->user()->getActiveRole()->getName() == 'Viewer myITS HRC') {
            // $data['total'] =  DB::table('pengajuan')->where('deleted_at', NULL)->count();
            $data['perlu_disetujui'] =  DB::table('pengajuan')->where('is_approved1', NULL)->whereNotIn('is_approved0', [0,2,NULL])->where('deleted_at', NULL)->count();
            $data['total_pengajuan'] =  DB::table('pengajuan')->where('is_approved0', 1)->where('deleted_at', NULL)->count();
            $data['total_pegawai'] =  DB::table('pengajuan')->where('is_approved0', 1)->where('is_approved1', 1)->where('deleted_at', NULL)->count();
            // dd($data['belum_approval_unit']);
            $data['diterima'] =  DB::table('pengajuan')->where('is_approved1', 1)->where('deleted_at', NULL)->count();
            $data['revisi_sdmo'] =  DB::table('pengajuan')->where('is_approved1', 2)->where('deleted_at', NULL)->count();
            $data['ditolak'] =  DB::table('pengajuan')->where('is_approved1', 0)->where('deleted_at', NULL)->count();
            $data['thl'] =  DB::table('pengajuan')->where('id_jenis_pengajuan', 'thl')->where('is_approved1', 1)->where('deleted_at', NULL)->count();
            $data['kontrak'] =  DB::table('pengajuan')->where('id_jenis_pengajuan', 'kontrak')->where('is_approved1', 1)->where('deleted_at', NULL)->count();
            $data['magang'] =  DB::table('pengajuan')->where('id_jenis_pengajuan', 'magang')->where('is_approved1', 1)->where('deleted_at', NULL)->count();

            $unitRepository = new UnitRepository();
            $data['unit'] = $unitRepository->getAll();

            // Pengajuan Dosen
            $pengajuan_dosen['id_unit'] = $data['id_unit'];
            $pengajuan_dosen['perlu_disetujui'] =  DB::table('pengajuan_dosen')->where('is_approved1', NULL)->whereNotIn('is_approved0', [0,2,NULL])->where('deleted_at', NULL)->count();
            $pengajuan_dosen['total_pengajuan'] =  DB::table('pengajuan_dosen')->where('is_approved0', 1)->where('deleted_at', NULL)->count();
            $pengajuan_dosen['total_pegawai'] =  DB::table('pengajuan_dosen')->where('is_approved0', 1)->where('is_approved1', 1)->where('deleted_at', NULL)->count();
            $pengajuan_dosen['diterima'] =  DB::table('pengajuan_dosen')->where('is_approved1', 1)->where('deleted_at', NULL)->count();
            $pengajuan_dosen['revisi_sdmo'] =  DB::table('pengajuan_dosen')->where('is_approved1', 2)->where('deleted_at', NULL)->count();
            $pengajuan_dosen['ditolak'] =  DB::table('pengajuan_dosen')->where('is_approved1', 0)->where('deleted_at', NULL)->count();
            
            $pengajuan_dosen['mkbwajib'] =  DB::table('pengajuan_dosen')->where('id_jenis_pengajuan_dosen', 'mkbwajib')->where('is_approved1', 1)->where('deleted_at', NULL)->count();
            $pengajuan_dosen['nonmkbwajib'] =  DB::table('pengajuan_dosen')->where('id_jenis_pengajuan_dosen', 'nonmkbwajib')->where('is_approved1', 1)->where('deleted_at', NULL)->count();
            $id_jenis_pengajuan_dosen_tiap_unit = DB::table('pengajuan_dosen')
                    ->selectRaw("count(CASE WHEN id_jenis_pengajuan_dosen = 'mkbwajib' then 1 ELSE NULL END) as jumlah_mkbwajib")
                    ->addSelect(DB::raw("count(CASE WHEN id_jenis_pengajuan_dosen = 'nonmkbwajib' then 1 ELSE NULL END) as jumlah_nonmkbwajib"))
                    ->addSelect('id_unit')
                    ->groupBy('id_unit');
            $pengajuan_dosen['unit'] = DB::table('unit', 'u')
                    ->select(['u.nama', 'u.id_unit'])
                    ->leftJoinSub(
                        $id_jenis_pengajuan_dosen_tiap_unit,
                        'pd',
                        'u.id_unit',
                        '=',
                        'pd.id_unit'
                    )
                    ->addSelect('pd.jumlah_mkbwajib')
                    ->addSelect('pd.jumlah_nonmkbwajib')
                    ->get();

            return view('Beranda::super_admin', compact('page', 'data', 'pengajuan_dosen'));
        } else if(Session::get('is_validator')) {

            $data['total'] =  DB::table('pengajuan')->where('id_unit', $data['id_unit'])->where('deleted_at', NULL)->count();
            $data['perlu_disetujui'] =  DB::table('pengajuan')->where('id_unit', $data['id_unit'])->where('is_approved0', NULL)->where('deleted_at', NULL)->count();
            $data['diterima'] =  DB::table('pengajuan')->where('id_unit', $data['id_unit'])->where('is_approved0', 1)->where('deleted_at', NULL)->count();
            $data['ditolak'] =  DB::table('pengajuan')->where('id_unit', $data['id_unit'])->where('is_approved0', 0)->where('deleted_at', NULL)->count();
            $data['revisi_unit'] =  DB::table('pengajuan')->where('id_unit', $data['id_unit'])->where('is_approved0', 2)->where('deleted_at', NULL)->count();

            $data['thl'] =  DB::table('pengajuan')->where('id_unit', $data['id_unit'])->where('id_jenis_pengajuan', 'thl')->where('is_approved1', 1)->where('deleted_at', NULL)->count();
            $data['kontrak'] =  DB::table('pengajuan')->where('id_unit', $data['id_unit'])->where('id_jenis_pengajuan', 'kontrak')->where('is_approved1', 1)->where('deleted_at', NULL)->count();
            $data['magang'] =  DB::table('pengajuan')->where('id_unit', $data['id_unit'])->where('id_jenis_pengajuan', 'magang')->where('is_approved1', 1)->where('deleted_at', NULL)->count();

            // Pengajuan Dosen

            $pengajuan_dosen['id_unit'] = $data['id_unit'];
            $pengajuan_dosen['total'] =  DB::table('pengajuan_dosen')->where('id_unit', $pengajuan_dosen['id_unit'])->where('deleted_at', NULL)->count();
            $pengajuan_dosen['perlu_disetujui'] =  DB::table('pengajuan_dosen')->where('id_unit', $pengajuan_dosen['id_unit'])->where('is_approved0', NULL)->where('deleted_at', NULL)->count();
            $pengajuan_dosen['diterima'] =  DB::table('pengajuan_dosen')->where('id_unit', $pengajuan_dosen['id_unit'])->where('is_approved0', 1)->where('deleted_at', NULL)->count();
            $pengajuan_dosen['ditolak'] =  DB::table('pengajuan_dosen')->where('id_unit', $pengajuan_dosen['id_unit'])->where('is_approved0', 0)->where('deleted_at', NULL)->count();
            $pengajuan_dosen['revisi_unit'] =  DB::table('pengajuan_dosen')->where('id_unit', $pengajuan_dosen['id_unit'])->where('is_approved0', 2)->where('deleted_at', NULL)->count();
            
            $pengajuan_dosen['mkbwajib'] =  DB::table('pengajuan_dosen')->where('id_unit', $pengajuan_dosen['id_unit'])->where('id_jenis_pengajuan_dosen', 'mkbwajib')->where('is_approved1', 1)->where('deleted_at', NULL)->count();
            $pengajuan_dosen['nonmkbwajib'] =  DB::table('pengajuan_dosen')->where('id_unit', $pengajuan_dosen['id_unit'])->where('id_jenis_pengajuan_dosen', 'nonmkbwajib')->where('is_approved1', 1)->where('deleted_at', NULL)->count();

            return view('Beranda::kepala_kantor', compact('page', 'data', 'pengajuan_dosen'));
        }
        else if($data['nama_unit'] = sso()->user()->getActiveRole()->getName() == 'Administrator') {
            $data['total'] =  DB::table('pengajuan')->where('id_unit', $data['id_unit'])->where('deleted_at', NULL)->count();
            $data['approval_unit'] =  DB::table('pengajuan')->where('id_unit', $data['id_unit'])->where('is_approved0', NULL)->where('deleted_at', NULL)->count();
            $data['diterima_unit'] =  DB::table('pengajuan')->where('id_unit', $data['id_unit'])->where('is_approved0', 1)->where('deleted_at', NULL)->count();
            $data['ditolak_unit'] =  DB::table('pengajuan')->where('id_unit', $data['id_unit'])->where('is_approved0', 0)->where('deleted_at', NULL)->count();
            $data['revisi_unit'] =  DB::table('pengajuan')->where('id_unit', $data['id_unit'])->where('is_approved0', 2)->where('deleted_at', NULL)->count();

            $data['approval_sdmo'] =  DB::table('pengajuan')->where('id_unit', $data['id_unit'])->where('is_approved1', NULL)->whereNotIn('is_approved0', [0,2,NULL])->where('deleted_at', NULL)->count();
            $data['diterima_sdmo'] =  DB::table('pengajuan')->where('id_unit', $data['id_unit'])->where('is_approved1', 1)->where('deleted_at', NULL)->count();
            $data['ditolak_sdmo'] =  DB::table('pengajuan')->where('id_unit', $data['id_unit'])->where('is_approved1', 0)->where('deleted_at', NULL)->count();
            $data['revisi_sdmo'] =  DB::table('pengajuan')->where('id_unit', $data['id_unit'])->where('is_approved1', 2)->where('deleted_at', NULL)->count();
            $data['perlu_revisi'] =  DB::table('pengajuan')->where('id_unit', $data['id_unit'])->where('is_approved0', 2)->orWhere('is_approved1', 2)->where('deleted_at', NULL)->count();

            $data['perlu_revisi'] =  DB::table('pengajuan')->where('id_unit', $data['id_unit'])->where('is_approved0', 2)->orWhere('is_approved1', 2)->where('deleted_at', NULL)->count();

            $data['thl'] =  DB::table('pengajuan')->where('id_unit', $data['id_unit'])->where('id_jenis_pengajuan', 'thl')->where('is_approved1', 1)->where('deleted_at', NULL)->count();
            $data['kontrak'] =  DB::table('pengajuan')->where('id_unit', $data['id_unit'])->where('id_jenis_pengajuan', 'kontrak')->where('is_approved1', 1)->where('deleted_at', NULL)->count();
            $data['magang'] =  DB::table('pengajuan')->where('id_unit', $data['id_unit'])->where('id_jenis_pengajuan', 'magang')->where('is_approved1', 1)->where('deleted_at', NULL)->count();

            // Pengajuan dosen
            $pengajuan_dosen['id_unit'] = $data['id_unit'];
            $pengajuan_dosen['total'] =  DB::table('pengajuan_dosen')->where('id_unit', $pengajuan_dosen['id_unit'])->where('deleted_at', NULL)->count();
            $pengajuan_dosen['approval_unit'] =  DB::table('pengajuan_dosen')->where('id_unit', $pengajuan_dosen['id_unit'])->where('is_approved0', NULL)->where('deleted_at', NULL)->count();
            $pengajuan_dosen['diterima_unit'] =  DB::table('pengajuan_dosen')->where('id_unit', $pengajuan_dosen['id_unit'])->where('is_approved0', 1)->where('deleted_at', NULL)->count();
            $pengajuan_dosen['ditolak_unit'] =  DB::table('pengajuan_dosen')->where('id_unit', $pengajuan_dosen['id_unit'])->where('is_approved0', 0)->where('deleted_at', NULL)->count();
            $pengajuan_dosen['revisi_unit'] =  DB::table('pengajuan_dosen')->where('id_unit', $pengajuan_dosen['id_unit'])->where('is_approved0', 2)->where('deleted_at', NULL)->count();

            $pengajuan_dosen['approval_sdmo'] =  DB::table('pengajuan_dosen')->where('id_unit', $pengajuan_dosen['id_unit'])->where('is_approved1', NULL)->whereNotIn('is_approved0', [0,2,NULL])->where('deleted_at', NULL)->count();
            $pengajuan_dosen['diterima_sdmo'] =  DB::table('pengajuan_dosen')->where('id_unit', $pengajuan_dosen['id_unit'])->where('is_approved1', 1)->where('deleted_at', NULL)->count();
            $pengajuan_dosen['ditolak_sdmo'] =  DB::table('pengajuan_dosen')->where('id_unit', $pengajuan_dosen['id_unit'])->where('is_approved1', 0)->where('deleted_at', NULL)->count();
            $pengajuan_dosen['revisi_sdmo'] =  DB::table('pengajuan_dosen')->where('id_unit', $pengajuan_dosen['id_unit'])->where('is_approved1', 2)->where('deleted_at', NULL)->count();
            $pengajuan_dosen['perlu_revisi'] =  DB::table('pengajuan_dosen')->where('id_unit', $pengajuan_dosen['id_unit'])->where('is_approved0', 2)->orWhere('is_approved1', 2)->where('deleted_at', NULL)->count();

            $pengajuan_dosen['perlu_revisi'] =  DB::table('pengajuan_dosen')->where('id_unit', $pengajuan_dosen['id_unit'])->where('is_approved0', 2)->orWhere('is_approved1', 2)->where('deleted_at', NULL)->count();

            $pengajuan_dosen['mkbwajib'] =  DB::table('pengajuan_dosen')->where('id_unit', $pengajuan_dosen['id_unit'])->where('id_jenis_pengajuan_dosen', 'mkbwajib')->where('is_approved1', 1)->where('deleted_at', NULL)->count();
            $pengajuan_dosen['nonmkbwajib'] =  DB::table('pengajuan_dosen')->where('id_unit', $pengajuan_dosen['id_unit'])->where('id_jenis_pengajuan_dosen', 'nonmkbwajib')->where('is_approved1', 1)->where('deleted_at', NULL)->count();


            return view('Beranda::admin', compact('page', 'data', 'pengajuan_dosen'));
        }
    }
}
