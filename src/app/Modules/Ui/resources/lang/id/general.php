<?php

return [
    'beranda' => 'Beranda',
    'sunting' => 'Sunting',
    'ganti' => 'Ganti',
    'batal' => 'Batal',
    'aktif' => 'Aktif',
    'tidak_aktif' => 'Tidak aktif',
    'sebelumnya' => 'Sebelumnya',
    'selanjutnya' => 'Selanjutnya',
    'kelola_akun' => 'Kelola Akun',
    'mode_terang' => 'Mode terang',
    'mode_gelap' => 'Mode gelap',
    'ganti_hak_akses' => 'Ganti hak akses',
    'kembali_ke_sso' => 'Kembali ke myITS SSO',
    'keluar' => 'Keluar',

    'hak_akses' => 'Hak Akses',
    'hak_akses_sekarang' => 'Hak akses Anda saat ini:',
    'pilih_hak_akses' => 'Pilih hak akses...',
    'hak_akses_sukses_diubah' => 'Hak akses berhasil diubah',
    'tidak_punya_hak_akses' => 'Anda tidak berhak memiliki hak akses tersebut',
];
