<?php

return [
    'berkas' => 'Document',
    'berkas_saya' => 'My Document',

    'beranda' => 'Dashboard',
    'sunting' => 'Edit',
    'ganti' => 'Change',
    'batal' => 'Cancel',
    'aktif' => 'Active',
    'tidak_aktif' => 'Inactive',
    'sebelumnya' => 'Previous',
    'selanjutnya' => 'Next',
    'kelola_akun' => 'Manage Account',
    'mode_terang' => 'Light mode',
    'mode_gelap' => 'Dark mode',
    'ganti_hak_akses' => 'Change role',
    'kembali_ke_sso' => 'Back to myITS SSO',
    'keluar' => 'Logout',

    'hak_akses' => 'Roles',
    'hak_akses_sekarang' => 'Your current role:',
    'pilih_hak_akses' => 'Choose role...',
    'hak_akses_sukses_diubah' => 'Role succesfully changed',
    'tidak_punya_hak_akses' => 'You are not entitled to the role',
];
