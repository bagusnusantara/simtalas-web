        <div class="aside-header">
            <a class="navbar-brand" href="{{ url('') }}" class="aside-logo">
                @if (session('skin') == 'dark')
                    <img src="{{ asset('assets/img/myits-hr-contract-white.png') }}" height="30" alt="" class="aside-logo">
                @else
                    <img src="{{ asset('assets/img/myits-hr-contract-blue.png') }}" height="30" alt="" class="aside-logo">
                @endif
            </a>
            <a href="" class="aside-menu-link">
                <i data-feather="menu"></i>
                <i data-feather="x"></i>
            </a>
        </div>
        <div class="aside-body">
            <ul class="nav nav-aside">
                <x-Ui::menu />
            </ul>
        </div>
