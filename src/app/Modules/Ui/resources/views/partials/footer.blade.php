                <div class="d-none d-sm-block">
                    <div class="card card-body bg-transparent mg-t-10">
                        <div class="d-flex align-items-center row row-xs">
                            <div class="col-10 d-flex justify-content-start">
                                <span class="tx-medium tx-color-03 tx-13">Copyright &copy; <script>document.write(new Date().getFullYear())</script> Institut Teknologi Sepuluh Nopember</span>
                            </div>
                            <div class="col-2 mg-lg-t-0 d-flex justify-content-end">
                                @if (session('skin') == 'dark')
                                    <img src="{{ url('assets/img/advhum-white.png') }}" height="60" class="mg-r-10">
                                @else
                                    <img src="{{ url('assets/img/advhum-blue.png') }}" height="60" class="mg-r-10">
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-sm-none">
                    <div class="card card-body bg-transparent mg-t-10 mg-b-10">
                        <div class="d-flex align-items-center row row-xs">
                            <div class="col-12 d-flex align-items-center justify-content-center">
                                <span class="tx-medium tx-color-03 tx-13">Copyright &copy; <script>document.write(new Date().getFullYear())</script> ITS</span>
                            </div>
                            <div class="col-12 mg-t-10 d-flex align-items-center justify-content-center">
                                @if (session('skin') == 'dark')
                                    <img src="{{ url('assets/img/advhum-white.png') }}" height="60" class="mg-r-10">
                                @else
                                    <img src="{{ url('assets/img/advhum-blue.png') }}" height="60" class="mg-r-10">
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
