<nav class="nav r-25 pos-fixed z-index-10">
    <div class="dropdown dropdown-profile">
        <a href="" class="dropdown-link" data-toggle="dropdown" data-display="static">
            <div class="avatar avatar-sm">
                <img src="{{ sso()->user()->getPicture() }}" onerror="this.src='{{ url('assets/img/avatar15.jpg') }}'" class="rounded-circle" alt="">
            </div>
        </a>
        <div class="dropdown-menu dropdown-menu-right allow-focus">
            <div class="avatar avatar-lg mg-r-auto mg-l-auto">
                <img src="{{ sso()->user()->getPicture() }}" onerror="this.src='{{ url('assets/img/avatar15.jpg') }}'" class="rounded-circle" alt="">
            </div>
            <h5 class="tx-medium tx-montserrat mg-b-0 mg-t-15 text-center">
                {{ sso()->user()->getName() }}
            </h5>
            <p class="tx-12 tx-color-03 text-center mg-b-0">{{ sso()->user()->getActiveRole()->getName() }}</p>
            <p class="tx-12 tx-color-03 text-center">{{ sso()->user()->getActiveRole()->getOrgName() ? sso()->user()->getActiveRole()->getOrgName() : '' }}</p>
            <hr>
            @if (session('skin') == 'dark')
                <a href="{{ route('Setting::change.skin', 'light') }}" class="dropdown-item">
                    <i data-feather="sun"></i>{{ __('Ui::general.mode_terang') }}
                </a>
            @else
                <a href="{{ route('Setting::change.skin', 'dark') }}" class="dropdown-item">
                    <i data-feather="moon"></i>{{ __('Ui::general.mode_gelap') }}
                </a>
            @endif
            <a href="" class="dropdown-item" data-toggle="modal" data-target="#chgRoleUser" data-animation="effect-scale">
                <i data-feather="user-check"></i>{{ __('Ui::general.ganti_hak_akses') }}
            </a>
            <a href="{{ url('logout') }}" class="dropdown-item">
                <i data-feather="log-out"></i>{{ __('Ui::general.keluar') }}
            </a>
        </div>
    </div>
</nav>
