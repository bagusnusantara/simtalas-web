<div class="dropdown">
    <a class="btn btn-its-icon tx-uppercase dropdown-toggle" href="#" data-toggle="dropdown">
        {{ app()->getLocale() }}
    </a>
    <div class="dropdown-menu">
        <a href="{{ route('Setting::change.locale', 'id') }}" class="dropdown-item">ID - Indonesia</a>
        <a href="{{ route('Setting::change.locale', 'en') }}" class="dropdown-item">EN - English</a>
    </div>
</div>
