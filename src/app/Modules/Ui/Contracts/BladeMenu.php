<?php

namespace App\Modules\Ui\Contracts;


interface BladeMenu
{
    public function renderMenu(): string;
}
