<?php


namespace App\Modules\Ui\Components;


use App\Modules\Ui\Contracts\BladeMenu;
use Dptsi\Modular\Base\BaseModule;
use Dptsi\Modular\Exception\InvalidModuleClass;

class Menu extends \Illuminate\View\Component
{
    public array $menus;

    public function render()
    {
        $this->menus = [];
        foreach (config('modules.modules') ?? [] as $module_name => $module_properties) {
            if (!$module_properties['enabled']) {
                continue;
            }

            $module = new $module_properties['module_class'](
                $module_properties,
                $module_name == config('modules.default_module')
            );
            if (!($module instanceof BaseModule)) {
                throw new InvalidModuleClass();
            }

            if ($module instanceof BladeMenu) {
                $this->menus[] = $module->renderMenu();
            }
        }

        return view('Ui::components.menu');
    }
}
