<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RincianTugas extends Model
{
    use HasFactory;
    protected $table = 'rincian_tugas';
}
