<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BebanKerjaDosen extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'beban_kerja_dosen';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id_beban_kerja_dosen';
    
    /**
     * Indicates if the model's ID is auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_jenis_beban_kerja_dosen',
        'deskripsi',
        'kelas',
        'jumlah',
        'upah',
        'id_mkb_wajib',
    ];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::creating(function ($beban_kerja_dosen) {
            if (!$beban_kerja_dosen['id_beban_kerja_dosen']) {
                $beban_kerja_dosen['id_beban_kerja_dosen'] = (string) \Illuminate\Support\Str::uuid()->toString();
            }
        });
    }

    public function pengajuan_dosen()
    {
        return $this->belongsTo(PengajuanDosen::class, 'id_pengajuan_dosen', 'id_pengajuan_dosen');
    }

    public function jenis_beban_kerja_dosen()
    {
        return $this->belongsTo(JenisBebanKerjaDosen::class, 'id_jenis_beban_kerja_dosen', 'id_jenis_beban_kerja_dosen');
    }

    public function mkb_wajib()
    {
        return $this->belongsTo(MataKuliahBersamaWajib::class, 'id_mkb_wajib', 'id_mkb_wajib');
    }

    public static function is_pengajaran($jenis_beban_kerja) {
        return $jenis_beban_kerja == '1' || $jenis_beban_kerja == '2' || $jenis_beban_kerja == '3' || $jenis_beban_kerja == '4';
    }
}
