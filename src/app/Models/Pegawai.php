<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pegawai extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'pegawai';
    protected $dates = ['deleted_at'];
    protected $primaryKey = 'id_pegawai';

    protected $fillable = [
		'id_pengajuan',
		'nama',
		'nik',
		'tgl_lahir',
		'jenis_kelamin',
		'id_pendidikan',
		'alamat',
		'email',
		'hp',
		'id_status_bpjs',
		'nomor_bpjs',
		'pas_foto',
		'ktp',
		'ijazah',
		'updater',
		'tgl_nonaktif',
		'alasan_nonaktif'
    ];
}
