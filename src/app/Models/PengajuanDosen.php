<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PengajuanDosen extends Model
{
    use HasFactory;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pengajuan_dosen';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id_pengajuan_dosen';
    
    /**
     * Indicates if the model's ID is auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_status_pengajuan_dosen', 'id_jenis_pengajuan_dosen', 'nik',
        'id_unit', 'tanggal_mulai_kerja', 'tanggal_akhir_kerja',
        'tanggal_pengajuan', 'tanggal_pengesahan',
        'is_approved0', 'id_approved0', 'name_approved0', 'date_approved0', 'alasan0',
        'is_approved1', 'id_approved1', 'name_approved1', 'date_approved1', 'alasan1',
        'no_surat'
    ];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::creating(function ($pengajuan_dosen) {
            if (!$pengajuan_dosen['id_pengajuan_dosen']) {
                $pengajuan_dosen['id_pengajuan_dosen'] = (string) \Illuminate\Support\Str::uuid()->toString();
            }
        });
    }

    
    protected $dates = ['deleted_at'];

    public function beban_kerja_dosen()
    {
        return $this->hasMany(BebanKerjaDosen::class, 'id_pengajuan_dosen', 'id_pengajuan_dosen');
    }

    public function dosen()
    {
        return $this->belongsTo(Dosen::class, 'nik', 'nik');
    }

    public function get_sks_pengajaran() {
        return $this->beban_kerja_dosen()
                ->where(function($query) {
                        $query->where('id_jenis_beban_kerja_dosen', '1')
                        ->orWhere('id_jenis_beban_kerja_dosen', '2')
                        ->orWhere('id_jenis_beban_kerja_dosen', '3')
                        ->orWhere('id_jenis_beban_kerja_dosen', '4');
                })
                ->sum('jumlah');
    }

    public function is_mengajar_mkb_wajib() {
        return $this->id_jenis_pengajuan_dosen == 'mkbwajib';
    }
}
