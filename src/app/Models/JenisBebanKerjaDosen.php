<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JenisBebanKerjaDosen extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'jenis_beban_kerja_dosen';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id_jenis_beban_kerja_dosen';
    
    /**
     * Indicates if the model's ID is auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    public function beban_kerja_dosen()
    {
        return $this->hasMany(BebanKerjaDosen::class, 'id_jenis_beban_kerja_dosen', 'id_jenis_beban_kerja_dosen');
    }
}
