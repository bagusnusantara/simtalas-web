<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dosen extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'dosen';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'nik';
    
    /**
     * Indicates if the model's ID is auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'gelar_depan',
        'gelar_belakang',
        'npp',
        'kepakaran',
        'email',
        'nomor_hp',
        'nama',
        'nik',
        'nidn',
        'tgl_lahir',
        'jenis_kelamin',
        'id_pendidikan',
        'alamat',
        'tempat_lahir',
    ];
    
    protected $dates = ['deleted_at'];

    public function pengajuan_dosen()
    {
        return $this->hasMany(PengajuanDosen::class, 'nik', 'nik');
    }
}
