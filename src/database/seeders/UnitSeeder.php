<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

class UnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('unit')->insert([
            'id_unit' => Uuid::uuid4()->toString(),
            'nama' => 'Direktorat Pengembangan Teknologi dan Sistem Informasi',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('unit')->insert([
            'id_unit' => Uuid::uuid4()->toString(),
            'nama' => 'Departemen Informatika',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('unit')->insert([
            'id_unit' => Uuid::uuid4()->toString(),
            'nama' => 'Departemen Sistem Informasi',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('unit')->insert([
            'id_unit' => Uuid::uuid4()->toString(),
            'nama' => 'Departemen Matematika',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
    }
}
