<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PendidikanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pendidikan')->insert([
            'id_pendidikan' => 'sd',
            'nama' => 'SD',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('pendidikan')->insert([
            'id_pendidikan' => 'smp',
            'nama' => 'SMP',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('pendidikan')->insert([
            'id_pendidikan' => 'sma',
            'nama' => 'SMA/MA/SMK',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('pendidikan')->insert([
            'id_pendidikan' => 'd1',
            'nama' => 'D1',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('pendidikan')->insert([
            'id_pendidikan' => 'd2',
            'nama' => 'D2',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('pendidikan')->insert([
            'id_pendidikan' => 'd3',
            'nama' => 'D3',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('pendidikan')->insert([
            'id_pendidikan' => 'd4',
            'nama' => 'D4/S1',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('pendidikan')->insert([
            'id_pendidikan' => 's2',
            'nama' => 'S2',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('pendidikan')->insert([
            'id_pendidikan' => 's3',
            'nama' => 'S3',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        
    }
}
