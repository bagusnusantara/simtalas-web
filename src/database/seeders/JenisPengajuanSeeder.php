<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JenisPengajuanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jenis_pengajuan')->insert([
            'id_jenis_pengajuan' => 'thl',
            'nama' => 'THL (Tenaga Harian Lepas)',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('jenis_pengajuan')->insert([
            'id_jenis_pengajuan' => 'kontrak',
            'nama' => 'Tenaga Kontrak',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        DB::table('jenis_pengajuan')->insert([
            'id_jenis_pengajuan' => 'magang',
            'nama' => 'Magang Berbayar',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d'),
        ]);
        
        
    }
}
