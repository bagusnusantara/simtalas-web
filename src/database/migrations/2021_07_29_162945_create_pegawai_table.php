<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePegawaiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pegawai', function (Blueprint $table) {
            $table->uuid('id_pegawai')->primary();
            $table->uuid('id_pengajuan');
            $table->string('nama');
            $table->string('nik')->unique();
            $table->dateTime('tgl_lahir');
            $table->string('jenis_kelamin');
            $table->string('id_pendidikan');
            $table->string('alamat');
            $table->string('email')->unique();
            $table->string('hp')->nullable();
            $table->string('id_status_bpjs')->nullable();
            $table->string('nomor_bpjs')->nullable();
            $table->string('pas_foto')->nullable();
            $table->string('ktp')->nullable();
            $table->string('ijazah')->nullable();
            $table->uuid('updater');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pegawai');
    }
}
