<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePengajuanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengajuan', function (Blueprint $table) {
            $table->uuid('id_pengajuan')->primary();
            $table->string('id_jenis_pengajuan');
            $table->string('id_status_pengajuan');
            $table->uuid('id_unit');
            $table->string('nik');
            $table->string('rincian_tugas')->nullable();
            $table->string('honor');
            $table->string('uang_makan');
            $table->dateTime('tgl_mulai_kerja')->nullable();
            $table->dateTime('tgl_akhir_kerja')->nullable();
            $table->string('no_surat')->nullable();
            $table->tinyInteger('is_approved0')->nullable();
            $table->tinyInteger('is_approved1')->nullable();
            $table->string('id_approved0')->nullable();
            $table->string('id_approved1')->nullable();
            $table->timestamp('date_approved0')->nullable();
            $table->timestamp('date_approved1')->nullable();
            $table->dateTime('tgl_pengajuan')->nullable();
            $table->uuid('updater');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengajuan');
    }
}
