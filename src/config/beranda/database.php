<?php

return [
    'driver' => 'sqlsrv',
    'url' => env('BERANDA_DATABASE_URL'),
    'host' => env('BERANDA_DB_HOST', 'localhost'),
    'port' => env('BERANDA_DB_PORT', '1433'),
    'database' => env('BERANDA_DB_DATABASE', 'forge'),
    'username' => env('BERANDA_DB_USERNAME', 'forge'),
    'password' => env('BERANDA_DB_PASSWORD', ''),
    'charset' => 'utf8',
    'prefix' => '',
    'prefix_indexes' => true,
];