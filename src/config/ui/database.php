<?php

return [
    'driver' => 'sqlsrv',
    'url' => env('UI_DATABASE_URL'),
    'host' => env('UI_DB_HOST', 'localhost'),
    'port' => env('UI_DB_PORT', '1433'),
    'database' => env('UI_DB_DATABASE', 'forge'),
    'username' => env('UI_DB_USERNAME', 'forge'),
    'password' => env('UI_DB_PASSWORD', ''),
    'charset' => 'utf8',
    'prefix' => '',
    'prefix_indexes' => true,
];