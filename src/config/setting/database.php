<?php

return [
    'driver' => 'sqlsrv',
    'url' => env('SETTING_DATABASE_URL'),
    'host' => env('SETTING_DB_HOST', 'localhost'),
    'port' => env('SETTING_DB_PORT', '1433'),
    'database' => env('SETTING_DB_DATABASE', 'forge'),
    'username' => env('SETTING_DB_USERNAME', 'forge'),
    'password' => env('SETTING_DB_PASSWORD', ''),
    'charset' => 'utf8',
    'prefix' => '',
    'prefix_indexes' => true,
];