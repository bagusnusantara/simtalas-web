<?php

return [
    'driver' => 'sqlsrv',
    'url' => env('BERKAS_DATABASE_URL'),
    'host' => env('BERKAS_DB_HOST', 'localhost'),
    'port' => env('BERKAS_DB_PORT', '1433'),
    'database' => env('BERKAS_DB_DATABASE', 'forge'),
    'username' => env('BERKAS_DB_USERNAME', 'forge'),
    'password' => env('BERKAS_DB_PASSWORD', ''),
    'charset' => 'utf8',
    'prefix' => '',
    'prefix_indexes' => true,
];