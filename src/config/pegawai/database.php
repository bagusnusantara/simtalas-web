<?php

return [
    'driver' => 'sqlsrv',
    'url' => env('PEGAWAI_DATABASE_URL'),
    'host' => env('PEGAWAI_DB_HOST', 'localhost'),
    'port' => env('PEGAWAI_DB_PORT', '1433'),
    'database' => env('PEGAWAI_DB_DATABASE', 'forge'),
    'username' => env('PEGAWAI_DB_USERNAME', 'forge'),
    'password' => env('PEGAWAI_DB_PASSWORD', ''),
    'charset' => 'utf8',
    'prefix' => '',
    'prefix_indexes' => true,
];