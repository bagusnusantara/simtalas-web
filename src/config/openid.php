<?php

return [
    'provider'                  => env('OPENID_PROVIDER'),
    'client_id'                 => env('OPENID_CLIENT_ID'),
    'client_secret'             => env('OPENID_CLIENT_SECRET'),
    'redirect_uri'              => env('OPENID_REDIRECT_URI'),
    'post_logout_redirect_uri'  => env('OPENID_POST_LOGOUT_REDIRECT_URI'),
    'scope'                     => env('OPENID_SCOPE'),
    'allowed_roles'             => [
        'Administrator',
        'Super Administrator',
        'Kepala Kantor',
        'Direktur', 
        'Kadep', 
        'Dekan',
        'Kepala Unit', 
        'Kepala Biro', 
        'Kepala UPT',
        'Viewer myITS HRC',
    ],
    'is_validator'             => [
        'Kepala Kantor',
        'Direktur', 
        'Kadep', 
        'Dekan',
        'Kepala Unit', 
        'Kepala Biro',
        'Kepala UPT',
    ],
];
